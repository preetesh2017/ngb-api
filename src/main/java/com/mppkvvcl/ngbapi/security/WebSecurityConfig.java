package com.mppkvvcl.ngbapi.security;

import com.mppkvvcl.ngbapi.security.services.LoginSessionService;
import com.mppkvvcl.ngbapi.security.services.UserDetailService;
import com.mppkvvcl.ngbapi.security.services.UserService;
import com.mppkvvcl.ngbapi.utility.GlobalResources;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

/**
 * Created by NITISH on 24-04-2017.
 */
@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    private final String AUTHENTICATION_URL_ENDPOINT = "/authentication/login";

    private final String LOGOUT_URL_ENDPOINT = "/authentication/logout/**";

    private final String BACKEND_API_PREFIX = "/**";

    /**
     * getting whole application logger object for performing logs. This logger will only be
     * used in this class methods only.
     */
    private static final Logger logger = GlobalResources.getLogger(WebSecurityConfig.class);

    @Autowired
    private UserService userService;

    @Autowired
    private UserDetailService userDetailService;

    @Autowired
    private JWTAuthenticationEntryPoint jwtAuthEndPoint;

    @Autowired
    private LoginSessionService loginSessionService;

    /**
     * Asking spring to inject EntityManager for underlying hibernate
     * for creating sessions and transactions;
     */
    /*@PersistenceContext
    private EntityManager entityManager;

    private Session getCurrentSession()  {
        logger.info("getCurrentSession() : " + "getting session object");
        return entityManager.unwrap(Session.class);
    }*/

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable().authorizeRequests()
                .antMatchers(HttpMethod.POST, AUTHENTICATION_URL_ENDPOINT).permitAll()
                .antMatchers(HttpMethod.PUT,LOGOUT_URL_ENDPOINT).authenticated()
                .antMatchers(BACKEND_API_PREFIX).authenticated()
                .and()
                // We filter the api/login requests
                .addFilterBefore(new JWTLoginFilter(AUTHENTICATION_URL_ENDPOINT, authenticationManager(),userDetailService,loginSessionService),UsernamePasswordAuthenticationFilter.class)
                // And filter other requests to check the presence of JWT in header
                .addFilterBefore(new JWTAuthenticationFilter(),UsernamePasswordAuthenticationFilter.class)
                .exceptionHandling();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        // Create a default account
        /*auth.inMemoryAuthentication()
                .withUser("admin")
                .password("password")
                .roles("ADMIN");*/
       /* auth.jdbcAuthentication().dataSource(dataSource)
                .usersByUsernameQuery("select username,password,1 from nsc.user where username = ?")
                .authoritiesByUsernameQuery("select username,role from nsc.user where username = ?");*/
       auth.userDetailsService(userService).passwordEncoder(passwordEncoder());
    }

    /*@Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers(LOGOUT_URL_ENDPOINT);
    }*/

    @Bean
    public PasswordEncoder passwordEncoder(){
        PasswordEncoder encoder = new BCryptPasswordEncoder();
        return encoder;
    }
}
