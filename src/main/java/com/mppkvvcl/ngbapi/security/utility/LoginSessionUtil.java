package com.mppkvvcl.ngbapi.security.utility;

import com.mppkvvcl.ngbapi.utility.GlobalResources;
import eu.bitwalker.useragentutils.Browser;
import eu.bitwalker.useragentutils.UserAgent;
import eu.bitwalker.useragentutils.Version;
import org.slf4j.Logger;
import org.springframework.cache.annotation.Cacheable;

public class LoginSessionUtil {

    private static final Logger logger = GlobalResources.getLogger(LoginSessionUtil.class);

    @Cacheable("browserNames")
    public static String getBrowserName(String userAgentString){
        final String methodName = "getBrowserVersion() : ";
        logger.info(methodName + "called");
        String browserName = null;
        if(userAgentString != null){
            UserAgent userAgent = UserAgent.parseUserAgentString(userAgentString);
            if(userAgent != null){
                Browser browser = userAgent.getBrowser();
                if(browser != null){
                    browserName = browser.getName();
                }
            }
        }
        return browserName;
    }

    @Cacheable("browserVersions")
    public static String getBrowserVersion(String userAgentString){
        final String methodName = "getBrowserVersion() : ";
        logger.info(methodName + "called");
        String browserVersion = null;
        if(userAgentString != null){
            UserAgent userAgent = UserAgent.parseUserAgentString(userAgentString);
            if(userAgent != null){
                Browser browser = userAgent.getBrowser();
                if(browser != null){
                    Version version = browser.getVersion(userAgentString);
                    browserVersion = version.getVersion();
                }
            }
        }
        return browserVersion;
    }
}
