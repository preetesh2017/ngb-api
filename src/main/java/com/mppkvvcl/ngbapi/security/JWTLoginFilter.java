package com.mppkvvcl.ngbapi.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mppkvvcl.ngbinterface.interfaces.LoginSessionInterface;
import com.mppkvvcl.ngbapi.security.beans.LoginSession;
import com.mppkvvcl.ngbapi.security.beans.User;
import com.mppkvvcl.ngbapi.security.services.LoginSessionService;
import com.mppkvvcl.ngbapi.security.services.UserDetailService;
import com.mppkvvcl.ngbapi.security.utility.LoginSessionUtil;
import com.mppkvvcl.ngbapi.utility.GlobalResources;
import org.slf4j.Logger;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Enumeration;

/**
 * Created by NITISH on 24-04-2017.
 */
public class JWTLoginFilter extends AbstractAuthenticationProcessingFilter {

    private static final Logger logger = GlobalResources.getLogger(JWTLoginFilter.class);

    private UserDetailService userDetailService;

    private LoginSessionService loginSessionService;

    public JWTLoginFilter(String url, AuthenticationManager authenticationManager, UserDetailService userDetailService, LoginSessionService loginSessionService){
        super(new AntPathRequestMatcher(url));
        setAuthenticationManager(authenticationManager);
        this.userDetailService = userDetailService;
        this.loginSessionService = loginSessionService;
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws AuthenticationException, IOException, ServletException {
        String methodName = "attemptAuthentication() : ";
        logger.info(methodName + "called");
        logger.info(methodName + "Attempting authentication");
        User credentials = new ObjectMapper().readValue(httpServletRequest.getInputStream(),User.class);
        return getAuthenticationManager().authenticate(new UsernamePasswordAuthenticationToken(credentials.getUsername(),credentials.getPassword()));
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult) throws IOException, ServletException {
        String methodName = "successfulAuthentication() : ";
        logger.info(methodName + "called");
        final String username = authResult.getName();
        LoginSessionInterface savedSession = saveSession(request,username);
        TokenAuthenticationService.addAuthentication(response,username,savedSession,userDetailService);
        logger.info(methodName +"Authentication successful forwarding request " + username);
        request.setAttribute("username",username);
        chain.doFilter(request,response);
    }

    private void printRequestHeaders(HttpServletRequest request){
        Enumeration headerNames = request.getHeaderNames();
        while (headerNames.hasMoreElements()) {
            String key = (String) headerNames.nextElement();
            String value = request.getHeader(key);
            logger.info(key + " " + value);
        }
        logger.info("x-forwarded-for " + request.getHeader("x-forwarded-for"));
        logger.info("x-real-ip " + request.getHeader("x-real-ip"));
        logger.info("x-forwarded-server " + request.getHeader("x-forwarded-server"));
        logger.info("x-forwarded-host " + request.getHeader("x-forwarded-host"));
    }

    private LoginSessionInterface saveSession(HttpServletRequest request, String username){
        LoginSessionInterface insertedLoginSession = null;
        if(request != null){
            String browserName = null;
            String browserVersion = null;
            String userAgentString = request.getHeader("User-Agent");

            browserName = LoginSessionUtil.getBrowserName(userAgentString);
            browserVersion = LoginSessionUtil.getBrowserVersion(userAgentString);

            String ipAddress = request.getHeader("X-FORWARDED-FOR");
            if(ipAddress == null){
                ipAddress = request.getRemoteAddr();
            }
            if(ipAddress != null){
                int index = ipAddress.lastIndexOf(":");
                ipAddress = ipAddress.substring(index + 1);
            }
            //logger.info(methodName + "IP Address : " + ipAddress + " Browser : " + browserName + " Version : " + browserVersion);
            LoginSessionInterface loginSession = new LoginSession();
            loginSession.setUsername(username);
            loginSession.setLogin(GlobalResources.getCurrentDate());
            loginSession.setIp(ipAddress);
            loginSession.setBrowserName(browserName);
            loginSession.setBrowserVersion(browserVersion);
            insertedLoginSession = loginSessionService.insert(loginSession);
        }
        return insertedLoginSession;
    }
}
