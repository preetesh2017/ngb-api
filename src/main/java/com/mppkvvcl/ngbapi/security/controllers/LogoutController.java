package com.mppkvvcl.ngbapi.security.controllers;

import com.mppkvvcl.ngbinterface.interfaces.LoginSessionInterface;
import com.mppkvvcl.ngbapi.security.beans.User;
import com.mppkvvcl.ngbapi.security.services.LoginSessionService;
import com.mppkvvcl.ngbapi.utility.GlobalResources;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "authentication/logout")
public class LogoutController {

    /**
     * getting whole application logger object for performing logs. This logger will only be
     * used in this class methods only.
     */
    private static final Logger logger = GlobalResources.getLogger(LogoutController.class);

    @Autowired
    private LoginSessionService loginSessionService;

    @RequestMapping(method = RequestMethod.PUT,value = "/session/id/{sessionId}")
    public ResponseEntity logout(@PathVariable("sessionId")long sessionId) throws Exception{
        String methodName = "logout() : ";
        logger.info(methodName + "called for sessionId " + sessionId);
        ResponseEntity<?> responseEntity = null;
        LoginSessionInterface existingLoginSession = loginSessionService.getById(sessionId);
        if (existingLoginSession != null) {
            existingLoginSession.setLogout(GlobalResources.getCurrentDate());
            LoginSessionInterface updatedExistingLoginSession = loginSessionService.update(existingLoginSession);
            if(updatedExistingLoginSession != null){
                responseEntity = new ResponseEntity(HttpStatus.OK);
            }else{
                responseEntity = new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
            }
        }
        return responseEntity;
    }
}