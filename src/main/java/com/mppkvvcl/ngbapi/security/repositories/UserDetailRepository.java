package com.mppkvvcl.ngbapi.security.repositories;

import com.mppkvvcl.ngbentity.beans.UserDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by PREETESH on 7/25/2017.
 */
@Repository
public interface UserDetailRepository extends JpaRepository<UserDetail, Long> {
    public UserDetail findByName(String name);

    public UserDetail findByUsername(String username);

    public List<UserDetail> findByLocationCodeAndRole(String locationCode, String role);
}
