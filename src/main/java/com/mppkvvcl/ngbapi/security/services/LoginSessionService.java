package com.mppkvvcl.ngbapi.security.services;

import com.mppkvvcl.ngbinterface.interfaces.LoginSessionInterface;
import com.mppkvvcl.ngbapi.security.dao.LoginSessionDAO;
import com.mppkvvcl.ngbapi.utility.GlobalResources;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LoginSessionService {

    /**
     * To get Logger object for logging in current class.
     */
    private Logger logger = GlobalResources.getLogger(LoginSessionService.class);

    @Autowired
    private LoginSessionDAO loginSessionDAO;

    public LoginSessionInterface insert(LoginSessionInterface loginSessionInterface){
        final String methodName = "insert() : ";
        logger.info(methodName + "called");
        LoginSessionInterface insertedLoginSession = null;
        if(loginSessionInterface != null){
            insertedLoginSession = loginSessionDAO.add(loginSessionInterface);
        }
        return insertedLoginSession;
    }

    public LoginSessionInterface getById(long id){
        final String methodName = "insert() : ";
        logger.info(methodName + "called for id " + id);
        LoginSessionInterface existingLoginSession = null;
        existingLoginSession = loginSessionDAO.getById(id);
        return existingLoginSession;
    }

    public LoginSessionInterface update(LoginSessionInterface loginSessionInterface){
        final String methodName = "update() : ";
        logger.info(methodName + "called");
        LoginSessionInterface updatedLoginSession = null;
        if(loginSessionInterface != null){
            updatedLoginSession = loginSessionDAO.update(loginSessionInterface);
        }
        return updatedLoginSession;
    }
}
