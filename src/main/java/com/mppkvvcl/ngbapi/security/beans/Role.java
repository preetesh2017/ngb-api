package com.mppkvvcl.ngbapi.security.beans;

import com.mppkvvcl.ngbinterface.interfaces.RoleInterface;

import javax.persistence.*;

/**
 * Created by NITISH on 14-07-2017.
 */
@Entity(name = "Role")
@Table(name = "role")
public class Role implements RoleInterface {

    public static final String OAG = "oag";

    public static final String JE = "je";

    public static final String AE = "ae";

    public static final String OIC = "oic";

    public static final String EE = "ee";

    public static final String SE = "se";

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "role")
    private String role;

    @Column(name = "priority")
    private int priority;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    @Override
    public String toString() {
        return "Role{" +
                "id=" + id +
                ", role='" + role + '\'' +
                ", priority=" + priority +
                '}';
    }
}
