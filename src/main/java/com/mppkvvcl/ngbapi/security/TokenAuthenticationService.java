package com.mppkvvcl.ngbapi.security;

import com.mppkvvcl.ngbinterface.interfaces.LoginSessionInterface;
import com.mppkvvcl.ngbapi.security.services.UserDetailService;
import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbentity.beans.UserDetail;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import org.slf4j.Logger;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;

import static java.util.Collections.emptyList;

/**
 * Created by NITISH on 24-04-2017.
 */
public class TokenAuthenticationService {

    /**
     * getting whole application logger object for performing logs. This logger will only be
     * used in this class methods only.
     */
    private static final Logger logger = GlobalResources.getLogger(TokenAuthenticationService.class);

    //private static final long EXPIRATIONTIME = 24*60*60*1000; // one day expiry
    private static final long EXPIRATIONTIME = 31*60*1000; // 30 minutes expiry
    private static final String SECRET = "nitishkumar";
    private static final String TOKEN_PREFIX = "Bearer";
    private static final String HEADER_STRING = "Authorization";

    public static void addAuthentication(HttpServletResponse res, String username, LoginSessionInterface loginSession, UserDetailService userDetailService) throws IOException{
        String methodName = "addAuthentication() : ";
        UserDetail loggedInUser = userDetailService.getByUsername(username);
        if(loggedInUser != null && loginSession != null){
            logger.info(methodName + "loggedInUser is not null creating JWT");
            String JWT = Jwts.builder()
                    .setSubject(loggedInUser.getUsername())
                    .claim("name",loggedInUser.getName())
                    .claim("role",loggedInUser.getRole())
                    .claim("designation",loggedInUser.getDesignation())
                    .claim("status",loggedInUser.getStatus())
                    .claim("locationCode",loggedInUser.getLocationCode())
                    .claim("sessionId",loginSession.getId())
                    .setExpiration(new Date(System.currentTimeMillis() + EXPIRATIONTIME))
                    .signWith(SignatureAlgorithm.HS256,SECRET)
                    .compact();
            res.addHeader(HEADER_STRING,TOKEN_PREFIX+" "+JWT);
        }else{
            logger.error(methodName + " loggedInUser is null. Please check. Throwing exception");
            ((HttpServletResponse) res).sendError(HttpServletResponse.SC_UNAUTHORIZED,"User not found for username " + username);
        }
    }

    public static Authentication getAuthentication(HttpServletRequest request) throws AuthenticationException,ExpiredJwtException,SignatureException {
        String methodName = "getAuthentication() : ";
        logger.info(methodName + "called");
        String token = request.getHeader(HEADER_STRING);
        //logger.info(methodName +"recieved token is " + token);
        try {
            if (token != null) {
                String user = Jwts.parser()
                        .setSigningKey(SECRET)
                        .parseClaimsJws(token.replace(TOKEN_PREFIX, ""))
                        .getBody().getSubject();
                return user != null ?
                        new UsernamePasswordAuthenticationToken(user, null, emptyList()) :
                        null;

            }
        }catch (AuthenticationException ae){
            logger.error(methodName + "got AuthenticationException as " + ae.getMessage());
            throw ae;
        }catch (ExpiredJwtException eje){
            logger.error(methodName + "got ExpiredJwtException as " + eje.getMessage());
            throw eje;
        }catch (SignatureException signatureException){
            logger.error(methodName + "got SignatureException as " + signatureException.getMessage());
            throw signatureException;
        }catch (Exception exception){
            logger.error(methodName + "Got Exception as " + exception.getMessage());
        }
        return null;
    }

}
