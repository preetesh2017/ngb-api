package com.mppkvvcl.ngbapi.custombeans;

import com.mppkvvcl.ngbinterface.interfaces.ZoneInterface;

import java.util.List;

/**
 * Created by PREETESH on 11/6/2017.
 */
public class ZoneConsumerData {

    private ZoneInterface zone;
    private long consumers;
    private long activeConsumers;
    private long inactiveConsumers;
    private List<GroupConsumerData> groups;

    public long getConsumers() {
        return consumers;
    }

    public void setConsumers(long consumers) {
        this.consumers = consumers;
    }

    public long getActiveConsumers() {
        return activeConsumers;
    }

    public void setActiveConsumers(long activeConsumers) {
        this.activeConsumers = activeConsumers;
    }

    public long getInactiveConsumers() {
        return inactiveConsumers;
    }

    public void setInactiveConsumers(long inactiveConsumers) {
        this.inactiveConsumers = inactiveConsumers;
    }

    public List<GroupConsumerData> getGroups() {
        return groups;
    }

    public void setGroups(List<GroupConsumerData> groups) {
        this.groups = groups;
    }

    public ZoneInterface getZone() {
        return zone;
    }

    public void setZone(ZoneInterface zone) {
        this.zone = zone;
    }

    @Override
    public String toString() {
        return "ZoneConsumerData{" +
                "zone=" + zone +
                ", consumers=" + consumers +
                ", activeConsumers=" + activeConsumers +
                ", inactiveConsumers=" + inactiveConsumers +
                ", groups=" + groups +
                '}';
    }
}
