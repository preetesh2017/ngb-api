package com.mppkvvcl.ngbapi.custombeans;

import com.mppkvvcl.ngbentity.beans.LoadDetail;
import com.mppkvvcl.ngbentity.beans.XrayConnectionInformation;
import com.mppkvvcl.ngbinterface.interfaces.LoadDetailInterface;
import com.mppkvvcl.ngbinterface.interfaces.XrayConnectionInformationInterface;

import java.util.Date;

/**
 * Created by vikas on 01-Nov-17.
 */
public class CustomLoadDetail {

    private LoadDetailInterface loadDetail;

    private XrayConnectionInformationInterface xrayConnectionInformation;

    Boolean isLoadChange;

    Boolean isTariffChange;

    String meterPhase;

    Date minDateToLoadChange;

    public LoadDetailInterface getLoadDetail() {
        return loadDetail;
    }

    public void setLoadDetailInterface(LoadDetailInterface loadDetail) {
        this.loadDetail = loadDetail;
    }

    public void setLoadDetail(LoadDetail loadDetail) {
        this.loadDetail = loadDetail;
    }

    public XrayConnectionInformationInterface getXrayConnectionInformation() {
        return xrayConnectionInformation;
    }

    public void setXrayConnectionInformation(XrayConnectionInformation xrayConnectionInformation) {
        this.xrayConnectionInformation = xrayConnectionInformation;
    }

    public void setXrayConnectionInformationInterface(XrayConnectionInformationInterface xrayConnectionInformation) {
        this.xrayConnectionInformation = xrayConnectionInformation;
    }

    public Boolean getIsLoadChange() {
        return isLoadChange;
    }

    public void setIsLoadChange(Boolean loadChange) {
        isLoadChange = loadChange;
    }

    public Boolean getIsTariffChange() {
        return isTariffChange;
    }

    public void setIsTariffChange(Boolean tariffChange) {
        isTariffChange = tariffChange;
    }

    public String getMeterPhase() {
        return meterPhase;
    }

    public void setMeterPhase(String meterPhase) {
        this.meterPhase = meterPhase;
    }

    public Date getMinDateToLoadChange() {
        return minDateToLoadChange;
    }

    public void setMinDateToLoadChange(Date minDateToLoadChange) {
        this.minDateToLoadChange = minDateToLoadChange;
    }

    @Override
    public String toString() {
        return "CustomLoadDetail{" +
                "loadDetail=" + loadDetail +
                ", xrayConnectionInformation=" + xrayConnectionInformation +
                ", isLoadChange=" + isLoadChange +
                ", isTariffChange=" + isTariffChange +
                ", meterPhase='" + meterPhase + '\'' +
                ", minDateToLoadChange=" + minDateToLoadChange +
                '}';
    }
}
