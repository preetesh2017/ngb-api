package com.mppkvvcl.ngbapi.custombeans;

import com.mppkvvcl.ngbentity.beans.Bill;
import com.mppkvvcl.ngbentity.beans.GMCAccounting;
import com.mppkvvcl.ngbinterface.interfaces.BillInterface;
import com.mppkvvcl.ngbinterface.interfaces.GMCAccountingInterface;

/**
 * Created by vikas on 22-Nov-17.
 */
public class CC4Bill {

    private BillInterface billInterface;

    private CustomReadMaster customReadMaster;

    private GMCAccountingInterface gmcAccountingInterface;

    public BillInterface getBillInterface() {
        return billInterface;
    }

    public void setBill(BillInterface bill) {
        if(bill != null) {
            this.billInterface = bill;
        }
    }

    public void setBillInterface(Bill billInterface) {
        if(billInterface != null) {
            this.billInterface = billInterface;
        }
    }

    public CustomReadMaster getCustomReadMaster() {
        return customReadMaster;
    }

    public void setCustomReadMaster(CustomReadMaster customReadMaster) {
        this.customReadMaster = customReadMaster;
    }

    public GMCAccountingInterface getGmcAccountingInterface() {
        return gmcAccountingInterface;
    }

    public void setGmcAccounting(GMCAccountingInterface gmcAccountingInterface) {
        this.gmcAccountingInterface = gmcAccountingInterface;
    }

    public void setGmcAccountingInterface(GMCAccounting gmcAccountingInterface) {
        this.gmcAccountingInterface = gmcAccountingInterface;
    }

    @Override
    public String toString() {
        return "CC4Bill{" +
                "billInterface=" + billInterface +
                ", customReadMaster=" + customReadMaster +
                ", gmcAccountingInterface=" + gmcAccountingInterface +
                '}';
    }
}
