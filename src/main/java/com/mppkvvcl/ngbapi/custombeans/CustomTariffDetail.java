package com.mppkvvcl.ngbapi.custombeans;

import com.mppkvvcl.ngbentity.beans.*;
import com.mppkvvcl.ngbinterface.interfaces.*;

import java.util.Date;
import java.util.List;

/**
 * Created by vikas on 10/6/2017.
 */
public class CustomTariffDetail extends TariffDetail {

    private TariffChangeDetailInterface tariffChangeDetail;

    private LoadDetailInterface loadDetail;

    private SeasonalConnectionInformationInterface seasonalConnectionInformation;

    private Read read;

    Boolean isLoadChange;

    Boolean isTariffChange;

    String meterPhase;

    Date minDateToTariffChange;

    Date minDateToTariffChangeWithLoad;

    private List<? extends TariffChangeMappingInterface> tariffChangeMappings;

    private XrayConnectionInformationInterface xrayConnectionInformation;

    public TariffChangeDetailInterface getTariffChangeDetail() {
        return tariffChangeDetail;
    }

    public void setTariffChangeDetail(TariffChangeDetail tariffChangeDetail) {
        if(tariffChangeDetail != null){
            this.tariffChangeDetail = tariffChangeDetail ;
        }
    }

    public void setTariffChangeDetailInterface(TariffChangeDetailInterface tariffChangeDetail) {
        if(tariffChangeDetail != null){
            this.tariffChangeDetail = tariffChangeDetail;
        }
    }


    public LoadDetailInterface getLoadDetail() {
        return loadDetail;
    }

    public void setLoadDetail(LoadDetail loadDetailInterface) {
        if(loadDetailInterface != null){
            this.loadDetail = loadDetailInterface;
        }
    }

    public void setLoadDetailInterface(LoadDetailInterface loadDetailInterface) {
        if(loadDetailInterface != null){
            this.loadDetail = loadDetailInterface;
        }
    }

    public SeasonalConnectionInformationInterface getSeasonalConnectionInformation() {
        return seasonalConnectionInformation;
    }

    public void setSeasonalConnectionInformation(SeasonalConnectionInformation seasonalConnectionInformation) {
        if(seasonalConnectionInformation != null){
            this.seasonalConnectionInformation = seasonalConnectionInformation;
        }
    }

    public void setSeasonalConnectionInformationInterface(SeasonalConnectionInformationInterface seasonalConnectionInformation) {
        if(seasonalConnectionInformation != null){
            this.seasonalConnectionInformation = seasonalConnectionInformation;
        }
    }

    public Read getRead() {
        return read;
    }

    public void setRead(Read read) {
        this.read = read;
    }

    public List<? extends TariffChangeMappingInterface> getTariffChangeMappings() {
        return tariffChangeMappings;
    }

    public void setTariffChangeMappings(List<TariffChangeMapping> tariffChangeMappings) {
        if(tariffChangeMappings != null){
            this.tariffChangeMappings = tariffChangeMappings;
        }
    }

    public void setTariffChangeMappingsInterface(List<? extends TariffChangeMappingInterface> tariffChangeMappings) {
        if(tariffChangeMappings != null){
            this.tariffChangeMappings = tariffChangeMappings;
        }
    }

    public XrayConnectionInformationInterface getXrayConnectionInformationInterface() {
        return xrayConnectionInformation;
    }

    public void setXrayConnectionInformationInterface(XrayConnectionInformationInterface xrayConnectionInformation) {
        if(xrayConnectionInformation != null) {
            this.xrayConnectionInformation = xrayConnectionInformation;
        }
    }

    public void setXrayConnectionInformation(XrayConnectionInformation xrayConnectionInformation) {
        if(xrayConnectionInformation != null) {
            this.xrayConnectionInformation = xrayConnectionInformation;
        }
    }

    public Boolean getIsLoadChange() {
        return isLoadChange;
    }

    public void setIsLoadChange(Boolean loadChange) {
        isLoadChange = loadChange;
    }

    public Boolean getIsTariffChange() {
        return isTariffChange;
    }

    public void setIsTariffChange(Boolean tariffChange) {
        isTariffChange = tariffChange;
    }

    public String getMeterPhase() {
        return meterPhase;
    }

    public void setMeterPhase(String meterPhase) {
        this.meterPhase = meterPhase;
    }

    public Date getMinDateToTariffChange() {
        return minDateToTariffChange;
    }

    public void setMinDateToTariffChange(Date minDateToTariffChange) {
        this.minDateToTariffChange = minDateToTariffChange;
    }

    public Date getMinDateToTariffChangeWithLoad() {
        return minDateToTariffChangeWithLoad;
    }

    public void setMinDateToTariffChangeWithLoad(Date minDateToTariffChangeWithLoad) {
        this.minDateToTariffChangeWithLoad = minDateToTariffChangeWithLoad;
    }

    @Override
    public String toString() {
        return "CustomTariffDetail{" +
                super.toString() +
                "tariffChangeDetail=" + tariffChangeDetail +
                ", loadDetail=" + loadDetail +
                ", seasonalConnectionInformation=" + seasonalConnectionInformation +
                ", read=" + read +
                ", isLoadChange=" + isLoadChange +
                ", isTariffChange=" + isTariffChange +
                ", meterPhase='" + meterPhase + '\'' +
                ", minDateToTariffChange=" + minDateToTariffChange +
                ", minDateToTariffChangeWithLoad=" + minDateToTariffChangeWithLoad +
                ", tariffChangeMappings=" + tariffChangeMappings +
                ", xrayConnectionInformation=" + xrayConnectionInformation +
                '}';
    }
}
