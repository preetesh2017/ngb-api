package com.mppkvvcl.ngbapi.services;

import com.mppkvvcl.ngbapi.custombeans.CustomWindowDetail;
import com.mppkvvcl.ngbapi.security.services.UserDetailService;
import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.CashWindowStatusInterface;
import com.mppkvvcl.ngbinterface.interfaces.WindowDetailInterface;
import com.mppkvvcl.ngbdao.daos.WindowDetailDAO;
import com.mppkvvcl.ngbentity.beans.UserDetail;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * Created by PREETESH on 7/25/2017.
 */
@Service
public class WindowDetailService {

    /**
     * Requesting spring to get singleton WindowDetailRepository object.
     */
    @Autowired
    private WindowDetailDAO windowDetailDAO;

    /**
     * Requesting spring to get singleton cashWindowRepository object.
     */
    @Autowired
    CashWindowStatusService cashWindowStatusService;

    /**
     * Requesting spring to get singleton userTRepository object.
     */
    @Autowired
    private UserDetailService userDetailService;

    /**
     * Getting logger object from global resources for current class.
     */
    private static final Logger logger = GlobalResources.getLogger(WindowDetailService.class);

    /**
     * This getUnAssignedWindowsForUser method fetch custom cash windows started for user table from backend database.<br>
     * Return allWindowsOfLocation successful else return null.<br><br>
     * param loggedInUser<br><br>
     * return customWindowDetail<br>
     */
    public CustomWindowDetail getUnAssignedWindowsForUser(String loggedInUser) {
        String methodName = " getUnAssignedWindowsForUser(): ";
        CustomWindowDetail customWindowDetail = null;
        logger.info(methodName + "fetch custom cash windows started for user: "+loggedInUser);
        UserDetail userDetail = userDetailService.getByUsername(loggedInUser);
        if(userDetail != null ){
            String location = userDetail.getLocationCode();
            List<WindowDetailInterface> allWindowsOfLocation = windowDetailDAO.getByLocationCode(location);
            logger.info(methodName + "fetch nos: "+allWindowsOfLocation.size()+" windows for location "+location);
            Date todayDate = GlobalResources.getCurrentDate();
            if(allWindowsOfLocation != null && allWindowsOfLocation.size() > 0 ){
                List<CashWindowStatusInterface> alreadyAssignedWindowsOfLocation = cashWindowStatusService.getAssignedWindowsByLocationAndDate(location,todayDate);
                logger.info(methodName + "fetch nos: "+alreadyAssignedWindowsOfLocation.size()+" open windows for location "+location);
                int nosOfOpenWindows = alreadyAssignedWindowsOfLocation.size();
                if(nosOfOpenWindows > 0){
                    alreadyAssignedWindowsOfLocation.forEach(cashWindowStatus -> {
                        String windowName = cashWindowStatus.getWindowName();
                        allWindowsOfLocation.removeIf(windowDetail -> windowDetail.getName().equals(windowName));
                    });
                    if(allWindowsOfLocation.size() > 0){
                        logger.info(methodName + "free windows for location, nos: "+allWindowsOfLocation.size());
                        customWindowDetail = new CustomWindowDetail();
                        customWindowDetail.setWindowDetail(allWindowsOfLocation);
                        customWindowDetail.setUserDetail(userDetail);
                        customWindowDetail.setWindowOpenDate(todayDate);
                    }else{
                        logger.info(methodName + "no free windows for location "+allWindowsOfLocation.size());
                    }
                }else{
                    customWindowDetail = new CustomWindowDetail();
                    customWindowDetail.setWindowDetail(allWindowsOfLocation);
                    customWindowDetail.setUserDetail(userDetail);
                    customWindowDetail.setWindowOpenDate(todayDate);
                }
            }else{
                logger.error(methodName + "no window found for location in window detail table "+location);
            }
        }else{
            logger.error(methodName + "userDetail details not found for userDetail "+loggedInUser);
        }
        return customWindowDetail;
    }




    /**
     * This getUnAssignedWindowsForUser method fetch custom cash windows started for user table from backend database.<br>
     * Return allWindowsOfLocation successful else return null.<br><br>
     * param loggedInUser<br><br>
     * return customWindowDetail<br>
     */
    public CustomWindowDetail getUnAssignedWindowsListOfLocation() {
        String methodName = " getUnAssignedWindowsListOfLocation(): ";
        CustomWindowDetail customWindowDetail = null;
        logger.info(methodName + "fetch custom cash windows started for user: ");
        UserDetail userDetail = userDetailService.getLoggedInUserDetails();
        if(userDetail != null ){
            String location = userDetail.getLocationCode();
            List<WindowDetailInterface> allWindowsOfLocation = windowDetailDAO.getByLocationCode(location);
            logger.info(methodName + "fetch nos: "+allWindowsOfLocation.size()+" windows for location "+location);
            Date todayDate = GlobalResources.getCurrentDate();
            if(allWindowsOfLocation != null && allWindowsOfLocation.size() > 0 ){
                List<CashWindowStatusInterface> alreadyAssignedWindowsOfLocation = cashWindowStatusService.getAssignedWindowsByLocationAndDate(location,todayDate);
                logger.info(methodName + "fetch nos: "+alreadyAssignedWindowsOfLocation.size()+" open windows for location "+location);
                int nosOfOpenWindows = alreadyAssignedWindowsOfLocation.size();
                if(nosOfOpenWindows > 0){
                    alreadyAssignedWindowsOfLocation.forEach(cashWindowStatus -> {
                        String windowName = cashWindowStatus.getWindowName();
                        allWindowsOfLocation.removeIf(windowDetail -> windowDetail.getName().equals(windowName));
                    });
                    if(allWindowsOfLocation.size() > 0){
                        logger.info(methodName + "free windows for location, nos: "+allWindowsOfLocation.size());
                        customWindowDetail = new CustomWindowDetail();
                        customWindowDetail.setWindowDetail(allWindowsOfLocation);
                        customWindowDetail.setUserDetail(userDetail);
                        customWindowDetail.setWindowOpenDate(todayDate);
                    }else{
                        logger.info(methodName + "no free windows for location "+allWindowsOfLocation.size());
                    }
                }else{
                    customWindowDetail = new CustomWindowDetail();
                    customWindowDetail.setWindowDetail(allWindowsOfLocation);
                    customWindowDetail.setUserDetail(userDetail);
                    customWindowDetail.setWindowOpenDate(todayDate);
                }
            }else{
                logger.error(methodName + "no window found for location in window detail table "+location);
            }
        }else{
            logger.error(methodName + "userDetail details not found for userDetail ");
        }
        return customWindowDetail;
    }
}
