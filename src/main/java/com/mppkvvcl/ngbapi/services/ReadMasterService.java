package com.mppkvvcl.ngbapi.services;

import com.mppkvvcl.ngbapi.custombeans.Read;
import com.mppkvvcl.ngbapi.security.services.UserDetailService;
import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbapi.utility.NGBAPIUtility;
import com.mppkvvcl.ngbdao.daos.ReadMasterDAO;
import com.mppkvvcl.ngbentity.beans.*;
import com.mppkvvcl.ngbinterface.interfaces.*;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * Created by PREETESH on 6/30/2017.
 */
@Service
public class ReadMasterService {

    /**
     * getting whole application logger object for performing logs. This logger will only be
     * used in this class methods only.
     */
    private static final Logger logger = GlobalResources.getLogger(ReadMasterService.class);

    @Autowired
    private ReadMasterDAO readMasterDAO;

    @Autowired
    private ReadMasterPFService readMasterPFService;

    @Autowired
    private ReadMasterKWService readMasterKWService;

    @Autowired

    private TariffService tariffService;

    @Autowired
    private ConsumerConnectionInformationService consumerConnectionInformationService;

    @Autowired
    private ReadTypeConfiguratorService readTypeConfiguratorService;

    @Autowired
    private MeterMasterService meterMasterService;

    @Autowired
    private ConsumerMeterMappingService consumerMeterMappingService;

    @Autowired
    private ScheduleService scheduleService;

    @Autowired
    private ConsumerConnectionAreaInformationService consumerConnectionAreaInformationService;

    @Autowired
    private UserDetailService userDetailService;

    @Autowired
    private ConsumerNoMasterService consumerNoMasterService;

    public long getCountByConsumerNo(String consumerNo) {
        final String methodName = "getCountByConsumerNo() : ";
        logger.info(methodName + "called");
        return readMasterDAO.getCountByConsumerNo(consumerNo);
    }

    public List<ReadMasterInterface> getByConsumerNo(String consumerNo) {
        String methodName = " getByConsumerNo() : ";
        logger.info(methodName + "called " + consumerNo);
        List<ReadMasterInterface> readings = null;
        if (consumerNo != null) {
            consumerNo = consumerNo.trim();
            readings = readMasterDAO.getByConsumerNo(consumerNo);
        }
        return readings;
    }

    public ReadMasterInterface getLatestReadingByConsumerNo(String consumerNo) throws Exception {
        String methodName = " getLatestReadingByConsumerNo(): ";
        logger.info(methodName + "called " + consumerNo);
        ReadMasterInterface latestReading = null;
        ReadTypeConfiguratorInterface readTypeConfigurator = null;
        if (consumerNo != null) {
            consumerNo = consumerNo.trim();
            latestReading = readMasterDAO.getTopByConsumerNoOrderByIdDesc(consumerNo);
            if (latestReading != null) {
                logger.info(methodName + " Successfully retrieved latest reading by consumer number" + consumerNo);
                long readingId = latestReading.getId();
                logger.info(methodName + "fetching readTypeConfigurator for consumer" + consumerNo);
                readTypeConfigurator = readTypeConfiguratorService.getByConsumerNo(consumerNo);
                if (readTypeConfigurator != null) {
                    logger.info(methodName + " Successfully retrieved readTypeConfigurator for consumer number" + consumerNo);
                    if (readTypeConfigurator.isKw()) {
                        logger.info(methodName + " Retrieving KW reading for consumer number" + consumerNo);
                        ReadMasterKWInterface readMasterKW = readMasterKWService.getByReadMasterId(readingId);
                        if (readMasterKW != null) {
                            logger.info(methodName + " successfully Retrieved KW reading for consumer number" + consumerNo);
                            latestReading.setReadMasterKW(readMasterKW);
                        } else {
                            logger.error(methodName + " ReadMasterKW received is null" + consumerNo);
                        }
                    }
                    if (readTypeConfigurator.isPf()) {
                        logger.info(methodName + " Retrieving PF reading for consumer number" + consumerNo);
                        ReadMasterPFInterface readMasterPF = readMasterPFService.getByReadMasterId(readingId);
                        if (readMasterPF != null) {
                            logger.info(methodName + " successfully Retrieved PF reading for consumer number" + consumerNo);
                            latestReading.setReadMasterPF(readMasterPF);
                        } else {
                            logger.error(methodName + " ReadMasterPF received is null" + consumerNo);
                        }
                    }

                } else {
                    logger.error(methodName + " no ReadTypeConfigurator found for consumer number " + consumerNo);
                    throw new Exception(" no ReadTypeConfigurator found for consumer");
                }
            } else {
                logger.error(methodName + " no reading retrieved for consumer number " + consumerNo);
                throw new Exception("No reading retrieved for consumer");
            }

        } else {
            logger.error(methodName + "consumerNo passed is null in method.");
            throw new Exception("consumerNo passed is null in method.");
        }
        return latestReading;
    }

    public ReadMasterInterface getLatestReadingByConsumerNoAndBillMonthAndReplacementFlag(String consumerNo, String billMonth, String replacementFlag) {
        String methodName = " getLatestReadingByConsumerNoAndBillMonthAndReplacementFlag(): ";
        logger.info(methodName + " service method called for fetching Readings for consumer " + consumerNo);
        ReadMasterInterface latestReading = null;
        if (consumerNo != null && billMonth != null && replacementFlag != null) {
            consumerNo = consumerNo.trim();
            List<ReadMasterInterface> latestReadings = readMasterDAO.getByConsumerNoAndBillMonthAndReplacementFlagOrderByIdDesc(consumerNo, billMonth, replacementFlag);
            logger.info(methodName + " retrieved latest readings by consumer number " + consumerNo + " for bill month " + billMonth + " Replacement flag " + replacementFlag + " readings " + latestReadings);
            if (latestReadings != null && latestReadings.size() > 0) {
                latestReading = latestReadings.get(0);
                if (latestReading != null) {
                    long readingId = latestReading.getId();
                    logger.info(methodName + "fetching readTypeConfigurator for consumer" + consumerNo);
                    ReadTypeConfiguratorInterface readTypeConfigurator = readTypeConfiguratorService.getByConsumerNo(consumerNo);
                    if (readTypeConfigurator != null) {
                        logger.info(methodName + " Successfully retrieved readTypeConfigurator for consumer number" + consumerNo + " readTypeConfigurator " + readTypeConfigurator);
                        if (readTypeConfigurator.isKw()) {
                            logger.info(methodName + " Retrieving KW reading for consumer number" + consumerNo);
                            ReadMasterKWInterface readMasterKW = readMasterKWService.getByReadMasterId(readingId);
                            if (readMasterKW != null) {
                                logger.info(methodName + " successfully Retrieved KW reading for consumer number" + consumerNo);
                                latestReading.setReadMasterKW(readMasterKW);
                            } else {
                                logger.error(methodName + " ReadMasterKW received is null" + consumerNo);
                            }
                        }

                        if (readTypeConfigurator.isPf()) {
                            logger.info(methodName + " Retrieving PF reading for consumer number" + consumerNo);
                            ReadMasterPFInterface readMasterPF = readMasterPFService.getByReadMasterId(readingId);
                            if (readMasterPF != null) {
                                logger.info(methodName + " successfully Retrieved PF reading for consumer number" + consumerNo);
                                latestReading.setReadMasterPF(readMasterPF);
                            } else {
                                logger.error(methodName + " ReadMasterPF received is null" + consumerNo);
                            }
                        }
                    } else {
                        logger.error(methodName + " no ReadTypeConfigurator found for consumer number " + consumerNo);
//                        throw new Exception("no ReadTypeConfigurator found for consumer ");
                    }
                }
            } else {
                logger.error(methodName + " no reading retrieved for consumer number " + consumerNo);
            }
        } else {
            logger.error(methodName + "consumerNo passed is null in method.");
//            throw new Exception("consumerNo passed is null in method.");
        }
        return latestReading;
    }


    /**
     * Added Transactional behaviour in insertion of read master
     * The rollbackFor parameter asks spring to rollback if any exception occure which is child of exception
     * <p>
     * param readToInsert
     * return
     */
    @Transactional(rollbackFor = Exception.class)
    public ReadMasterInterface insertReadMasterWithReadMasterKWAndReadMasterPF(ReadMasterInterface readToInsert) throws Exception {
        String methodName = " insertReadMasterWithReadMasterKWAndReadMasterPF() : ";
        ReadMasterInterface insertedRead = null;
        if (readToInsert != null) {
            String consumerNo = readToInsert.getConsumerNo();
            logger.info(methodName + " service method called for inserting Readings for consumer " + consumerNo);
            if (consumerNo != null) {
                consumerNo = consumerNo.trim();
                logger.info(methodName + "Fetching Consumer Connection Area Information");
                ConsumerConnectionAreaInformationInterface consumerConnectionAreaInformation = consumerConnectionAreaInformationService.getByConsumerNo(consumerNo);
                if (consumerConnectionAreaInformation != null && consumerConnectionAreaInformation.getGroupNo() != null) {
                    logger.info(methodName + "fetching Schedule ");
                    ScheduleInterface schedule = scheduleService.getLatestNotSubmittedPendingScheduleByConsumerNo(consumerNo);
                    if (schedule != null) {
                        logger.info(methodName + "Fetched Schedule  " + schedule);
                        String scheduleBillMonth = schedule.getBillMonth();
                        Date startReadingDate = schedule.getStartReadingDate();
                        Date endReadingDate = schedule.getEndReadingDate();
                        Date formDate = readToInsert.getReadingDate();
                        if (startReadingDate != null && endReadingDate != null && formDate != null) {
                            boolean dateBetweenFlag = GlobalResources.checkDateBetween(startReadingDate, endReadingDate, formDate);
                            if (dateBetweenFlag) {
                                logger.info(methodName + " Fetching previous read for consumer : " + consumerNo);
                                ReadMasterInterface previousRead = getLatestReadingByConsumerNo(consumerNo);
                                if (previousRead != null) {
                                    logger.info(methodName + "previous read is not null proceeding");
                                    String previousReplacementFlag = previousRead.getReplacementFlag();
                                    String previousBillMonth = previousRead.getBillMonth();
                                    Date previousDate = previousRead.getReadingDate();
                                    if (previousDate != null) {
                                        boolean readDateFlag = GlobalResources.checkDateBetweenPreviousDateAndCurrentDate(previousDate, formDate);
                                        if (readDateFlag) {
                                            logger.info(methodName + "previousReplacementFlag" + previousReplacementFlag + "schedule Bill month " + scheduleBillMonth + "Previous Read Bill month" + previousBillMonth);
                                            if (previousReplacementFlag.equals(ReadMasterInterface.REPLACEMENT_FLAG_NORMAL_READ) && scheduleBillMonth.equals(previousBillMonth)) {
                                                logger.error(methodName + "Read already present for Schedule bill month for Consumer no" + consumerNo);
                                                insertedRead = null;
                                                throw new Exception("Read already present for schedule bill month.");
                                            }
                                            BigDecimal previousReading = previousRead.getReading();
                                            BigDecimal currentReading = readToInsert.getReading();
                                            BigDecimal propagatedAssessment = BigDecimal.ZERO;
                                            logger.info(methodName + "Comparing Previous Read And Current Read");
                                            int i = currentReading.compareTo(previousReading);
                                            if (i >= 0) {
                                                BigDecimal differenceReadPassed = readToInsert.getDifference().setScale(3, BigDecimal.ROUND_HALF_UP);
                                                BigDecimal difference = currentReading.subtract(previousReading).setScale(3, BigDecimal.ROUND_HALF_UP);
                                                logger.info(methodName + " form difference " + differenceReadPassed);
                                                logger.info(methodName + " server difference " + difference);
                                                logger.info(methodName + " Calculating difference for readpassed and readstored for consumer : " + consumerNo);
                                                if (difference.equals(differenceReadPassed)) {
                                                    logger.info(methodName + "differences from form and server matches proceeding");
                                                    logger.info(methodName + " Calculating difference with MF for readpassed and readstored for consumer : " + consumerNo);
                                                    ReadTypeConfiguratorInterface readTypeConfigurator = readTypeConfiguratorService.getByConsumerNo(consumerNo);
                                                    if (readTypeConfigurator != null) {
                                                        if (readTypeConfigurator.isKwh()) {
                                                            logger.info(methodName + " configurator says to insert kwh, inserting kwh");
                                                            List<ConsumerMeterMappingInterface> consumerMeterMappings = consumerMeterMappingService.getByConsumerNoAndMappingStatus(consumerNo, ConsumerMeterMappingInterface.STATUS_ACTIVE);
                                                            if (consumerMeterMappings != null && consumerMeterMappings.size() > 0) {
                                                                ConsumerMeterMappingInterface consumerMeterMapping = consumerMeterMappings.get(0);
                                                                if (consumerMeterMapping != null) {
                                                                    if ((previousReplacementFlag.equals(ReadMasterInterface.REPLACEMENT_FLAG_START_READ) || previousReplacementFlag.equals(ReadMasterInterface.REPLACEMENT_FLAG_CTR_READ))
                                                                            && previousBillMonth.equals(scheduleBillMonth)) {
                                                                        logger.info(methodName + "Last Inserted Reading is a replacement reading for same month. " +
                                                                                "Hence carrying forward total consumption as assessed consumption");
                                                                        propagatedAssessment = previousRead.getTotalConsumption().setScale(3, BigDecimal.ROUND_HALF_UP);
                                                                    }
                                                                    BigDecimal mf = consumerMeterMappingService.getMFByConsumerNoForActiveMeter(consumerNo);
                                                                    if (mf != null && mf.compareTo(BigDecimal.ZERO) == 1) {
                                                                        BigDecimal consumption = difference.multiply(mf);
                                                                        BigDecimal assessment = BigDecimal.ZERO;
                                                                        logger.info(methodName + "difference : " + difference + " * " + "mf : " + mf + " = " + consumption);
                                                                        if (readToInsert.getAssessment() != null) {
                                                                            assessment = readToInsert.getAssessment().setScale(3, BigDecimal.ROUND_HALF_UP);
                                                                        }
                                                                        logger.info(methodName + "Calculating total Consumption ");
                                                                        BigDecimal totalConsumption = consumption.add(assessment).add(propagatedAssessment);
                                                                        logger.info(methodName + "consumption : " + consumption + " + " + " assessment: " + assessment + " = " + totalConsumption);
                                                                        readToInsert.setMeterIdentifier(consumerMeterMapping.getMeterIdentifier());
                                                                        readToInsert.setBillMonth(scheduleBillMonth);
                                                                        readToInsert.setGroupNo(consumerConnectionAreaInformation.getGroupNo());
                                                                        readToInsert.setReadingDiaryNo(consumerConnectionAreaInformation.getReadingDiaryNo());
                                                                        readToInsert.setReplacementFlag(ReadMasterInterface.REPLACEMENT_FLAG_NORMAL_READ);
                                                                        readToInsert.setMf(mf);
                                                                        readToInsert.setConsumption(consumption);
                                                                        readToInsert.setAssessment(assessment);
                                                                        readToInsert.setPropagatedAssessment(propagatedAssessment);
                                                                        readToInsert.setTotalConsumption(totalConsumption);
                                                                        logger.info(methodName + " inserting read master  for consumer : " + consumerNo + "read master" + readToInsert);
                                                                        insertedRead = insert(readToInsert);
                                                                        if (insertedRead != null) {
                                                                            logger.info(methodName + " successfully inserted read master  for consumer : " + consumerNo);
                                                                            ReadMasterKWInterface insertedReadMasterKW = null;
                                                                            if (readTypeConfigurator.isKw()) {
                                                                                logger.info(methodName + " configurator says to insert kw, inserting kw");
                                                                                ReadMasterKWInterface readMasterKW = readToInsert.getReadMasterKW();
                                                                                logger.info(methodName + " inserting read master KW for consumer : " + consumerNo);
                                                                                if (readMasterKW != null && readMasterKW.getMeterMD() != null) {
                                                                                    BigDecimal multipliedMD = readMasterKW.getMeterMD().multiply(mf).setScale(3, BigDecimal.ROUND_HALF_UP);
                                                                                    readMasterKW.setReadMasterId(insertedRead.getId());
                                                                                    readMasterKW.setMultipliedMD(multipliedMD);
                                                                                    setAuditDetails(readMasterKW);
                                                                                    if ((previousReplacementFlag.equals(ReadMasterInterface.REPLACEMENT_FLAG_START_READ) || previousReplacementFlag.equals(ReadMasterInterface.REPLACEMENT_FLAG_CTR_READ))
                                                                                            && previousBillMonth.equals(scheduleBillMonth)) {
                                                                                        logger.info(methodName + "Previous Read Stored is Replacement, Comparing stored KW with given KW ");
                                                                                        ReadMasterKWInterface previousReadMasterKW = readMasterKWService.getByReadMasterId(previousRead.getId());
                                                                                        if (previousReadMasterKW != null) {
                                                                                            BigDecimal oldBilledKW = previousReadMasterKW.getBillingDemand();
                                                                                            BigDecimal greaterKW = multipliedMD.max(oldBilledKW);
                                                                                            readMasterKW.setBillingDemand(greaterKW);
                                                                                        } else {
                                                                                            readMasterKW.setBillingDemand(multipliedMD);
                                                                                        }
                                                                                    } else {
                                                                                        readMasterKW.setBillingDemand(multipliedMD);
                                                                                    }
                                                                                    insertedReadMasterKW = readMasterKWService.insert(readMasterKW);
                                                                                    if (insertedReadMasterKW != null) {
                                                                                        logger.info(methodName + " successfully inserted read master KW for consumer : " + consumerNo);
                                                                                    } else {
                                                                                        logger.info(methodName + "unable to save KW Read, aborting request");
                                                                                        throw new Exception("unable to save KW Read, aborting request");
                                                                                    }
                                                                                } else {
                                                                                    logger.error(methodName + "Read Type configurator returned true for KW but ReadMasterKw is found null.");
                                                                                    throw new Exception("KW is required for such class of consumer");
                                                                                }
                                                                            } else {
                                                                                logger.info(methodName + " read type configurator does not allows to add kw reading");
                                                                            }

                                                                            insertedRead.setReadMasterKW(insertedReadMasterKW);

                                                                            ReadMasterPFInterface insertedReadMasterPF = null;
                                                                            if (readTypeConfigurator.isPf()) {
                                                                                logger.info(methodName + " configurator says to insert pf, inserting pf");
                                                                                ReadMasterPFInterface readMasterPF = readToInsert.getReadMasterPF();
                                                                                logger.info(methodName + " inserting read master PF for consumer : " + consumerNo);
                                                                                if (readMasterPF != null && readMasterPF.getMeterPF() != null) {
                                                                                    BigDecimal pf = readMasterPF.getMeterPF().setScale(3, BigDecimal.ROUND_HALF_UP);
                                                                                    readMasterPF.setReadMasterId(insertedRead.getId());
                                                                                    setAuditDetails(readMasterPF);
                                                                                    if ((previousReplacementFlag.equals(ReadMasterInterface.REPLACEMENT_FLAG_START_READ) || previousReplacementFlag.equals(ReadMasterInterface.REPLACEMENT_FLAG_CTR_READ))
                                                                                            && previousBillMonth.equals(scheduleBillMonth)) {
                                                                                        logger.info(methodName + "Previous Read Stored is Replacement, Comparing stored PF with given PF ");
                                                                                        ReadMasterPFInterface presentReadMasterPF = readMasterPFService.getByReadMasterId(previousRead.getId());
                                                                                        if (presentReadMasterPF != null) {
                                                                                            logger.info(methodName + "Read Fetched for PF");
                                                                                            BigDecimal oldBilledPF = presentReadMasterPF.getBillingPF();
                                                                                            BigDecimal smallerPF = oldBilledPF.min(pf);
                                                                                            readMasterPF.setBillingPF(smallerPF);
                                                                                        } else {
                                                                                            logger.info(methodName + "No Read found for PF");
                                                                                            readMasterPF.setBillingPF(pf);
                                                                                        }
                                                                                    } else {
                                                                                        readMasterPF.setMeterPF(pf);
                                                                                        readMasterPF.setBillingPF(pf);
                                                                                    }
                                                                                    insertedReadMasterPF = readMasterPFService.insert(readMasterPF);
                                                                                    if (insertedReadMasterPF != null) {
                                                                                        logger.info(methodName + " successfully inserted read master PF for consumer : " + consumerNo);
                                                                                    } else {
                                                                                        logger.error(methodName + "unable to save KW Read, aborting request");
                                                                                        throw new Exception("unable to save KW Read, aborting request");
                                                                                    }
                                                                                } else {
                                                                                    logger.error(methodName + "Read Type configurator returned true for PF but ReadMasterPF is found null.");
                                                                                    throw new Exception("PF is required for such class of consumer");
                                                                                }
                                                                            } else {
                                                                                logger.info(methodName + " read type configurator does not allows to add pf reading");
                                                                            }
                                                                            insertedRead.setReadMasterPF(insertedReadMasterPF);
                                                                        } else {
                                                                            logger.error(methodName + "Couldn't insert Read Master that is couldnt save reading corresponding to kwh");
                                                                            throw new Exception("Unable to insert kwh reading");
                                                                        }
                                                                    } else {
                                                                        logger.error(methodName + "MF is less than ZERO, aborting request");
                                                                        throw new Exception("MF is less than ZERO, aborting request");
                                                                    }
                                                                } else {
                                                                    logger.error(methodName + "consumer meter mapping is null for consumer No " + consumerNo);
                                                                    throw new Exception("No active meter on consumer");
                                                                }
                                                            } else {
                                                                logger.error(methodName + " consumers meter mappings are null for consumer no " + consumerNo);
                                                                throw new Exception("No active meter on consumer");
                                                            }
                                                        } else {
                                                            logger.error(methodName + " read type configurator does not allows to add kwh reading,aborting request");
                                                            throw new Exception("You cannot punch kwh reading for such class of consumer");
                                                        }
                                                    } else {
                                                        logger.error(methodName + "no read type configurator found for consumerNo: " + consumerNo + " aborting meter insert");
                                                        throw new Exception("You cannot punch kwh reading for unmetered consumer");
                                                    }

                                                } else {
                                                    logger.error(methodName + " record not updated for consumer  difference Mismatch" + consumerNo);
                                                    throw new Exception("Mismatch in reading difference occurred at server end");
                                                }
                                            } else {
                                                logger.error(methodName + "Current Read is less than previous read");
                                                throw new Exception(methodName + "Current Read is less than previous read");
                                            }
                                        } else {
                                            logger.error(methodName + "Given Input date is less than Previous read Date OR greater than Current Date");
                                            throw new Exception("Given Input date is less than Previous read Date OR greater than Current Date");
                                        }
                                    } else {
                                        logger.error(methodName + "Previous Date not found in Previous Read");
                                        throw new Exception("Previous Date not found in Previous Read");
                                    }
                                } else {
                                    logger.error(methodName + "Previous Read for consumerNo is null in method.");
                                    throw new Exception("No previous reading exists for consumer ");
                                }
                            } else {
                                logger.error(methodName + "Reading Date is not between schedule StartReadingDate and EndReadingDate");
                                throw new Exception("Reading Date is not between schedule StartReadingDate and EndReadingDate");
                            }
                        } else {
                            logger.error(methodName + "Schedule Start Date Missing Or Schedule End Date Missing OR Form Date Missing");
                            throw new Exception("Schedule Start Date Missing Or Schedule End Date Missing OR Form Date Missing");
                        }
                    } else {
                        logger.error(methodName + "No schedule found in pending status with Not submitted flag for Consumer no" + consumerNo);
                        throw new Exception("No schedule present in pending status.");
                    }
                } else {
                    logger.error(methodName + "Consumer Connection Area Information not found for Consumer no" + consumerNo);
                }
            } else {
                logger.error(methodName + "consumerNo passed is null in method.");
                throw new Exception("Enter correct consumer number");
            }
        } else {
            logger.error(methodName + "Read master is null in method .");
            throw new Exception("Current Reading is not correct");
        }
        return insertedRead;
    }

    /**
     * This method takes final Read( in patterns 9999 or 99999 or 999999) and Current Read to insert Meter ReStart Read.
     * param currentRead
     * param meterReStartFinalRead
     * return
     * throws Exception
     */
    @Transactional(rollbackFor = Exception.class)
    public ReadMasterInterface insertMeterReStartRead(ReadMaster currentRead, long meterReStartFinalRead) throws Exception {
        String methodName = " insertMeterReStartRead() : ";
        logger.info(methodName + "called");
        ReadMasterInterface insertedRead = null;
        ReadMasterInterface finalRead = new ReadMaster();
        ReadMasterInterface insertedFinalRead = null;
        ReadMasterInterface startRead = new ReadMaster();
        ReadMasterInterface insertedStartRead = null;
        if (currentRead != null && meterReStartFinalRead > 0) {
            String consumerNo = currentRead.getConsumerNo();
            if (consumerNo != null) {
                consumerNo = consumerNo.trim();
                logger.info(methodName + "Fetching Consumer Connection Area Information");
                ConsumerConnectionAreaInformationInterface consumerConnectionAreaInformation = consumerConnectionAreaInformationService.getByConsumerNo(consumerNo);
                if (consumerConnectionAreaInformation != null && consumerConnectionAreaInformation.getGroupNo() != null) {
                    logger.info(methodName + "fetching Schedule for Group No : " + consumerConnectionAreaInformation.getGroupNo());
                    ScheduleInterface schedule = scheduleService.getLatestNotSubmittedPendingScheduleByConsumerNo(consumerNo);
                    if (schedule != null) {
                        logger.info(methodName + "Fetched Schedule  " + schedule);
                        String scheduleBillMonth = schedule.getBillMonth();
                        Date startReadingDate = schedule.getStartReadingDate();
                        Date endReadingDate = schedule.getEndReadingDate();
                        Date formDate = currentRead.getReadingDate();
                        if (startReadingDate != null && endReadingDate != null && formDate != null) {
                            boolean dateBetweenFlag = GlobalResources.checkDateBetween(startReadingDate, endReadingDate, formDate);
                            logger.info(methodName + "Checking Reading Form Date with Schedule Start Date & Schedule end date");
                            if (dateBetweenFlag) {
                                logger.info(methodName + " Fetching previous read for consumer : " + consumerNo);
                                ReadMasterInterface previousRead = getLatestReadingByConsumerNo(consumerNo);
                                if (previousRead != null) {
                                    logger.info(methodName + "previous read is not null proceeding");
                                    String previousReplacementFlag = previousRead.getReplacementFlag();
                                    String previousBillMonth = previousRead.getBillMonth();
                                    Date previousDate = previousRead.getReadingDate();
                                    if (previousDate != null) {
                                        logger.info(methodName + "Checking Reading Form date with Previous Read Date And Server Current Date");
                                        boolean readDateFlag = GlobalResources.checkDateBetweenPreviousDateAndCurrentDate(previousDate, formDate);
                                        if (readDateFlag) {
                                            logger.info(methodName + "previousReplacementFlag:" + previousReplacementFlag + "schedule Bill month:" + scheduleBillMonth + "Previous Read Bill month" + previousBillMonth);
                                            if (previousReplacementFlag.equals(ReadMasterInterface.REPLACEMENT_FLAG_NORMAL_READ) && scheduleBillMonth.equals(previousBillMonth)) {
                                                logger.error(methodName + "Read already present for Schedule bill month for Consumer no" + consumerNo);
                                                insertedRead = null;
                                                throw new Exception("Read already present for Schedule bill month for Consumer no");
                                            }
                                            BigDecimal previousReading = previousRead.getReading();
                                            BigDecimal currentReading = currentRead.getReading();
                                            BigDecimal propagatedAssessment = BigDecimal.ZERO;
                                            BigDecimal finalBillingDemand = BigDecimal.ZERO;
                                            BigDecimal finalBillingPF = BigDecimal.ZERO;
                                            BigDecimal meterRoundWrapFinalRead = new BigDecimal(meterReStartFinalRead);
                                            BigDecimal difference = meterRoundWrapFinalRead.subtract(previousReading).setScale(3, BigDecimal.ROUND_HALF_UP);
                                            logger.info(methodName);
                                            int compareFinalRead = meterRoundWrapFinalRead.compareTo(previousReading);
                                            if (compareFinalRead < 0) {
                                                logger.info(methodName + "given final read (meter digits) is less than previous read");
                                                throw new Exception("given final read (meter digits) is less than previous read");
                                            }
                                            logger.info(methodName + " fetching read Type Configurator with consumer : " + consumerNo);
                                            ReadTypeConfiguratorInterface readTypeConfigurator = readTypeConfiguratorService.getByConsumerNo(consumerNo);
                                            if (readTypeConfigurator != null) {
                                                if (readTypeConfigurator.isKwh()) {
                                                    logger.info(methodName + " configurator says to insert kwh, inserting kwh");
                                                    List<ConsumerMeterMappingInterface> consumerMeterMappings = consumerMeterMappingService.getByConsumerNoAndMappingStatus(consumerNo, ConsumerMeterMappingInterface.STATUS_ACTIVE);
                                                    if (consumerMeterMappings != null && consumerMeterMappings.size() > 0) {
                                                        ConsumerMeterMappingInterface consumerMeterMapping = consumerMeterMappings.get(0);
                                                        logger.info(methodName + "Fetched Consumer meter mapping for Consumer No : " + consumerNo);
                                                        if (consumerMeterMapping != null) {
                                                            if ((previousReplacementFlag.equals(ReadMasterInterface.REPLACEMENT_FLAG_START_READ) || previousReplacementFlag.equals(ReadMasterInterface.REPLACEMENT_FLAG_CTR_READ))
                                                                    && previousBillMonth.equals(scheduleBillMonth)) {
                                                                logger.info(methodName + "Last Inserted Reading is a replacement reading for same month. " +
                                                                        "Hence carrying forward total consumption as assessed consumption");
                                                                propagatedAssessment = previousRead.getTotalConsumption().setScale(3, BigDecimal.ROUND_HALF_UP);
                                                            }
                                                            logger.info(methodName + "Fetching MF for Consumer No :" + consumerNo);
                                                            BigDecimal mf = consumerMeterMappingService.getMFByConsumerNoForActiveMeter(consumerNo);
                                                            if (mf != null && mf.compareTo(BigDecimal.ZERO) == 1) {
                                                                BigDecimal consumption = difference.multiply(mf).setScale(3, BigDecimal.ROUND_HALF_UP);
                                                                BigDecimal assessment = BigDecimal.ZERO;
                                                                logger.info(methodName + "Calculating total Consumption ");
                                                                BigDecimal totalConsumption = consumption.add(assessment).add(propagatedAssessment);
                                                                logger.info(methodName + "Setting FinalRead object");
                                                                finalRead.setBillMonth(scheduleBillMonth);
                                                                finalRead.setGroupNo(consumerConnectionAreaInformation.getGroupNo());
                                                                finalRead.setReadingDiaryNo(consumerConnectionAreaInformation.getReadingDiaryNo());
                                                                finalRead.setConsumerNo(consumerNo);
                                                                finalRead.setMeterIdentifier(consumerMeterMapping.getMeterIdentifier());
                                                                finalRead.setReadingDate(currentRead.getReadingDate());
                                                                finalRead.setReadingType(ReadMasterInterface.READING_TYPE_NORMAL);
                                                                finalRead.setMeterStatus(ReadMasterInterface.METER_STATUS_METER_RESTART);
                                                                finalRead.setReplacementFlag(ReadMasterInterface.REPLACEMENT_FLAG_FINAL_READ);
                                                                finalRead.setMf(mf);
                                                                finalRead.setSource(ReadMasterInterface.SOURCE_MANUAL);
                                                                finalRead.setReading(meterRoundWrapFinalRead);
                                                                finalRead.setDifference(difference);
                                                                finalRead.setMf(mf);
                                                                finalRead.setConsumption(consumption);
                                                                finalRead.setAssessment(BigDecimal.ZERO);
                                                                finalRead.setPropagatedAssessment(propagatedAssessment);
                                                                finalRead.setTotalConsumption(totalConsumption);
                                                                logger.info(methodName + "inserting Final Read for Meter Restart for Consumer no :" + consumerNo);
                                                                insertedFinalRead = insert(finalRead);
                                                                if (insertedFinalRead != null) {
                                                                    logger.info(methodName + "Inserted Final Read For Meter Restart for Consumer no :" + consumerNo);
                                                                    if (readTypeConfigurator.isKw()) {
                                                                        logger.info(methodName + "Configurator says to insert KW ");
                                                                        ReadMasterKWInterface finalReadMasterKW = new ReadMasterKW();
                                                                        ReadMasterKWInterface insertedFinalReadMasterKW = null;
                                                                        ReadMasterKWInterface previousReadMasterKW = readMasterKWService.getByReadMasterId(previousRead.getId());
                                                                        if ((previousReplacementFlag.equals(ReadMasterInterface.REPLACEMENT_FLAG_START_READ) || previousReplacementFlag.equals(ReadMasterInterface.REPLACEMENT_FLAG_CTR_READ))
                                                                                && previousBillMonth.equals(scheduleBillMonth)) {
                                                                            logger.info(methodName + "Previous Read Stored is Replacement for Consumer no : " + consumerNo);
                                                                            if (previousReadMasterKW != null) {
                                                                                finalBillingDemand = previousReadMasterKW.getBillingDemand();
                                                                                finalReadMasterKW.setBillingDemand(finalBillingDemand);
                                                                            } else {
                                                                                logger.info(methodName + "Unable to fetch previous KW Read for Consumer no " + consumerNo);
                                                                            }
                                                                        } else {
                                                                            finalReadMasterKW.setBillingDemand(finalBillingDemand);
                                                                        }
                                                                        finalReadMasterKW.setReadMasterId(insertedFinalRead.getId());
                                                                        finalReadMasterKW.setMeterMD(BigDecimal.ZERO);
                                                                        finalReadMasterKW.setMultipliedMD(BigDecimal.ZERO);
                                                                        setAuditDetails(finalReadMasterKW);
                                                                        insertedFinalReadMasterKW = readMasterKWService.insert(finalReadMasterKW);
                                                                        if (insertedFinalReadMasterKW != null) {
                                                                            logger.info(methodName + "Inserted ReadMaster KW Read for Final Read");
                                                                        } else {
                                                                            logger.error(methodName + "Some error in insertion of Read Master KW");
                                                                            throw new Exception(methodName + "Some error in insertion of Read Master KW");
                                                                        }
                                                                    } else {
                                                                        logger.info(methodName + " read type configurator does not allows to add kw reading");
                                                                    }
                                                                    if (readTypeConfigurator.isPf()) {
                                                                        logger.info(methodName + " configurator says to insert pf, inserting pf");
                                                                        ReadMasterPFInterface readMasterPF = new ReadMasterPF();
                                                                        ReadMasterPFInterface insertedReadMasterPF = null;
                                                                        if ((previousReplacementFlag.equals(ReadMasterInterface.REPLACEMENT_FLAG_START_READ) || previousReplacementFlag.equals(ReadMasterInterface.REPLACEMENT_FLAG_CTR_READ))
                                                                                && previousBillMonth.equals(scheduleBillMonth)) {
                                                                            logger.info(methodName + "Previous Read Stored is Replacement, Comparing stored PF with given PF ");
                                                                            ReadMasterPFInterface presentReadMasterPF = readMasterPFService.getByReadMasterId(previousRead.getId());
                                                                            if (presentReadMasterPF != null) {
                                                                                logger.info(methodName + "Read Fetched for PF");
                                                                                finalBillingPF = presentReadMasterPF.getBillingPF();
                                                                                readMasterPF.setBillingPF(finalBillingPF);
                                                                            } else {
                                                                                logger.info(methodName + "No Read found for PF for Consumer No :" + consumerNo);
                                                                            }
                                                                        } else {
                                                                            readMasterPF.setBillingPF(finalBillingPF);
                                                                        }
                                                                        readMasterPF.setReadMasterId(insertedFinalRead.getId());
                                                                        readMasterPF.setMeterPF(BigDecimal.ZERO);
                                                                        setAuditDetails(readMasterPF);
                                                                        logger.info(methodName + "Inserting PF for Final Read");
                                                                        insertedReadMasterPF = readMasterPFService.insert(readMasterPF);
                                                                        if (insertedReadMasterPF != null) {
                                                                            logger.info(methodName + " Inserted read master PF for consumer : " + consumerNo);
                                                                        } else {
                                                                            logger.error(methodName + "unable to save PF Read, aborting request");
                                                                            throw new Exception("unable to save PF Read, aborting request");
                                                                        }
                                                                    } else {
                                                                        logger.info(methodName + " read type configurator does not allows to add pf reading");
                                                                    }
                                                                    startRead.setBillMonth(scheduleBillMonth);
                                                                    startRead.setGroupNo(consumerConnectionAreaInformation.getGroupNo());
                                                                    startRead.setReadingDiaryNo(consumerConnectionAreaInformation.getReadingDiaryNo());
                                                                    startRead.setConsumerNo(consumerNo);
                                                                    startRead.setMeterIdentifier(consumerMeterMapping.getMeterIdentifier());
                                                                    startRead.setReadingDate(currentRead.getReadingDate());
                                                                    startRead.setReadingType(ReadMasterInterface.READING_TYPE_NORMAL);
                                                                    startRead.setMeterStatus(ReadMasterInterface.METER_STATUS_METER_RESTART);
                                                                    startRead.setReplacementFlag(ReadMasterInterface.REPLACEMENT_FLAG_START_READ);
                                                                    startRead.setMf(mf);
                                                                    startRead.setSource(ReadMasterInterface.SOURCE_MANUAL);
                                                                    startRead.setReading(BigDecimal.ZERO);
                                                                    startRead.setDifference(BigDecimal.ZERO);
                                                                    startRead.setMf(mf);
                                                                    startRead.setConsumption(BigDecimal.ZERO);
                                                                    startRead.setAssessment(BigDecimal.ZERO);
                                                                    startRead.setPropagatedAssessment(insertedFinalRead.getTotalConsumption());
                                                                    startRead.setTotalConsumption(insertedFinalRead.getTotalConsumption());
                                                                    logger.info(methodName + "inserting Start Read for Meter Restart");
                                                                    insertedStartRead = insert(startRead);
                                                                    if (insertedStartRead != null) {
                                                                        logger.info(methodName + "inserted Start Read for Meter Restart");
                                                                        if (readTypeConfigurator.isKw()) {
                                                                            logger.info(methodName + "Configurator says to insert KW ");
                                                                            ReadMasterKWInterface startReadMasterKW = new ReadMasterKW();
                                                                            ReadMasterKWInterface insertedStartReadMasterKW = null;
                                                                            startReadMasterKW.setReadMasterId(insertedStartRead.getId());
                                                                            startReadMasterKW.setMeterMD(BigDecimal.ZERO);
                                                                            startReadMasterKW.setMultipliedMD(BigDecimal.ZERO);
                                                                            startReadMasterKW.setBillingDemand(finalBillingDemand);
                                                                            setAuditDetails(startReadMasterKW);
                                                                            logger.info(methodName + "Inserting Read Master KW for Final Read");
                                                                            insertedStartReadMasterKW = readMasterKWService.insert(startReadMasterKW);
                                                                            if (insertedStartReadMasterKW != null) {
                                                                                logger.info(methodName + "Inserted ReadMaster KW Read Final Read");
                                                                            } else {
                                                                                logger.error(methodName + "Some error in insertion of Read Master KW");
                                                                                throw new Exception(methodName + "Some error in insertion of Read Master KW");
                                                                            }
                                                                        } else {
                                                                            logger.info(methodName + " read type configurator does not allows to add kw reading");
                                                                        }
                                                                        if (readTypeConfigurator.isPf()) {
                                                                            logger.info(methodName + " configurator says to insert pf, inserting pf");
                                                                            ReadMasterPFInterface startReadMasterPF = new ReadMasterPF();
                                                                            ReadMasterPFInterface insertedStartReadMasterPF = null;

                                                                            startReadMasterPF.setReadMasterId(insertedStartRead.getId());
                                                                            startReadMasterPF.setMeterPF(BigDecimal.ZERO);
                                                                            startReadMasterPF.setBillingPF(finalBillingPF);
                                                                            setAuditDetails(startReadMasterPF);
                                                                            logger.info(methodName + "Inserting PF read for Final Read of Meter ReStart For Consumer no");
                                                                            insertedStartReadMasterPF = readMasterPFService.insert(startReadMasterPF);
                                                                            if (insertedStartReadMasterPF != null) {
                                                                                logger.info(methodName + " successfully inserted read master PF for consumer : " + consumerNo);
                                                                            } else {
                                                                                logger.error(methodName + "unable to save KW Read, aborting request");
                                                                                throw new Exception("unable to save KW Read, aborting request");
                                                                            }
                                                                        } else {
                                                                            logger.info(methodName + " read type configurator does not allows to add pf reading");
                                                                        }
                                                                        if (currentRead.getAssessment() != null) {
                                                                            assessment = currentRead.getAssessment().setScale(3, BigDecimal.ROUND_HALF_UP);
                                                                        }
                                                                        BigDecimal currentReadPropagatedAssessment = insertedStartRead.getTotalConsumption();
                                                                        consumption = currentReading.multiply(mf);
                                                                        logger.info(methodName + "Calculating Total Consumption for Current Read for Consumer No:" + consumerNo);
                                                                        BigDecimal currentReadTotalConsumption = consumption.add(assessment).add(currentReadPropagatedAssessment);
                                                                        currentRead.setMeterIdentifier(consumerMeterMapping.getMeterIdentifier());
                                                                        currentRead.setBillMonth(scheduleBillMonth);
                                                                        currentRead.setGroupNo(consumerConnectionAreaInformation.getGroupNo());
                                                                        currentRead.setReadingDiaryNo(consumerConnectionAreaInformation.getReadingDiaryNo());
                                                                        currentRead.setSource(ReadMasterInterface.SOURCE_MANUAL);
                                                                        currentRead.setReplacementFlag(ReadMasterInterface.REPLACEMENT_FLAG_NORMAL_READ);
                                                                        currentRead.setMf(mf);
                                                                        currentRead.setMeterStatus(ReadMasterInterface.METER_STATUS_METER_RESTART);
                                                                        currentRead.setConsumption(consumption);
                                                                        currentRead.setAssessment(assessment);
                                                                        currentRead.setPropagatedAssessment(currentReadPropagatedAssessment);
                                                                        currentRead.setTotalConsumption(currentReadTotalConsumption);
                                                                        logger.info(methodName + " inserting Current Read for consumer : " + consumerNo);
                                                                        insertedRead = insert(currentRead);
                                                                        if (insertedRead != null) {
                                                                            logger.info(methodName + "Inserted Current Read  for consumer : " + consumerNo);
                                                                            ReadMasterKWInterface insertedReadMasterKW = null;
                                                                            if (readTypeConfigurator.isKw()) {
                                                                                logger.info(methodName + " configurator says to insert kw, inserting kw");
                                                                                ReadMasterKWInterface readMasterKW = currentRead.getReadMasterKW();
                                                                                logger.info(methodName + " inserting read master KW for consumer : " + consumerNo);
                                                                                if (readMasterKW != null && readMasterKW.getMeterMD() != null) {
                                                                                    BigDecimal multipliedMD = readMasterKW.getMeterMD().multiply(mf).setScale(3, BigDecimal.ROUND_HALF_UP);
                                                                                    BigDecimal greaterKW = multipliedMD.max(finalBillingDemand);
                                                                                    readMasterKW.setReadMasterId(insertedRead.getId());
                                                                                    readMasterKW.setMultipliedMD(multipliedMD);
                                                                                    readMasterKW.setBillingDemand(greaterKW);
                                                                                    setAuditDetails(readMasterKW);
                                                                                    insertedReadMasterKW = readMasterKWService.insert(readMasterKW);
                                                                                    if (insertedReadMasterKW != null) {
                                                                                        logger.info(methodName + " successfully inserted read master KW for consumer : " + consumerNo);
                                                                                    } else {
                                                                                        logger.info(methodName + "unable to save KW Read, aborting request");
                                                                                        throw new Exception("unable to save KW Read, aborting request");
                                                                                    }
                                                                                } else {
                                                                                    logger.error(methodName + "Read Type configurator returned true for KW but ReadMasterKw is found null.");
                                                                                    throw new Exception("KW is required for such class of consumer");
                                                                                }
                                                                            } else {
                                                                                logger.info(methodName + " read type configurator does not allows to add kw reading");
                                                                            }
                                                                            insertedRead.setReadMasterKW(insertedReadMasterKW);
                                                                            ReadMasterPFInterface insertedReadMasterPF = null;
                                                                            if (readTypeConfigurator.isPf()) {
                                                                                logger.info(methodName + " configurator says to insert pf, inserting pf");
                                                                                ReadMasterPFInterface readMasterPF = currentRead.getReadMasterPF();
                                                                                logger.info(methodName + " inserting read master PF for consumer : " + consumerNo);
                                                                                if (readMasterPF != null && readMasterPF.getMeterPF() != null) {
                                                                                    BigDecimal pf = readMasterPF.getMeterPF().setScale(3, BigDecimal.ROUND_HALF_UP);
                                                                                    if (finalBillingPF.compareTo(BigDecimal.ZERO) != 0) {
                                                                                        BigDecimal smallerPF = finalBillingPF.min(pf);
                                                                                        readMasterPF.setBillingPF(smallerPF);
                                                                                    } else {
                                                                                        readMasterPF.setBillingPF(pf);
                                                                                    }
                                                                                    readMasterPF.setReadMasterId(insertedRead.getId());
                                                                                    readMasterPF.setMeterPF(pf);
                                                                                    setAuditDetails(readMasterPF);
                                                                                    insertedReadMasterPF = readMasterPFService.insert(readMasterPF);
                                                                                    if (insertedReadMasterPF != null) {
                                                                                        logger.info(methodName + " successfully inserted read master PF for consumer : " + consumerNo);
                                                                                    } else {
                                                                                        logger.error(methodName + "unable to save KW Read, aborting request");
                                                                                        throw new Exception("unable to save KW Read, aborting request");
                                                                                    }
                                                                                } else {
                                                                                    logger.error(methodName + "Read Type configurator returned true for PF but ReadMasterPF is found null.");
                                                                                    throw new Exception("PF is required for such class of consumer");
                                                                                }
                                                                            } else {
                                                                                logger.info(methodName + " read type configurator does not allows to add pf reading");
                                                                            }
                                                                            insertedRead.setReadMasterPF(insertedReadMasterPF);
                                                                        } else {
                                                                            logger.error(methodName + "Couldn't insert Read Master that is couldn't save reading corresponding to kwh");
                                                                            throw new Exception("Unable to insert kwh reading");
                                                                        }
                                                                    } else {
                                                                        logger.error(methodName + "Some error in insertion of Start Read of Meter Restart, aborting request");
                                                                        throw new Exception("Some error in insertion of Start Read of Meter Restart");
                                                                    }
                                                                } else {
                                                                    logger.error(methodName + "Some error in insertion of Final Read of Meter Restart, aborting request");
                                                                    throw new Exception("Some error in insertion of Final Read of Meter Restart");
                                                                }
                                                            } else {
                                                                logger.error(methodName + "MF is less than ZERO, aborting request");
                                                                throw new Exception("MF is less than ZERO, aborting request");
                                                            }
                                                        } else {
                                                            logger.error(methodName + "consumer meter mapping is null for consumer No " + consumerNo);
                                                            throw new Exception("No active meter on consumer");
                                                        }
                                                    } else {
                                                        logger.error(methodName + " consumers meter mappings are null for consumer no " + consumerNo);
                                                        throw new Exception("No active meter on consumer");
                                                    }
                                                } else {
                                                    logger.error(methodName + " read type configurator does not allows to add kwh reading,aborting request");
                                                    throw new Exception("You cannot punch kwh reading for such class of consumer");
                                                }
                                            } else {
                                                logger.error(methodName + "no read type configurator found for consumerNo: " + consumerNo + " aborting meter insert");
                                                throw new Exception("You cannot punch kwh reading for UnMetered consumer");
                                            }

                                        } else {
                                            logger.error(methodName + "Given Input date is less than Previous read Date OR greater than Current Date");
                                            throw new Exception("Given Input date is less than Previous read Date OR greater than Current Date");
                                        }
                                    } else {
                                        logger.error(methodName + "Previous Date not found in Previous Read");
                                        throw new Exception("Previous Date not found in Previous Read");
                                    }
                                } else {
                                    logger.error(methodName + "Previous Read for consumerNo is null in method.");
                                    throw new Exception("No previous reading exists for consumer ");
                                }
                            } else {
                                logger.error(methodName + "Reading Date is not between schedule StartReadingDate and EndReadingDate");
                                throw new Exception("Reading Date is not between schedule StartReadingDate and EndReadingDate");
                            }
                        } else {
                            logger.error(methodName + "Schedule Start Date Missing Or Schedule End Date Missing OR Form Date Missing");
                            throw new Exception("Schedule Start Date Missing Or Schedule End Date Missing OR Form Date Missing");
                        }
                    } else {
                        logger.error(methodName + "No schedule found in pending status with Not submitted flag for Consumer no" + consumerNo);
                        throw new Exception(methodName + "No schedule found in pending status with Not submitted flag for Consumer no");
                    }
                } else {
                    logger.error(methodName + "Consumer Connection Area Information not found for Consumer no" + consumerNo);
                }
            } else {
                logger.error(methodName + "consumerNo passed is null in method.");
                throw new Exception("Enter correct consumer number");
            }
        } else {
            logger.error(methodName + "Read master is null in method .");
            throw new Exception("Current Reading is not correct");
        }
        return insertedRead;
    }

    /**
     * Added by Vikas
     * param consumerNo
     * param finalRead
     * param startRead
     * param oldMf
     */

    @Transactional(rollbackFor = Exception.class)
    public ReadMasterInterface insertMeterReplacementRead(String consumerNo, ReadMasterInterface finalRead, ReadMasterInterface startRead, BigDecimal oldMf) throws Exception {
        String methodName = "insertMeterReplacementRead() : ";
        logger.info(methodName + "called");
        ReadMasterInterface presentRead;
        ReadMasterInterface insertedStartRead = null;
        ReadMasterKWInterface finalReadKW = null;
        ReadMasterPFInterface finalReadPF = null;
        if (consumerNo != null && finalRead != null && startRead != null) {
            presentRead = getLatestReadingByConsumerNo(consumerNo);
            long presentReadId = presentRead.getId();
            if (presentRead != null) {
                logger.info(methodName + "Successfully fetched present read Master" + presentRead);
                Date previousDate = presentRead.getReadingDate();
                Date removalDate = finalRead.getReadingDate();
                if (previousDate != null && removalDate != null) {
                    boolean readDateFlag = GlobalResources.checkDateBetweenPreviousDateAndCurrentDate(previousDate, removalDate);
                    if (readDateFlag) {
                        logger.info(methodName + "Fetching latestCompletedScheduleByConsumerNo ");
                        ScheduleInterface latestCompletedSchedule = scheduleService.getLatestCompletedScheduleByConsumerNo(consumerNo);
                        if (latestCompletedSchedule != null) {
                            logger.info(methodName + "Successfully fetched latestCompletedScheduleByConsumerNo" + latestCompletedSchedule);
                            String latestCompletedBillMonth = latestCompletedSchedule.getBillMonth();
                            logger.info(methodName + "fetching ConsumerConnection Area Information");
                            ConsumerConnectionAreaInformationInterface consumerConnectionAreaInformation = consumerConnectionAreaInformationService.getByConsumerNo(consumerNo);
                            if (consumerConnectionAreaInformation != null) {
                                logger.info(methodName + "fetching ReadTypeConfigurator by Consumer no");
                                ReadTypeConfiguratorInterface readTypeConfigurator = readTypeConfiguratorService.getByConsumerNo(consumerNo);
                                if (readTypeConfigurator != null) {
                                    logger.info(methodName + "fetched ReadTypeConfigurator by Consumer no");
                                    if (readTypeConfigurator.isKwh()) {
                                        logger.info(methodName + " configurator says to insert kwh, inserting kwh");
                                        String updatedBillMonth = GlobalResources.getNextMonth(latestCompletedBillMonth);
                                        String presentBillMonth = presentRead.getBillMonth();
                                        BigDecimal oldReading = presentRead.getReading();
                                        BigDecimal finalKWH = finalRead.getReading();
                                        int i = finalKWH.compareTo(oldReading);
                                        logger.info(methodName + "Comparing Final read with previous read Stored in ReadMaster");
                                        if (i >= 0) {
                                            logger.info(methodName + "Got difference of Final read and previous read positive or Zero ");
                                            logger.info(methodName + "creating reading data for insertion");
                                            BigDecimal difference = finalKWH.subtract(oldReading).setScale(3, BigDecimal.ROUND_HALF_UP);
                                            BigDecimal consumption = difference.multiply(oldMf).setScale(3, BigDecimal.ROUND_HALF_UP);
                                            BigDecimal assessment = finalRead.getAssessment().setScale(3, BigDecimal.ROUND_HALF_UP);
                                            String presentReplacementFlag = presentRead.getReplacementFlag();
                                            BigDecimal propagatedAssessment = BigDecimal.ZERO;
                                            if (presentReplacementFlag.equals(ReadMasterInterface.REPLACEMENT_FLAG_NORMAL_READ) && presentBillMonth.equals(updatedBillMonth)) {
                                                logger.error(methodName + " Normal read already inserted for next Billing Month");
                                                throw new Exception(" Normal read already inserted for next Billing Month");
                                            }
                                            if ((presentReplacementFlag.equals(ReadMasterInterface.REPLACEMENT_FLAG_START_READ) || presentReplacementFlag.equals(ReadMasterInterface.REPLACEMENT_FLAG_CTR_READ))
                                                    && presentRead.getBillMonth().equals(updatedBillMonth)) {
                                                logger.info(methodName + "Last Inserted Reading is a replacement reading for same month. " +
                                                        "Hence carrying forward total consumption as assessed consumption");
                                                propagatedAssessment = presentRead.getTotalConsumption();
                                            }

                                            BigDecimal totalConsumption = consumption.add(assessment).setScale(3, BigDecimal.ROUND_HALF_UP).add(propagatedAssessment).setScale(3, BigDecimal.ROUND_HALF_UP);
                                            finalRead.setAssessment(assessment);
                                            finalRead.setBillMonth(updatedBillMonth);
                                            finalRead.setDifference(difference);
                                            finalRead.setConsumption(consumption);
                                            finalRead.setPropagatedAssessment(propagatedAssessment);
                                            finalRead.setMf(oldMf);
                                            finalRead.setTotalConsumption(totalConsumption);
                                            finalRead.setSource(ReadMasterInterface.SOURCE_MANUAL);
                                            finalRead.setReplacementFlag(ReadMasterInterface.REPLACEMENT_FLAG_FINAL_READ);
                                            finalRead.setGroupNo(consumerConnectionAreaInformation.getGroupNo());
                                            finalRead.setReadingDiaryNo(consumerConnectionAreaInformation.getReadingDiaryNo());
                                            ReadMasterInterface insertedFinalRead = insert(finalRead);
                                            if (insertedFinalRead != null) {
                                                logger.info(methodName + "Successfully inserted final read in ReadMaster" + insertedFinalRead);
                                                if (readTypeConfigurator.isKw()) {
                                                    logger.info(methodName + " configurator says to insert kw, inserting kw");
                                                    finalReadKW = finalRead.getReadMasterKW();
                                                    if (finalReadKW != null) {
                                                        logger.info(methodName + "Inserting Final Read KW  " + finalReadKW);
                                                        BigDecimal md = finalReadKW.getMeterMD();
                                                        BigDecimal multipliedMD = md.multiply(oldMf).setScale(3, BigDecimal.ROUND_CEILING);
                                                        logger.info(methodName + "Present flag" + presentReplacementFlag + "month " + presentRead.getBillMonth() + "Updated bill month" + updatedBillMonth);
                                                        if ((presentReplacementFlag.equals(ReadMasterInterface.REPLACEMENT_FLAG_START_READ) || presentReplacementFlag.equals(ReadMasterInterface.REPLACEMENT_FLAG_CTR_READ))
                                                                && presentRead.getBillMonth().equals(updatedBillMonth)) {
                                                            logger.info(methodName + "Previous Read Stored is Replacement, Comparing stored KW with given KW ");
                                                            ReadMasterKWInterface presentReadMasterKW = readMasterKWService.getByReadMasterId(presentReadId);
                                                            if (presentReadMasterKW != null) {
                                                                BigDecimal oldBilledKW = presentReadMasterKW.getBillingDemand();
                                                                BigDecimal greaterKW = multipliedMD.max(oldBilledKW);
                                                                finalReadKW.setBillingDemand(greaterKW);
                                                            } else {
                                                                finalReadKW.setBillingDemand(multipliedMD);
                                                            }
                                                        } else {
                                                            finalReadKW.setBillingDemand(multipliedMD);
                                                        }
                                                        finalReadKW.setReadMasterId(insertedFinalRead.getId());
                                                        finalReadKW.setMeterMD(md);
                                                        finalReadKW.setMultipliedMD(multipliedMD);
                                                        setAuditDetails(finalReadKW);
                                                        ReadMasterKWInterface insertedReadMasterKW = readMasterKWService.insert(finalReadKW);
                                                        if (insertedReadMasterKW != null) {
                                                            logger.info(methodName + " successfully inserted read master KW for consumer : " + consumerNo);
                                                        } else {
                                                            logger.error(methodName + "Some error in insertion of KW in ReadMasterKW");
                                                            throw new Exception("Some error in insertion of KW in ReadMasterKW");
                                                        }
                                                    } else {
                                                        logger.error(methodName + "Final Read KW not found in finalRead but required as per configurator");
                                                        throw new Exception("Final Read KW not found in finalRead but required as per configurator");
                                                    }
                                                }
                                                if (readTypeConfigurator.isPf() && insertedFinalRead != null) {
                                                    finalReadPF = finalRead.getReadMasterPF();
                                                    if (finalReadPF != null) {
                                                        BigDecimal pf = finalReadPF.getMeterPF().setScale(3, BigDecimal.ROUND_CEILING);
                                                        if ((presentReplacementFlag.equals(ReadMasterInterface.REPLACEMENT_FLAG_START_READ) || presentReplacementFlag.equals(ReadMasterInterface.REPLACEMENT_FLAG_CTR_READ))
                                                                && presentRead.getBillMonth().equals(updatedBillMonth)) {
                                                            logger.info(methodName + "Previous Read Stored is Replacement, Comparing stored PF with given PF ");
                                                            ReadMasterPFInterface presentReadMasterPF = readMasterPFService.getByReadMasterId(presentReadId);
                                                            if (presentReadMasterPF != null) {
                                                                logger.info(methodName + "Read Fetched for PF");
                                                                BigDecimal oldBilledPF = presentReadMasterPF.getBillingPF();
                                                                BigDecimal smallerPF = oldBilledPF.min(pf);
                                                                finalReadPF.setBillingPF(smallerPF);
                                                            } else {
                                                                logger.info(methodName + "No Read found for PF");
                                                                finalReadPF.setBillingPF(pf);
                                                            }

                                                        } else {
                                                            finalReadPF.setMeterPF(pf);
                                                            finalReadPF.setBillingPF(pf);
                                                        }
                                                        finalReadPF.setReadMasterId(insertedFinalRead.getId());
                                                        setAuditDetails(finalReadPF);
                                                        ReadMasterPFInterface insertedReadMasterPF = readMasterPFService.insert(finalReadPF);
                                                        if (insertedReadMasterPF != null) {
                                                            logger.info(methodName + "successfully inserted read master KW for consumer : " + consumerNo);
                                                        } else {
                                                            logger.error(methodName + "Unable to insert FinalReadPF");
                                                            throw new Exception("Unable to insert FinalReadPF");
                                                        }
                                                    } else {
                                                        logger.error(methodName + "Final Read PF not found in finalRead but required as per configurator");
                                                        throw new Exception("Final Read PF not found in finalRead but required as per configurator");
                                                    }
                                                }
                                                if (insertedFinalRead != null && startRead != null) {
                                                    BigDecimal startReadAssessment = startRead.getAssessment();
                                                    BigDecimal startPropagatedAssessment = insertedFinalRead.getTotalConsumption();
                                                    BigDecimal startTotalConsumption = startReadAssessment.add(startPropagatedAssessment);
                                                    startRead.setBillMonth(updatedBillMonth);
                                                    startRead.setSource(ReadMasterInterface.SOURCE_MANUAL);
                                                    startRead.setDifference(BigDecimal.ZERO);
                                                    startRead.setConsumption(BigDecimal.ZERO);
                                                    startRead.setAssessment(startReadAssessment);
                                                    startRead.setReplacementFlag(ReadMasterInterface.REPLACEMENT_FLAG_START_READ);
                                                    startRead.setPropagatedAssessment(startPropagatedAssessment);
                                                    startRead.setTotalConsumption(startTotalConsumption);
                                                    startRead.setGroupNo(consumerConnectionAreaInformation.getGroupNo());
                                                    startRead.setReadingDiaryNo(consumerConnectionAreaInformation.getReadingDiaryNo());
                                                    insertedStartRead = insert(startRead);
                                                    if (insertedStartRead != null) {
                                                        logger.info(methodName + "Successfully inserted Start read in ReadMaster" + insertedStartRead);
                                                        if (readTypeConfigurator.isKw()) {
                                                            logger.info(methodName + " configurator says to insert Start kw, inserting kw");
                                                            ReadMasterKW readMasterKW = new ReadMasterKW();
                                                            if (finalReadKW != null) {
                                                                readMasterKW.setBillingDemand(finalReadKW.getBillingDemand());
                                                            } else {
                                                                readMasterKW.setBillingDemand(BigDecimal.ZERO);
                                                            }
                                                            readMasterKW.setReadMasterId(insertedStartRead.getId());
                                                            readMasterKW.setMeterMD(BigDecimal.ZERO);
                                                            readMasterKW.setMultipliedMD(BigDecimal.ZERO);
                                                            setAuditDetails(readMasterKW);
                                                            ReadMasterKWInterface insertedReadMasterKW = readMasterKWService.insert(readMasterKW);
                                                            if (insertedReadMasterKW != null) {
                                                                logger.info(methodName + " successfully inserted read master KW for consumer : " + consumerNo);
                                                            } else {
                                                                logger.error(methodName + "Some error in insertion of KW in ReadMasterKW");
                                                                throw new Exception("Some error in insertion of KW in ReadMasterKW");
                                                            }
                                                        } else {
                                                            logger.error(methodName + "read type configurator does not allows to add KW reading");
                                                        }
                                                        if (readTypeConfigurator.isPf() && insertedStartRead != null) {
                                                            logger.info(methodName + " configurator says to insert Start pf, inserting pf" + startRead);
                                                            ReadMasterPF readMasterPF = new ReadMasterPF();

                                                            if (finalReadPF != null) {
                                                                readMasterPF.setBillingPF(finalReadPF.getBillingPF());
                                                            } else {
                                                                readMasterPF.setBillingPF(BigDecimal.ZERO);
                                                            }
                                                            readMasterPF.setReadMasterId(insertedStartRead.getId());
                                                            readMasterPF.setMeterPF(BigDecimal.ZERO);
                                                            setAuditDetails(readMasterPF);
                                                            ReadMasterPFInterface insertedReadMasterPF = readMasterPFService.insert(readMasterPF);

                                                            if (insertedReadMasterPF != null) {
                                                                logger.info(methodName + "successfully inserted read master PF for consumer : " + consumerNo);
                                                            } else {
                                                                logger.error(methodName + "Some error in insertion of PF in ReadMasterPF");
                                                                throw new Exception("Some error in insertion of PF in ReadMasterPF");
                                                            }
                                                        } else {
                                                            logger.error(methodName + "read type configurator does not allows to add PF reading");
                                                        }
                                                    } else {
                                                        logger.error(methodName + "Unable to insert Start read in Read Master");
                                                        throw new Exception("Unable to insert Start read in Read Master");
                                                    }
                                                } else {
                                                    logger.error(methodName + "Inserted Final Read is null. OR Start read given is null Aborting Start Read insertion");
                                                    throw new Exception("Inserted Final Read is null. OR Start read given is null Aborting Start Read insertion");
                                                }
                                            } else {
                                                logger.error(methodName + "unable to insert Final read in Read Master");
                                                throw new Exception("unable to insert Final read in Read Master");
                                            }
                                        } else {
                                            logger.error(methodName + " Final read kwh given is less than Previous read kwh");
                                            throw new Exception(" Final read kwh given is less than Previous read kwh");
                                        }
                                    } else {
                                        logger.error(methodName + "read type configurator does not allows to add kwh reading,aborting request");
                                        throw new Exception("read type configurator does not allows to add kwh reading,aborting request");
                                    }

                                } else {
                                    logger.error(methodName + "read type configurator not found or read type configurator does not allows to add reading,aborting request");
                                    throw new Exception("read type configurator not found or read type configurator does not allows to add reading,aborting request");
                                }
                            } else {
                                logger.error(methodName + "Consumer Connection Area information not found" + consumerNo);
                                throw new Exception("Consumer Connection Area information not found");
                            }
                        } else {
                            logger.error(methodName + "No latestCompletedScheduleByConsumerNo found for consumer no " + consumerNo);
                            throw new Exception("No latestCompletedScheduleByConsumerNo found for consumer no");
                        }
                    } else {
                        logger.error(methodName + "Removal Date is less than Previous Read Date Or Removal Date is greater than Current Date ");
                        throw new Exception("Removal Date is less than Previous Read Date Or Removal Date is greater than Current Date ");
                    }

                } else {
                    logger.error(methodName + "previous Date in Previous Read is missing OR removal Date in Final read is missing");
                    throw new Exception("previous Date in Previous Read is missing OR removal Date in Final read is missing");
                }
            } else {
                logger.error(methodName + "No Present Read found for consumer no " + consumerNo);
                throw new Exception("No Present Read found for consumer no");
            }
        } else {
            logger.error(methodName + "Input parameters are null");
            throw new Exception("Input parameters are null");
        }
        return insertedStartRead;
    }

    /**
     * Added by vikas
     *
     * @param consumerNo
     * @param finalRead
     * @param oldMf
     * @return
     * @throws Exception
     */
    public ReadMasterInterface insertCTRReplacementRead(String consumerNo, ReadMasterInterface finalRead, BigDecimal oldMf) throws Exception {
        String methodName = "insertCTRReplacementRead() :";
        logger.info(methodName + "Got request to insert FR read in ReadMasterService");
        ReadMasterInterface presentRead;
        ReadMasterInterface insertedFinalRead = null;
        if (consumerNo != null && finalRead != null && oldMf != null) {
            presentRead = getLatestReadingByConsumerNo(consumerNo);
            if (presentRead != null) {
                Date previousReadDate = presentRead.getReadingDate();
                Date removalReadDate = finalRead.getReadingDate();
                if (previousReadDate != null && removalReadDate != null) {
                    boolean readDateFlag = GlobalResources.checkDateBetweenPreviousDateAndCurrentDate(previousReadDate, removalReadDate);
                    if (readDateFlag) {
                        logger.info(methodName + "Successfully fetched present read Master");
                        ScheduleInterface latestCompletedSchedule = scheduleService.getLatestCompletedScheduleByConsumerNo(consumerNo);
                        if (latestCompletedSchedule != null) {
                            logger.info(methodName + "Successfully fetched latestCompletedScheduleByConsumerNo");
                            String latestCompletedScheduleBillMonth = latestCompletedSchedule.getBillMonth();
                            logger.info(methodName + "fetching ConsumerConnection Area Information");
                            ConsumerConnectionAreaInformationInterface consumerConnectionAreaInformation = consumerConnectionAreaInformationService.getByConsumerNo(consumerNo);
                            if (consumerConnectionAreaInformation != null) {
                                ReadTypeConfiguratorInterface readTypeConfigurator = readTypeConfiguratorService.getByConsumerNo(consumerNo);
                                if (readTypeConfigurator.isKwh()) {
                                    String updatedBillMonth = GlobalResources.getNextMonth(latestCompletedScheduleBillMonth);
                                    BigDecimal oldReading = presentRead.getReading();
                                    String presentReplacementFlag = presentRead.getReplacementFlag();
                                    BigDecimal propagatedAssessment = BigDecimal.ZERO;
                                    String presentBillMonth = presentRead.getBillMonth();
                                    if (presentReplacementFlag.equals(ReadMasterInterface.REPLACEMENT_FLAG_NORMAL_READ) && presentBillMonth.equals(updatedBillMonth)) {
                                        logger.error(methodName + " Normal read already inserted for next Billing Month");
                                        throw new Exception(" Normal read already inserted for next Billing Month");
                                    }
                                    if ((presentReplacementFlag.equals(ReadMasterInterface.REPLACEMENT_FLAG_START_READ) || presentReplacementFlag.equals(ReadMasterInterface.REPLACEMENT_FLAG_CTR_READ))
                                            && presentRead.getBillMonth().equals(updatedBillMonth)) {
                                        logger.info(methodName + "Last Inserted Reading is a replacement reading for same month. " +
                                                "Hence carrying forward total consumption as assessed consumption");
                                        propagatedAssessment = presentRead.getTotalConsumption();
                                    }
                                    BigDecimal finalReading = finalRead.getReading();
                                    int i = finalReading.compareTo(oldReading);
                                    logger.info(methodName + "Comparing Final read with previous read Stored in ReadMaster");
                                    if (i >= 0) {
                                        logger.info(methodName + "Got difference of Final read and previous read positive or Zero ");
                                        BigDecimal difference = finalReading.subtract(oldReading).setScale(3, BigDecimal.ROUND_HALF_UP);
                                        BigDecimal consumption = difference.multiply(oldMf).setScale(3, BigDecimal.ROUND_HALF_UP);
                                        BigDecimal assessment = finalRead.getAssessment().setScale(3, BigDecimal.ROUND_HALF_UP);
                                        BigDecimal totalConsumption = consumption.add(assessment).add(propagatedAssessment).setScale(3, BigDecimal.ROUND_HALF_UP);
                                        finalRead.setBillMonth(updatedBillMonth);
                                        finalRead.setDifference(difference);
                                        finalRead.setConsumption(consumption);
                                        finalRead.setMf(oldMf);
                                        finalRead.setPropagatedAssessment(propagatedAssessment);
                                        finalRead.setTotalConsumption(totalConsumption);
                                        finalRead.setSource(ReadMasterInterface.SOURCE_MANUAL);
                                        finalRead.setReplacementFlag(ReadMasterInterface.REPLACEMENT_FLAG_CTR_READ);
                                        finalRead.setGroupNo(consumerConnectionAreaInformation.getGroupNo());
                                        finalRead.setReadingDiaryNo(consumerConnectionAreaInformation.getReadingDiaryNo());
                                        insertedFinalRead = insert(finalRead);
                                        if (insertedFinalRead != null) {
                                            logger.info(methodName + "Successfully inserted final read in ReadMaster" + insertedFinalRead);
                                            if (readTypeConfigurator.isKw()) {
                                                logger.info(methodName + " configurator says to insert kw, inserting kw" + finalRead);
                                                ReadMasterKWInterface finalReadKW = finalRead.getReadMasterKW();
                                                if (finalReadKW != null) {
                                                    logger.info(methodName + " inserting KW in ReadMasterKW");
                                                    long presentReadId = presentRead.getId();
                                                    BigDecimal md = finalReadKW.getMeterMD();
                                                    BigDecimal multipliedMD = md.multiply(oldMf).setScale(3, BigDecimal.ROUND_CEILING);
                                                    if ((presentReplacementFlag.equals(ReadMasterInterface.REPLACEMENT_FLAG_START_READ) || presentReplacementFlag.equals(ReadMasterInterface.REPLACEMENT_FLAG_CTR_READ))
                                                            && presentRead.getBillMonth().equals(updatedBillMonth)) {
                                                        logger.info(methodName + "Previous Read Stored is Replacement, Comparing stored KW with given KW ");
                                                        ReadMasterKWInterface presentReadMasterKW = readMasterKWService.getByReadMasterId(presentReadId);
                                                        if (presentReadMasterKW != null) {
                                                            BigDecimal oldBilledKW = presentReadMasterKW.getBillingDemand();
                                                            BigDecimal greaterKW = multipliedMD.max(oldBilledKW);
                                                            finalReadKW.setBillingDemand(greaterKW);
                                                        } else {
                                                            finalReadKW.setBillingDemand(multipliedMD);
                                                        }
                                                    } else {
                                                        finalReadKW.setBillingDemand(multipliedMD);
                                                    }
                                                    finalReadKW.setReadMasterId(insertedFinalRead.getId());
                                                    finalReadKW.setMeterMD(md);
                                                    finalReadKW.setMultipliedMD(multipliedMD);
                                                    setAuditDetails(finalReadKW);
                                                    ReadMasterKWInterface insertedReadMasterKW = readMasterKWService.insert(finalReadKW);
                                                    if (insertedReadMasterKW != null) {
                                                        logger.info(methodName + " successfully inserted read master KW for consumer : " + consumerNo);
                                                    } else {
                                                        logger.error(methodName + "Some error in insertion of KW in ReadMasterKW");
                                                        throw new Exception("Some error in insertion of KW in ReadMasterKW");
                                                    }
                                                } else {
                                                    logger.error(methodName + "Final Read KW not found in finalRead");
                                                    throw new Exception("Final Read KW not found in finalRead");
                                                }
                                            } else {
                                                logger.error(methodName + "read type configurator does not allows to add KW reading");
                                            }
                                            if (readTypeConfigurator.isPf()) {
                                                logger.info(methodName + " configurator says to insert pf, inserting pf");
                                                ReadMasterPF readMasterPF = new ReadMasterPF();
                                                long presentReadId = presentRead.getId();
                                                ReadMasterPFInterface finalReadPF = finalRead.getReadMasterPF();
                                                BigDecimal pf = finalReadPF.getMeterPF();
                                                if ((presentReplacementFlag.equals(ReadMasterInterface.REPLACEMENT_FLAG_START_READ) || presentReplacementFlag.equals(ReadMasterInterface.REPLACEMENT_FLAG_CTR_READ))
                                                        && presentRead.getBillMonth().equals(updatedBillMonth)) {
                                                    logger.info(methodName + "Previous Read Stored is Replacement, Comparing stored PF with given PF ");
                                                    ReadMasterPFInterface presentReadMasterPF = readMasterPFService.getByReadMasterId(presentReadId);
                                                    if (presentReadMasterPF != null) {
                                                        logger.info(methodName + "Read Fetched for PF");
                                                        BigDecimal oldBilledPF = presentReadMasterPF.getBillingPF();
                                                        BigDecimal smallerPF = oldBilledPF.min(pf);
                                                        readMasterPF.setBillingPF(smallerPF);
                                                    } else {
                                                        logger.info(methodName + "No Read found for PF");
                                                        readMasterPF.setBillingPF(pf);
                                                    }

                                                } else {
                                                    readMasterPF.setMeterPF(pf);
                                                    readMasterPF.setBillingPF(pf);
                                                }
                                                readMasterPF.setReadMasterId(insertedFinalRead.getId());
                                                readMasterPF.setMeterPF(pf);
                                                readMasterPF.setBillingPF(pf);
                                                setAuditDetails(readMasterPF);
                                                ReadMasterPFInterface insertedReadMasterPF = readMasterPFService.insert(readMasterPF);
                                                if (insertedReadMasterPF != null) {
                                                    logger.info(methodName + "successfully inserted read master KW for consumer : " + consumerNo);
                                                }
                                            } else {
                                                logger.error(methodName + "Some error in insertion of PF in ReadMasterPF");
                                                throw new Exception("Some error in insertion of PF in ReadMasterPF");
                                            }
                                        } else {
                                            logger.error(methodName + "unable to insert Final read in Read Master");
                                            throw new Exception("unable to insert Final read in Read Master");
                                        }
                                    } else {
                                        logger.error(methodName + " Final read given is less than Previous read");
                                        throw new Exception("Final read given is less than Previous read");
                                    }
                                } else {
                                    logger.error(methodName + "read type configurator does not allows to add reading,aborting request");
                                    throw new Exception("read type configurator does not allows to add reading,aborting request");
                                }
                            } else {
                                logger.error(methodName + "Consumer Connection Area Information not found for Consumer No" + consumerNo);
                                throw new Exception("Consumer Connection Area Information not found for Consumer No");
                            }
                        } else {
                            logger.error(methodName + "No latestCompletedScheduleByConsumerNo found");
                            throw new Exception("No latestCompletedScheduleByConsumerNo found");
                        }
                    } else {
                        logger.error(methodName + "Final Read date is less than Previous read Date OR greater than Current date");
                        throw new Exception("Final Read date is less than Previous read Date OR greater than Current date");
                    }
                } else {
                    logger.error(methodName + "Previous Read Date in Previous Read is missing OR Final Read Date in Final Read is Missing");
                    throw new Exception("Previous Read Date in Previous Read is missing OR Final Read Date in Final Read is Missing");
                }
            } else {
                logger.error(methodName + "for given Consumer no ReadMaster exist");
                throw new Exception("for given Consumer no ReadMaster exist");
            }
        } else {
            logger.error(methodName + "inputs parameters are null");
            throw new Exception("inputs parameters are null");
        }
        return insertedFinalRead;
    }


    /**
     * Added By: Preetesh Date 11 July 2017
     * This Method with return the editable readings
     * <p>
     * param consumerNo
     * return
     */
    public List<ReadMasterInterface> getReadingsByConsumerNoForReadingsEditing(String consumerNo) throws Exception {

        String methodName = " getReadingsByConsumerNoForReadingsEditing(): ";
        logger.info(methodName + " service method called for fetching Readings for consumer " + consumerNo);
        List<ReadMasterInterface> readings = null;
        if (consumerNo != null) {
            consumerNo = consumerNo.trim();
            String groupNo = consumerConnectionAreaInformationService.getGroupOfConsumerByConsumerNo(consumerNo);
            String currentBillMonth = null;
            if (groupNo != null) {
                String lastParticipatedBillMonth = scheduleService.getLastParticipatedBillMonthByGroupNo(groupNo);
                logger.info(methodName + " last participated bill month for consumer's group " + groupNo + " is " + lastParticipatedBillMonth);
                if (lastParticipatedBillMonth != null) {
                    currentBillMonth = GlobalResources.getNextMonth(lastParticipatedBillMonth);
                    logger.info(methodName + " current bill month for consumer's group " + groupNo + " is " + currentBillMonth);

                    if (currentBillMonth != null) {
                        List<ReadMasterInterface> readingsInCurrentBillMonth = getByConsumerNoAndBillMonthOrdered(consumerNo, currentBillMonth);
                        if (readingsInCurrentBillMonth != null && readingsInCurrentBillMonth.size() > 0) {
                            ReadMasterInterface lastBillMonthRead = getLatestReadingByConsumerNoAndBillMonthAndReplacementFlag(consumerNo, lastParticipatedBillMonth, ReadMasterInterface.REPLACEMENT_FLAG_NORMAL_READ);
                            if (lastBillMonthRead != null) {
                                logger.info(methodName + " Successfully retrieved latest reading by consumer number" + consumerNo + " reading as " + lastBillMonthRead);
                                readings = new ArrayList<>();
                                readings.add(lastBillMonthRead);
                                readings.addAll(readingsInCurrentBillMonth);
                                logger.info(methodName + " service method added Readings for consumer " + readings);
                            } else {
                                /**
                                 * checking for nsc
                                 */
                                ReadMasterInterface nscRead = getLatestReadingByConsumerNoAndBillMonthAndReplacementFlag(consumerNo, currentBillMonth, ReadMasterInterface.REPLACEMENT_FLAG_NEW_CONNECTION);
                                if (nscRead != null) {
                                    List<ReadMasterInterface> newReadings = getByConsumerNoAndBillMonthOrdered(consumerNo, currentBillMonth);
                                    readings = new ArrayList<>();
                                    readings.addAll(newReadings);

                                }

                                logger.error(methodName + "last bill month Read not found");
                            }
                        } else {
                            logger.error(methodName + "no reading found for editing " + groupNo);
                            throw new Exception("No reading found for editing");
                        }
                    }
                } else {
                    logger.error(methodName + "last bill month not found");
                    throw new Exception("last bill month not found");
                }
            } else {
                logger.error(methodName + "groupNo not found");
                throw new Exception("GroupNo not found");
            }
        } else {
            logger.error(methodName + "consumerNo passed is null in method.");
            throw new Exception("ConsumerNo passed is null in method.");
        }
        return readings;
    }

    /**
     * Added By: Preetesh Date 11 July 2017
     * This Method with return readings in ascending order i.e. latest reading will come at last.
     * <p>
     * param consumerNo
     * return
     */

    List<ReadMasterInterface> getByConsumerNoAndBillMonthOrdered(String consumerNo, String billMonth) {
        String methodName = " getByConsumerNoAndBillMonthOrdered(): ";
        logger.info(methodName + " service method called for fetching Readings for consumer " + consumerNo);
        List<ReadMasterInterface> readings = null;
        if (consumerNo != null && billMonth != null) {
            ReadTypeConfiguratorInterface readTypeConfigurator = readTypeConfiguratorService.getByConsumerNo(consumerNo);
            if (readTypeConfigurator != null) {
                logger.info(methodName + " Successfully retrieved readTypeConfigurator for consumer number" + consumerNo + " read type configurator " + readTypeConfigurator);
                readings = readMasterDAO.getByConsumerNoAndBillMonthOrderByIdAsc(consumerNo, billMonth);
                logger.info(methodName + "readings retrieved for consumer" + consumerNo + " for bill month " + billMonth + " as " + readings);
                if (readings != null && readings.size() > 0) {
                    readings.forEach(readMaster -> {
                        if (readMaster != null) {
                            long readingId = readMaster.getId();
                            logger.info(methodName + "fetching readTypeConfigurator for consumer" + consumerNo);
                            if (readTypeConfigurator.isKw()) {
                                logger.info(methodName + " Retrieving KW reading for consumer number" + consumerNo);
                                ReadMasterKWInterface readMasterKW = readMasterKWService.getByReadMasterId(readingId);
                                if (readMasterKW != null) {
                                    logger.info(methodName + " successfully Retrieved KW reading for consumer number" + consumerNo);
                                    readMaster.setReadMasterKW(readMasterKW);
                                } else {
                                    logger.error(methodName + " ReadMasterKW received is null" + consumerNo);
                                }
                            }
                            if (readTypeConfigurator.isPf()) {
                                logger.info(methodName + " Retrieving PF reading for consumer number" + consumerNo);
                                ReadMasterPFInterface readMasterPF = readMasterPFService.getByReadMasterId(readingId);
                                if (readMasterPF != null) {
                                    logger.info(methodName + " successfully Retrieved PF reading for consumer number" + consumerNo);
                                    readMaster.setReadMasterPF(readMasterPF);
                                } else {
                                    logger.error(methodName + " ReadMasterPF received is null" + consumerNo);
                                }
                            }
                        } else {
                            logger.error(methodName + " null reading retrieved for consumer number " + consumerNo);
                        }
                    });
                } else {
                    logger.error(methodName + " null reading retrieved for consumer number" + consumerNo);
                }
            } else {
                logger.error(methodName + " no ReadTypeConfigurator found for consumer number " + consumerNo);
            }
        } else {
            logger.error(methodName + "consumerNo or Bill Month passed is null in method.");
        }
        return readings;
    }

    public List<ReadMasterInterface> getByConsumerNoForBillCorrection(String consumerNo) throws Exception {
        String methodName = " getByConsumerNoForBillCorrection() : ";
        logger.info(methodName + "called for consumer no " + consumerNo);
        List<ReadMasterInterface> readings = null;
        if (consumerNo != null) {
            consumerNo = consumerNo.trim();
            String latestBilledMonth = null;
            String previousBillMonth = null;
            String groupNo = consumerConnectionAreaInformationService.getGroupOfConsumerByConsumerNo(consumerNo);
            if (groupNo != null) {
                latestBilledMonth = scheduleService.getLastParticipatedBillMonthByGroupNo(groupNo);
                logger.info(methodName + " latest participated bill month for consumer's group " + groupNo + " is " + latestBilledMonth);
                if (latestBilledMonth != null) {
                    try {
                        previousBillMonth = GlobalResources.getPreviousMonth(latestBilledMonth);
                        logger.info(methodName + " previous bill month for consumer's group " + groupNo + " is " + previousBillMonth);
                    } catch (Exception e) {
                        e.getStackTrace();
                    }
                    if (previousBillMonth != null) {
                        List<ReadMasterInterface> readingsInLatestBillMonth = getByConsumerNoAndBillMonthOrdered(consumerNo, latestBilledMonth);
                        if (readingsInLatestBillMonth != null && readingsInLatestBillMonth.size() > 0) {
                            ReadMasterInterface previousBillMonthRead = getLatestReadingByConsumerNoAndBillMonthAndReplacementFlag(consumerNo, previousBillMonth, ReadMasterInterface.REPLACEMENT_FLAG_NORMAL_READ);
                            if (previousBillMonthRead != null) {
                                logger.info(methodName + " Successfully retrieved previous month reading by consumer number" + consumerNo + " reading as " + previousBillMonthRead);
                                readings = new ArrayList<>();
                                readings.add(previousBillMonthRead);
                                readings.addAll(readingsInLatestBillMonth);
                                logger.info(methodName + " service method added Readings for consumer for Bill correction" + readings);
                            } else {
                                logger.error(methodName + "last bill month Read not found");
                            }
                        } else {
                            logger.error(methodName + "no reading found for editing " + groupNo);
                        }
                    }
                } else {
                    logger.error(methodName + "last bill month not found");
                }
            } else {
                logger.error(methodName + "groupNo not found");
            }
        } else {
            logger.error(methodName + "consumerNo passed is null in method.");
        }
        return readings;
    }

    /**
     * Added By: Preetesh Date 17 July 2017
     *
     * @param consumerNo
     * @param readingsToUpdate
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public List<ReadMasterInterface> updateReadingsByConsumerNo(String consumerNo, List<? extends ReadMasterInterface> readingsToUpdate) throws Exception {
        String methodName = "updateReadingsByConsumerNo() : ";
        logger.info(methodName + "called");
        List<ReadMasterInterface> updatedReadings = null;
        if (readingsToUpdate != null) {
            logger.info(methodName + " service method called for updating Readings for consumer " + consumerNo);
            if (consumerNo != null && readingsToUpdate != null) {
                consumerNo = consumerNo.trim();
                logger.info(methodName + " Fetching previous reading for consumer : " + consumerNo);
                List<ReadMasterInterface> existingReadings = getReadingsByConsumerNoForReadingsEditing(consumerNo);
                if (existingReadings != null) {
                    logger.info(methodName + "existing readingsToUpdate is not null proceeding" + existingReadings);
                    boolean isValidReadings = validateReadings(readingsToUpdate, existingReadings);
                    if (isValidReadings) {
                        ReadTypeConfiguratorInterface readTypeConfigurator = readTypeConfiguratorService.getByConsumerNo(consumerNo);
                        if (readTypeConfigurator != null) {
                            updatedReadings = new ArrayList<ReadMasterInterface>();
                            for (ReadMasterInterface readingToUpdate : readingsToUpdate) {
                                ReadMasterInterface updatedReading = null;
                                if (readTypeConfigurator.isKwh()) {
                                    logger.info(methodName + "updating ReadMasterKWH");
                                    updatedReading = update(readingToUpdate);
                                }
                                if (readTypeConfigurator.isKw()) {
                                    logger.info(methodName + "readTypeConfigurator says to update KW");
                                    if (updatedReading != null) {
                                        logger.info(methodName + "updating ReadMasterKW");
                                        ReadMasterKWInterface readMasterKWToUpdate = readingToUpdate.getReadMasterKW();
                                        setUpdatedDetails(readMasterKWToUpdate);
                                        ReadMasterKWInterface updatedReadMasterKW = readMasterKWService.insert(readMasterKWToUpdate);
                                        if (updatedReadMasterKW != null) {
                                            updatedReading.setReadMasterKW(updatedReadMasterKW);
                                        } else {
                                            logger.error(methodName + "unable to update ReadMasterKW for ReadMasterKWH. Aborting Transaction");
                                            throw new Exception("unable to update ReadMasterKW for ReadMasterKWH. Aborting Transaction");
                                        }
                                    }
                                }
                                if (readTypeConfigurator.isPf()) {
                                    logger.info(methodName + "readTypeConfigurator says to update PF");
                                    if (updatedReading != null) {
                                        logger.info(methodName + "updating ReadMasterPF");
                                        ReadMasterPFInterface readMasterPFToUpdate = readingToUpdate.getReadMasterPF();
                                        setUpdatedDetails(readMasterPFToUpdate);
                                        ReadMasterPFInterface updatedReadMasterPF = readMasterPFService.insert(readMasterPFToUpdate);
                                        if (updatedReadMasterPF != null) {
                                            readingToUpdate.setReadMasterPF(updatedReadMasterPF);
                                        } else {
                                            logger.error(methodName + "unable to update ReadMasterPF for ReadMasterKWH. Aborting Transaction");
                                            throw new Exception("unable to update ReadMasterPF for ReadMasterKWH. Aborting Transaction");
                                        }
                                    }
                                }
                                if (updatedReading != null) {
                                    updatedReadings.add(updatedReading);
                                } else {
                                    logger.error(methodName + "Unable to update ReadMasterKW throwing exception to abort transaction");
                                    throw new Exception("Unable to update ReadMasterKW throwing exception to abort transaction");
                                }
                            }
                        } else {
                            logger.error(methodName + "read type configurator not found");
                            throw new Exception("read type configurator not found");
                        }

                    } else {
                        logger.error(methodName + "some error in validating readingsToUpdate");
                        throw new Exception("some error in validating readingsToUpdate");
                    }
                } else {
                    logger.error(methodName + " no reading available for editing" + existingReadings);
                    throw new Exception(" no reading available for editing");
                }
            } else {
                logger.error(methodName + "consumerNo or readingsToUpdate passed is null in method.");
                throw new Exception("Enter correct consumer number");
            }
        } else {
            logger.error(methodName + "readingsToUpdate passed are null in method.");
            throw new Exception("readingsToUpdate passed are null ");
        }
        return updatedReadings;
    }


    private boolean validateReadings(List<? extends ReadMasterInterface> updatedReadings, List<? extends ReadMasterInterface> existingReadings) {
        String methodName = " validateReadings() ";
        boolean valid = true;
        if (updatedReadings != null && existingReadings != null) {
            int noOfReadingsEditable = existingReadings.size() - 1;
            if (noOfReadingsEditable == updatedReadings.size()) {
                for (int updatedReadingsCounter = 0, existingReadingsCounter = 1; updatedReadingsCounter < updatedReadings.size(); updatedReadingsCounter++, existingReadingsCounter++) {
                    ReadMasterInterface passedReadMaster = updatedReadings.get(updatedReadingsCounter);
                    ReadMasterInterface previousReadMaster = existingReadings.get(existingReadingsCounter);
                    if (passedReadMaster != null && previousReadMaster != null) {
                        if (passedReadMaster.getId() == previousReadMaster.getId()) {
                            BigDecimal difference = passedReadMaster.getDifference();
                            if (difference != null && difference.compareTo(BigDecimal.ZERO) < 0) {
                                logger.error(methodName + "difference is null or negative breaking from loop " + passedReadMaster);
                                valid = false;
                                break;
                            }
                        } else {
                            logger.error(methodName + "id not matching breaking");
                            valid = false;
                            break;
                        }
                    } else {
                        logger.error(methodName + "one of the updatedReadings is null");
                        valid = false;
                        break;
                    }
                }
            } else {
                logger.error(methodName + "no of updatedReadings passed are not matching with editable updatedReadings count");
                valid = false;
            }
        } else {
            logger.error(methodName + "incomplete data");
            valid = false;
        }
        return valid;
    }

    private void setAuditDetails(ReadMasterInterface readMaster) {
        String methodName = "setAuditDetails()  :";
        if (readMaster != null) {
            Date date = GlobalResources.getCurrentDate();
            String user = GlobalResources.getLoggedInUser();
            readMaster.setUpdatedOn(date);
            readMaster.setUpdatedBy(user);
            readMaster.setCreatedOn(date);
            readMaster.setCreatedBy(user);
        } else {
            logger.error(methodName + "given input is null");
        }
    }

    private void setAuditDetails(ReadMasterPFInterface readMasterPF) {
        String methodName = "setAuditDetails()  :";
        if (readMasterPF != null) {
            Date date = GlobalResources.getCurrentDate();
            String user = GlobalResources.getLoggedInUser();
            readMasterPF.setUpdatedOn(date);
            readMasterPF.setUpdatedBy(user);
            readMasterPF.setCreatedOn(date);
            readMasterPF.setCreatedBy(user);
        } else {
            logger.error(methodName + "given input is null");
        }
    }

    private void setAuditDetails(ReadMasterKWInterface readMasterKW) {
        String methodName = "setAuditDetails()  :";
        if (readMasterKW != null) {
            Date date = GlobalResources.getCurrentDate();
            String user = GlobalResources.getLoggedInUser();
            readMasterKW.setUpdatedOn(date);
            readMasterKW.setUpdatedBy(user);
            readMasterKW.setCreatedOn(date);
            readMasterKW.setCreatedBy(user);
        } else {
            logger.error(methodName + "given input is null");
        }
    }

    private void setUpdatedDetails(ReadMasterInterface readMaster) {
        String methodName = "setUpdatedDetails()  :";
        if (readMaster != null) {
            readMaster.setUpdatedOn(GlobalResources.getCurrentDate());
            readMaster.setUpdatedBy(GlobalResources.getLoggedInUser());
        } else {
            logger.error(methodName + "given input is null");
        }
    }

    private void setUpdatedDetails(ReadMasterPFInterface readMasterPF) {
        String methodName = "setUpdatedDetails()  :";
        if (readMasterPF != null) {
            readMasterPF.setUpdatedOn(GlobalResources.getCurrentDate());
            readMasterPF.setUpdatedBy(GlobalResources.getLoggedInUser());
        } else {
            logger.error(methodName + "given input is null");
        }
    }

    private void setUpdatedDetails(ReadMasterKWInterface readMasterKW) {
        String methodName = "setUpdatedDetails()  :";
        if (readMasterKW != null) {
            readMasterKW.setUpdatedOn(GlobalResources.getCurrentDate());
            readMasterKW.setUpdatedBy(GlobalResources.getLoggedInUser());
        } else {
            logger.error(methodName + "given input is null");
        }
    }

    /**
     * This method insert the final Read of PDC consumer
     *
     * @param consumerNo
     * @param finalRead
     * @param customRead
     * @return
     * @throws Exception
     */
    @Transactional(rollbackFor = Exception.class)
    public ReadMasterInterface insertFinalRead(String consumerNo, ReadMasterInterface finalRead, Read customRead) throws Exception {
        String methodName = " insertFinalRead : ";

        logger.info(methodName + "Started PDC check process for ConsumerNo "+consumerNo);

        //read master logic start
        String loggedInUser = GlobalResources.getLoggedInUser();
        Date currentDate = GlobalResources.getCurrentDate();
        ReadMasterInterface insertedRead = null;

        ReadTypeConfiguratorInterface readTypeConfigurator = customRead.getReadTypeConfigurator();
        ReadMasterInterface previousRead = customRead.getReadMaster();
        BigDecimal mf = customRead.getMf();
        if (previousRead == null || readTypeConfigurator == null || mf == null) {
            logger.error(methodName + "custom read has some input null");
            throw new Exception(" custom read has some input null ");
        }

        logger.info(methodName + " setting schedule bill month ");
        ScheduleInterface schedule = customRead.getSchedule();
        String scheduleBillMonth;
        if (schedule != null) {
            logger.info(methodName + " Not-Submitted, Pending Schedule present for consumer's group ");
            scheduleBillMonth = schedule.getBillMonth();
        } else {
            logger.info(methodName + " Not-Submitted, Pending Schedule not present for consumer's group ");
            schedule = scheduleService.getLatestCompletedScheduleByConsumerNo(consumerNo);
            if (schedule == null) {
                logger.error(methodName + "completed schedule not found");
                throw new Exception("completed schedule not found");
            }
            String lastScheduleBillMonth = schedule.getBillMonth();
            scheduleBillMonth = GlobalResources.getNextMonth(lastScheduleBillMonth);
        }

        logger.info(methodName + "Fetching Consumer Connection Area Information");
        ConsumerConnectionAreaInformationInterface consumerConnectionAreaInformation = consumerConnectionAreaInformationService.getByConsumerNo(consumerNo);
        if (consumerConnectionAreaInformation == null || consumerConnectionAreaInformation.getGroupNo() == null) {
            logger.error(methodName + "consumerConnectionAreaInformation or group no found null");
            throw new Exception("consumerConnectionAreaInformation");
        }

        logger.info(methodName + " Fetching previous read for consumer : " + consumerNo);
        BigDecimal previousReading = previousRead.getReading();
        BigDecimal currentReading = finalRead.getReading();
        if (previousReading == null || currentReading == null) {
            logger.error(methodName + "previousReading or current reading not found in Previous Read");
            throw new Exception("previousReading or current reading not found in Previous Read");
        }
        logger.info(methodName + "previous read is not null proceeding");

        logger.info(methodName + "Comparing Previous Read And Current Read");
        int i = currentReading.compareTo(previousReading);
        if (i < 0) {
            logger.error(methodName + "Current Read is less than previous read");
            throw new Exception(methodName + "Current Read is less than previous read");
        }

        Date previousReadingDate = previousRead.getReadingDate();
        Date finalReadingDate = finalRead.getReadingDate();
        if (previousReadingDate == null || finalReadingDate == null) {
            logger.error(methodName + "previousReadingDate or currentReadingDate not found in Previous Read");
            throw new Exception("previousReadingDate or currentReadingDate not found in Previous Read");
        }
        if (finalReadingDate.before(previousReadingDate) || finalReadingDate.after(currentDate)) {
            logger.error(methodName + "Given Input date is less than Previous read Date OR greater than Current Date");
            throw new Exception("Given Input date is less than Previous read Date OR greater than Current Date");
        }
        BigDecimal differenceReadPassed = finalRead.getDifference().setScale(3, BigDecimal.ROUND_HALF_UP);
        BigDecimal difference = currentReading.subtract(previousReading).setScale(3, BigDecimal.ROUND_HALF_UP);
        logger.info(methodName + " Calculating difference for readPassed and readCalculated at server for consumer : " + consumerNo + " form difference " + differenceReadPassed +
                " server difference " + difference);

        //checking for not equal by using !
        if (!difference.equals(differenceReadPassed)) {
            logger.error(methodName + " record not updated for consumer  difference Mismatch" + consumerNo);
            throw new Exception("Mismatch in reading difference occurred at server end");
        }
        logger.info(methodName + "differences from form and server matches proceeding");
        logger.info(methodName + " Calculating difference with MF for readPassed and readStored for consumer : " + consumerNo);


        if (readTypeConfigurator.isKwh()) {
            /**
             * if read type configurator's KWH is true, consumer is metered, and meter has to be there in mapping
             */
            logger.info(methodName + " retrieving consumer meter mapping");
            List<ConsumerMeterMappingInterface> consumerMeterMappings = consumerMeterMappingService.getByConsumerNoAndMappingStatus(consumerNo, ConsumerMeterMappingInterface.STATUS_ACTIVE);
            if (consumerMeterMappings == null || consumerMeterMappings.size() == 0) {
                logger.error(methodName + " consumers meter mappings are null for consumer no " + consumerNo);
                throw new Exception("No active meter on consumer");
            }
            ConsumerMeterMappingInterface consumerMeterMapping = consumerMeterMappings.get(0);
            if (consumerMeterMapping == null) {
                logger.error(methodName + "consumer meter mapping is null for consumer No " + consumerNo);
                throw new Exception("No active meter on consumer");
            }

            logger.info(methodName + " configurator says to insert kwh, inserting kwh");
            String previousReplacementFlag = previousRead.getReplacementFlag();
            String previousBillMonth = previousRead.getBillMonth();
            BigDecimal propagatedAssessment = BigDecimal.ZERO;
            if ((previousReplacementFlag.equals(ReadMasterInterface.REPLACEMENT_FLAG_START_READ) || previousReplacementFlag.equals(ReadMasterInterface.REPLACEMENT_FLAG_CTR_READ))
                    && previousBillMonth.equals(scheduleBillMonth)) {
                logger.info(methodName + "Last Inserted Reading is a replacement reading for same month. " +
                        "Hence carrying forward total consumption as assessed consumption");
                propagatedAssessment = previousRead.getTotalConsumption().setScale(3, BigDecimal.ROUND_HALF_UP);
            }

            if (mf.compareTo(BigDecimal.ZERO) < 1) {
                logger.error(methodName + "MF is less than ZERO, aborting request");
                throw new Exception("MF is less than ZERO, aborting request");
            }
            BigDecimal consumption = difference.multiply(mf);
            BigDecimal assessment = BigDecimal.ZERO;
            logger.info(methodName + "difference : " + difference + " * " + "mf : " + mf + " = " + consumption);
            if (finalRead.getAssessment() != null) {
                assessment = finalRead.getAssessment().setScale(3, BigDecimal.ROUND_HALF_UP);
            }
            finalRead.setMf(mf);
            logger.info(methodName + "Calculating total Consumption ");
            BigDecimal totalConsumption = consumption.add(assessment).add(propagatedAssessment);
            logger.info(methodName + "consumption : " + consumption + " + " + " assessment: " + assessment + " = " + totalConsumption);
            finalRead.setMeterIdentifier(consumerMeterMapping.getMeterIdentifier());
            finalRead.setBillMonth(scheduleBillMonth);
            finalRead.setGroupNo(consumerConnectionAreaInformation.getGroupNo());
            finalRead.setReadingDiaryNo(consumerConnectionAreaInformation.getReadingDiaryNo());
            finalRead.setSource(ReadMasterInterface.SOURCE_MANUAL);
            finalRead.setReplacementFlag(ReadMasterInterface.REPLACEMENT_FLAG_FINAL_READ);
            finalRead.setMeterStatus(ReadMasterInterface.METER_STATUS_WORKING);
            finalRead.setConsumption(consumption);
            finalRead.setAssessment(assessment);
            finalRead.setPropagatedAssessment(propagatedAssessment);
            finalRead.setTotalConsumption(totalConsumption);
            finalRead.setUsedOnBill(ReadMasterInterface.NOT_USED_ON_BILL);
            logger.info(methodName + " inserting read master  for consumer : " + consumerNo);
            logger.info("Read to insert Before " + finalRead);

            insertedRead = insert(finalRead);
            if (insertedRead == null) {
                logger.error(methodName + "Couldn't insert Read Master that is couldnt save reading corresponding to kwh");
                throw new Exception("Unable to insert kwh reading");
            }
            logger.info(methodName + " successfully inserted read master  for consumer : " + consumerNo);
            ReadMasterKWInterface insertedReadMasterKW = null;
            if (readTypeConfigurator.isKw()) {
                logger.info(methodName + " configurator says to insert kw, inserting kw");
                ReadMasterKWInterface readMasterKW = finalRead.getReadMasterKW();
                logger.info(methodName + " inserting read master KW for consumer : " + consumerNo);
                if (readMasterKW == null || readMasterKW.getMeterMD() == null) {
                    logger.error(methodName + "Read Type configurator returned true for KW but ReadMasterKw is found null.");
                    throw new Exception("KW is required for such class of consumer");
                }
                BigDecimal multipliedMD = readMasterKW.getMeterMD().multiply(mf).setScale(3, BigDecimal.ROUND_HALF_UP);
                readMasterKW.setReadMasterId(insertedRead.getId());
                readMasterKW.setMultipliedMD(multipliedMD);
                setAuditDetails(readMasterKW);

                if ((previousReplacementFlag.equals(ReadMasterInterface.REPLACEMENT_FLAG_START_READ) || previousReplacementFlag.equals(ReadMasterInterface.REPLACEMENT_FLAG_CTR_READ))
                        && previousBillMonth.equals(scheduleBillMonth)) {
                    logger.info(methodName + "Previous Read Stored is Replacement, Comparing stored KW with given KW ");
                    ReadMasterKWInterface previousReadMasterKW = readMasterKWService.getByReadMasterId(previousRead.getId());
                    if (previousReadMasterKW != null) {
                        BigDecimal oldBilledKW = previousReadMasterKW.getBillingDemand();
                        BigDecimal greaterKW = multipliedMD.max(oldBilledKW);
                        readMasterKW.setBillingDemand(greaterKW);
                    } else {
                        readMasterKW.setBillingDemand(multipliedMD);
                    }
                } else {
                    readMasterKW.setBillingDemand(multipliedMD);
                }
                insertedReadMasterKW = readMasterKWService.insert(readMasterKW);
                if (insertedReadMasterKW == null) {
                    logger.info(methodName + "unable to save KW Read, aborting request");
                    throw new Exception("unable to save KW Read, aborting request");
                }
                logger.info(methodName + " successfully inserted read master KW for consumer : " + consumerNo);

            } else {
                logger.info(methodName + " read type configurator does not allows to add kw reading");
            }
            insertedRead.setReadMasterKW(insertedReadMasterKW);

            //checking for PF
            ReadMasterPFInterface insertedReadMasterPF = null;
            if (readTypeConfigurator.isPf()) {
                logger.info(methodName + " configurator says to insert pf, inserting pf");
                ReadMasterPFInterface readMasterPF = finalRead.getReadMasterPF();
                logger.info(methodName + " inserting read master PF for consumer : " + consumerNo);
                if (readMasterPF == null && readMasterPF.getMeterPF() == null) {
                    logger.error(methodName + "Read Type configurator returned true for PF but ReadMasterPF is found null.");
                    throw new Exception("PF is required for such class of consumer");
                }
                BigDecimal pf = readMasterPF.getMeterPF().setScale(3, BigDecimal.ROUND_HALF_UP);
                readMasterPF.setReadMasterId(finalRead.getId());
                setAuditDetails(readMasterPF);
                if ((previousReplacementFlag.equals(ReadMasterInterface.REPLACEMENT_FLAG_START_READ) || previousReplacementFlag.equals(ReadMasterInterface.REPLACEMENT_FLAG_CTR_READ))
                        && previousBillMonth.equals(scheduleBillMonth)) {
                    logger.info(methodName + "Previous Read Stored is Replacement, Comparing stored PF with given PF ");
                    ReadMasterPFInterface presentReadMasterPF = readMasterPFService.getByReadMasterId(previousRead.getId());
                    if (presentReadMasterPF != null) {
                        logger.info(methodName + "Read Fetched for PF");
                        BigDecimal oldBilledPF = presentReadMasterPF.getBillingPF();
                        BigDecimal smallerPF = oldBilledPF.min(pf);
                        readMasterPF.setBillingPF(smallerPF);
                    } else {
                        logger.info(methodName + "No Read found for PF");
                        readMasterPF.setBillingPF(pf);
                    }
                } else {
                    readMasterPF.setMeterPF(pf);
                    readMasterPF.setBillingPF(pf);
                }
                insertedReadMasterPF = readMasterPFService.insert(readMasterPF);
                if (insertedReadMasterPF == null) {
                    logger.error(methodName + "unable to save KW Read, aborting request");
                    throw new Exception("unable to save KW Read, aborting request");
                }
                logger.info(methodName + " successfully inserted read master PF for consumer : " + consumerNo);
            } else {
                logger.info(methodName + " read type configurator does not allows to add pf reading");
            }
            insertedRead.setReadMasterPF(insertedReadMasterPF);
        } else {
            logger.error(methodName + " read type configurator does not allows to add kwh reading,aborting request");
            throw new Exception("You cannot punch kwh reading for such class of consumer");
        }
        return insertedRead;
    }

    @Transactional(rollbackFor = Exception.class)
    public ReadMasterInterface insertStartRead(ReadMasterInterface readToInsert) throws Exception {
        String methodName = " insertStartRead() ";
        ReadMasterInterface insertedRead = null;
        logger.info(methodName + "clicked");
        if (readToInsert == null) {
            logger.error(methodName + "given read to insert is null");
            throw new Exception("given read to insert is null");
        }
        String consumerNo = readToInsert.getConsumerNo();
        if (consumerNo == null) {
            logger.info(methodName + " consumer no given is null");
        }
        consumerNo = consumerNo.trim();
        logger.info(methodName + "Fetching Consumer Connection Area Information");
        ConsumerConnectionAreaInformationInterface consumerConnectionAreaInformation = consumerConnectionAreaInformationService.getByConsumerNo(consumerNo);
        if (consumerConnectionAreaInformation == null || consumerConnectionAreaInformation.getGroupNo() == null) {
            logger.error(methodName + " unable to fetch consumer connection area information for consumer no :" + consumerNo);
            throw new Exception("unable to fetch consumer connection area information for consumer");
        }
        logger.info(methodName + "fetched consumer connection area information");
        logger.info(methodName + "fetching read type configurator for consumer no :" + consumerNo);
        ReadTypeConfiguratorInterface readTypeConfigurator = readTypeConfiguratorService.getByConsumerNo(consumerNo);
        if (readTypeConfigurator == null) {
            logger.error(methodName + "unable to fetch read type configurator");
            throw new Exception("unable to fetch read type configurator");
        }
        logger.info(methodName + "fetched read type configurator");
        if (!readTypeConfigurator.isKwh()) {
            logger.info(methodName + "read type configurator doesn't allow to add KWH read");
            throw new Exception("read type configurator doesn't allow to add KWH read");
        }
        logger.info(methodName + " configurator says to insert kwh, inserting kwh");
        BigDecimal mf = consumerMeterMappingService.getMFByConsumerNoForActiveMeter(consumerNo);
        logger.info(methodName + "selected mf is :" + mf);
        int mfCompared = mf.compareTo(BigDecimal.ZERO);
        if (mfCompared <= 0) {
            logger.error(methodName + "fetched MF is zero");
            throw new Exception("fetched MF is zero");
        }
        logger.info(methodName + "fetching schedule ");
        ScheduleInterface schedule = scheduleService.getLatestCompletedScheduleByConsumerNo(consumerNo);
        if (schedule == null) {
            logger.error(methodName + "unable to fetch schedule for consumer no" + consumerNo);
            throw new Exception("unable to fetch schedule");
        }
        String latestCompletedBillMonth = schedule.getBillMonth();
        String updatedBillMonth = GlobalResources.getNextMonth(latestCompletedBillMonth);
        logger.info(methodName + "setting read Master variables ");

        readToInsert.setBillMonth(updatedBillMonth);
        readToInsert.setGroupNo(consumerConnectionAreaInformation.getGroupNo());
        readToInsert.setReadingDiaryNo(consumerConnectionAreaInformation.getReadingDiaryNo());
        readToInsert.setSource(ReadMasterInterface.SOURCE_MANUAL);
        readToInsert.setReplacementFlag(ReadMasterInterface.REPLACEMENT_FLAG_START_READ);
        readToInsert.setMf(mf);
        readToInsert.setMeterStatus(ReadMasterInterface.METER_STATUS_WORKING);
        readToInsert.setConsumption(BigDecimal.ZERO);
        readToInsert.setAssessment(BigDecimal.ZERO);
        readToInsert.setPropagatedAssessment(BigDecimal.ZERO);
        readToInsert.setTotalConsumption(BigDecimal.ZERO);
        setAuditDetails(readToInsert);
        logger.info(methodName + " inserting read master  for consumer : " + consumerNo);
        insertedRead = insert(readToInsert);
        if (insertedRead == null) {
            logger.error(methodName + "some error in insertion of read master");
            throw new Exception("some error in insertion of read master");
        }
        logger.info(methodName + " inserted read master  for consumer : " + consumerNo);
        ReadMasterKWInterface insertedReadMasterKW = null;
        if (readTypeConfigurator.isKw()) {
            logger.info(methodName + " configurator says to insert kw, inserting kw");
            ReadMasterKWInterface readMasterKW = new ReadMasterKW();
            logger.info(methodName + " inserting read master KW for consumer : " + consumerNo);
            readMasterKW.setReadMasterId(insertedRead.getId());
            readMasterKW.setMeterMD(BigDecimal.ZERO);
            readMasterKW.setMultipliedMD(BigDecimal.ZERO);
            readMasterKW.setBillingDemand(BigDecimal.ZERO);
            setAuditDetails(readMasterKW);
            logger.info(methodName + "inserting read master KW");
            insertedReadMasterKW = readMasterKWService.insert(readMasterKW);
            if (insertedReadMasterKW == null) {
                logger.info(methodName + "unable to save KW Read, aborting request");
                throw new Exception("unable to save KW Read, aborting request");
            }
            logger.info(methodName + " inserted read master KW for consumer : " + consumerNo);
        } else {
            logger.info(methodName + " read type configurator does not allows to add kw reading");
        }
        insertedRead.setReadMasterKW(insertedReadMasterKW);
        ReadMasterPFInterface insertedReadMasterPF = null;
        if (readTypeConfigurator.isPf()) {
            logger.info(methodName + " configurator says to insert pf, inserting pf");
            ReadMasterPFInterface readMasterPF = new ReadMasterPF();
            readMasterPF.setReadMasterId(readToInsert.getId());
            readMasterPF.setMeterPF(BigDecimal.ZERO);
            readMasterPF.setBillingPF(BigDecimal.ZERO);
            setAuditDetails(readMasterPF);
            logger.info(methodName + " inserting read master PF for consumer : " + consumerNo);
            insertedReadMasterPF = readMasterPFService.insert(readMasterPF);
            if (insertedReadMasterPF == null) {
                logger.error(methodName + "unable to insert PF ");
                throw new Exception("unable to insert PF");
            }
            logger.info(methodName + "inserted read master PF for consumer : " + consumerNo);
        } else {
            logger.info(methodName + "read type configurator does not allows to add PF reading");
        }
        insertedRead.setReadMasterPF(insertedReadMasterPF);
        return insertedRead;
    }

    /**
     * in below method ReadTypeConfigurator is not used to make decision for MD and PF. Below method is used in CC4.
     *
     * @param consumerNo
     * @param billMonth
     * @param replacementFlag
     * @param usedOnBill
     * @return
     */
    public List<ReadMasterInterface> getByConsumerNoAndBillMonthAndReplacementFlagAndUsedOnBillOrderByIdDesc(String consumerNo, String billMonth, String replacementFlag, Boolean usedOnBill) {
        String methodName = "getByConsumerNoAndBillMonthAndReplacementFlagAndUsedOnBillOrderByIdDesc() : ";
        logger.info(methodName + "Called");
        List<ReadMasterInterface> readings = null;
        List<ReadMasterInterface> readMasterInterfaces = new ArrayList<>();
        if (consumerNo != null && billMonth != null && replacementFlag != null && usedOnBill != null) {
            readings = readMasterDAO.getByConsumerNoAndBillMonthAndReplacementFlagAndUsedOnBillOrderByIdDesc(consumerNo, billMonth, replacementFlag, usedOnBill);
            if (readings != null) {
                for (ReadMasterInterface readMasterInterface : readings) {
                    ReadMasterKWInterface readMasterKWInterface = readMasterKWService.getByReadMasterId(readMasterInterface.getId());
                    readMasterInterface.setReadMasterKW(readMasterKWInterface);

                    ReadMasterPFInterface readMasterPFInterface = readMasterPFService.getByReadMasterId(readMasterInterface.getId());
                    readMasterInterface.setReadMasterPF(readMasterPFInterface);

                    readMasterInterfaces.add(readMasterInterface);
                }
            }
        }
        return readMasterInterfaces;
    }

    public List<ReadMasterInterface> getTop2ByConsumerNoAndBillMonthOrderByIdDesc(String consumerNo, String billMonth) {
        String methodName = "getTop2ByConsumerNoAndBillMonthOrderByIdDesc() : ";
        logger.info(methodName + "Called");
        List<ReadMasterInterface> readings = null;
        List<ReadMasterInterface> readMasterInterfaces = new ArrayList<>();
        if (consumerNo != null && billMonth != null) {
            readings = readMasterDAO.getTop2ByConsumerNoAndBillMonthOrderByIdDesc(consumerNo, billMonth);
            if (readings != null) {
                for (ReadMasterInterface readMasterInterface : readings) {
                    ReadMasterKWInterface readMasterKWInterface = readMasterKWService.getByReadMasterId(readMasterInterface.getId());
                    readMasterInterface.setReadMasterKW(readMasterKWInterface);

                    ReadMasterPFInterface readMasterPFInterface = readMasterPFService.getByReadMasterId(readMasterInterface.getId());
                    readMasterInterface.setReadMasterPF(readMasterPFInterface);

                    readMasterInterfaces.add(readMasterInterface);
                }
            }
        }
        return readMasterInterfaces;
    }

    public ReadMasterInterface insert(ReadMasterInterface readMaster) {
        String methodName = "insert ";
        ReadMasterInterface insertedReadMaster = null;
        logger.info(methodName + "Called");
        if (readMaster != null) {
            setAuditDetails(readMaster);
            insertedReadMaster = readMasterDAO.add(readMaster);
        }
        return insertedReadMaster;
    }


    public ReadMasterInterface update(ReadMasterInterface readMaster){
        String methodName = "update ";
        ReadMasterInterface updatedReadMaster = null ;
        logger.info(methodName + "Called");
        if (readMaster != null) {
            setUpdatedDetails(readMaster);
            updatedReadMaster = readMasterDAO.update(readMaster);
        }
        return updatedReadMaster;
    }

    public ReadMasterInterface getByConsumerNoAndReplacementFlag(String consumerNo, String replacementFlag) {
        final String methodName = "getByConsumerNoAndReplacementFlag() : ";
        logger.info(methodName + "called");
        ReadMasterInterface fetchedReadMaster = null;
        if (consumerNo != null && replacementFlag != null) {
            fetchedReadMaster = readMasterDAO.getByConsumerNoAndReplacementFlag(consumerNo, replacementFlag);
            if (fetchedReadMaster != null) {
                long readingId = fetchedReadMaster.getId();
                ReadTypeConfiguratorInterface readTypeConfigurator = readTypeConfiguratorService.getByConsumerNo(consumerNo);
                if (readTypeConfigurator != null) {
                    if (readTypeConfigurator.isKw()) {
                        ReadMasterKWInterface readMasterKW = readMasterKWService.getByReadMasterId(readingId);
                        if (readMasterKW != null) {
                            fetchedReadMaster.setReadMasterKW(readMasterKW);
                        }
                    }
                    if (readTypeConfigurator.isPf()) {
                        ReadMasterPFInterface readMasterPF = readMasterPFService.getByReadMasterId(readingId);
                        if (readMasterPF != null) {
                            fetchedReadMaster.setReadMasterPF(readMasterPF);
                        }
                    }
                }
            }

        }
        return fetchedReadMaster;
    }

    /**
     * coded by nitish.
     * This method returns read master's object in a paginated way
     *
     * @param consumerNo
     * @param pageNumber
     * @param pageSize
     * @param sortOrder
     * @return
     */
    public List<ReadMasterInterface> getByConsumerNoOrderByBillMonthWithPagination(String consumerNo, int pageNumber, int pageSize, String sortOrder) {
        String methodName = " getByConsumerNoOrderByBillMonthWithPagination() : ";
        logger.info(methodName + "called " + consumerNo);
        List<ReadMasterInterface> readings = Collections.emptyList();
        pageNumber = pageNumber - 1; //since postgresql offset works with 0 based index
        if (consumerNo != null && pageNumber >= 0) {
            if (sortOrder != null && sortOrder.equalsIgnoreCase(NGBAPIUtility.SORT_ORDER_ASCENDING)) {
                Pageable pageable = new PageRequest(pageNumber, pageSize, Sort.Direction.ASC, "id");
                readings = readMasterDAO.getByConsumerNoOrderByBillMonthAscendingWithPageable(consumerNo, pageable);
            } else {
                Pageable pageable = new PageRequest(pageNumber, pageSize, Sort.Direction.DESC, "id");
                readings = readMasterDAO.getByConsumerNoOrderByBillMonthDescendingWithPageable(consumerNo, pageable);
            }
        }
        return readings;
    }

    /**  todo update method with propagation logic with KW ,PF and read used on bill flag.
     * this method compares KWH and set difference if current read is greater then previous read. else returns null
     * @param readMasterInterface
     * @param errorMessage
     * @return
     */
    public ReadMasterInterface setDifferenceWithLatestReadMaster(ReadMasterInterface readMasterInterface , ErrorMessage errorMessage) {
        String methodName = "setDifferenceWithLatestReadMaster() : ";
        logger.info(methodName + "called");
        if (readMasterInterface == null || readMasterInterface.getConsumerNo() == null || readMasterInterface.getReading() == null) {
            errorMessage.setErrorMessage("given input is null");
            return null;
        }
        BigDecimal currentReading = readMasterInterface.getReading();
        String consumerNo = readMasterInterface.getConsumerNo();
        try {
            ReadMasterInterface previousReadMasterInterface = getLatestReadingByConsumerNo(consumerNo);
            if (previousReadMasterInterface == null || previousReadMasterInterface.getReading() == null) {
                errorMessage.setErrorMessage("some error to fetch previous reading.");
                return null;
            }
            readMasterInterface.setDifference(currentReading.subtract(previousReadMasterInterface.getReading()));
        } catch (RuntimeException exception) {
            logger.error(methodName + "Received Runtime Exception with error message as :" + exception.getMessage());
            errorMessage.setErrorMessage("some error to fetch previous read.");
            return null;

        } catch (Exception exception) {
            logger.error(methodName + "Received Exception with error message as :" + exception.getMessage());
            errorMessage.setErrorMessage(exception.getMessage());
            return null;
        }
        return readMasterInterface;
    }

    public ReadMasterInterface replaceOldServiceNoOneWithNewServiceNo(ReadMasterInterface readMasterInterface) {
        final String methodName = "replaceOldServiceNoOneWithNewServiceNo() : ";
        logger.info(methodName + "called");
        ConsumerNoMasterInterface consumerNoMasterInterface = null;
        if (readMasterInterface != null) {
            String username = GlobalResources.getLoggedInUser();
            if (username == null) {
                logger.error(methodName + "unable to get logged in username.");
                return null;
            }
            UserDetail userDetail = userDetailService.getByUsername(username);
            if (userDetail == null || userDetail.getLocationCode() == null) {
                logger.error(methodName + "unable to get user detail.");
                return null;
            }
            String locationCode = userDetail.getLocationCode();
            String oldServiceNoOne = readMasterInterface.getConsumerNo();
            consumerNoMasterInterface = consumerNoMasterService.getByOldServiceNoOneAndLocationCode(oldServiceNoOne, locationCode);
            if (consumerNoMasterInterface == null || consumerNoMasterInterface.getConsumerNo() == null) {
                logger.error(methodName + "some error to fetch consumer master. ");
                return null;
            }
            String consumerNo = consumerNoMasterInterface.getConsumerNo();
            readMasterInterface.setConsumerNo(consumerNo);
        }
        return readMasterInterface;
    }
}
