package com.mppkvvcl.ngbapi.services;

import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbdao.daos.PartPaymentDAO;
import com.mppkvvcl.ngbentity.beans.ErrorMessage;
import com.mppkvvcl.ngbinterface.interfaces.BillInterface;
import com.mppkvvcl.ngbinterface.interfaces.PartPaymentInterface;
import com.mppkvvcl.ngbinterface.interfaces.PaymentInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by PREETESH on 8/2/2017.
 */
@Service
public class PartPaymentService  {

    private static final Logger logger = GlobalResources.getLogger(PartPaymentService.class);

    @Autowired
    private PartPaymentDAO partPaymentDAO;

    @Autowired
    private BillService billService;

    public List<PartPaymentInterface> getByConsumerNoAndApplied(String consumerNo, boolean applied) {
        String methodName = "getByConsumerNoAndApplied() : ";
        logger.info(methodName + "called");
        List<PartPaymentInterface> partPayments = null;
        if (consumerNo != null) {
            partPayments = new ArrayList<PartPaymentInterface>();
            partPayments = partPaymentDAO.getByConsumerNoAndAppliedOrderByIdDesc(consumerNo,applied);
            if (partPayments != null && partPayments.size()>0) {
                logger.info(methodName + "Successfully fetched part payment for consumer "+consumerNo+
                        " applied "+applied);
            } else {
                logger.error(methodName + "no records present for consumer "+consumerNo+
                        " applied "+applied);
            }
        } else {
            logger.error(methodName + "consumer no is null");
        }
        return partPayments;
    }

    public PartPaymentInterface getNotAppliedPartPaymentForConsumerNo(String consumerNo) {
        String methodName = "getNotAppliedPartPaymentForConsumerNo(): ";
        List<PartPaymentInterface> partPayments;
        PartPaymentInterface partPayment = null;
        logger.info(methodName + "fetching part payment details ");
        if (consumerNo != null) {
            partPayments = partPaymentDAO.getByConsumerNoAndAppliedOrderByIdDesc(consumerNo,false);
            if (partPayments != null && partPayments.size()>0) {
                partPayment = partPayments.get(0);
                logger.info(methodName + "Successfully fetched current part payment for consumer "+consumerNo);
            } else {
                logger.error(methodName + "no records present for consumer "+consumerNo);
            }
        } else {
            logger.error(methodName + "consumer no is null");
        }
        return partPayment;
    }

    public PartPaymentInterface update(PartPaymentInterface partPayment) {
        String methodName = "update() : ";
        logger.info(methodName + "called");
        PartPaymentInterface savedPartPayment = null;
        if (partPayment != null) {
            savedPartPayment = partPaymentDAO.add(partPayment);
        }else{
            logger.error(methodName + "part payment is null");
        }
        return savedPartPayment;
    }

    public PartPaymentInterface updateWithCheck(PartPaymentInterface partPaymentToUpdate, ErrorMessage errorMessage) {
        String methodName = "updateWithCheck() : ";
        logger.info(methodName + "called");
        PartPaymentInterface savedPartPayment = null;
        if (partPaymentToUpdate == null) {
            logger.error(methodName + "part payment to update is null");
            errorMessage.setErrorMessage("part payment to update is null");
            return null;
        }
        PartPaymentInterface existingPartPayment = partPaymentDAO.getById(partPaymentToUpdate.getId());
        if(existingPartPayment == null){
            logger.error(methodName + "part payment not found with id");
            errorMessage.setErrorMessage("part payment not found with id");
            return null;
        }

        if(existingPartPayment.isApplied()){
            logger.error(methodName + "part payment has participated in a payment, can not delete now");
            errorMessage.setErrorMessage("part payment has participated in a payment, can not delete now");
            return null;
        }

        String consumerNo = partPaymentToUpdate.getConsumerNo();
        long partPaymentAmount = partPaymentToUpdate.getAmount();

        if(consumerNo == null || partPaymentAmount < PaymentInterface.MINIMUM_PAYMENT_AMOUNT ){
            logger.error("one of the parameters in  PartPayment is null");
            errorMessage.setErrorMessage("one of the parameters in  PartPayment is null");
            return null;
        }
        BillInterface bill = billService.getLatestBillByConsumerNo(consumerNo);
        if(bill == null){
            logger.error("Bill not available for given consumer");
            errorMessage.setErrorMessage("Bill not available for given consumer");
            return null;
        }
        BigDecimal billAmount = bill.getNetBill();
        if(billAmount == null ){
            logger.error("Bill amount found null");
            errorMessage.setErrorMessage("Bill amount not available for given consumer");
            return null;
        }

        if(partPaymentAmount >= billAmount.longValue()){
            logger.error("Part Payment Amount can't be greater than net bill amount");
            errorMessage.setErrorMessage("Part Payment Amount can't be greater than net bill amount");
            return null;
        }
        Date currentDate = GlobalResources.getCurrentDate();
        String loggedInUser = GlobalResources.getLoggedInUser();
        partPaymentToUpdate.setApprovedBy(loggedInUser);
        partPaymentToUpdate.setUpdatedOn(currentDate);
        partPaymentToUpdate.setUpdatedBy(loggedInUser);
        savedPartPayment = partPaymentDAO.update(partPaymentToUpdate);
        if(savedPartPayment == null){
            logger.error("some error in updating part payment");
            errorMessage.setErrorMessage("some error in updating part payment");
            return null;
        }
        return savedPartPayment;
    }

    public PartPaymentInterface getByConsumerNoAndAppliedAndUpdatedOn(String consumerNo, boolean appliedTrue, Date updatedOn) {
        final String methodName = "getByConsumerNoAndAppliedAndUpdatedOn() : ";
        logger.info(methodName + "called for consumer no " + consumerNo + " " + appliedTrue + " " + updatedOn);
        PartPaymentInterface partPayment = null;
        if(consumerNo != null && updatedOn != null){
            partPayment = partPaymentDAO.getByConsumerNoAndAppliedAndUpdatedOn(consumerNo,appliedTrue,updatedOn);
        }
        return partPayment;
    }

    public PartPaymentInterface insertPartPayment(PartPaymentInterface partPayment, ErrorMessage errorMessage) {
        final String methodName = "insertPartPayment() : ";
        logger.info(methodName + "called ");
        PartPaymentInterface insertedPartPayment = null;
        if(partPayment == null) {
            logger.error("parameter PartPayment is null");
            errorMessage.setErrorMessage("parameter partPayment is null");
            return null;
        }
        String consumerNo = partPayment.getConsumerNo();
        long partPaymentAmount = partPayment.getAmount();

        if(consumerNo == null || partPaymentAmount < PaymentInterface.MINIMUM_PAYMENT_AMOUNT ){
            logger.error("one of the parameters in  PartPayment is null");
            errorMessage.setErrorMessage("one of the parameters in  PartPayment is null");
            return null;
        }
        BillInterface bill = billService.getLatestBillByConsumerNo(consumerNo);
        if(bill == null){
            logger.error("Bill not available for given consumer");
            errorMessage.setErrorMessage("Bill not available for given consumer");
            return null;
        }
        BigDecimal billAmount = bill.getNetBill();
        if(billAmount == null ){
            logger.error("Bill amount found null");
            errorMessage.setErrorMessage("Bill amount not available for given consumer");
            return null;
        }

        if(partPaymentAmount >= billAmount.longValue()){
            logger.error("Part Payment Amount can't be greater than net bill amount");
            errorMessage.setErrorMessage("Part Payment Amount can't be greater than net bill amount");
            return null;
        }
        Date currentDate = GlobalResources.getCurrentDate();
        String loggedInUser = GlobalResources.getLoggedInUser();
        partPayment.setApplied(PartPaymentInterface.APPLIED_FALSE);
        partPayment.setApprovedBy(loggedInUser);
        partPayment.setCreatedOn(currentDate);
        partPayment.setCreatedBy(loggedInUser);
        partPayment.setUpdatedOn(currentDate);
        partPayment.setUpdatedBy(loggedInUser);

        insertedPartPayment = partPaymentDAO.add(partPayment);
        if(insertedPartPayment == null){
            logger.error("some error in saving part payment");
            errorMessage.setErrorMessage("some error in saving part payment");
            return null;
        }
        return insertedPartPayment;
    }

    public List<PartPaymentInterface> getByConsumerNo(String consumerNo,ErrorMessage errorMessage) {
        final String methodName = "insertPartPayment() : ";
        logger.info(methodName + "called ");
        List<PartPaymentInterface> partPayments = null;
        if(consumerNo == null) {
            logger.error("parameter consumerNo is null");
            errorMessage.setErrorMessage("parameter consumerNo is null");
            return null;
        }

        partPayments = partPaymentDAO.getByConsumerNo(consumerNo);
        if(partPayments == null || partPayments.size()<= 0){
            logger.error("No part payments found for given consumer no");
            errorMessage.setErrorMessage("No part payments found for given consumer no");
            return null;
        }
        return partPayments;
    }

    public boolean delete(long id, ErrorMessage errorMessage) {
        String methodName = "delete() : ";
        logger.info(methodName + "called");
        boolean deleted = false;
        if(id > 0){
            PartPaymentInterface partPaymentInterface = partPaymentDAO.getById(id);
            if(partPaymentInterface != null){
                deleted = partPaymentDAO.deleteById(id);
            }else{
                errorMessage.setErrorMessage("part payment not found with id");
                logger.error("part payment not found with id");
            }
        }
        return deleted;
    }
}
