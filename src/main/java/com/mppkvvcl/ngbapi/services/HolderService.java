package com.mppkvvcl.ngbapi.services;

import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbdao.daos.*;
import com.mppkvvcl.ngbengine.factories.HolderFactory;
import com.mppkvvcl.ngbengine.interfaces.HolderInterface;
import com.mppkvvcl.ngbentity.beans.TariffDetail;
import com.mppkvvcl.ngbinterface.interfaces.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
public class HolderService {

    private static final Logger logger = LoggerFactory.getLogger(HolderService.class);

    /**
     * Asking spring to inject EntityManager for underlying hibernate
     * for creating sessions and transactions;
     */
    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private ConsumerConnectionInformationDAO consumerConnectionInformationDAO;

    @Autowired
    private LoadDetailDAO loadDetailDAO;

    @Autowired
    private TariffDAO tariffDAO;

    @Autowired
    private SubCategoryDAO subCategoryDAO;

    @Autowired
    private BillDAO billDAO;

    @Autowired
    private AgricultureBill6MonthlyDAO agricultureBill6MonthlyDAO;

    @Autowired
    private PaymentDAO paymentDAO;

    @Autowired
    private AdjustmentDAO adjustmentDAO;

    @Autowired
    private SurchargeDAO surchargeDAO;

    @Autowired
    private AgricultureUnitDAO agricultureUnitDAO;

    @Autowired
    private SlabDAO slabDAO;

    @Autowired
    private FCADAO fcadao;

    @Autowired
    private PowerFactorDAO powerFactorDAO;

    @Autowired
    private IrrigationSchemeDAO irrigationSchemeDAO;

    //below variables are for lv1.1 101
    @Autowired
    private ReadMasterService readMasterService;

    @Autowired
    private ConsumerMeterMappingDAO consumerMeterMappingDAO;

    @Autowired
    private MeterRentDAO meterRentDAO;

    @Autowired
    private ElectricityDutyDAO electricityDutyDAO;

    @Autowired
    private MinimumChargeDAO minimumChargeDAO;

    @Autowired
    private ConsumerInformationDAO consumerInformationDAO;

    @Autowired
    private ScheduleDAO scheduleDAO;

    @Autowired
    private UnmeteredUnitDAO unmeteredUnitDAO;

    @Autowired
    private SubsidyDAO subsidyDAO;

    @Autowired
    private EmployeeConnectionMappingDAO employeeConnectionMappingDAO;

    @Autowired
    private EmployeeRebateDAO employeeRebateDAO;

    @Autowired
    private OnlinePaymentRebateDAO onlinePaymentRebateDAO;

    @Autowired
    private PrepaidMeterRebateDAO prepaidMeterRebateDAO;

    @Autowired
    private PromptPaymentIncentiveDAO promptPaymentIncentiveDAO;

    @Autowired
    private SecurityDepositDAO securityDepositDAO;

    @Autowired
    private SecurityDepositInterestRateDAO securityDepositInterestRateDAO;

    @Autowired
    private SecurityDepositInterestDAO securityDepositInterestDAO;

    @Autowired
    private PurposeSubsidyMappingDAO purposeSubsidyMappingDAO;

    @Autowired
    private LoadFactorIncentiveConfigurationDAO loadFactorIncentiveConfigurationDAO;

    @Autowired
    private ExcessDemandSubcategoryMappingDAO excessDemandSubcategoryMappingDAO;

    @Autowired
    private XrayConnectionInformationDAO xrayConnectionInformationDAO;

    @Autowired
    private XrayConnectionFixedChargeRateDAO xrayConnectionFixedChargeRateDAO;

    @Autowired
    private WeldingSurchargeConfigurationDAO weldingSurchargeConfigurationDAO;

    @Autowired
    private GMCDAO gmcDAO;

    @Autowired
    private GMCAccountingService gmcAccountingService;

    public HolderInterface prepareHolderForBillCorrection(final ConsumerNoMasterInterface consumerNoMasterInterface, final TariffDetailInterface tariffDetailInterface, final ScheduleInterface scheduleInterface, ReadMasterInterface readMaster)  {
        final String methodName = "prepareHolderForBillCorrection() : ";
        logger.info(methodName + "called");
        HolderInterface holderInterface = null;
        if(consumerNoMasterInterface != null && scheduleInterface != null && tariffDetailInterface != null && readMaster != null) {

            holderInterface = HolderFactory.buildHolder();

            final String consumerNo = consumerNoMasterInterface.getConsumerNo();

            //setting ConsumerNoMasterInterface
            holderInterface.setConsumerNoMaster(consumerNoMasterInterface);

            //setting ConsumerInformationInterface
            ConsumerInformationInterface consumerInformationInterface = consumerInformationDAO.getByConsumerNo(consumerNo);
            holderInterface.setConsumerInformationInterface(consumerInformationInterface);

            //setting ConsumerConnectionInformationInterface
            ConsumerConnectionInformationInterface consumerConnectionInformationInterface = consumerConnectionInformationDAO.getByConsumerNo(consumerNo);
            holderInterface.setConsumerConnectionInformationInterface(consumerConnectionInformationInterface);

            //setting ScheduleInterface
            holderInterface.setSchedule(scheduleInterface);

            String currentScheduleBillMonth = scheduleInterface.getBillMonth();
            ScheduleInterface previousScheduleInterface = null;
            String previousBillMonth = null;
            if (currentScheduleBillMonth != null) {
                previousBillMonth = GlobalResources.getPreviousMonth(currentScheduleBillMonth);
                previousScheduleInterface = scheduleDAO.getByGroupNoAndBillMonthAndBillStatusAndSubmitted(scheduleInterface.getGroupNo(), previousBillMonth, ScheduleInterface.BILL_STATUS_COMPLETED, ScheduleInterface.SUBMITTED_Y);
            }
            //setting Last BillInterface
            BillInterface previousBill = null;
            List<BillInterface> billInterface = billDAO.getByConsumerNoAndBillMonthAndDeleted(consumerNo, previousBillMonth, BillInterface.DELETED_FALSE);
            if (billInterface != null && billInterface.size() == 1) {
                previousBill = billInterface.get(0);
            }
            holderInterface.setBill(previousBill);


            List<ConsumerMeterMappingInterface> consumerMeterMappingInterfaces = consumerMeterMappingDAO.getByConsumerNoAndMappingStatus(consumerNo, ConsumerMeterMappingInterface.STATUS_ACTIVE);
            ConsumerMeterMappingInterface consumerMeterMappingInterface = null;
            if (consumerMeterMappingInterfaces != null && consumerMeterMappingInterfaces.size() > 0) {
                consumerMeterMappingInterface = consumerMeterMappingInterfaces.get(0);
                if (consumerMeterMappingInterface != null) {
                    MeterMasterInterface meterMasterInterface = consumerMeterMappingInterface.getMeterMaster();
                    holderInterface.setMeterMasterInterface(meterMasterInterface);
                }
            }

            //setting rates and setting in passed TariffDetail
            prepareTariff(consumerNo, tariffDetailInterface, scheduleInterface, previousScheduleInterface, consumerInformationInterface, consumerMeterMappingInterface, consumerConnectionInformationInterface, previousBill);
            holderInterface.setTariffDetailInterface(tariffDetailInterface);

            long subCategoryCode = tariffDetailInterface.getSubcategoryCode();
            if (subCategoryCode == 101 || subCategoryCode == 103) {
                long alternateSubCategoryCode = 0;
                if (subCategoryCode == 101) {
                    alternateSubCategoryCode = 102;
                } else if (subCategoryCode == 103) {
                    alternateSubCategoryCode = 104;
                }
                TariffDetailInterface alternateTariffDetailInterface = new TariffDetail();
                alternateTariffDetailInterface.setTariffCode("LV1.2");
                alternateTariffDetailInterface.setSubcategoryCode(alternateSubCategoryCode);
                prepareTariff(consumerNo, alternateTariffDetailInterface, scheduleInterface, previousScheduleInterface, consumerInformationInterface, consumerMeterMappingInterface, consumerConnectionInformationInterface, previousBill);

                holderInterface.setAlternateTariffDetailInterface(alternateTariffDetailInterface);
            }

            //setting LoadDetailInterfaces
            List<LoadDetailInterface> loadDetailInterfaces = loadDetailDAO.getByConsumerNo(consumerNo);     //changes required  get with old and new bill date
            holderInterface.setLoadDetails(loadDetailInterfaces);

            //setting Agriculture6MonthlyBill
            AgricultureBill6MonthlyInterface agricultureBill6MonthlyInterface = agricultureBill6MonthlyDAO.getTopByConsumerNo(consumerNo);
            holderInterface.setAgricultureBill6MonthlyBill(agricultureBill6MonthlyInterface);

            //setting Payments
            List<PaymentInterface> paymentInterfaces = paymentDAO.getByConsumerNoAndPostingBillMonthAndDeletedAndPosted(consumerNo, currentScheduleBillMonth, PaymentInterface.DELETED_FALSE, PaymentInterface.POSTED_TRUE);
            holderInterface.setPayments(paymentInterfaces);

            detachPayments(paymentInterfaces);

            //setting Adjustments
            List<AdjustmentInterface> adjustmentInterfaces = adjustmentDAO.getByConsumerNoAndPostingBillMonthAndPostedAndDeleted(consumerNo, currentScheduleBillMonth, AdjustmentInterface.POSTED_TRUE, AdjustmentInterface.DELETED_FALSE);
            holderInterface.setAdjustments(adjustmentInterfaces);

            detachAdjustments(adjustmentInterfaces);

            holderInterface.setFlatRateBillMonthOne("APR");
            holderInterface.setFlatRateBillMonthTwo("OCT");

            //Below code is for lv1.1
            //ReadMasterInterface currentReadMasterInterface = readMasterService.getLatestReadingByConsumerNoAndBillMonthAndReplacementFlag(consumerNo,scheduleInterface.getBillMonth(),ReadMasterInterface.REPLACEMENT_FLAG_NORMAL_READ);
            holderInterface.setCurrentReadMasterInterface(readMaster);

            ReadMasterInterface previousReadMasterInterface = readMasterService.getLatestReadingByConsumerNoAndBillMonthAndReplacementFlag(consumerNo, previousBillMonth, ReadMasterInterface.REPLACEMENT_FLAG_NORMAL_READ);
            if (previousReadMasterInterface == null) {
                previousReadMasterInterface = readMasterService.getByConsumerNoAndReplacementFlag(consumerNo, ReadMasterInterface.REPLACEMENT_FLAG_NEW_CONNECTION);
            }
            holderInterface.setPreviousReadMasterInterface(previousReadMasterInterface);

            OnlinePaymentRebateInterface onlinePaymentRebateInterface = onlinePaymentRebateDAO.getByDate(scheduleInterface.getBillDate());
            holderInterface.setOnlinePaymentRebateInterface(onlinePaymentRebateInterface);

            PrepaidMeterRebateInterface prepaidMeterRebateInterface = prepaidMeterRebateDAO.getByDate(scheduleInterface.getBillDate());
            holderInterface.setPrepaidMeterRebateInterface(prepaidMeterRebateInterface);

            PromptPaymentIncentiveInterface promptPaymentIncentiveInterface = promptPaymentIncentiveDAO.getByDate(scheduleInterface.getBillDate());
            holderInterface.setPromptPaymentIncentiveInterface(promptPaymentIncentiveInterface);

            //setting GMCAccountingInterface

            GMCAccountingInterface gmcAccountingInterface = gmcAccountingService.getGMCAccountingByReplacingCurrentValuesWithPreviousValues(consumerNo);
            holderInterface.setGmcAccountingInterface(gmcAccountingInterface);
            logger.info(methodName + "setting GMC interface   " + gmcAccountingInterface);
            detachGMCAccounting(gmcAccountingInterface);


            ///below code for Security deposit interest calculation

            SecurityDepositInterestInterface securityDepositInterestInterfaces = securityDepositInterestDAO.getTopByConsumerNoOrderByIdDesc(consumerNo);
            if (securityDepositInterestInterfaces != null) {
                    Date startInterestCalculationDate = securityDepositInterestInterfaces.getCalculationStartDate();
                    Date endInterestCalculationDate = securityDepositInterestInterfaces.getCalculationEndDate();
                    if (startInterestCalculationDate != null && endInterestCalculationDate != null) {
                        List<SecurityDepositInterface> securityDepositInterfaces = securityDepositDAO.getByConsumerNoAndEffectiveStartDateAndEffectiveEndDateOrderByIdAsc(consumerNo, startInterestCalculationDate, endInterestCalculationDate);
                        holderInterface.setSecurityDepositInterfaces(securityDepositInterfaces);

                        List<SecurityDepositInterestRateInterface> securityDepositInterestRateInterface = securityDepositInterestRateDAO.getByEffectiveStartDateAndEffectiveEndDateOrderByIdAsc(startInterestCalculationDate, endInterestCalculationDate);
                        holderInterface.setSecurityDepositInterestRateInterfaces(securityDepositInterestRateInterface);

                        detachSecurityDepositInterest(securityDepositInterestInterfaces);
                        holderInterface.setSecurityDepositInterestInterface(securityDepositInterestInterfaces);
                    }
            }
//            else if (securityDepositInterestInterfaces != null && securityDepositInterestInterfaces.size() > 0) {
//                SecurityDepositInterestInterface currentMonthSecurityDepositInterestInterface = securityDepositInterestInterfaces.get(0);
//                if (currentMonthSecurityDepositInterestInterface != null) {
//                    Date startInterestCalculationDate = consumerConnectionInformationInterface.getConnectionDate();
//                    Date endInterestCalculationDate = scheduleInterface.getBillDate();
//                    logger.info(methodName + "start date is "+startInterestCalculationDate);
//                    logger.info(methodName + "end  date is "+endInterestCalculationDate);
//                    if (startInterestCalculationDate != null && endInterestCalculationDate != null) {
//                        List<SecurityDepositInterface> securityDepositInterfaces = securityDepositDAO.getByConsumerNoAndEffectiveStartDateAndEffectiveEndDateOrderByIdAsc(consumerNo, startInterestCalculationDate, endInterestCalculationDate);
//                        holderInterface.setSecurityDepositInterfaces(securityDepositInterfaces);
//
//                        List<SecurityDepositInterestRateInterface> securityDepositInterestRateInterface = securityDepositInterestRateDAO.getByEffectiveStartDateAndEffectiveEndDateOrderByIdAsc(startInterestCalculationDate,endInterestCalculationDate);
//                        holderInterface.setSecurityDepositInterestRateInterfaces(securityDepositInterestRateInterface);
//                    }
//                }
// }
        }
        return holderInterface;
    }

    private void prepareTariff(String consumerNo,TariffDetailInterface tariffDetailInterface,ScheduleInterface scheduleInterface,ScheduleInterface previousScheduleInterface,ConsumerInformationInterface consumerInformationInterface,ConsumerMeterMappingInterface consumerMeterMappingInterface,ConsumerConnectionInformationInterface consumerConnectionInformationInterface,BillInterface previousBill){
        final String methodName = "prepareTariff() : ";
        logger.info(methodName + "called");
        //setting TariffDetailInterfaces
        //holderInterface.setTariffDetailInterface(tariffDetailInterface);
        logger.info("td "+tariffDetailInterface+" s "+scheduleInterface+" ps "+previousScheduleInterface+" cons"+ consumerInformationInterface+ "cnn "+consumerConnectionInformationInterface);
        if(tariffDetailInterface != null && scheduleInterface != null && previousScheduleInterface != null && consumerInformationInterface != null && consumerConnectionInformationInterface != null){

            List<TariffInterface> tariffInterfaces = null;
            logger.info(methodName + "Fetching Tariff Objects for code " + tariffDetailInterface.getTariffCode()
                    + " startBillDate " + previousScheduleInterface.getBillDate() + " endBillDate " + scheduleInterface.getBillDate());
            tariffInterfaces = tariffDAO.getByTariffCodeAndEffectiveStartDateAndEffectiveEndDate(tariffDetailInterface.getTariffCode(),getTariffFetchDate(previousScheduleInterface.getBillDate()),getTariffFetchDate(scheduleInterface.getBillDate()));

            if(tariffInterfaces == null){
                logger.error(methodName + "TariffInterfaces is null returning");
                return;
            }
            logger.info(methodName + "Got " + tariffInterfaces.size() + " tariff interfaces");
            //fetching subcategories for each tariff
            for(TariffInterface tariffInterface : tariffInterfaces){
                logger.info(methodName + "fetching subactegory object" +
                        " for tariffId " + tariffInterface.getId() + " and subcategorycode " + tariffDetailInterface.getSubcategoryCode());

                //setting subcategory
                SubCategoryInterface subCategoryInterface = subCategoryDAO.getByTariffIdAndCode(tariffInterface.getId(),tariffDetailInterface.getSubcategoryCode());
                if(subCategoryInterface != null){

                    //preparing rates of normal subcategory object
                    prepareRate(tariffInterface,subCategoryInterface,scheduleInterface,consumerNo,consumerInformationInterface);

                    Date billingStartDate = null;
                    Date billingEndDate = scheduleInterface.getBillDate();

                    if(previousBill != null){
                        billingStartDate = previousBill.getBillDate();
                    }else {
                        billingStartDate = consumerConnectionInformationInterface.getConnectionDate();
                    }

                    List<FCAInterface> fcaInterfaces = fcadao.getByEffectiveStartDateAndEffectiveEndDate(billingStartDate,billingEndDate);
                    subCategoryInterface.setFcaInterfaces(fcaInterfaces);
                    if(consumerMeterMappingInterface != null){
                        MeterMasterInterface meterMasterInterface = consumerMeterMappingInterface.getMeterMaster();
                        if(meterMasterInterface != null){
                            MeterRentInterface meterRentInterface = meterRentDAO.getByMeterCodeAndMeterCapacity(meterMasterInterface.getCode(),meterMasterInterface.getCapacity());
                            subCategoryInterface.setMeterRentInterface(meterRentInterface);
                        }
                    }

                    //below code is for 509 & others
                    AgricultureUnitInterface agricultureUnitInterface = agricultureUnitDAO.getBySubcategoryCodeAndBillMonth(subCategoryInterface.getCode(),scheduleInterface.getBillMonth().trim());
                    if(agricultureUnitInterface != null){
                        //setting Alternate Subcategory
                        SubCategoryInterface alternateSubCategoryInterfaceLV5 = subCategoryDAO.getByTariffIdAndCode(agricultureUnitInterface.getTariffId(),agricultureUnitInterface.getAlternateSubcategoryCode());
                        logger.info(methodName + "Got Alternate SubCategory for LV5 as " + alternateSubCategoryInterfaceLV5);
                        if(alternateSubCategoryInterfaceLV5 != null){
                            logger.info(methodName + "Setting Alternate Subcategory for LV5");
                            //setting Agriculture Unit Interface in Alternate Sub Category
                            alternateSubCategoryInterfaceLV5.setAgricultureUnitInterface(agricultureUnitInterface);
                            //preparing rate for alternate subcategory object
                            prepareRate(tariffInterface,alternateSubCategoryInterfaceLV5,scheduleInterface,consumerNo,consumerInformationInterface);
                        }
                        tariffInterface.setAlternateSubCategoryInterface(alternateSubCategoryInterfaceLV5);

                    }
                }
                tariffInterface.setSubCategoryInterface(subCategoryInterface);
            }
            //setting Tariffs
            tariffDetailInterface.setTariffInterfaces(tariffInterfaces);
        }
    }

    private void prepareRate(TariffInterface tariffInterface,SubCategoryInterface subCategoryInterface, ScheduleInterface scheduleInterface, String consumerNo,ConsumerInformationInterface consumerInformationInterface){
        if(scheduleInterface != null && consumerNo != null){
            logger.info("prepare rate called");
            //Setting surcharge interfaces
            List<SurchargeInterface> surchargeInterfaces = surchargeDAO.getByTariffIdAndSubcategoryCode(subCategoryInterface.getTariffId(),subCategoryInterface.getCode());
            subCategoryInterface.setSurchargeInterfaces(surchargeInterfaces);

            //setting slabs
            List<SlabInterface> slabInterfaces = slabDAO.getByTariffIdAndSlabIdOrderById(subCategoryInterface.getTariffId(),subCategoryInterface.getSlabId());
            subCategoryInterface.setSlabInterfaces(slabInterfaces);

            //setting FCA Interface
            FCAInterface fcaInterface = fcadao.getByDate(scheduleInterface.getBillDate());
            subCategoryInterface.setFcaInterface(fcaInterface);

            //setting PowerFactorInterface
            PowerFactorInterface powerFactorInterface = powerFactorDAO.getByPowerFactorValueAndDate(BigDecimal.ZERO,scheduleInterface.getBillDate());
            subCategoryInterface.setPowerFactorInterface(powerFactorInterface);

            //setting PowerFactorInterfaces
            List<PowerFactorInterface> powerFactorInterfaces = powerFactorDAO.getByDate(scheduleInterface.getBillDate());
            subCategoryInterface.setPowerFactorInterfaces(powerFactorInterfaces);

            WeldingSurchargeConfigurationInterface weldingSurchargeConfigurationInterface = weldingSurchargeConfigurationDAO.getByTariffIdAndSubcategoryCode(tariffInterface.getId(),subCategoryInterface.getCode());
            subCategoryInterface.setWeldingSurchargeConfigurationInterface(weldingSurchargeConfigurationInterface);

            //setting Load Factor Incentive list to get load factor configuration
            List<LoadFactorIncentiveConfigurationInterface> loadFactorIncentiveConfigurationInterfaces = loadFactorIncentiveConfigurationDAO.getByDate(scheduleInterface.getBillDate());
            subCategoryInterface.setLoadFactorIncentiveConfigurationInterfaces(loadFactorIncentiveConfigurationInterfaces);

            //setting IrrigationSchemeInterface
            IrrigationSchemeInterface irrigationSchemeInterface = irrigationSchemeDAO.getByConsumerNo(consumerNo);
            subCategoryInterface.setIrrigationSchemeInterface(irrigationSchemeInterface);

            //below code is for 101,102 and other
            List<ElectricityDutyInterface> electricityDutyInterfaces = electricityDutyDAO.getBySubCategoryCodeAndDate(subCategoryInterface.getCode(),scheduleInterface.getBillDate());
            subCategoryInterface.setElectricityDutyInterfaces(electricityDutyInterfaces);

            MinimumChargeInterface minimumChargeInterface = minimumChargeDAO.getByTariffIdAndSubcategoryCode(tariffInterface.getId(),subCategoryInterface.getCode());
            subCategoryInterface.setMinimumChargeInterface(minimumChargeInterface);

            //Getting GMC as per subcategory code and tariff id.
            GMCInterface gmcInterface = gmcDAO.getBySubcategoryCodeAndTariffId(subCategoryInterface.getCode(),tariffInterface.getId());
            subCategoryInterface.setGmcInterface(gmcInterface);

            UnmeteredUnitInterface unmeteredUnitInterface = unmeteredUnitDAO.getByTariffIdAndSubcategoryCode(tariffInterface.getId(),subCategoryInterface.getCode());
            tariffInterface.setUnmeteredUnitInterface(unmeteredUnitInterface);

            List<ExcessDemandSubcategoryMappingInterface> excessDemandSubcategoryMappingInterfaces = excessDemandSubcategoryMappingDAO.getByTariffIdAndSubcategoryCode(tariffInterface.getId(),subCategoryInterface.getCode());
            subCategoryInterface.setExcessDemandSubcategoryMappingInterfaces(excessDemandSubcategoryMappingInterfaces);

            List<XrayConnectionFixedChargeRateInterface> xrayConnectionFixedChargeRateInterfaces = xrayConnectionFixedChargeRateDAO.getByTariffIdAndSubcategoryCode(tariffInterface.getId(),subCategoryInterface.getCode());
            subCategoryInterface.setXrayConnectionFixedChargeRateInterfaces(xrayConnectionFixedChargeRateInterfaces);

            //Setting here xray connection information irrespective of any flag. Setting this detail in subcategory after discussion with NITISH sir
            List<XrayConnectionInformationInterface> xrayConnectionInformationInterfaces = xrayConnectionInformationDAO.getByConsumerNoAndStatus(consumerNo,"ACTIVE");
            if(xrayConnectionInformationInterfaces != null && xrayConnectionInformationInterfaces.size() == 1){
                XrayConnectionInformationInterface xrayConnectionInformationInterface = xrayConnectionInformationInterfaces.get(0);
                if(xrayConnectionInformationInterface != null){
                    subCategoryInterface.setXrayConnectionInformationInterface(xrayConnectionInformationInterface);
                }
            }

            //Setting purpose subsidy mapping for special behaviour for Purpose Of Installations.
            List<PurposeSubsidyMappingInterface> purposeSubsidyMappingInterfaces = purposeSubsidyMappingDAO.getBySubcategoryCodeAndEffectiveStartDateAndEffectiveEndDate(
                    subCategoryInterface.getCode(),
                    scheduleInterface.getBillDate());

            subCategoryInterface.setPurposeSubsidyMappingInterfaces(purposeSubsidyMappingInterfaces);

            if(consumerInformationInterface != null){
                boolean isBpl = consumerInformationInterface.getIsBPL();
                List<SubsidyInterface> subsidyInterfaces = subsidyDAO.getByCategoryAndIsBplAndSubcategoryCodeAndDate(consumerInformationInterface.getCategory(),isBpl,subCategoryInterface.getCode(),scheduleInterface.getBillDate());
                subCategoryInterface.setSubsidyInterfaces(subsidyInterfaces);

                boolean isEmployee = consumerInformationInterface.getIsEmployee();
                if(isEmployee){
                    List<EmployeeConnectionMappingInterface> employeeConnectionMappingInterfaces = employeeConnectionMappingDAO.getByConsumerNoAndStatus(consumerNo,"ACTIVE"); //Hardcoding for now
                    if(employeeConnectionMappingInterfaces != null && employeeConnectionMappingInterfaces.size() == 1){
                        EmployeeConnectionMappingInterface employeeConnectionMappingInterface = employeeConnectionMappingInterfaces.get(0);
                        if(employeeConnectionMappingInterface != null){
                            EmployeeMasterInterface employeeMasterInterface = employeeConnectionMappingInterface.getEmployeeMaster();
                            if(employeeMasterInterface != null){
                                EmployeeRebateInterface employeeRebateInterface = employeeRebateDAO.getByCodeAndDate(employeeMasterInterface.getRebateCode(),scheduleInterface.getBillDate());
                                subCategoryInterface.setEmployeeRebateInterface(employeeRebateInterface);
                            }
                        }
                    }
                }
            }
        }
    }

    private String getTariffFetchDate(Date tariffDate){
        if(tariffDate != null){
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
            return simpleDateFormat.format(tariffDate);
        }
        return null;
    }

    private void detachPayments(List<PaymentInterface> paymentInterfaces){
        final String methodName = "detachPayments() : ";
        logger.info(methodName + "called");
        if(paymentInterfaces != null){
            for(PaymentInterface paymentInterface : paymentInterfaces){
                entityManager.detach(paymentInterface);
            }
        }
    }

    private void detachAdjustments(List<AdjustmentInterface> adjustmentInterfaces){
        final String methodName = "detachAdjustments() : ";
        logger.info(methodName + "called");
        if(adjustmentInterfaces != null){
            for(AdjustmentInterface adjustmentInterface : adjustmentInterfaces){
                entityManager.detach(adjustmentInterface);
            }
        }
    }


    public HolderInterface prepareHolderForPDC(final ConsumerNoMasterInterface consumerNoMasterInterface, final TariffDetailInterface tariffDetailInterface, final ReadMasterInterface readMaster,final ScheduleInterface scheduleInterface){
        final String methodName = "prepareHolderForPDC() : ";
        logger.info(methodName + "called");
        HolderInterface holderInterface = null;
        if(consumerNoMasterInterface != null && scheduleInterface != null && tariffDetailInterface != null ){

            holderInterface = HolderFactory.buildHolder();

            final String consumerNo = consumerNoMasterInterface.getConsumerNo();

            //setting ConsumerNoMasterInterface
            holderInterface.setConsumerNoMaster(consumerNoMasterInterface);

            //setting ConsumerInformationInterface
            ConsumerInformationInterface consumerInformationInterface = consumerInformationDAO.getByConsumerNo(consumerNo);
            holderInterface.setConsumerInformationInterface(consumerInformationInterface);

            //setting ConsumerConnectionInformationInterface
            ConsumerConnectionInformationInterface consumerConnectionInformationInterface = consumerConnectionInformationDAO.getByConsumerNo(consumerNo);
            holderInterface.setConsumerConnectionInformationInterface(consumerConnectionInformationInterface);

            //setting ScheduleInterface

            holderInterface.setSchedule(scheduleInterface);

            String currentScheduleBillMonth = scheduleInterface.getBillMonth();
            ScheduleInterface previousScheduleInterface = null;
            if(currentScheduleBillMonth != null){
                String previousBillMonth = GlobalResources.getPreviousMonth(currentScheduleBillMonth);
                previousScheduleInterface = scheduleDAO.getByGroupNoAndBillMonthAndBillStatusAndSubmitted(scheduleInterface.getGroupNo(),previousBillMonth,ScheduleInterface.BILL_STATUS_COMPLETED,ScheduleInterface.SUBMITTED_Y);
            }
            //setting Last BillInterface
            BillInterface billInterface = billDAO.getTopByConsumerNoAndDeleted(consumerNo,BillInterface.DELETED_FALSE);
            holderInterface.setBill(billInterface);

            List<ConsumerMeterMappingInterface> consumerMeterMappingInterfaces = consumerMeterMappingDAO.getByConsumerNoAndMappingStatus(consumerNo,ConsumerMeterMappingInterface.STATUS_ACTIVE);
            ConsumerMeterMappingInterface consumerMeterMappingInterface = null;
            if(consumerMeterMappingInterfaces != null && consumerMeterMappingInterfaces.size() > 0){
                consumerMeterMappingInterface = consumerMeterMappingInterfaces.get(0);
                if(consumerMeterMappingInterface != null){
                    MeterMasterInterface meterMasterInterface = consumerMeterMappingInterface.getMeterMaster();
                    holderInterface.setMeterMasterInterface(meterMasterInterface);
                }
            }

            //setting rates and setting in passed TariffDetail
            prepareTariff(consumerNo,tariffDetailInterface,scheduleInterface,previousScheduleInterface,consumerInformationInterface,consumerMeterMappingInterface,consumerConnectionInformationInterface,billInterface);
            holderInterface.setTariffDetailInterface(tariffDetailInterface);

            long subCategoryCode = tariffDetailInterface.getSubcategoryCode();
            if(subCategoryCode == 101 || subCategoryCode == 103){
                long alternateSubCategoryCode = 0;
                if(subCategoryCode == 101){
                    alternateSubCategoryCode = 102;
                }else if(subCategoryCode == 103){
                    alternateSubCategoryCode = 104;
                }
                TariffDetailInterface alternateTariffDetailInterface = new TariffDetail();
                alternateTariffDetailInterface.setTariffCode("LV1.2");
                alternateTariffDetailInterface.setSubcategoryCode(alternateSubCategoryCode);
                prepareTariff(consumerNo,alternateTariffDetailInterface,scheduleInterface,previousScheduleInterface,consumerInformationInterface,consumerMeterMappingInterface,consumerConnectionInformationInterface,billInterface);

                holderInterface.setAlternateTariffDetailInterface(alternateTariffDetailInterface);
            }

            //setting LoadDetailInterfaces
            List<LoadDetailInterface> loadDetailInterfaces = loadDetailDAO.getByConsumerNo(consumerNo);
            holderInterface.setLoadDetails(loadDetailInterfaces);

            //setting Agriculture6MonthlyBill
            AgricultureBill6MonthlyInterface agricultureBill6MonthlyInterface = agricultureBill6MonthlyDAO.getTopByConsumerNo(consumerNo);
            holderInterface.setAgricultureBill6MonthlyBill(agricultureBill6MonthlyInterface);

            //setting Payments
            List<PaymentInterface> paymentInterfaces = paymentDAO.getByConsumerNoAndDeletedAndPosted(consumerNo,false,false);
            holderInterface.setPayments(paymentInterfaces);

            detachPayments(paymentInterfaces);

            //setting Adjustments
            List<AdjustmentInterface> adjustmentInterfaces = adjustmentDAO.getByConsumerNoAndPostedAndDeleted(consumerNo, AdjustmentInterface.POSTED_FALSE,AdjustmentInterface.DELETED_FALSE);
            holderInterface.setAdjustments(adjustmentInterfaces);

            detachAdjustments(adjustmentInterfaces);

            holderInterface.setFlatRateBillMonthOne("APR");
            holderInterface.setFlatRateBillMonthTwo("OCT");

            //Below code is for lv1.1
            //ReadMasterInterface currentReadMasterInterface = readMasterService.getLatestReadingByConsumerNoAndBillMonthAndReplacementFlag(consumerNo,scheduleInterface.getBillMonth(),ReadMasterInterface.REPLACEMENT_FLAG_NORMAL_READ);
            holderInterface.setCurrentReadMasterInterface(readMaster);

            String previousBillMonth = GlobalResources.getPreviousMonth(scheduleInterface.getBillMonth());
            ReadMasterInterface previousReadMasterInterface = readMasterService.getLatestReadingByConsumerNoAndBillMonthAndReplacementFlag(consumerNo,previousBillMonth,ReadMasterInterface.REPLACEMENT_FLAG_NORMAL_READ);
            if(previousReadMasterInterface == null){
                previousReadMasterInterface = readMasterService.getByConsumerNoAndReplacementFlag(consumerNo,ReadMasterInterface.REPLACEMENT_FLAG_NEW_CONNECTION);
            }
            holderInterface.setPreviousReadMasterInterface(previousReadMasterInterface);

            OnlinePaymentRebateInterface onlinePaymentRebateInterface = onlinePaymentRebateDAO.getByDate(scheduleInterface.getBillDate());
            holderInterface.setOnlinePaymentRebateInterface(onlinePaymentRebateInterface);

            PrepaidMeterRebateInterface prepaidMeterRebateInterface = prepaidMeterRebateDAO.getByDate(scheduleInterface.getBillDate());
            holderInterface.setPrepaidMeterRebateInterface(prepaidMeterRebateInterface);

            PromptPaymentIncentiveInterface promptPaymentIncentiveInterface = promptPaymentIncentiveDAO.getByDate(scheduleInterface.getBillDate());
            holderInterface.setPromptPaymentIncentiveInterface(promptPaymentIncentiveInterface);

        //Preetesh added on 8 Jan 2018
            //setting GMCAccountingInterface

            GMCAccountingInterface gmcAccountingInterface = gmcAccountingService.getGMCAccountingByReplacingCurrentValuesWithPreviousValues(consumerNo);
            holderInterface.setGmcAccountingInterface(gmcAccountingInterface);
            logger.info(methodName + "setting GMC interface   " + gmcAccountingInterface);
            detachGMCAccounting(gmcAccountingInterface);


            ///below code for Security deposit interest calculation

            List<SecurityDepositInterestInterface> securityDepositInterestInterfaces = securityDepositInterestDAO.getTop2ByConsumerNoOrderByIdDesc(consumerNo);
            if (securityDepositInterestInterfaces != null && securityDepositInterestInterfaces.size() > 1) {
                SecurityDepositInterestInterface previousMonthSecurityDepositInterestInterface = securityDepositInterestInterfaces.get(1);
                if (previousMonthSecurityDepositInterestInterface != null) {
                    Date startInterestCalculationDate = previousMonthSecurityDepositInterestInterface.getCalculationStartDate();
                    Date endInterestCalculationDate = previousMonthSecurityDepositInterestInterface.getCalculationEndDate();
                    if (startInterestCalculationDate != null && endInterestCalculationDate != null) {
                        List<SecurityDepositInterface> securityDepositInterfaces = securityDepositDAO.getByConsumerNoAndEffectiveStartDateAndEffectiveEndDateOrderByIdAsc(consumerNo, startInterestCalculationDate, endInterestCalculationDate);
                        holderInterface.setSecurityDepositInterfaces(securityDepositInterfaces);

                        List<SecurityDepositInterestRateInterface> securityDepositInterestRateInterface = securityDepositInterestRateDAO.getByEffectiveStartDateAndEffectiveEndDateOrderByIdAsc(startInterestCalculationDate, endInterestCalculationDate);
                        holderInterface.setSecurityDepositInterestRateInterfaces(securityDepositInterestRateInterface);

                        detachSecurityDepositInterest(previousMonthSecurityDepositInterestInterface);
                        holderInterface.setSecurityDepositInterestInterface(previousMonthSecurityDepositInterestInterface);
                    }
                }
            } else if (securityDepositInterestInterfaces != null && securityDepositInterestInterfaces.size() > 0) {
                SecurityDepositInterestInterface currentMonthSecurityDepositInterestInterface = securityDepositInterestInterfaces.get(0);
                if (currentMonthSecurityDepositInterestInterface != null) {
                    Date startInterestCalculationDate = consumerConnectionInformationInterface.getConnectionDate();
                    Date endInterestCalculationDate = scheduleInterface.getBillDate();
                    logger.info(methodName + "start date is "+startInterestCalculationDate);
                    logger.info(methodName + "end  date is "+endInterestCalculationDate);
                    if (startInterestCalculationDate != null && endInterestCalculationDate != null) {
                        List<SecurityDepositInterface> securityDepositInterfaces = securityDepositDAO.getByConsumerNoAndEffectiveStartDateAndEffectiveEndDateOrderByIdAsc(consumerNo, startInterestCalculationDate, endInterestCalculationDate);
                        holderInterface.setSecurityDepositInterfaces(securityDepositInterfaces);

                        List<SecurityDepositInterestRateInterface> securityDepositInterestRateInterface = securityDepositInterestRateDAO.getByEffectiveStartDateAndEffectiveEndDateOrderByIdAsc(startInterestCalculationDate,endInterestCalculationDate);
                        holderInterface.setSecurityDepositInterestRateInterfaces(securityDepositInterestRateInterface);
                    }
                }

            }

        }else{
            logger.info(methodName+" one of the parameter is null");
        }
        return holderInterface;
    }



    private void detachGMCAccounting(GMCAccountingInterface gmcAccountingInterface){
        final String methodName = "detachGMCAccounting() : ";
        logger.info(methodName + "called");
        if(gmcAccountingInterface != null){
            entityManager.detach(gmcAccountingInterface);
        }
    }

    private void detachSecurityDepositInterest(SecurityDepositInterestInterface securityDepositInterestInterface){
        final String methodName = "detachSecurityDepositInterest() : ";
        logger.info(methodName + "called");
        if(securityDepositInterestInterface != null){
            entityManager.detach(securityDepositInterestInterface);
        }
    }

}
