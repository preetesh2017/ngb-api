package com.mppkvvcl.ngbapi.services;

import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.FCAInterface;
import com.mppkvvcl.ngbdao.daos.FCADAO;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Date;

@Service
public class FCAService {
    /**
     * Asking Spring to inject FCARepository dependency in the variable
     * so that we get various handles for performing CRUD operation on  table
     * at the backend Database
     */
    @Autowired
    private FCADAO fcadao;

    /**
     * getting whole application logger object for performing logs. This logger will only be
     * used in this class methods only.
     */
    private static final Logger logger = GlobalResources.getLogger(FCAService.class);


    /**
     * This method takes Date  and fetch applicable fca object against it.
     * Return fca if successful else return null.<br><br>
     * param date<br>
     * return fca<br>
     */
    public FCAInterface getByDate(Date date){
        String methodName = "getByDate():";
        FCAInterface fca = null;
        logger.info(methodName+" Calling fcadao to find fca with date " + date);
        if(date != null){
            fca = fcadao.getByDate(date);
            if (fca != null){
                logger.info(methodName+" Successfully fetched fca record for date " + date);
            } else {
                logger.error(methodName+" unable to fetch record for date " + date);
            }
        } else {
            logger.error(methodName+"got request null");
        }
        return  fca;
    }
}
