package com.mppkvvcl.ngbapi.services;

import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.ConsumerConnectionAreaInformationInterface;
import com.mppkvvcl.ngbdao.daos.ConsumerConnectionAreaInformationDAO;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by SUMIT on 14-06-2017.
 */
@Service
public class ConsumerConnectionAreaInformationService {
    /**
     * Asking Spring to inject ConsumerConnectionAreaInformationRepository dependency in the variable
     * so that we get various handles for performing CRUD operation on consumer_connection_area_information table
     * at the backend Database
     */
    @Autowired
    private ConsumerConnectionAreaInformationDAO consumerConnectionAreaInformationDAO;
    /**
     * getting whole application logger object for performing logs. This logger will only be
     * used in this class methods only.
     */
    private static final Logger logger = GlobalResources.getLogger(ConsumerConnectionAreaInformationService.class);

    /**
     * This method takes consumerConnectionAreaInformation object and insert it into the backend database.
     * Return insertedConsumerConnectionAreaInformation if insertion successful else return null.<br><br>
     * param consumerConnectionAreaInformation
     * return insertedConsumerConnectionAreaInformation
     */
    public ConsumerConnectionAreaInformationInterface insert(ConsumerConnectionAreaInformationInterface consumerConnectionAreaInformation) {
        String methodName = "insert: ";
        ConsumerConnectionAreaInformationInterface insertedConsumerConnectionAreaInformation = null;
        logger.info(methodName + " consumer connection area information  started");
        if (consumerConnectionAreaInformation != null) {
            insertedConsumerConnectionAreaInformation = consumerConnectionAreaInformationDAO.add(consumerConnectionAreaInformation);
            if (insertedConsumerConnectionAreaInformation != null) {
                logger.info(methodName + "successfully inserted ConsumerConnectionAreaInformation row");
            } else {
                logger.error(methodName + "Insertion of ConsumerConnectionAreaInformation failed");
            }
        } else {
            logger.info(methodName + "ConsumerConnectionAreaInformation to insert is null ");
        }
        return insertedConsumerConnectionAreaInformation;
    }

    /**
     * This method takes consumerNo and fetch consumerConnectionAreaInformation from backend database.
     * Return consumerConnectionAreaInformation if successful else return null.<br><br>
     * param consumerNo<br>
     * return consumerConnectionAreaInformation
     */

    public ConsumerConnectionAreaInformationInterface getByConsumerNo(String consumerNo) {

        String methodName = " getByConsumerNo(): ";
        logger.info(methodName + " service method called fetching area information for consumer " + consumerNo);
        ConsumerConnectionAreaInformationInterface consumerConnectionAreaInformation = null;
        if (consumerNo != null) {
            consumerNo.trim();
            consumerConnectionAreaInformation = consumerConnectionAreaInformationDAO.getByConsumerNo(consumerNo);
            if (consumerConnectionAreaInformation != null) {
                logger.info(methodName + " Successfully retrieved area information for consumer number" + consumerNo);
            } else {
                logger.error(methodName + " nothing retrieved for consumer number:" + consumerNo);
            }
        } else {
            logger.error(methodName + "consumerNo passed is null in method.");
        }
        return consumerConnectionAreaInformation;
    }


    /**
     * This method takes consumerNo and fetch groupNo from backend database.
     * Return groupNo if successful else return null.<br><br>
     * param consumerNo<br>
     * return groupNo
     */
    public String getGroupOfConsumerByConsumerNo(String consumerNo) throws Exception {
        String methodName = " getGroupOfConsumerByConsumerNo(): ";
        logger.info(methodName + " service method called fetching group no for consumer " + consumerNo);
        ConsumerConnectionAreaInformationInterface consumerConnectionAreaInformation = null;
        String groupNo = null;
        consumerNo.trim();
        if (consumerNo != null) {
            consumerConnectionAreaInformation = consumerConnectionAreaInformationDAO.getByConsumerNo(consumerNo);
            if (consumerConnectionAreaInformation != null ) {
                logger.info(methodName + " Successfully retrieved area information for consumer number" + consumerNo);
                groupNo = consumerConnectionAreaInformation.getGroupNo();
            } else {
                logger.error(methodName + " nothing retrieved for consumer number:" + consumerNo);
                throw new Exception("Consumer Connection Area Information not found for Consumer");
            }
        } else {
            logger.error(methodName + "consumerNo passed is null in method.");
            throw new Exception("ConsumerNo passed is null in method.");
        }
        return groupNo;
    }

}


