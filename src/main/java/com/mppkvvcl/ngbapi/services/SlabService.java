package com.mppkvvcl.ngbapi.services;

import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.SlabInterface;
import com.mppkvvcl.ngbdao.daos.SlabDAO;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by MITHLESH on 14-07-2017.
 */
@Service
public class SlabService {

    /**
     * getting whole application logger object for performing logs. This logger will only be
     * used in this class methods only.
     */
    private Logger logger = GlobalResources.getLogger(SlabService.class);
    /**
     *Asking Spring to inject SlabRepository dependency in the variable
     * so that we get various handles for performing CRUD operation on Slab table
     * at the backend Database
     */
    @Autowired
    private SlabDAO slabDAO;

    /**
     * This getByTariffId() method is used to fetch object of Slab using input parameter tariffId <br>
     * Return list if successful else return null.<br><br>
     * param tariffId<br><br>
     * return slabs<br>
     */
    public List<SlabInterface> getByTariffId(long tariffId){
        String methodName = "getByTariffId() : ";
        logger.info(methodName + "Request received from SlabContoller");
        List<SlabInterface> slabs = null;
        if(tariffId != 0){
            logger.info(methodName + "Calling repo to fetch List<Slab> with tariffId in parameter as :"+tariffId);
            slabs = slabDAO.getByTariffId(tariffId);
            if(slabs != null){
                if(slabs.size() > 0){
                    logger.info(methodName + "successfully got List<Slab> from repo with size"+slabs.size());
                }else{
                    logger.info(methodName + "successfully got List<Slab> from repo with size"+slabs.size());
                }
            }else{
                logger.error(methodName + "Fetched List<Slab> is null");
            }
        }else{
            logger.error(methodName + "Received tariffId is zero");
        }
        return slabs;
    }

    /**
     * This getByTariffIdAndSlabId() method is used to fetch List of Slab using input parameters tariffId and slabId<br>
     * Return list if successful else return null.<br><br>
     * param tariffId<br>
     * param slabId<br><br>
     * return slabs<br>
     */
    public List<SlabInterface> getByTariffIdAndSlabId(long tariffId , String slabId){
        String methodName = "getByTariffIdAndSlabId() : ";
        logger.info(methodName + "Request Received with tariffId and slabId");
        List<SlabInterface> slabs = null;
        if(tariffId != 0 && slabId != null){
            logger.info(methodName + "Calling repo to fetch List<Slab> with tariffId as "+tariffId+" and slabId as "+slabId);
            slabs = slabDAO.getByTariffIdAndSlabIdOrderById(tariffId , slabId);
            if(slabs != null){
                if(slabs.size() != 0){
                    logger.info(methodName + "successfully got List<Slab> from repo with size"+slabs.size());
                }else{
                    logger.info(methodName + "successfully got List<Slab> from repo with size"+slabs.size());
                }
            }else{
                logger.error(methodName + "Fetched List<Slab> is null");
            }
        }else{
            logger.error(methodName + "Received tariffId or slabId is null");
        }
        return slabs;
    }
}
