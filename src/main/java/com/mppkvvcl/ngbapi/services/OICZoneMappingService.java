package com.mppkvvcl.ngbapi.services;

import com.mppkvvcl.ngbinterface.interfaces.OICZoneMappingInterface;
import com.mppkvvcl.ngbdao.daos.OICZoneMappingDAO;
import com.mppkvvcl.ngbapi.utility.GlobalResources;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * coded by nitish on 23-07-2017
 */
@Service
public class OICZoneMappingService {

    /**
     * Getting whole logger object from global resources for current class.
     */
    private static final Logger logger = GlobalResources.getLogger(OICZoneMappingService.class);

    @Autowired
    private OICZoneMappingDAO oicZoneMappingDAO;

    /**
     * gives underlying record from passed username
     * @param username
     * @return
     */
    public OICZoneMappingInterface getByUsername(final String username){
        final String methodName = "getByUsername() : ";
        logger.info(methodName + "called with username " + username);
        OICZoneMappingInterface oicZoneMapping = null;
        if(username != null){
            oicZoneMapping = oicZoneMappingDAO.getByUsername(username);
        }
        return oicZoneMapping;
    }

    /**
     * gives underlying record from passed zoneId
     * @param zoneId
     * @return
     */
    public OICZoneMappingInterface getByZoneId(final long zoneId){
        final String methodName = "getByZoneId() : ";
        logger.info(methodName + "called with zone id " + zoneId);
        OICZoneMappingInterface oicZoneMapping = null;
        oicZoneMapping = oicZoneMappingDAO.getByZoneId(zoneId);
        return oicZoneMapping;
    }
}
