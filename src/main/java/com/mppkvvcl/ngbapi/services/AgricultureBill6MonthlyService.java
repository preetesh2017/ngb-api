package com.mppkvvcl.ngbapi.services;

import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.AgricultureBill6MonthlyInterface;
import com.mppkvvcl.ngbdao.daos.AgricultureBill6MonthlyDAO;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AgricultureBill6MonthlyService {
    /**
     * getting whole application logger object for performing logs. This logger will only be
     * used in this class methods only.
     */
    Logger logger = GlobalResources.getLogger(AgricultureBill6MonthlyService.class);

    /**
     * Asking spring to inject dependency on AgricultureBill6MonthlyRepository.
     * So that we can use it to perform various operation through repo.
     */
    @Autowired
    AgricultureBill6MonthlyDAO agricultureBill6MonthlyDAO;


    public AgricultureBill6MonthlyInterface getTopByConsumerNo(String consumerNo){
        String methodName = "getTopByConsumerNo() : ";
        logger.info(methodName + "called for consumer no: "+consumerNo);
        AgricultureBill6MonthlyInterface agricultureBill6Monthly = null;
        if(consumerNo == null){
            logger.error(methodName + "Consumer No input found null. Please check.");
            return agricultureBill6Monthly;
        }
        agricultureBill6Monthly = agricultureBill6MonthlyDAO.getTopByConsumerNoOrderByIdDesc(consumerNo);
        return agricultureBill6Monthly;
    }
}
