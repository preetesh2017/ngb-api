package com.mppkvvcl.ngbapi.services;

import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbdao.daos.GMCDAO;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by ANSHIKA on 27-07-2017.
 */
@Service
public class GMCService {

    /**
     * Requesting spring to get singleton GMCRepository object.
     */
    @Autowired
    private GMCDAO gmcdao;

    /**
     * Getting whole logger object from GlobalResources for current class.
     */
    private static final Logger logger = GlobalResources.getLogger(GMCService.class);
}
