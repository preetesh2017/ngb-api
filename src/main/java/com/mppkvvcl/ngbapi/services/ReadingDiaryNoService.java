package com.mppkvvcl.ngbapi.services;

import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.GroupInterface;
import com.mppkvvcl.ngbinterface.interfaces.ReadingDiaryNoInterface;
import com.mppkvvcl.ngbdao.daos.ReadingDiaryNoDAO;
import com.mppkvvcl.ngbentity.beans.ErrorMessage;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ReadingDiaryNoService {
    /**
     * getting whole application logger object for performing logs. This logger will only be
     * used in this class methods only.
     */
    private static final Logger logger = GlobalResources.getLogger(ReadingDiaryNoService.class);

    /**
     * Asking Spring to inject ReadingDiaryNoDAO dependency in the variable
     * so that we get various handles for performing CRUD operation on ReadingDiaryNo table
     * at the backend Database
     */
    @Autowired
    private ReadingDiaryNoDAO readingDiaryNoDAO;

    @Autowired
    private GroupService groupService;

    public List<ReadingDiaryNoInterface> getByGroupNo(String groupNo){
        String methodName = "getByGroupNo() : ";
        List<ReadingDiaryNoInterface> readingDiaryNos = null;
        logger.info(methodName + "called with input group no " + groupNo);
        if(groupNo != null){
            logger.info(methodName + "calling repository for getting all reading diary of group no :" + groupNo);
            readingDiaryNos = readingDiaryNoDAO.getByGroupNo(groupNo);
        }else{
            logger.error(methodName + "got request to handle has group no null.Please check.");
        }
        return readingDiaryNos;
    }

    /**
     * CreatedBy: Preetesh , Date 1/Nov/2017
     * @param readingDiaryNoToCreate
     * @param errorMessage
     * @return
     */
    public ReadingDiaryNoInterface create(ReadingDiaryNoInterface readingDiaryNoToCreate, ErrorMessage errorMessage) {
        String methodName = " create() : ";
        logger.info(methodName + "called");
        ReadingDiaryNoInterface createdReadingDiary = null;
        if(readingDiaryNoToCreate == null) {
            logger.error(methodName + "Input param ReadingDiary found null");
            errorMessage.setErrorMessage("Input param readingDiary found null");
            return null;
        }
        String readingDiaryNo = readingDiaryNoToCreate.getReadingDiaryNo();
        String groupNo =  readingDiaryNoToCreate.getGroupNo();

        if(readingDiaryNo == null || groupNo == null){
            logger.error(methodName + "ReadingDiaryNo object's parameters found null");
            errorMessage.setErrorMessage("ReadingDiaryNo object's parameters found null");
            return null;
        }

        GroupInterface groupInterface = groupService.getByGroupNo(groupNo);

        if(groupInterface == null){
            logger.error(methodName + "group given in ReadingDiary object does to exist");
            errorMessage.setErrorMessage("group given in ReadingDiary object does to exist");
            return null;
        }

        ReadingDiaryNoInterface existingReadingDiaryNo = readingDiaryNoDAO.getByGroupNoAndReadingDiaryNo(groupNo,readingDiaryNo);
        if(existingReadingDiaryNo != null){
            logger.error(methodName + "reading diary already present");
            errorMessage.setErrorMessage("reading diary already present");
            return null;
        }

        createdReadingDiary = readingDiaryNoDAO.add(readingDiaryNoToCreate);
        if (createdReadingDiary == null){
            logger.error(methodName + "Unable to create ReadingDiary");
            errorMessage.setErrorMessage("Unable to create ReadingDiary");
            return null;
        }
        return createdReadingDiary;
    }

    public ReadingDiaryNoInterface getByGroupNoAndReadingDiaryNo(String groupNo, String readingDiaryNo) {
        final String methodName = "getByGroupNoAndReadingDiaryNo() : ";
        logger.info(methodName + "called");
        ReadingDiaryNoInterface readingDiary = null;
        if(groupNo != null && readingDiaryNo != null){
            readingDiary = readingDiaryNoDAO.getByGroupNoAndReadingDiaryNo(groupNo,readingDiaryNo);
        }
        return readingDiary;
    }
}
