package com.mppkvvcl.ngbapi.services;

import com.mppkvvcl.ngbapi.factories.BPLConnectionMappingFactory;
import com.mppkvvcl.ngbapi.factories.ConsumerInformationHistoryFactory;
import com.mppkvvcl.ngbapi.factories.EmployeeConnectionMappingFactory;
import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbapi.utility.NGBAPIUtility;
import com.mppkvvcl.ngbdao.daos.ConsumerInformationDAO;
import com.mppkvvcl.ngbinterface.interfaces.*;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Column;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by SUMIT on 08-06-2017.
 */
@Service
public class ConsumerInformationService {
    /**
     * getting whole application logger object for performing logs. This logger will only be
     * used in this class methods only.
     */
    private static final Logger logger = GlobalResources.getLogger(ConsumerInformationService.class);

    /**
     * Asking spring to inject EntityManager for underlying hibernate
     * for creating sessions and transactions;
     */
    @PersistenceContext
    private EntityManager entityManager;

    /**
     * Asking Spring to inject ConsumerInformationRepository dependency in the variable
     * so that we get various handles for performing CRUD operation on consumer_information table
     * at the backend Database
     */
    @Autowired
    private ConsumerInformationDAO consumerInformationDAO;

    @Autowired
    private ConsumerInformationHistoryService consumerInformationHistoryService;

    @Autowired
    private BillService billService;

    @Autowired
    private ScheduleService scheduleService;

    @Autowired
    private BPLConnectionMappingService bplConnectionMappingService;

    @Autowired
    private EmployeeConnectionMappingService employeeConnectionMappingService;

    @Autowired
    private TariffDescriptionService tariffDescriptionService;

    @Autowired
    private ConsumerNoMasterService consumerNoMasterService;

    @Autowired
    private EmployeeMasterService employeeMasterService;

    @Autowired
    private GlobalResources globalResources;

    /**
     * This service method returns ConsumerInformation object for passed consumerNo.<br><br>
     * param consumerNo<br>
     * return consumerInformation
     */


    public ConsumerInformationInterface getConsumerInformationByConsumerNo(String consumerNo){
        String methodName = "getConsumerInformationByConsumerNo() : ";
        logger.info(methodName + "called "+consumerNo);
        ConsumerInformationInterface consumerInformation=null;
        if(consumerNo != null) {
            consumerInformation = consumerInformationDAO.getByConsumerNo(consumerNo);
        }
        return consumerInformation;
    }

    /**
     * This service method returns ConsumerInformation object for passed consumerName.<br><br>
     * param consumerName<br>
     * return consumersInformation
     *
     */
    public List<ConsumerInformationInterface> getConsumersInformationByConsumerName(String consumerName){
        String methodName = "getConsumersInformationByConsumerName : ";
        List<ConsumerInformationInterface> consumersInformation=null;
        logger.info(methodName + " Got input consumerName "+consumerName);
        if(consumerName != null){
            logger.info(methodName + " calling ConsumerInformationRepository to get Consumers Information");
            consumersInformation=consumerInformationDAO.getByConsumerName(consumerName);
            if(consumersInformation != null){
                if(consumersInformation.size()>0){
                    logger.info(methodName + "successfully got ConsumersInformation as "+consumersInformation);
                }else {
                    logger.info(methodName + "consumer information list returned has zero count");
                }
            }else{
                logger.info(methodName + "Unable to get ConsumersInformation with input parameter consumerName : "+consumerName);
            }
        }else{
            logger.info(methodName + "input parameter consumerName is null");
        }
        return consumersInformation;
    }

//added by Preetesh 10/09/2017

    public ConsumerInformationInterface update(ConsumerInformationInterface consumerInformation) {
        ConsumerInformationInterface updatedConsumerInformation=null;
        if(consumerInformation != null){
            consumerInformation.setUpdatedBy(GlobalResources.getLoggedInUser());
            consumerInformation.setUpdatedOn(GlobalResources.getCurrentDate());
            updatedConsumerInformation =consumerInformationDAO.update(consumerInformation);
        }
        return updatedConsumerInformation;
    }

    public ConsumerInformationInterface updateConsumerInformation(ConsumerInformationInterface consumerInformationToUpdate)throws Exception {

        String methodName = "updateConsumerInformation() : ";
        ConsumerInformationInterface updatedConsumerInformation=null;
        logger.info(methodName + " Got input "+consumerInformationToUpdate);
        if(consumerInformationToUpdate == null ){
            logger.info(methodName + "one or more input parameter found null");
            throw new Exception("one or more input parameter found null");
        }
        String consumerNo = consumerInformationToUpdate.getConsumerNo();
        ConsumerInformationInterface consumerInformation = getConsumerInformationByConsumerNo(consumerNo);
        if(consumerInformation == null){
            logger.info(methodName + "consumer not found with consumer-no "+consumerNo);
            throw new Exception("consumer not found with consumer-no "+consumerNo);
        }

        updatedConsumerInformation = update(consumerInformationToUpdate);
        if(updatedConsumerInformation == null){
            logger.info(methodName + "updating failed ");
            throw new Exception(" updation failed ");
        }
        return updatedConsumerInformation;
    }

    /**
     * Developed by Nitish on 18112017.
     * Understand the code and concept before changing anything or using the code.
     * The below method updates columns of the consumer information for which a key
     * is found in the provided method.
     *
     * @param consumerNo
     * @param consumerInformationMap
     * @return
     * @throws Exception
     */
    @Transactional(rollbackFor = Exception.class)
    public ConsumerInformationInterface updateConsumerInformation(final String consumerNo,Map<String,String> consumerInformationMap) throws Exception{
        final String methodName = "updateConsumerInformation() : ";
        logger.info(methodName + "called for " + consumerNo);

        ConsumerInformationInterface updatedConsumerInformationInterface = null;
        if(consumerNo == null || consumerInformationMap == null){
            logger.error(methodName + "Inputs are null");
            return null;
        }

        ConsumerInformationInterface consumerInformationInterface = consumerInformationDAO.getByConsumerNo(consumerNo);
        if(consumerInformationInterface == null){
            logger.error(methodName + "No ConsumerInformation Found");
            return null;
        }
        logger.info(methodName + "Got consumer as " + consumerInformationInterface);

        detachConsumerInformation(consumerInformationInterface);

        List<ConsumerInformationHistoryInterface> consumerInformationHistoryInterfaces = new ArrayList<>();
        for(Map.Entry<String,String> entry : consumerInformationMap.entrySet()){
            if(entry == null) continue;

            String key = entry.getKey();
            String value = entry.getValue();

            //continuing if key has isBPL or isEmployee. Since they are not updatable from this method
            if(key.contains("isBPL") || key.contains("isEmployee")){
                logger.info(methodName + "Key " + key +" has isBPL or isEmployee, which is not allowed to be updated from this method.continuing");
                continue;
            }

            Method setterMethod = NGBAPIUtility.getSetterForField(key,consumerInformationInterface);
            Method getterMethod = NGBAPIUtility.getGetterForField(key,consumerInformationInterface);

            if(setterMethod == null && getterMethod == null){
                logger.error(methodName + "Setter/Getter Method Not found for key " + key + " continuing");
                continue;
            }
            try{
                Field field = NGBAPIUtility.getFieldByName(key,consumerInformationInterface);
                if(field == null){
                    logger.error(methodName + "Field Not found for key " + key + " continuing");
                    continue;
                }
                Column columnAnnotation = NGBAPIUtility.getColumnAnnotationByNameValue(field);
                if(columnAnnotation == null){
                    logger.error(methodName + "ColumnAnnotation Not found for key " + key + " continuing");
                    continue;
                }

                logger.info(methodName + "calling " + getterMethod.getName() + " to get old value ");
                Object getResult = getterMethod.invoke(consumerInformationInterface,null);
                String olderValue = "";
                if(getResult != null){
                    olderValue = getResult.toString();
                }
                logger.info(methodName + "Old Value is " + olderValue + " new value is " + value);
                ConsumerInformationHistoryInterface consumerInformationHistoryInterface = ConsumerInformationHistoryFactory.build();
                consumerInformationHistoryInterface.setConsumerNo(consumerNo);
                consumerInformationHistoryInterface.setPropertyName(columnAnnotation.name());
                consumerInformationHistoryInterface.setPropertyValue(olderValue);
                consumerInformationHistoryInterface.setEndDate(GlobalResources.getCurrentDate());

                //Adding to list to insert after every column update
                consumerInformationHistoryInterfaces.add(consumerInformationHistoryInterface);

                logger.info(methodName + "ConsumerInformation before updating " + consumerInformationInterface);
                logger.info(methodName + "calling " + setterMethod.getName() + " with value " + value);

                setterMethod.invoke(consumerInformationInterface,value);
                setUpdateAuditDetails(consumerInformationInterface);
                logger.info(methodName + "ConsumerInformation after updating " + consumerInformationInterface);
            }catch (Exception exception){
                logger.error(methodName + "Exception while calling getter");
            }
        }


        if(consumerInformationMap.size() != consumerInformationHistoryInterfaces.size()){
            logger.error(methodName + "Map Size and HistoryInformation does not match. Aborting and rolling back");
            throw new Exception("Map Size and HistoryInformation does not match. Aborting and rolling back");
        }

        List<ConsumerInformationHistoryInterface> insertedConsumerInformationHistoryInterfaces = consumerInformationHistoryService.insert(consumerInformationHistoryInterfaces);
        if(insertedConsumerInformationHistoryInterfaces == null || insertedConsumerInformationHistoryInterfaces.size() != consumerInformationHistoryInterfaces.size()){
            logger.error(methodName + "Inserted HistoryInformations not equal to passed HistoryInformations.Aborting and rolling back");
            throw new Exception("Map Size and HistoryInformation does not match. Aborting and rolling back");
        }

        updatedConsumerInformationInterface = consumerInformationDAO.update(consumerInformationInterface);
        if(updatedConsumerInformationInterface == null){
            logger.error(methodName + "Unable to Inserted ConsumerInformation.Aborting and rolling back");
            throw new Exception("Unable to Inserted ConsumerInformation.Aborting and rolling back");
        }
        return updatedConsumerInformationInterface;
    }

    @Transactional(rollbackFor = Exception.class)
    public ConsumerInformationInterface updateBPLConsumerInformation(final String consumerNo, Map<String, String> consumerInformationMap) throws Exception {
        final String methodName = "updateBPLConsumerInformation() : ";
        logger.info(methodName + "called for " + consumerNo);

        ConsumerInformationInterface updatedConsumerInformationInterface = null;
        if (consumerNo == null || consumerInformationMap == null) {
            logger.error(methodName + "Inputs are null");
            return null;
        }

        ConsumerInformationInterface consumerInformationInterface = consumerInformationDAO.getByConsumerNo(consumerNo);
        if (consumerInformationInterface == null) {
            logger.error(methodName + "No ConsumerInformation Found");
            return null;
        }
        logger.info(methodName + "Got consumer as " + consumerInformationInterface);

        detachConsumerInformation(consumerInformationInterface);

        List<ConsumerInformationHistoryInterface> consumerInformationHistoryInterfaces = new ArrayList<>();

        String isBPLKey = "isBPL";
        String bplNoKey = "bplNo";
        String value = consumerInformationMap.get(isBPLKey);
        boolean isBPL = false;
        String bplNo = null;
        if (value != null && value.equalsIgnoreCase("true")) {
            isBPL = true;
            bplNo = consumerInformationMap.get(bplNoKey);
            if (bplNo == null) {
                logger.error(methodName + "isBPL is true & BPLNo is NULL.Aborting");
                return null;
            }
        }
        Method setterMethod = NGBAPIUtility.getSetterForField(isBPLKey, consumerInformationInterface);
        Method getterMethod = NGBAPIUtility.getGetterForField(isBPLKey, consumerInformationInterface);

        if (setterMethod == null && getterMethod == null) {
            logger.error(methodName + "Setter/Getter Method Not found for isBPLKey " + isBPLKey + " continuing");
            return null;
        }
        Field field = NGBAPIUtility.getFieldByName(isBPLKey, consumerInformationInterface);
        logger.info(methodName + "field  " + field);
        if (field == null) {
            logger.error(methodName + "Field Not found for isBPLKey " + isBPLKey + " continuing");
            return null;
        }
        Column columnAnnotation = NGBAPIUtility.getColumnAnnotationByNameValue(field);
        logger.info(methodName + "columnAnnotation " + columnAnnotation);
        if (columnAnnotation == null) {
            logger.error(methodName + "ColumnAnnotation Not found for isBPLKey " + isBPLKey + " continuing");
            return null;
        }

        logger.info(methodName + "calling " + getterMethod.getName() + " to get old value ");
        Object getResult = getterMethod.invoke(consumerInformationInterface, null);
        String olderValue = "";
        if (getResult != null) {
            olderValue = getResult.toString();
        }
        String billMonthToInsert = null;
        BillInterface bill = billService.getLatestBillByConsumerNo(consumerNo);
        if (bill != null && bill.getBillMonth() != null) {
            billMonthToInsert = bill.getBillMonth();
        } else {
            ScheduleInterface schedule = scheduleService.getLatestCompletedScheduleByConsumerNo(consumerNo);
            if (schedule == null || schedule.getBillMonth() == null) {
                logger.error(methodName + "no completed schedule found");
                throw new Exception("no completed schedule found");
            }
            billMonthToInsert = schedule.getBillMonth();
        }

        logger.info(methodName + "Old Value is " + olderValue + " new value is " + value);
        ConsumerInformationHistoryInterface consumerInformationHistoryInterface = ConsumerInformationHistoryFactory.build();
        consumerInformationHistoryInterface.setConsumerNo(consumerNo);
        consumerInformationHistoryInterface.setPropertyName(columnAnnotation.name());
        consumerInformationHistoryInterface.setPropertyValue(olderValue);
        consumerInformationHistoryInterface.setEndDate(GlobalResources.getCurrentDate());
        consumerInformationHistoryInterface.setEndBillMonth(billMonthToInsert);

        //Adding to list to insert after every column update
        consumerInformationHistoryInterfaces.add(consumerInformationHistoryInterface);

        logger.info(methodName + "ConsumerInformation before updating " + consumerInformationInterface);
        logger.info(methodName + "calling " + setterMethod.getName() + " with value " + value);

        if (isBPL) {
            setterMethod.invoke(consumerInformationInterface, true);
        } else if (value != null && value.equalsIgnoreCase("false")) {
            setterMethod.invoke(consumerInformationInterface, false);
        }
        setUpdateAuditDetails(consumerInformationInterface);
        logger.info(methodName + "ConsumerInformation after updating " + consumerInformationInterface);

        List<ConsumerInformationHistoryInterface> insertedConsumerInformationHistoryInterfaces = consumerInformationHistoryService.insert(consumerInformationHistoryInterfaces);
        if (insertedConsumerInformationHistoryInterfaces == null || insertedConsumerInformationHistoryInterfaces.size() != consumerInformationHistoryInterfaces.size()) {
            logger.error(methodName + "Inserted HistoryInformations not equal to passed HistoryInformations.Aborting and rolling back");
            throw new Exception("Map Size and HistoryInformation does not match. Aborting and rolling back");
        }

        updatedConsumerInformationInterface = consumerInformationDAO.update(consumerInformationInterface);
        if (updatedConsumerInformationInterface == null) {
            logger.error(methodName + "Unable to Inserted ConsumerInformation.Aborting and rolling back");
            throw new Exception("Unable to Inserted ConsumerInformation.Aborting and rolling back");
        }

        ConsumerNoMasterInterface consumerNoMaster = consumerNoMasterService.getByConsumerNo(consumerNo);
        if (consumerNoMaster == null) {
            logger.error(methodName + "Given consumer no doesn't exist in  ConsumerNoMaster");
            throw new Exception("consumer no is invalid");
        }
        String status = consumerNoMaster.getStatus();
        if (status == null) {
            logger.error("status is null for consumer no :" + consumerNo);
            throw new Exception("status is null");
        }
        //Below validation Checks whether consumer is INACTIVE (PDC)
        if (status.equalsIgnoreCase(ConsumerNoMasterInterface.STATUS_INACTIVE)) {
            logger.error(methodName + "Consumer is " + status);
            throw new Exception("consumer is in-active");
        }

        //creating new BPL connection mapping
        if (isBPL) {
            boolean isEmployee = checkIsEmployeeByConsumerNo(consumerNo);
            if (isEmployee) {
                logger.error(methodName + "Employee mapping is present for consumer no");
                throw new Exception("consumer is employee, not allowed to tag BPL");
            }
            BPLConnectionMappingInterface bplConnectionMappingInterfaceToInsert = BPLConnectionMappingFactory.build();
            BPLConnectionMappingInterface insertedBPLConnectionMapping = null;
            boolean bplNoUsed = bplConnectionMappingService.checkBPLNoUsed(bplNo, BPLConnectionMappingInterface.STATUS_ACTIVE);
            if (bplNoUsed) {
                logger.error(methodName + "given BPL no is active to other consumer.");
                throw new Exception("given BPL no is active to other consumer");
            }
            boolean isDomestic = tariffDescriptionService.checkTariffDescriptionByConsumerNo(consumerNo, TariffDescriptionInterface.TARIFF_DESCRIPTION_DOMESTIC);
            if (!isDomestic) {
                logger.error(methodName + "consumer category should be domestic,for BPL.");
                throw new Exception("consumer category should be domestic,for BPL.");
            }

            bplConnectionMappingInterfaceToInsert.setConsumerNo(consumerNo);
            bplConnectionMappingInterfaceToInsert.setBplNo(bplNo);
            bplConnectionMappingInterfaceToInsert.setStatus(BPLConnectionMappingInterface.STATUS_ACTIVE);
            bplConnectionMappingInterfaceToInsert.setStartBillMonth(GlobalResources.getNextMonth(billMonthToInsert));
            bplConnectionMappingInterfaceToInsert.setStartDate(GlobalResources.getCurrentDate());

            insertedBPLConnectionMapping = bplConnectionMappingService.insert(bplConnectionMappingInterfaceToInsert);
            if (insertedBPLConnectionMapping == null) {
                logger.error(methodName + "some error to insert new BPL connection mapping");
                throw new Exception("some error to insert new BPL connection mapping");
            }
        } else if (value != null && value.equalsIgnoreCase("false")) {
            List<BPLConnectionMappingInterface> bplConnectionMappingInterfaces = bplConnectionMappingService.getByConsumerNoAndStatus(consumerNo, BPLConnectionMappingInterface.STATUS_ACTIVE);
            if (bplConnectionMappingInterfaces == null || bplConnectionMappingInterfaces.size() != 1) {
                logger.error(methodName + "ACTIVE BPL Connection Mapping not found.Aborting");
                throw new Exception("ACTIVE BPL Connection Mapping not found.Aborting");
            }
            BPLConnectionMappingInterface existingBPLConnectionMappingInterface = bplConnectionMappingInterfaces.get(0);
            if (existingBPLConnectionMappingInterface == null) {
                logger.error(methodName + "ACTIVE BPL Connection Mapping not found.Aborting");
                throw new Exception("ACTIVE BPL Connection Mapping not found.Aborting");
            }
            existingBPLConnectionMappingInterface.setStatus(BPLConnectionMappingInterface.STATUS_INACTIVE);
            existingBPLConnectionMappingInterface.setEndDate(GlobalResources.getCurrentDate());
            existingBPLConnectionMappingInterface.setEndBillMonth(billMonthToInsert);
            BPLConnectionMappingInterface updatedBPLConnectionMappingInterface = bplConnectionMappingService.update(existingBPLConnectionMappingInterface);
            if (updatedBPLConnectionMappingInterface == null) {
                logger.error(methodName + "Unable to update ACTIVE BPL Connection Mapping.Aborting");
                throw new Exception("Unable to update ACTIVE BPL Connection Mapping.Aborting");
            }
        }
        return updatedConsumerInformationInterface;
    }


    @Transactional(rollbackFor = Exception.class)
    public ConsumerInformationInterface updateEmployeeConsumerInformation(final String consumerNo, Map<String, String> consumerInformationMap) throws Exception {
        final String methodName = "updateEmployeeConsumerInformation() : ";
        logger.info(methodName + "called for " + consumerNo);

        ConsumerInformationInterface updatedConsumerInformationInterface = null;
        if (consumerNo == null || consumerInformationMap == null) {
            logger.error(methodName + "Inputs are null");
            return null;
        }
        ConsumerInformationInterface consumerInformationInterface = consumerInformationDAO.getByConsumerNo(consumerNo);
        if (consumerInformationInterface == null) {
            logger.error(methodName + "No ConsumerInformation Found");
            return null;
        }
        logger.info(methodName + "Got consumer as " + consumerInformationInterface);

        detachConsumerInformation(consumerInformationInterface);

        List<ConsumerInformationHistoryInterface> consumerInformationHistoryInterfaces = new ArrayList<>();

        String isEmployeeKey = "isEmployee";
        String employeeNoKey = "employeeNo";
        String value = consumerInformationMap.get(isEmployeeKey);
        boolean isEmployee = false;
        String employeeNo = null;
        if (value != null && value.equalsIgnoreCase("true")) {
            isEmployee = true;
            employeeNo = consumerInformationMap.get(employeeNoKey);
            if (employeeNo == null) {
                logger.error(methodName + "isEmployee is true & employee No is NULL.Aborting");
                return null;
            }
        }
        Method setterMethod = NGBAPIUtility.getSetterForField(isEmployeeKey, consumerInformationInterface);
        Method getterMethod = NGBAPIUtility.getGetterForField(isEmployeeKey, consumerInformationInterface);

        if (setterMethod == null && getterMethod == null) {
            logger.error(methodName + "Setter/Getter Method Not found for isEmployeeKey " + isEmployeeKey + " continuing");
            return null;
        }
        Field field = NGBAPIUtility.getFieldByName(isEmployeeKey, consumerInformationInterface);
        logger.info(methodName + "field  " + field);
        if (field == null) {
            logger.error(methodName + "Field Not found for isEmployeeKey " + isEmployeeKey + " continuing");
            return null;
        }
        Column columnAnnotation = NGBAPIUtility.getColumnAnnotationByNameValue(field);
        logger.info(methodName + "columnAnnotation " + columnAnnotation);
        if (columnAnnotation == null) {
            logger.error(methodName + "ColumnAnnotation Not found for isEmployeeKey " + isEmployeeKey + " continuing");
            return null;
        }
        logger.info(methodName + "calling " + getterMethod.getName() + " to get old value ");
        Object getResult = getterMethod.invoke(consumerInformationInterface, null);
        String olderValue = "";
        if (getResult != null) {
            olderValue = getResult.toString();
        }
        String billMonthToInsert = null;
        BillInterface bill = billService.getLatestBillByConsumerNo(consumerNo);
        if (bill != null && bill.getBillMonth() != null) {
            billMonthToInsert = bill.getBillMonth();
        } else {
            ScheduleInterface schedule = scheduleService.getLatestCompletedScheduleByConsumerNo(consumerNo);
            if (schedule == null || schedule.getBillMonth() == null) {
                logger.error(methodName + "no completed schedule found");
                throw new Exception("no completed schedule found");
            }
            billMonthToInsert = schedule.getBillMonth();
        }
        logger.info(methodName + "Old Value is " + olderValue + " new value is " + value);
        ConsumerInformationHistoryInterface consumerInformationHistoryInterface = ConsumerInformationHistoryFactory.build();
        consumerInformationHistoryInterface.setConsumerNo(consumerNo);
        consumerInformationHistoryInterface.setPropertyName(columnAnnotation.name());
        consumerInformationHistoryInterface.setPropertyValue(olderValue);
        consumerInformationHistoryInterface.setEndDate(GlobalResources.getCurrentDate());
        consumerInformationHistoryInterface.setEndBillMonth(billMonthToInsert);

        //Adding to list to insert after every column update
        consumerInformationHistoryInterfaces.add(consumerInformationHistoryInterface);

        logger.info(methodName + "ConsumerInformation before updating " + consumerInformationInterface);
        logger.info(methodName + "calling " + setterMethod.getName() + " with value " + value);

        if (isEmployee) {
            setterMethod.invoke(consumerInformationInterface, true);
        } else if (value != null && value.equalsIgnoreCase("false")) {
            setterMethod.invoke(consumerInformationInterface, false);
        }
        setUpdateAuditDetails(consumerInformationInterface);
        logger.info(methodName + "ConsumerInformation after updating " + consumerInformationInterface);

        List<ConsumerInformationHistoryInterface> insertedConsumerInformationHistoryInterfaces = consumerInformationHistoryService.insert(consumerInformationHistoryInterfaces);
        if (insertedConsumerInformationHistoryInterfaces == null || insertedConsumerInformationHistoryInterfaces.size() != consumerInformationHistoryInterfaces.size()) {
            logger.error(methodName + "Inserted HistoryInformations not equal to passed HistoryInformations.Aborting and rolling back");
            throw new Exception("Map Size and HistoryInformation does not match. Aborting and rolling back");
        }

        updatedConsumerInformationInterface = consumerInformationDAO.update(consumerInformationInterface);
        if (updatedConsumerInformationInterface == null) {
            logger.error(methodName + "Unable to Inserted ConsumerInformation.Aborting and rolling back");
            throw new Exception("Unable to Inserted ConsumerInformation.Aborting and rolling back");
        }

        ConsumerNoMasterInterface consumerNoMaster = consumerNoMasterService.getByConsumerNo(consumerNo);
        if (consumerNoMaster == null) {
            logger.error(methodName + "Given consumer no doesn't exist in  ConsumerNoMaster");
            throw new Exception("consumer no is invalid");
        }
        String status = consumerNoMaster.getStatus();
        if (status == null) {
            logger.error("status is null for consumer no :" + consumerNo);
            throw new Exception("status is null");
        }
        //Below validation Checks whether consumer is INACTIVE (PDC)
        if (status.equalsIgnoreCase(ConsumerNoMasterInterface.STATUS_INACTIVE)) {
            logger.error(methodName + "Consumer is " + status);
            throw new Exception("consumer is in-active");
        }

        //creating new Employee connection mapping
        if (isEmployee) {
            boolean isBPL = checkIsBPLByConsumerNo(consumerNo);
            if (isBPL) {
                logger.error(methodName + "BPL mapping is present.");
                throw new Exception("consumer is BPL, not allowed to tag Employee");
            }
            EmployeeMasterInterface employeeMaster = employeeMasterService.getByEmployeeNo(employeeNo);
            if (employeeMaster == null) {
                logger.error(methodName + "employee no not found in employee master");
                throw new Exception("employee no not found in employee master");
            }
            // Below validation checks , employee no given is retired
            if (!(employeeMaster.getStatus().equals(EmployeeConnectionMappingInterface.STATUS_ACTIVE))) {
                logger.error(methodName + "employee no given is " + employeeMaster.getStatus());
                throw new Exception("employee no given is " + employeeMaster.getStatus());
            }

            EmployeeConnectionMappingInterface employeeConnectionMappingInterfaceToInsert = EmployeeConnectionMappingFactory.build();
            EmployeeConnectionMappingInterface insertedEmployeeConnectionMapping = null;
            boolean employeeNoUsed = employeeConnectionMappingService.checkEmployeeNoUsed(employeeNo, EmployeeConnectionMappingInterface.STATUS_ACTIVE);
            if (employeeNoUsed) {
                logger.error(methodName + "given Employee no is active to other consumer.");
                throw new Exception("given employee no is active to other consumer");
            }
            boolean isDomestic = tariffDescriptionService.checkTariffDescriptionByConsumerNo(consumerNo, TariffDescriptionInterface.TARIFF_DESCRIPTION_DOMESTIC);
            if (!isDomestic) {
                logger.error(methodName + "consumer category should be domestic,for Employee rebate.");
                throw new Exception("consumer category should be domestic,for Employee rebate.");
            }
            // fetching employee connection mapping to check whether on given consumer , any employee is active or not
            List<EmployeeConnectionMappingInterface> employeeConnectionMappingInterfaces = employeeConnectionMappingService.getByConsumerNoAndStatus(consumerNo, EmployeeConnectionMappingInterface.STATUS_ACTIVE);
            if (employeeConnectionMappingInterfaces != null && employeeConnectionMappingInterfaces.size() > 0) {
                EmployeeConnectionMappingInterface employeeConnectionMappingInterface = employeeConnectionMappingInterfaces.get(0);
                logger.error(methodName + "given consumer no is active with employee no:" + employeeConnectionMappingInterface.getEmployeeNo());
                throw new Exception("given consumer no is active with employee no:" + employeeConnectionMappingInterface.getEmployeeNo());
            }

            employeeConnectionMappingInterfaceToInsert.setConsumerNo(consumerNo);
            employeeConnectionMappingInterfaceToInsert.setEmployeeNo(employeeNo);
            employeeConnectionMappingInterfaceToInsert.setStatus(BPLConnectionMappingInterface.STATUS_ACTIVE);
            employeeConnectionMappingInterfaceToInsert.setStartBillMonth(GlobalResources.getNextMonth(billMonthToInsert));
            employeeConnectionMappingInterfaceToInsert.setStartDate(GlobalResources.getCurrentDate());

            insertedEmployeeConnectionMapping = employeeConnectionMappingService.insert(employeeConnectionMappingInterfaceToInsert);
            if (insertedEmployeeConnectionMapping == null) {
                logger.error(methodName + "some error to insert new employee connection mapping");
                throw new Exception("some error to insert new employee connection mapping");
            }
        } else if (value != null && value.equalsIgnoreCase("false")) {
            List<EmployeeConnectionMappingInterface> employeeConnectionMappingInterfaces = employeeConnectionMappingService.getByConsumerNoAndStatus(consumerNo, EmployeeConnectionMappingInterface.STATUS_ACTIVE);
            if (employeeConnectionMappingInterfaces == null || employeeConnectionMappingInterfaces.size() != 1) {
                logger.error(methodName + "ACTIVE employee Connection Mapping not found.Aborting");
                throw new Exception("ACTIVE employee Connection Mapping not found.Aborting");
            }
            EmployeeConnectionMappingInterface existingEmployeeConnectionMappingInterface = employeeConnectionMappingInterfaces.get(0);
            if (existingEmployeeConnectionMappingInterface == null) {
                logger.error(methodName + "ACTIVE employee Connection Mapping not found.Aborting");
                throw new Exception("ACTIVE employee Connection Mapping not found.Aborting");
            }
            existingEmployeeConnectionMappingInterface.setStatus(BPLConnectionMappingInterface.STATUS_INACTIVE);
            existingEmployeeConnectionMappingInterface.setEndDate(GlobalResources.getCurrentDate());
            existingEmployeeConnectionMappingInterface.setEndBillMonth(billMonthToInsert);
            EmployeeConnectionMappingInterface updatedEmployeeConnectionMappingInterface = employeeConnectionMappingService.update(existingEmployeeConnectionMappingInterface);
            if (updatedEmployeeConnectionMappingInterface == null) {
                logger.error(methodName + "Unable to update ACTIVE employee Connection Mapping.Aborting");
                throw new Exception("Unable to update ACTIVE employee Connection Mapping.Aborting");
            }
        }
        return updatedConsumerInformationInterface;
    }


    public boolean checkIsBPLByConsumerNo(String consumerNo) {
        String methodName = "checkIsBPLByConsumerNo() : ";
        logger.info(methodName + "called " + consumerNo);
        boolean isBPL = false;
        ConsumerInformationInterface consumerInformationInterface = getConsumerInformationByConsumerNo(consumerNo);
        if (consumerInformationInterface == null) {
            logger.error(methodName + "some error to fetch consumer Information");
            return false;
        }
        isBPL = consumerInformationInterface.getIsBPL();
        if (isBPL) {
            List<BPLConnectionMappingInterface> bplConnectionMappingInterfaces = bplConnectionMappingService.getByConsumerNoAndStatus(consumerNo, BPLConnectionMappingInterface.STATUS_ACTIVE);
            if (bplConnectionMappingInterfaces == null || bplConnectionMappingInterfaces.size() != 1) {
                logger.error(methodName + "ACTIVE BPL Connection Mapping not found.Aborting");
                isBPL = false;
            }
            BPLConnectionMappingInterface existingBPLConnectionMappingInterface = bplConnectionMappingInterfaces.get(0);
            if (existingBPLConnectionMappingInterface == null) {
                logger.error(methodName + "ACTIVE BPL Connection Mapping not found.Aborting");
                isBPL = false;
            }
        }
        return isBPL;
    }

    public boolean checkIsEmployeeByConsumerNo(String consumerNo) {
        String methodName = "checkIsEmployeeByConsumerNo() : ";
        logger.info(methodName + "called " + consumerNo);
        boolean isEmployee = false;
        ConsumerInformationInterface consumerInformationInterface = getConsumerInformationByConsumerNo(consumerNo);
        if (consumerInformationInterface == null) {
            logger.error(methodName + "some error to fetch consumer Information");
            return isEmployee;
        }
        isEmployee = consumerInformationInterface.getIsEmployee();
        if (isEmployee) {
            List<EmployeeConnectionMappingInterface> employeeConnectionMappingInterfaces = employeeConnectionMappingService.getByConsumerNoAndStatus(consumerNo, EmployeeConnectionMappingInterface.STATUS_ACTIVE);
            if (employeeConnectionMappingInterfaces == null || employeeConnectionMappingInterfaces.size() != 1) {
                logger.error(methodName + "ACTIVE Employee Connection Mapping not found.Aborting");
                isEmployee = false;
            }
            EmployeeConnectionMappingInterface existingEmployeeConnectionMappingInterface = employeeConnectionMappingInterfaces.get(0);
            if (existingEmployeeConnectionMappingInterface == null) {
                logger.error(methodName + "ACTIVE BPL Connection Mapping not found.Aborting");
                isEmployee = false;
            }
        }
        return isEmployee;
    }

    private void setAuditDetails(ConsumerInformationInterface consumerInformationInterface) {
        final String methodName = "setAuditDetails() : ";
        logger.info(methodName + "called");
        if (consumerInformationInterface != null) {
            String user = GlobalResources.getLoggedInUser();
            consumerInformationInterface.setCreatedBy(user + " " + globalResources.getProjectDetail());
            consumerInformationInterface.setCreatedOn(GlobalResources.getCurrentDate());
            consumerInformationInterface.setUpdatedBy(user + " " + globalResources.getProjectDetail());
            consumerInformationInterface.setUpdatedOn(GlobalResources.getCurrentDate());
        }
    }

    private void setUpdateAuditDetails(ConsumerInformationInterface consumerInformationInterface) {
        final String methodName = "setUpdateAuditDetails() : ";
        logger.info(methodName + "called");
        if (consumerInformationInterface != null) {
            String user = GlobalResources.getLoggedInUser();
            consumerInformationInterface.setUpdatedBy(user + " " + globalResources.getProjectDetail());
            consumerInformationInterface.setUpdatedOn(GlobalResources.getCurrentDate());
        }
    }

    private void detachConsumerInformation(ConsumerInformationInterface consumerInformationInterface){
        final String methodName = "detachConsumerInformation() : ";
        logger.info(methodName + "called");
        if(consumerInformationInterface != null){
            entityManager.detach(consumerInformationInterface);
        }
    }
}
