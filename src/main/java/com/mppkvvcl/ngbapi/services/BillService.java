package com.mppkvvcl.ngbapi.services;

import com.mppkvvcl.ngbapi.custombeans.CC4Bill;
import com.mppkvvcl.ngbapi.custombeans.CustomBill;
import com.mppkvvcl.ngbapi.custombeans.CustomReadMaster;
import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbapi.utility.NGBAPIUtility;
import com.mppkvvcl.ngbdao.daos.BillDAO;
import com.mppkvvcl.ngbengine.EngineIgnition;
import com.mppkvvcl.ngbengine.interfaces.HolderInterface;
import com.mppkvvcl.ngbentity.beans.ErrorMessage;
import com.mppkvvcl.ngbentity.beans.TariffDetail;
import com.mppkvvcl.ngbinterface.interfaces.*;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * Created by PREETESH on 7/31/2017.
 */
@Service
public class BillService {

    private static final Logger logger = GlobalResources.getLogger(BillService.class);

    @Autowired
    private BillDAO billDAO;

    @Autowired
    private PaymentService paymentService;

    @Autowired
    private ConfiguratorService configuratorService;

    @Autowired
    private PartPaymentService partPaymentService;

    @Autowired
    private InstrumentDishonourService instrumentDishonourService;

    @Autowired
    private ConsumerInformationService consumerInformationService;

    @Autowired
    private AgricultureBill6MonthlyService agricultureBill6MonthlyService;

    @Autowired
    private ConsumerNoMasterService consumerNoMasterService;

    @Autowired
    private ConsumerConnectionInformationService consumerConnectionInformationService;

    @Autowired
    private TariffDetailService tariffDetailService;

    @Autowired
    private ScheduleService scheduleService;

    @Autowired
    private HolderService holderService;

    @Autowired
    private ReadMasterService readMasterService;

    @Autowired
    private ReadMasterKWService readMasterKWService;

    @Autowired
    private ReadMasterPFService readMasterPFService;

    @Autowired
    private BillCorrectionService billCorrectionService;

    @Autowired
    private GlobalResources globalResources;

    @Autowired
    private GMCAccountingService gmcAccountingService;


    /**
     * 1. First retrieve the latest bill by consumer no
     * 2. then check if consumer wants to pay bill by cheque, if true
     *    a. check consumer is not in defaulters list
     *    b. set due date to cheque-due-date
     * 3. if cheque flag is false , set due date
     * 4. Check for partPayment flag, if true retrieve part payment amount.
     * 5. Retrieve all not-posted payments of consumer
     * 6. check payment date against the due date and set CurrentBillAmount or NetAmount
     * 7. set all amount variables of customBill bean and return it
     * @param instrumentFlag
     * @param partPaymentFlag
     * @param payDate
     * @param consumerNo
     * @return
     */

    public CustomBill getCustomBillByModeAndPartPaymentFlagAndDateAndConsumerNo(
            boolean instrumentFlag, boolean partPaymentFlag, String payDate, String consumerNo )
            throws RuntimeException, Exception{
        String methodName = " getCustomBillByModeAndPartPaymentFlagAndDateAndConsumerNo() : ";
        logger.info(methodName + "called for consumer no: " + consumerNo);
        CustomBill customBill = null;
        BillInterface bill = null;
        long billAmount = 0;
        long partPaymentAmount = 0;
        long totalPaid = 0;
        long balanceAmountToPay = 0;
        Date dueDate;
        boolean chequeAcceptable = true;
        logger.info("consNo: "+consumerNo+" instrumentFlag: "+instrumentFlag+" payDate "+payDate+" partPayment flag "+partPaymentFlag);
        if ( consumerNo != null && payDate != null ) {
            consumerNo = consumerNo.trim();
            Date paymentDate = GlobalResources.getDateFromString(payDate);
            bill = billDAO.getTopByConsumerNoAndDeletedOrderByIdDesc(consumerNo, BillInterface.DELETED_FALSE);
            logger.info(methodName + "bill retrieved "+bill);
            if(bill != null){
/**
 * checks for Cheque/PartPayment acceptability
 */
                if(instrumentFlag) {
                    chequeAcceptable = instrumentDishonourService.getValidityByConsumerNoAndDate(consumerNo, paymentDate);
                    if(chequeAcceptable){
                        logger.info(methodName + "cheque can be accepted from consumer "+consumerNo);
                        dueDate = bill.getChequeDueDate();
                        logger.info(methodName + "cheque due date selected "+dueDate);
                    }else{
                        logger.error(methodName + "consumer is in defaulter's list, can't take cheque ");
                        throw new Exception("consumer is in defaulter's list, can't take cheque ");
                    }
                }else{
                    dueDate = bill.getDueDate();
                    logger.info(methodName + "normal due date selected "+dueDate);
                }

                if(partPaymentFlag) {
                    PartPaymentInterface partPayment = partPaymentService.getNotAppliedPartPaymentForConsumerNo(consumerNo);
                    if (partPayment != null) {
                        logger.info(methodName + "found part payment approval " + partPayment);
                        Date approvedOn = GlobalResources.getDateWithoutTimeStamp(partPayment.getCreatedOn());
                        if (approvedOn != null) {
                            logger.info(methodName + "found part payment approval date as " + approvedOn+" payment date is "+paymentDate);
                            if (paymentDate.equals(approvedOn) || paymentDate.after(approvedOn)) {
                                long dayDifference = GlobalResources.getDateDiffInDays(paymentDate, approvedOn);
                                long partPaymentValidForDays = configuratorService.getValueForCode(ConfiguratorInterface.PART_PAYMENT_VALID_FOR_DAYS);
                                if (dayDifference <= partPaymentValidForDays) {
                                    partPaymentAmount = partPayment.getAmount();
                                    logger.info(methodName + "found part payment approval for consumer " + consumerNo);
                                }else{
                                    logger.error(methodName + "days difference is more than expected in part payment");
                                    throw new Exception("days difference is more than expected in part payment ");
                                }
                            } else {
                                logger.error(methodName + "payment date can not be before part payment approval date ");
                                throw new Exception("payment date can not be before part payment approval date ");
                            }
                        }else{
                            logger.error(methodName + "approve on date found null ");
                            throw new Exception("approve on date found null ");
                        }
                    } else {
                        logger.error(methodName + "consumer has no part payment approval ");
                        throw new Exception("consumer has no part payment approval ");
                    }
                }

                List<PaymentInterface> payments = paymentService.getByConsumerNoAndDeletedAndPosted(consumerNo, PaymentInterface.DELETED_FALSE, PaymentInterface.POSTED_FALSE);
                if(payments != null && payments.size() > 0){
                    for (PaymentInterface payment : payments) {
                        if(payment != null ){
                            long payAmount = payment.getAmount();
                            if( payAmount > 0){
                                totalPaid = totalPaid + payAmount;
                            }
                        }
                    }
                }else{
                    logger.info(methodName + "no payments found");
                }
                logger.info(methodName + "comparing payment date "+payDate+" with due date: "+dueDate);

                if(paymentDate.after(dueDate)){
                    billAmount = bill.getNetBill().longValue();
                }else{
                    billAmount = bill.getCurrentBill().longValue();
                }
                logger.info(methodName + " bill amount selected "+billAmount);
                balanceAmountToPay = billAmount - totalPaid;
                customBill = new CustomBill();
                customBill.setBill(bill);
                customBill.setTotalBillAmount(billAmount);
                customBill.setPartPaymentAmount(partPaymentAmount);
                customBill.setPaymentReceived(totalPaid);
                customBill.setBalanceAmountToPay(balanceAmountToPay);
            }else{
                logger.error(methodName + "bill not found");
                throw new Exception("bill not found ");
            }
        } else {
            logger.error(methodName + "consumerNo passed is null in method.");
            throw new Exception("consumerNo passed is null in method");
        }
        return customBill;
    }


    public BillInterface getByConsumerNo(String consumerNo) {
        String methodName = " getByConsumerNo() : ";
        logger.info(methodName + "called for consumer no: " + consumerNo);
        BillInterface bill = null;
        if (consumerNo != null) {
            bill = billDAO.getTopByConsumerNoAndDeletedOrderByIdDesc(consumerNo, BillInterface.DELETED_FALSE);
        }

        return bill;
    }


    public BillInterface getLatestBillByConsumerNo(String consumerNo) {
        String methodName = " getLatestBillByConsumerNo(): ";
        logger.info(methodName + "called");
        BillInterface latestBill = null;
        if (consumerNo != null) {
            latestBill = billDAO.getTopByConsumerNoAndDeletedOrderByIdDesc(consumerNo, BillInterface.DELETED_FALSE);
        }
        return latestBill;
    }

    public BillInterface getBillByConsumerNoAndBillMonth(String consumerNo, String billMonth)  {
        String methodName = " getBillByConsumerNoAndBillMonth() : ";
        logger.info(methodName + "called");
        List<BillInterface> bill = null;
        BillInterface billInterface = null;
        if (consumerNo != null && billMonth != null) {
            logger.info(methodName+ "calling cons"+consumerNo+ "bill month"+billMonth);
            bill = billDAO.getByConsumerNoAndBillMonthAndDeleted(consumerNo, billMonth, BillInterface.DELETED_FALSE);
            if(bill != null && bill.size() > 0){
                logger.info(methodName+ "inside if"+bill + "bill size"+bill.size());
                billInterface = bill.get(0);
            }
        }
        return billInterface;
    }

    public List<String> getAllBillMonthByConsumerNo(String consumerNo) throws Exception {
        String methodName = " getAllBillMonthByConsumerNo(): ";
        logger.info(methodName + "called for consumer no: " + consumerNo);
        List<String> billMonths = null;
        if (consumerNo == null) {
            logger.error(methodName + "consumerNo passed is null in method.");
            throw new Exception("consumerNo passed is null in method.");
        }
        consumerNo = consumerNo.trim();
        billMonths = billDAO.getDistinctBillMonthByConsumerNoOrderByIdDesc(consumerNo, BillInterface.DELETED_FALSE);
        if (billMonths == null || billMonths.size() == 0) {
            logger.error(methodName + " no Bill Month retrieved for consumer number " + consumerNo);
            throw new Exception("No Bill available for consumer");
        }
        return billMonths;
    }

    public CustomBill getCustomBillByInstrumentFlagAndAgricultureFlagAndPartPaymentFlagAndPaymentDateAndConsumerNo(boolean agricultureFlag, boolean instrumentFlag, boolean partPaymentFlag, String paymentDateString, String consumerNo)  throws RuntimeException, Exception{
        String methodName = " getCustomBillByInstrumentFlagAndAgricultureFlagAndPartPaymentFlagAndPaymentDateAndConsumerNo(): ";
        logger.info(methodName + "called for consumer no: " + consumerNo);
        CustomBill customBill = null;
        AgricultureBill6MonthlyInterface agricultureBill6Monthly = null;
        BillInterface bill = null;
        long billAmount = 0;
        long partPaymentAmount = 0;
        long totalPaid = 0;
        long balanceAmountToPay = 0;
        Date billChequeDueDate;
        Date billDueDate;
        BigDecimal netBillAmount;
        BigDecimal netBillWithSurcharge;

        Date dueDate;
        boolean chequeAcceptable;
        logger.info("consNo: "+consumerNo+" instrumentFlag: "+instrumentFlag+" payDate "+paymentDateString+" partPayment flag "+partPaymentFlag);
        if ( consumerNo == null || paymentDateString == null ) {
            logger.error(methodName + "consumerNo or PaymentDate passed is null in method.");
            throw new Exception("consumerNo or PaymentDate passed is null in method.");
        }
        consumerNo = consumerNo.trim();
        ConsumerInformationInterface consumerInformation = consumerInformationService.getConsumerInformationByConsumerNo(consumerNo);
        Date paymentDate = GlobalResources.getDateFromString(paymentDateString);
        if(agricultureFlag){
            agricultureBill6Monthly = agricultureBill6MonthlyService.getTopByConsumerNo(consumerNo);
            if(agricultureBill6Monthly == null){
                logger.error(methodName + "agriculture bill not found");
                throw new Exception("agriculture bill not found ");
            }
            billChequeDueDate = agricultureBill6Monthly.getChequeDueDate();
            billDueDate = agricultureBill6Monthly.getDueDate();
            netBillAmount = agricultureBill6Monthly.getNetBill();

            if(billChequeDueDate == null || billDueDate == null || netBillAmount == null){
                logger.error(methodName + "bill component found null");
                throw new Exception("bill component found null");
            }
            bill = getLatestBillByConsumerNo(consumerNo);
            if(bill == null){
                logger.error(methodName + "bill  found null");
                throw new Exception("bill  found null");
            }
            BigDecimal cumulativeSurcharge = bill.getCumulativeSurcharge();
            if(cumulativeSurcharge == null){
                logger.error(methodName + "cumulativeSurcharge  found null");
                throw new Exception("cumulativeSurcharge  found null");
            }
            netBillWithSurcharge = agricultureBill6Monthly.getNetBill().add(cumulativeSurcharge);
        }else{
            bill = getLatestBillByConsumerNo(consumerNo);
            if(bill == null){
                logger.error(methodName + " bill not found");
                throw new Exception(" bill not found ");
            }
            billChequeDueDate = bill.getChequeDueDate();
            billDueDate = bill.getDueDate();
            netBillAmount = bill.getNetBill();
            netBillWithSurcharge = bill.getNetBill().add(bill.getCurrentBillSurcharge());
        }
        //   bill = billRepository.findTopByConsumerNoAndDeletedOrderByIdDesc(consumerNo,Bill.DELETED_FALSE);
        logger.info(methodName + "bill parameter retrieved chequeDueDate "+billChequeDueDate+" DueDate "
                +billDueDate+" currentBill "+netBillAmount+" netBill "+netBillWithSurcharge );
        if(billChequeDueDate == null || billDueDate == null || netBillAmount == null || netBillWithSurcharge == null ){
            logger.error(methodName + "one of the bill parameter is null");
            throw new Exception("one of the bill parameter is null ");
        }
/**
 * checks for Cheque/PartPayment acceptability
 */
        if(instrumentFlag) {
            chequeAcceptable = instrumentDishonourService.getValidityByConsumerNoAndDate(consumerNo, paymentDate);
            if(chequeAcceptable){
                logger.info(methodName + "cheque can be accepted from consumer ");
                dueDate = billChequeDueDate;
                logger.info(methodName + "cheque due date selected "+dueDate);
            }else{
                logger.error(methodName + "consumer is in defaulter's list, can't take cheque ");
                throw new Exception("consumer is in defaulter's list, can't take cheque ");
            }
        }else{
            dueDate = billDueDate;
            logger.info(methodName + "normal due date selected "+dueDate);
        }

        if(partPaymentFlag) {
            PartPaymentInterface partPayment = partPaymentService.getNotAppliedPartPaymentForConsumerNo(consumerNo);
            if (partPayment == null) {
                logger.error(methodName + "consumer has no part payment approval ");
                throw new Exception("consumer has no part payment approval ");
            }
            logger.info(methodName + "found part payment approval " + partPayment);
            Date approvedOn = GlobalResources.getDateWithoutTimeStamp(partPayment.getCreatedOn());
            if (approvedOn == null) {
                logger.error(methodName + "approve on date found null ");
                throw new Exception("approve on date found null ");
            }
            logger.info(methodName + "found part payment approval date as " + approvedOn+" payment date is "+paymentDate);
            if (paymentDate.before(approvedOn)) {
                logger.error(methodName + "payment date can not be before part payment approval date ");
                throw new Exception("payment date can not be before part payment approval date ");
            }
            long dayDifference = GlobalResources.getDateDiffInDays(paymentDate, approvedOn);
            long partPaymentValidForDays = configuratorService.getValueForCode(ConfiguratorInterface.PART_PAYMENT_VALID_FOR_DAYS);
            if (dayDifference > partPaymentValidForDays) {
                logger.error(methodName + "days difference is more than expected in part payment");
                throw new Exception("days difference is more than expected in part payment ");
            }
            partPaymentAmount = partPayment.getAmount();
            logger.info(methodName + "found part payment approval for consumer " + consumerNo);
        }

        List<PaymentInterface> payments = paymentService.getByConsumerNoAndDeletedAndPosted(consumerNo, PaymentInterface.DELETED_FALSE, PaymentInterface.POSTED_FALSE);
        if(payments != null && payments.size() > 0){
            for (PaymentInterface payment : payments) {
                if(payment != null ){
                    long payAmount = payment.getAmount();
                    if( payAmount > 0){
                        totalPaid = totalPaid + payAmount;
                    }
                }
            }
        }else{
            logger.info(methodName + "no payments found");
        }

        logger.info(methodName + "comparing payment date "+paymentDate+" with due date: "+dueDate);

        if(paymentDate.after(dueDate)){
            billAmount = netBillWithSurcharge.longValue();
        }else{
            billAmount = netBillAmount.longValue();
        }
        logger.info(methodName + " bill amount selected "+billAmount);
        balanceAmountToPay = billAmount - totalPaid;
        customBill = new CustomBill();
        customBill.setConsumerInformation(consumerInformation);
        customBill.setAgricultureBill6Monthly(agricultureBill6Monthly);
        customBill.setBill(bill);
        customBill.setTotalBillAmount(billAmount);
        customBill.setPartPaymentAmount(partPaymentAmount);
        customBill.setPaymentReceived(totalPaid);
        customBill.setBalanceAmountToPay(balanceAmountToPay);
        return customBill;
    }

    public List<BillInterface> getBillsByConsumerNo(String consumerNo) {
        String methodName = " getBillsByConsumerNo(): ";
        logger.info(methodName + " service method called for fetching latest bill for consumer " + consumerNo);
        List<BillInterface> bills = null;
        if (consumerNo != null) {
            bills = billDAO.getAllByConsumerNo(consumerNo, BillInterface.DELETED_FALSE);
        } else {
            logger.error(methodName + "consumer Number is null");
        }
        return bills;
    }

    public List<BillInterface> getByConsumerNoAndBillMonthOrderByIdAsc(String consumerNo, String billMonth) {
        String methodName = " getByConsumerNoAndBillMonthOrderByIdAsc(): ";
        logger.info(methodName + "called " + consumerNo);
        List<BillInterface> billInterfaces = null;
        if (consumerNo != null && billMonth != null) {
            billInterfaces = billDAO.getByConsumerNoAndBillMonthOrderByIdAsc(consumerNo, billMonth);
        }
        return billInterfaces;
    }

    public long getCountByConsumerNo(String consumerNo){
        final String methodName = "getCountByConsumerNo() : ";
        logger.info(methodName + "called");
        return billDAO.getCountByConsumerNo(consumerNo);
    }

    /**
     * coded by nitish
     * returns list of payments in paginated way as per passed parameters
     * @param consumerNo
     * @param sortBy
     * @param sortOrder
     * @param pageNumber
     * @param pageSize
     * @return
     */
    public List<BillInterface> getByConsumerNoWithPagination(String consumerNo, String sortBy, String sortOrder, int pageNumber, int pageSize){
        final String methodName = "getByConsumerNo() : ";
        logger.info(methodName + "called " + consumerNo + " " + sortBy);
        List<BillInterface> billInterfaces = Collections.emptyList();
        pageNumber = pageNumber - 1;
        if(consumerNo != null && sortBy != null && pageNumber >= 0){
            Pageable pageable = null;
            if(sortOrder != null && sortOrder.equalsIgnoreCase(NGBAPIUtility.SORT_ORDER_ASCENDING)){
                if(sortBy.equalsIgnoreCase("billMonth")){
                    pageable = new PageRequest(pageNumber,pageSize, Sort.Direction.ASC,"id");
                    billInterfaces = billDAO.getByConsumerNoOrderByBillMonthAscendingWithPageable(consumerNo,pageable);
                }else{
                    pageable = new PageRequest(pageNumber,pageSize, Sort.Direction.ASC,sortBy);;
                    billInterfaces = billDAO.getByConsumerNo(consumerNo,pageable);
                }
            }else{
                if(sortBy.equalsIgnoreCase("billMonth")){
                    pageable = new PageRequest(pageNumber,pageSize,Sort.Direction.DESC,"id");
                    billInterfaces = billDAO.getByConsumerNoOrderByBillMonthDescendingWithPageable(consumerNo,pageable);
                }else{
                    pageable = new PageRequest(pageNumber,pageSize,Sort.Direction.DESC,sortBy);
                    billInterfaces = billDAO.getByConsumerNo(consumerNo,pageable);
                }
            }
        }
        return billInterfaces;
    }

    /**
     * coded by nitish
     * returns list of bills in paginated way as per passed parameters
     * @param consumerNo
     * @param pageNumber
     * @param pageSize
     * @param sortOrder
     * @return
     */
    public List<BillInterface> getByConsumerNoOrderByBillMonthWithPagination(String consumerNo, int pageNumber, int pageSize, String sortOrder) {
        String methodName = " getByConsumerNoOrderByBillMonthWithPagination(): ";
        logger.info(methodName + "called " + consumerNo);
        List<BillInterface> bills = Collections.emptyList();
        pageNumber = pageNumber - 1;
        if (consumerNo != null && pageNumber >= 0) {
            if(sortOrder != null && sortOrder.equalsIgnoreCase(NGBAPIUtility.SORT_ORDER_ASCENDING)){
                Pageable pageable = new PageRequest(pageNumber,pageSize, Sort.Direction.ASC,"id");
                bills = billDAO.getByConsumerNoOrderByBillMonthAscendingWithPageable(consumerNo,pageable);
            }else{
                Pageable pageable = new PageRequest(pageNumber,pageSize,Sort.Direction.DESC,"id");
                bills = billDAO.getByConsumerNoOrderByBillMonthDescendingWithPageable(consumerNo,pageable);
            }
        }
        return bills;
    }

    public CC4Bill reBill(String consumerNo, ReadMasterInterface readingToUpdate, ErrorMessage errorMessage) {
        String methodName = "reBill() : ";
        logger.info(methodName + "called");
        BillInterface currentBill = null;
        BillInterface previousBill = null;
        Date previousBillDate = null;
        BillInterface updatedBill = null;
        if (readingToUpdate == null || readingToUpdate.getConsumerNo() == null || readingToUpdate.getBillMonth() == null || consumerNo == null) {
            errorMessage.setErrorMessage("Given input parameters are null");
            return null;
        }

        if (!consumerNo.equals(readingToUpdate.getConsumerNo())) {
            errorMessage.setErrorMessage("consumer no mismatch");
            return null;
        }
        currentBill = getLatestBillByConsumerNo(consumerNo);
        if (currentBill == null) {
            errorMessage.setErrorMessage("no bill found for given consumer no ");
            return null;
        }

        String currentBillMonth = currentBill.getBillMonth();
        Date currentBillDate = currentBill.getBillDate();
        String groupNo = currentBill.getGroupNo();
        String readingType = readingToUpdate.getReadingType();
        String readBillMonth = readingToUpdate.getBillMonth();

        if (currentBillMonth == null || currentBillDate == null || groupNo == null) {
            errorMessage.setErrorMessage(" bill details not found for given consumer no ");
            return null;
        }

        if (ReadMasterInterface.READING_TYPE_PFL.equals(readingType)) {
            errorMessage.setErrorMessage("for reading type PFL, re-bill not allowed");
            return null;
        }

        //fetching details for holderInterface service
        ConsumerNoMasterInterface consumerNoMaster = consumerNoMasterService.getByConsumerNo(consumerNo);
        if (consumerNoMaster == null) {
            errorMessage.setErrorMessage("Given consumer no doesn't exist in  ConsumerNoMaster");
            return null;
        }
        String status = consumerNoMaster.getStatus();
        if (status == null) {
            errorMessage.setErrorMessage("status found null for given consumer no ");
            return null;
        }
        //Below validation Checks whether consumer is INACTIVE (PDC)
        if (status.equals(ConsumerNoMasterInterface.STATUS_INACTIVE)) {
            errorMessage.setErrorMessage("Consumer is " + status);
            return null;
        }

        ReadMasterKWInterface readMasterKWToUpdate = readingToUpdate.getReadMasterKW();
        ReadMasterPFInterface readMasterPFToUpdate = readingToUpdate.getReadMasterPF();


        List<ReadMasterInterface> readMasterInterfaces = readMasterService.getTop2ByConsumerNoAndBillMonthOrderByIdDesc(consumerNo, currentBillMonth);
        if (readMasterInterfaces == null) {
            errorMessage.setErrorMessage("reading not found for given consumer no ");
            return null;
        }
        ReadMasterInterface readParticipatedInBill = readMasterInterfaces.get(0);

        if (readParticipatedInBill.getId() != readingToUpdate.getId()) {
            errorMessage.setErrorMessage("reading mismatch");
            return null;
        }

        if (!readParticipatedInBill.getBillMonth().equals(readBillMonth)) {
            errorMessage.setErrorMessage("bill month mis-match in reading ");
            return null;
        }

        String nextBillMonth = GlobalResources.getNextMonth(currentBillMonth);
        List<ReadMasterInterface> readings = readMasterService.getByConsumerNoAndBillMonthOrdered(consumerNo, nextBillMonth);
        if (readings != null && readings.size() > 0) {
            errorMessage.setErrorMessage("Reading already inserted for next billing month, Can't perform Bill-Correction");
            return null;
        }

        // this if will work if meter is replaced after bill generation  OR  next month reading is present.
        if (!(currentBillMonth.equals(readingToUpdate.getBillMonth()))) {
            errorMessage.setErrorMessage("bill-month in reading is different from  bill-month in latest bill");
            return null;
        }

        // below validation  checks for previous read stored is SR/CR then compare MD/PF  and proceed for Re-bill.
        if (readMasterInterfaces.size() > 1) {
            ReadMasterInterface previousReading = readMasterInterfaces.get(1);
            if (previousReading == null) {
                errorMessage.setErrorMessage("previous reading not found");
                return null;
            }

            if (previousReading.getReplacementFlag().equals(ReadMasterInterface.REPLACEMENT_FLAG_START_READ) || previousReading.getReplacementFlag().equals(ReadMasterInterface.REPLACEMENT_FLAG_CTR_READ)) {
                if (readMasterKWToUpdate != null) {
                    logger.info(methodName + "Previous Read Stored is Replacement, Comparing stored KW with given KW ");
                    ReadMasterKWInterface previousReadMasterKW = readMasterKWService.getByReadMasterId(previousReading.getId());
                    if (previousReadMasterKW != null) {
                        BigDecimal previousBillingMD = previousReadMasterKW.getBillingDemand();
                        BigDecimal currentBillingMD = readMasterKWToUpdate.getBillingDemand();
                        BigDecimal maxKW = currentBillingMD.max(previousBillingMD);
                        readMasterKWToUpdate.setBillingDemand(maxKW);
                    }
                }
                if (readMasterPFToUpdate != null) {
                    logger.info(methodName + "Previous Read Stored is Replacement, Comparing stored PF with given PF ");
                    ReadMasterPFInterface previousReadMasterPF = readMasterPFService.getByReadMasterId(previousReading.getId());
                    if (previousReadMasterPF != null) {
                        BigDecimal previousBillingPF = previousReadMasterPF.getBillingPF();
                        BigDecimal currentBillingPF = readMasterPFToUpdate.getBillingPF();
                        BigDecimal minPF = currentBillingPF.min(previousBillingPF);
                        readMasterPFToUpdate.setBillingPF(minPF);
                    }
                }
            }
        }


        // fetching previous bill of consumer
        String previousBillMonth = GlobalResources.getPreviousMonth(currentBillMonth);
        previousBill = getBillByConsumerNoAndBillMonth(consumerNo, previousBillMonth);

        if (previousBill != null) {
            previousBillDate = previousBill.getBillDate();
        } else {
            logger.info(methodName + "previous bill not found for consumer no :" + consumerNo);
            ConsumerConnectionInformationInterface consumerConnectionInformation = consumerConnectionInformationService.getByConsumerNo(consumerNo);
            if (consumerConnectionInformation == null) {
                errorMessage.setErrorMessage("consumer connection information not found for given consumer no");
                return null;
            }
            previousBillDate = consumerConnectionInformation.getConnectionDate();
        }
        // todo change function to get tariff detail  with old and new  BILL date
        TariffDetailInterface tariffDetail = tariffDetailService.getLatestTariffByConsumerNo(consumerNo);

        //fetching schedule for current bill (Bill-Month)
        ScheduleInterface schedule = scheduleService.getByGroupNoAndBillMonth(groupNo, currentBillMonth);
        if (schedule == null) {
            errorMessage.setErrorMessage("schedule not found for latest bill");
            return null;
        }

        HolderInterface holderInterface = holderService.prepareHolderForBillCorrection(consumerNoMaster, tariffDetail, schedule, readingToUpdate);
        if (holderInterface == null) {
            logger.error(methodName + " some error to prepare holder");
            errorMessage.setErrorMessage("some internal error");
            return null;
        }

        updatedBill = EngineIgnition.start(holderInterface);
        if (updatedBill == null) {
            errorMessage.setErrorMessage("some error in Re-bill");
            return null;
        }

        BillInterface billInterface = GlobalResources.convertCustomBillInterfaceToBillInterface(updatedBill);
        if (billInterface == null) {
            logger.error(methodName + "some error to convert Custom bill into bill from global resources for consumer no:" + updatedBill.getConsumerNo());
            errorMessage.setErrorMessage("some internal error");
            return null;
        }

        CustomReadMaster customReadMaster = GlobalResources.convertReadMasterToCustomReadMaster(readingToUpdate);

        if (customReadMaster == null) {
            errorMessage.setErrorMessage("some error to fetch read");
            return null;
        }
        logger.info(methodName + "bill is " + billInterface);
        CC4Bill cc4Bill = new CC4Bill();
        cc4Bill.setBill(billInterface);
        cc4Bill.setCustomReadMaster(customReadMaster);
        cc4Bill.setGmcAccounting(updatedBill.getGmcAccountingInterface());

        return cc4Bill;
    }

    @Transactional(rollbackFor = Exception.class)
    public CC4Bill insertCC4Bill(CC4Bill cc4Bill) throws Exception {
        String methodName = "insertCC4Bill() : ";
        logger.info(methodName + "called ");
        if (cc4Bill == null || cc4Bill.getBillInterface() == null) {
            logger.error(methodName + "given input is null");
            throw new Exception("given input is null");
        }
        BillInterface billToUpdate = cc4Bill.getBillInterface();

        String billMonth = billToUpdate.getBillMonth();
        String consumerNo = billToUpdate.getConsumerNo();

        if (billMonth == null || consumerNo == null) {
            logger.error(methodName + "given input is null.");
            throw new Exception("given input is null.");
        }

        CustomReadMaster customReadMaster = cc4Bill.getCustomReadMaster();

        if (customReadMaster == null || customReadMaster.getBillMonth() == null) {
            logger.error(methodName + "given input reading is null");
            throw new Exception("given input reading is null");
        }
        String readBillMonth = customReadMaster.getBillMonth();

        ReadMasterInterface readToUpdate = GlobalResources.convertCustomReadMasterToReadMaster(customReadMaster);

        List<ReadMasterInterface> readMasterInterfaces = readMasterService.getByConsumerNoAndBillMonthAndReplacementFlagAndUsedOnBillOrderByIdDesc(consumerNo, readBillMonth, "NR", true);
        if (readMasterInterfaces == null) {
            logger.error(methodName + "reading not found.");
            throw new Exception("reading not found");
        }

        ReadMasterInterface readParticipatedInBill = readMasterInterfaces.get(0);
        if (readParticipatedInBill == null || readParticipatedInBill.getBillMonth() == null) {
            logger.error(methodName + "reading not found.");
            throw new Exception("reading not found");
        }

        if (!(readParticipatedInBill.getBillMonth().equals(readBillMonth))) {
            logger.error(methodName + "read month mismatch, aborting bill correction.");
            throw new Exception("read month mismatch, aborting bill correction.");
        }

        readParticipatedInBill.setUsedOnBill(ReadMasterInterface.NOT_USED_ON_BILL);

        ReadMasterInterface updatedReadParticipatedInBill = readMasterService.update(readParticipatedInBill);
        ReadMasterInterface insertedRead = readMasterService.insert(readToUpdate);

        ReadMasterKWInterface readMasterKWInterfaceToUpdate = readToUpdate.getReadMasterKW();
        if (readMasterKWInterfaceToUpdate != null) {
            readMasterKWInterfaceToUpdate.setReadMasterId(insertedRead.getId());
            readMasterKWInterfaceToUpdate.setId(0);
            ReadMasterKWInterface insertedReadMasterKW = readMasterKWService.insert(readMasterKWInterfaceToUpdate);
            if (insertedReadMasterKW == null) {
                logger.error(methodName + "unable to insert KW.");
                throw new Exception("unable to insert KW.");
            }
        }

        ReadMasterPFInterface readMasterPFInterfaceToUpdate = readToUpdate.getReadMasterPF();
        if (readMasterPFInterfaceToUpdate != null) {
            readMasterPFInterfaceToUpdate.setReadMasterId(insertedRead.getId());
            readMasterPFInterfaceToUpdate.setId(0);
            ReadMasterPFInterface insertedReadMasterPFInterface = readMasterPFService.insert(readMasterPFInterfaceToUpdate);
            if (insertedReadMasterPFInterface == null) {
                logger.error(methodName + "unable to insert PF.");
                throw new Exception("unable to insert PF.");
            }
        }
        if (updatedReadParticipatedInBill == null || insertedRead == null) {
            logger.error(methodName + "some error to update reading, aborting request.");
            throw new Exception("some error to update reading, aborting request.");
        }

        BillInterface currentBill = getLatestBillByConsumerNo(consumerNo);
        if (currentBill == null || currentBill.getBillMonth() == null) {
            logger.error(methodName + "no bill found to update.");
            throw new Exception("no bill found to update.");
        }

        if (!(currentBill.getBillMonth().equals(billMonth))) {
            logger.error(methodName + "bill month mismatch, please try after some time.");
            throw new Exception("bill month mismatch, please try after some time.");
        }


        GMCAccountingInterface gmcAccountingInterface = cc4Bill.getGmcAccountingInterface();
        if (gmcAccountingInterface != null) {
            GMCAccountingInterface existingGMCAccountingInterface = gmcAccountingService.getByConsumerNo(consumerNo);
            if (existingGMCAccountingInterface == null) {
                logger.error(methodName + "Some error to fetch GMC accounting detail.");
                throw new Exception("Some error to fetch GMC accounting detail.");
            }
            gmcAccountingInterface.setId(existingGMCAccountingInterface.getId());
            GMCAccountingInterface insertedGMCAccountingInterface = gmcAccountingService.update(gmcAccountingInterface);
            if (insertedGMCAccountingInterface == null) {
                logger.error(methodName + "some error to insert GMC accounting interface.");
                throw new Exception("some error to insert GMC accounting interface.");
            }
        }

        // updating current bill deleted true
        setUpdateAuditDetails(billToUpdate);
        currentBill.setDeleted(BillInterface.DELETED_TRUE);
        billToUpdate.setDeleted(BillInterface.DELETED_FALSE);
        BillInterface updatedCurrentBill = update(currentBill);
        BillInterface correctedBill = insert(billToUpdate);

        if (updatedCurrentBill == null || correctedBill == null) {
            logger.error(methodName + "some error to insert bill.");
            throw new Exception("some error to insert bill.");
        }

        List<BillInterface> previousBillInterfaces = getByConsumerNoAndBillMonthOrderByIdAsc(consumerNo, billMonth); //don't check deleted flag
        if (previousBillInterfaces == null) {
            logger.error(methodName + "some error to get previous bill");
            throw new Exception("some error to get previous bill");
        }
        BillInterface firstBillFromEngine = previousBillInterfaces.get(0);
        if (firstBillFromEngine == null) {
            logger.error(methodName + "unable to fetch previous bill.");
            throw new Exception("unable to fetch previous bill.");
        }
        List<BillCorrectionInterface> existingBillCorrectionInterfaces = billCorrectionService.getByConsumerNoAndBillMonthAndDeleted(consumerNo, billMonth, false);
        if (existingBillCorrectionInterfaces != null && existingBillCorrectionInterfaces.size() > 0) {
            BillCorrectionInterface existingBillCorrectionInterface = existingBillCorrectionInterfaces.get(0);
            if (existingBillCorrectionInterface != null) {
                existingBillCorrectionInterface.setDeleted(true); //todo use final static
                BillCorrectionInterface updatedBillCorrectionInterface = billCorrectionService.update(existingBillCorrectionInterface);
                if (updatedBillCorrectionInterface == null) {
                    logger.error(methodName + "some error to update existing bill correction details.");
                    throw new Exception("some error to update existing bill correction details.");
                }
            }
        }
        BillCorrectionInterface billCorrectionInterface = billCorrectionService.calculateBillDifferenceComponent(firstBillFromEngine, correctedBill);
        if (billCorrectionInterface == null) {
            logger.error(methodName + "some error to calculate bill component difference.");
            throw new Exception("some error to calculate bill component difference.");
        }
        BillCorrectionInterface insertedBillCorrectionInterface = billCorrectionService.insert(billCorrectionInterface);
        if (insertedBillCorrectionInterface == null) {
            logger.error(methodName + "some error to insert bill component difference.");
            throw new Exception("some error to insert bill component difference.");
        }

        CustomReadMaster insertedCustomReadMaster = GlobalResources.convertReadMasterToCustomReadMaster(insertedRead);
        if (insertedCustomReadMaster == null) {
            logger.error("some error to convert read master from global resources");
            throw new Exception("some error to re-bill");
        }

        cc4Bill.setBill(correctedBill);
        cc4Bill.setCustomReadMaster(insertedCustomReadMaster);
        return cc4Bill;
    }

    //don't use setAuditDetails inside insert, because while saving Bill we are to set ngb-engine version in audit details.
    public BillInterface insert (BillInterface billInterface) {
        String methodName = "insert() : ";
        BillInterface insertedBill = null;
        logger.info(methodName + "called");
        if(billInterface != null) {
            insertedBill = billDAO.add(billInterface);
        }
        return insertedBill;
    }


    public BillInterface update (BillInterface billInterface) {
        String methodName = "update() : ";
        BillInterface updatedBill = null;
        logger.info(methodName + "called");
        if(billInterface != null) {
            setUpdateAuditDetails(billInterface);
            updatedBill = billDAO.update(billInterface);
        }
        return updatedBill;
    }

    private void setAuditDetails(BillInterface billInterface) {
        final String methodName = "setAuditDetails() : ";
        logger.info(methodName + "called");
        if (billInterface != null) {
            String user = GlobalResources.getLoggedInUser();
            billInterface.setCreatedBy(user + " " + globalResources.getProjectDetail());
            billInterface.setCreatedOn(GlobalResources.getCurrentDate());
            billInterface.setUpdatedBy(user + " " + globalResources.getProjectDetail());
            billInterface.setUpdatedOn(GlobalResources.getCurrentDate());
        }
    }

    private void setUpdateAuditDetails(BillInterface billInterface) {
        final String methodName = "setUpdateAuditDetails() : ";
        logger.info(methodName + "called");
        if (billInterface != null) {
            String user = GlobalResources.getLoggedInUser();
            billInterface.setUpdatedBy(user + " " + globalResources.getProjectDetail());
            billInterface.setUpdatedOn(GlobalResources.getCurrentDate());
        }
    }
}
