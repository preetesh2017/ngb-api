package com.mppkvvcl.ngbapi.services;

import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.EmployeeMasterInterface;
import com.mppkvvcl.ngbdao.daos.EmployeeMasterDAO;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by SUMIT on 15-06-2017.
 */
@Service
public class EmployeeMasterService {
    /**
     * Asking Spring to inject EmployeeMasterRepository dependency in the variable
     * so that we get various handles for performing CRUD operation on  table
     * at the backend Database
     */
    @Autowired
    private EmployeeMasterDAO employeeMasterDAO;

    /**
     * getting whole application logger object for performing logs. This logger will only be
     * used in this class methods only.
     */
    private static final Logger logger = GlobalResources.getLogger(EmployeeMasterService.class);

    /**
     * This insert method takes employeeMaster object and insert it into the table in the backend database.
     * return insertedEmployeeMaster if successful else return null.<br><br>
     * param employeeMaster<br>
     * return insertedEmployeeMaster<br>
     */
    public EmployeeMasterInterface insert(EmployeeMasterInterface employeeMaster){
        String methodName = " insert : ";
        EmployeeMasterInterface insertedEmployeeMaster = null;
        logger.info(methodName + "Started insertion for EmployeeMaster ");
        if(employeeMaster != null){
            logger.info(methodName + " Calling EmployeeMastereRepository for inserting EmployeeMaster ");
            insertedEmployeeMaster = employeeMasterDAO.add(employeeMaster);
            if (insertedEmployeeMaster != null){
                logger.info(methodName + "Successfully inserted one row for EmployeeMaster");
            }else {
                logger.error(methodName + "Unable to insert into EmployeeMaster. EmployeeMasterRepository returning null");
            }
        }else{
            logger.error(methodName + " Received EmployeeMaster object is null ");
        }
        return insertedEmployeeMaster;
    }

    /**
     * This method takes employeeNo  fetch employeeMaster object against it.
     * Return employeeMaster if successful else return null.<br><br>
     * param employeeNo<br>
     * return employeeMaster<br>
     */
    public EmployeeMasterInterface getByEmployeeNo(String employeeNo){
        String methodName = "getByEmployeeNo():";
        EmployeeMasterInterface employeeMaster = null;
        logger.info(methodName+"Calling EmployeeMasterRepository to find Employee for Employee No"+employeeNo);
        if(employeeNo != null){
            employeeMaster = employeeMasterDAO.getByEmployeeNo(employeeNo);
            if (employeeMaster != null){
                logger.info(methodName+"Successfully fetched employee record for employee no"+employeeNo);
            } else {
                logger.error(methodName+"unable to fetch record for Employee no"+employeeNo);
            }
        } else {
            logger.error(methodName+"got request null");
        }
        return  employeeMaster;
    }

    /**
     * This method takes employeeNo and Status fetch employeeMaster object against it.
     * Return employeeMaster if successful else return null.<br><br>
     * param employeeNo<br>
     * param status<br>
     * return employeeMaster<br>
     */
    public EmployeeMasterInterface getByEmployeeNoAndStatus(String employeeNo, String status){
        String methodName= "getByEmployeeNoAndStatus():";
        EmployeeMasterInterface employeeMaster = null;
        logger.info(methodName+"Calling EmployeeMasterRepository to find Employee details by Employee No"+employeeNo+"and status"+status);
        if(employeeNo != null && status != null){
            employeeMaster = employeeMasterDAO.getByEmployeeNoAndStatus(employeeNo, status);
            if(employeeMaster != null){
                logger.info(methodName+"Successfully fetched employee record for employee no"+employeeNo+"and status"+status);
            } else {
                logger.error(methodName+"unable to fetch record for Employee no"+employeeNo+"and status"+status);
            }
        } else{
            logger.error(methodName+"got request null");
        }
        return employeeMaster;
    }

    /**
     * This method takes employeeNo, type, status  fetch employeeMaster object against it.
     * Return employeeMaster if successful else return null.<br><br>
     * param employeeNo<br>
     * param type<br>
     * param status<br>
     * return employeeMaster<br>
     */
    public EmployeeMasterInterface getByEmployeeNoAndTypeAndStatus(String employeeNo, String type,String status){
        String methodName = "getByEmployeeNoAndTypeAndStatus() :";
        EmployeeMasterInterface employeeMaster = null;
        logger.info(methodName+"Calling EmployeeMasterRepository to find Employee details by Employee No"+employeeNo+"type"+type+"status"+status);
        if(employeeNo != null && type != null && status != null ){
            employeeMaster = employeeMasterDAO.getByEmployeeNoAndTypeAndStatus(employeeNo,type,status);
            if(employeeMaster != null){
                logger.info(methodName+"Successfully fetched employee record for employee no:"+employeeNo+"type:"+type+"status"+status);
            } else {
                logger.error(methodName+"unable to fetch record for Employee no:"+employeeNo+"type:"+type+"status"+status);
            }
        } else {
            logger.error(methodName+"got request null");
        }
        return employeeMaster;
    }
}
