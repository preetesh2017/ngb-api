package com.mppkvvcl.ngbapi.services;

import com.mppkvvcl.ngbapi.custombeans.Read;
import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.*;
import com.mppkvvcl.ngbentity.beans.ConsumerConnectionInformation;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

/**
 * Created by PREETESH on 7/4/2017.
 */
@Service
public class ReadService {

    /**
     * Getting whole logger object from global resources for current class.
     */
    private static final Logger logger = GlobalResources.getLogger(ReadService.class);

    /**
     * Requesting spring to get singleton ConsumerInformationService object.
     */
    @Autowired
    private ConsumerInformationService consumerInformationService;
    /**
     * Requesting spring to get singleton ConsumerMeterMappingService object.
     */
    @Autowired
    private ConsumerMeterMappingService consumerMeterMappingService;
    /**
     * Requesting spring to get singleton ScheduleService object.
     */
    @Autowired
    private ScheduleService scheduleService;
    /**
     * Requesting spring to get singleton ConsumerConnectionAreaInformationService object.
     */
    @Autowired
    private ConsumerConnectionAreaInformationService consumerConnectionAreaInformationService;
    /**
     * Requesting spring to get singleton ReadMasterService object.
     */
    @Autowired
    private ReadMasterService readMasterService;

    @Autowired
    private ReadTypeConfiguratorService readTypeConfiguratorService;

    @Autowired
    private ConsumerConnectionInformationService consumerConnectionInformationService;

    @Autowired
    private  ConsumerNoMasterService consumerNoMasterService;

    @Autowired
    private TariffDetailService tariffDetailService;

    @Autowired
    private TariffChangeDetailService tariffChangeDetailService;

    /**
     * This getLatestByConsumerNoForPunching method fetches selectedReadMaster for consumerNo <br>
     * Return read object if successful else return null.<br><br>
     * param consumerNo<br>
     * return read<br>
     */
    public Read getLatestByConsumerNoForPunching(String consumerNo) throws Exception{
        String methodName = " getLatestByConsumerNoForPunching() : ";
        Read read = null;
        ConsumerInformationInterface selectedConsumerInformation = null;
        BigDecimal selectedMF =  null;
        ScheduleInterface selectedSchedule = null;
        ReadMasterInterface selectedReadMaster = null;
        ReadTypeConfiguratorInterface readTypeConfigurator = null;
        ConsumerNoMasterInterface consumerNoMaster = null;
        TariffDetailInterface tariffDetail = null;
        TariffChangeDetailInterface tariffChangeDetail = null;
        ConsumerConnectionInformationInterface consumerConnectionInformation = null;
        logger.info(methodName+ "called");
        if (consumerNo == null) {
            logger.error(methodName + " consumer no passed is null, consumer no: "+consumerNo);
            throw new Exception("consumer no passed is null");
        }
        consumerNo = consumerNo.trim();
        logger.info(methodName+ "fetching consumer no master");
        consumerNoMaster = consumerNoMasterService.getByConsumerNo(consumerNo);
        if (consumerNoMaster == null) {
            logger.error(methodName + "Given consumer no doesn't exist in  ConsumerNoMaster");
            throw new Exception("Given consumer no doesn't exist in  ConsumerNoMaster");
        }
        logger.info(methodName + "fetched one row for consumerNoMaster" + consumerNoMaster);
        String status = consumerNoMaster.getStatus();
        if (status == null) {
            logger.error(methodName + "status found null for given consumer no :" + consumerNo);
            throw new Exception("status found null for given consumer no ");
        }
        //Below validation Checks whether consumer is INACTIVE (PDC)
        if (status.equals(ConsumerNoMasterInterface.STATUS_INACTIVE)) {
            logger.error(methodName + "Consumer is INACTIVE consumer No:" + consumerNo);
            throw new Exception("Consumer is inactive");
        }
        logger.info(methodName + "Fetching Tariff Detail for Consumer no: " + consumerNo);
        tariffDetail = tariffDetailService.getLatestTariffByConsumerNo(consumerNo);
        if (tariffDetail == null) {
            logger.error(methodName + "No tariff details found for Consumer no:" + consumerNo);
            throw new Exception("No tariff details found for Consumer");
        }
        tariffChangeDetail = tariffChangeDetailService.getByTariffDetailId(tariffDetail.getId());
        if (tariffChangeDetail != null) {
            logger.info(methodName+"tariff changed for consumer, tariff change detail exist for consumer no:"+consumerNo);
            if(tariffChangeDetail.getMeteringStatus().equals(ConsumerConnectionInformation.METERING_STATUS_UNMETERED)){
                logger.info(methodName+"consumer is of un-metered type");
                throw new Exception("consumer is of un-metered type");
            }
            logger.info(methodName+"consumer is of metered type , proceeding for fetch reading");
        }else {
            logger.info(methodName + "Tariff not changed previously, fetching consumer connection information for consumer no :"+consumerNo);
            consumerConnectionInformation = consumerConnectionInformationService.getByConsumerNo(consumerNo);
            if (consumerConnectionInformation == null) {
                logger.error(methodName + "Consumer connection information not found for consumer no :" + consumerNo);
                throw new Exception("Consumer connection information not found for consumer ");
            }
            logger.info(methodName+ "fetched consumer connection information for consumer no :"+consumerNo);
            if(consumerConnectionInformation.getMeteringStatus().equals(ConsumerConnectionInformation.METERING_STATUS_UNMETERED)){
                logger.info(methodName+"consumer is of un-metered type ");
                throw new Exception("consumer is of un-metered type ");
            }
            logger.info(methodName+"consumer is of metered type , proceeding for fetch reading");
        }
        selectedConsumerInformation = consumerInformationService.getConsumerInformationByConsumerNo(consumerNo);
        if(selectedConsumerInformation == null){
            logger.error(methodName + " consumer information not found for consumer no:"+consumerNo);
            throw new Exception("consumer information not found for consumer");
        }
        logger.info(methodName + " Consumer Information retrieved: "+selectedConsumerInformation);
        selectedMF = consumerMeterMappingService.getMFByConsumerNoForActiveMeter(consumerNo);
        logger.info(methodName + " MF retrieved: "+selectedMF);
        if(selectedMF == null){
            logger.error(methodName + " applied mf not found for consumer no:"+consumerNo);
            throw new Exception("applied mf not found for consumer ");
        }
        selectedSchedule = scheduleService.getLatestNotSubmittedPendingScheduleByConsumerNo(consumerNo);
        logger.info(methodName + " schedule retrieved: "+selectedSchedule);
        if(selectedSchedule == null){
            logger.info(methodName + "Schedule not found for given consumer's group");
            throw new Exception("Schedule not found for given consumer's group");
        }
        ReadMasterInterface existingReadingForSchedule = readMasterService.getLatestReadingByConsumerNoAndBillMonthAndReplacementFlag(consumerNo, selectedSchedule.getBillMonth(),ReadMasterInterface.REPLACEMENT_FLAG_NORMAL_READ);
        if (existingReadingForSchedule != null) {
            logger.error(methodName + " reading for consumer already exists for pending schedule"+existingReadingForSchedule);
            throw new Exception("Reading already punched");
        }
        logger.info(methodName + " no reading found for pending schedule in db, fetching latest reading of consumer for previous bill month");
        selectedReadMaster = readMasterService.getLatestReadingByConsumerNo(consumerNo);
        logger.info(methodName + " Got latest reading as "+selectedReadMaster);
        if( selectedReadMaster == null ){
            logger.error(methodName + " previous Reading not retrieved ");
            throw new Exception("previous Reading not retrieved");
        }
        readTypeConfigurator = readTypeConfiguratorService.getByConsumerNo(consumerNo);
        if (readTypeConfigurator == null) {
            logger.error(methodName + " read type configurator not found ");
            throw new Exception("read type configurator not found");
        }
        read = new Read();
        read.setConsumerInformation(selectedConsumerInformation);
        read.setMf(selectedMF);
        read.setSchedule(selectedSchedule);
        read.setReadMasterInterface(selectedReadMaster);
        read.setReadTypeConfigurator(readTypeConfigurator);
        logger.info(methodName + "read set successfully");
        return read;
    }

    public Read getLatestByConsumerNoForPunchingFinalReading(String consumerNo)throws Exception {
        String methodName = " getLatestByConsumerNoForPunchingFinalReading() ";
        logger.info(methodName+"called");
        Read read = null;
        ConsumerInformationInterface selectedConsumerInformation = null;
        BigDecimal selectedMF =  null;
        ScheduleInterface selectedSchedule = null;
        ReadMasterInterface selectedReadMaster = null;
        ReadTypeConfiguratorInterface readTypeConfigurator;
        if (consumerNo == null) {
            logger.error(methodName + " consumer no passed is null, consumer no: "+consumerNo);
            throw new Exception("consumer no passed is null");
        }
        consumerNo = consumerNo.trim();

        selectedConsumerInformation = consumerInformationService.getConsumerInformationByConsumerNo(consumerNo);
        logger.info(methodName + " Consumer Information retrieved: "+selectedConsumerInformation);
        if(selectedConsumerInformation == null) {
            logger.error(methodName + " consumer information not found for consumer no:"+consumerNo);
            throw new Exception("consumer information not found for consumer");
        }

        readTypeConfigurator = readTypeConfiguratorService.getByConsumerNo(consumerNo);
        if (readTypeConfigurator == null) {
            logger.error(methodName + " read type configurator not found ");
            throw new Exception("read type configurator not found");
        }

        ScheduleInterface lastCompletedSchedule = scheduleService.getLatestCompletedScheduleByConsumerNo(consumerNo);
        if(lastCompletedSchedule == null) {
            logger.error(methodName + " last completed schedule not found ");
            throw new Exception("last completed schedule not found");
        }

        selectedSchedule = scheduleService.createDummyScheduleForPDC(lastCompletedSchedule,GlobalResources.getCurrentDate());
        if(selectedSchedule == null){
            logger.error(methodName + " error in creating dummy schedule ");
            throw new Exception("error in creating dummy schedule");
        }

        ConsumerConnectionInformationInterface consumerConnectionInformation = consumerConnectionInformationService.getByConsumerNo(consumerNo);
        if(consumerConnectionInformation == null || consumerConnectionInformation.getMeteringStatus()==null){
            logger.error(methodName + " consumer connection information not found ");
            throw new Exception("consumer connection information not found");
        }

        String meteringStatus = consumerConnectionInformation.getMeteringStatus();
        if(meteringStatus.equals(ConsumerConnectionInformationInterface.METERING_STATUS_METERED)){
            selectedMF = consumerMeterMappingService.getMFByConsumerNoForActiveMeter(consumerNo);
            logger.info(methodName + " MF retrieved: "+selectedMF);
            if(selectedMF == null) {
                logger.error(methodName + " applied mf not found for consumer no:"+consumerNo);
                throw new Exception("applied mf not found for consumer ");
            }

            selectedReadMaster = readMasterService.getLatestReadingByConsumerNo(consumerNo);
            logger.info(methodName + " Got latest reading as "+selectedReadMaster);
            if( selectedReadMaster == null ) {
                logger.error(methodName + " previous Reading not retrieved ");
                throw new Exception("previous Reading not retrieved");
            }
        }else{
            logger.info(methodName + " consumer is unMetered consumer "+consumerConnectionInformation);
        }
        read = new Read();
        read.setConsumerInformation(selectedConsumerInformation);
        read.setMf(selectedMF);
        read.setReadMasterInterface(selectedReadMaster);
        read.setReadTypeConfigurator(readTypeConfigurator);
        read.setSchedule(selectedSchedule);
        logger.info(methodName + "read set successfully"+read);
        logger.info(methodName + " Returning from method");
        return read;
    }
}

