package com.mppkvvcl.ngbapi.services;

import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.MeterRentInterface;
import com.mppkvvcl.ngbdao.daos.MeterRentDAO;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by ANSHIKA on 23-08-2017.
 */
@Service
public class MeterRentService {

    /**
     * Getting whole logger object from global resources for current class.
     */
    private static Logger logger = GlobalResources.getLogger(MeterRentService.class);
    
    /**
     * Requesting spring to get singleton MeterRent Repository object.
     */
    @Autowired
    private MeterRentDAO meterRentDAO;

    /**
     * This method takes meterCode and fetch the list of meterRent from backend.
     * Return list of meterRent if successful else return null.
     * param meterCode
     * return list of meterRent
     */

    public List<MeterRentInterface> getByMeterCode(String meterCode)
    {
        String methodName = "getByMeterCode() : ";
        logger.info(methodName + "Got request to view list of meterRent from MeterRent table");
        List<MeterRentInterface> meterRents = null;
        if(meterCode != null){
            logger.info(methodName + "Calling meterRentDAO method to get list of meterRent");
            meterRents = meterRentDAO.getByMeterCode(meterCode);
        }
        return meterRents;
    }
}
