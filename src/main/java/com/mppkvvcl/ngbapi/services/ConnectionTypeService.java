package com.mppkvvcl.ngbapi.services;

import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.ConnectionTypeInterface;
import com.mppkvvcl.ngbdao.daos.ConnectionTypeDAO;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by ANSHIKA on 18-07-2017.
 */
@Service
public class ConnectionTypeService {
    /**
     * Requesting spring to get singleton ConnectionTypeRepository object.
     */
    @Autowired
    ConnectionTypeDAO connectionTypeDAO;

    /**
     * Getting logger object from global resources for current class.
     */
    private static final Logger logger = GlobalResources.getLogger(ConnectionTypeService.class);

    /**
     * This getAll method fetches the list of connection types from backend database.<br><br>
     * Return list if successful else return null.<br>
     * return  List of ConnectionType<br>
     */
    public List<? extends ConnectionTypeInterface> getAll(){
        String methodName = "getAll() : ";
        List<? extends ConnectionTypeInterface> connectionTypes = null;
        logger.info(methodName + "Got request to view all connectionTypes");
        connectionTypes = connectionTypeDAO.getAll();
        if(connectionTypes != null) {
            if (connectionTypes.size() > 0) {
                logger.info(methodName + "List received successfully with rows : " + connectionTypes.size());
            } else {
                logger.error(methodName + "No content found in list");
            }
        }else {
            logger.error(methodName + "List not found");
        }
        return connectionTypes;
    }
}
