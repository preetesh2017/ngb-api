package com.mppkvvcl.ngbapi.services;

import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbdao.daos.TariffDescriptionDAO;
import com.mppkvvcl.ngbinterface.interfaces.TariffDescriptionInterface;
import com.mppkvvcl.ngbinterface.interfaces.TariffDetailInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by SUMIT on 19-05-2017.
 **/
@Service
public class TariffDescriptionService {

    private static final Logger logger = GlobalResources.getLogger(TariffDescriptionService.class);

    @Autowired
    private TariffDescriptionDAO tariffDescriptionDAO;

    @Autowired
    private TariffDetailService tariffDetailService;

    /**
     * This getAll method fulfills tariffDescriptionController calls
     * for getting all tariffs from tariff resource.<br><br>
     * return tariffDescriptionDAO<br>
     */
    public List<? extends TariffDescriptionInterface> getAll(){
        final String methodName = "getAll() : ";
        logger.info(methodName + "called");
        return tariffDescriptionDAO.getAll();
    }

    /**
     * This method returns TariffDescription for the supplied tariff for e.g on passing LV2 it returns COMMERCIAL<br><br>
     * param tariffCategory<br><br>
     * return tariffDescriptionDAO<br>
     */
    public TariffDescriptionInterface getByTariffCategory(String tariffCategory){
        final String methodName = "getByTariffCategory() : ";
        logger.info(methodName + "called with tariffCategory " + tariffCategory);
        TariffDescriptionInterface tariffDescriptionInterface = null;
        if(tariffCategory != null){
            tariffDescriptionInterface = tariffDescriptionDAO.getByTariffCategory(tariffCategory);
        }
        return tariffDescriptionInterface;
    }

    public boolean checkTariffDescriptionByConsumerNo (String consumerNo ,String tariffDescriptionToCheck) {
        String methodName = "checkTariffDescriptionByConsumerNo() : ";
        logger.info(methodName + "called " + consumerNo + " And tariff description is " + tariffDescriptionToCheck);
        TariffDescriptionInterface tariffDescriptionInterface = null;
        if (consumerNo != null && tariffDescriptionToCheck != null) {
            TariffDetailInterface tariffDetailInterface = tariffDetailService.getLatestTariffByConsumerNo(consumerNo);
            if (tariffDetailInterface == null) {
                logger.error(methodName + "tariff detail not found.");
                return false;
            }
            String tariffCode = tariffDetailInterface.getTariffCode();
            if (tariffCode == null) {
                logger.error(methodName + "tariff code is null.");
                return false;
            }
            String tariffCategory = GlobalResources.getTariffCategoryFromTariffCode(tariffCode);
            if (tariffCategory == null) {
                logger.error(methodName + "tariff category not found.");
                return false;
            }
            tariffDescriptionInterface = getByTariffCategory(tariffCategory);
            if (tariffDescriptionInterface == null) {
                logger.error(methodName + "tariff description is null.");
                return false;
            }
            String consumerTariffDescription = tariffDescriptionInterface.getTariffDescription();
            if (consumerTariffDescription == null || consumerTariffDescription.isEmpty()) {
                logger.error(methodName + "some error to get tariff description.");
                return false;
            }
            if (consumerTariffDescription.equalsIgnoreCase(tariffDescriptionToCheck)) {
                return true;
            }
        }
        return false;
    }
}
