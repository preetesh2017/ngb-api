package com.mppkvvcl.ngbapi.services;

import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.ConsumerConnectionMeterInformationInterface;
import com.mppkvvcl.ngbdao.daos.ConsumerConnectionMeterInformationDAO;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by SUMIT on 14-06-2017.
 */
@Service
public class ConsumerConnectionMeterInformationService  {
    /**
     * Asking Spring to inject consumerConnectionMeterInformationRepository dependency in the variable
     * so that we get various handles for performing CRUD operation on consumer connection meter information table
     * at the backend Database
     */
    @Autowired
    private ConsumerConnectionMeterInformationDAO consumerConnectionMeterInformationDAO ;

    /**
     * getting whole application logger object for performing logs. This logger will only be
     * used in this class methods only.
     */
    private static final Logger logger = GlobalResources.getLogger(ConsumerConnectionMeterInformationService.class);

    /**
     * This method takes consumerConnectionMeterInformation object and insert it into the backend database.
     * Return insertedConsumerConnectionMeterInformation if successful else return null.<br><br>
     * param consumerConnectionMeterInformation<br>
     * return insertedConsumerConnectionMeterInformation
     */
    public ConsumerConnectionMeterInformationInterface insert(ConsumerConnectionMeterInformationInterface consumerConnectionMeterInformation){
        String methodName = "insert : " ;
        ConsumerConnectionMeterInformationInterface insertedConsumerConnectionMeterInformation = null;
        logger.info(methodName + "Started insertion for ConsumerConnectionMeterInformation ");
        if(consumerConnectionMeterInformation != null){
            logger.info(methodName + "Calling ConsumerConnectionMeterInformationRepository for inserting ConsumerConnectionMeterInformation");
            insertedConsumerConnectionMeterInformation = consumerConnectionMeterInformationDAO.add(consumerConnectionMeterInformation);
            if (insertedConsumerConnectionMeterInformation != null) {
                logger.info(methodName + "Successfully inserted consumerConnectionMeterInformation Mappping");
            }else{
                logger.error(methodName + "unable to insert consumer connection meter information row. Repository sending null");
            }
        }else{
            logger.error(methodName + "consumerConnectionMeterInformation received is null .");
        }
        return insertedConsumerConnectionMeterInformation ;
    }

    /**
     * By Preetesh
     * This method takes consumerNo and fetch a consumerConnectionMeterInformation object from backend database.
     * Return consumerConnectionMeterInformation iof successful else return null.
     * param consumerNo<br>
     * return consumerConnectionMeterInformation
     */
    public ConsumerConnectionMeterInformationInterface getByConsumerNo(String consumerNo) {
        String methodName = " getByConsumerNo(): ";
        logger.info(methodName + " service method called for fetching Readings for consumer " + consumerNo);
        ConsumerConnectionMeterInformationInterface consumerConnectionMeterInformation = null;
        if (consumerNo != null) {
            consumerNo = consumerNo.trim();
            consumerConnectionMeterInformation = consumerConnectionMeterInformationDAO.getByConsumerNo(consumerNo);
            if (consumerConnectionMeterInformation != null ) {
                logger.info(methodName + " Successfully retrieved " + consumerConnectionMeterInformation +
                        " readings by consumer number" );
            } else {
                logger.error(methodName + " nothing retrieved for consumer number:" + consumerNo);
            }
        } else {
            logger.error(methodName + "consumerNo passed is null in method.");
        }
        return consumerConnectionMeterInformation;
    }
}
