package com.mppkvvcl.ngbapi.services;

import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbdao.daos.TariffLoadMappingDAO;
import com.mppkvvcl.ngbinterface.interfaces.TariffLoadMappingInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by vikas on 02-Nov-17.
 */

@Service
public class TariffLoadMappingService {

    /**
     * Getting whole logger object from GlobalResources for current class.
     */
    private static final Logger logger = GlobalResources.getLogger(TariffLoadMappingService.class);


    /**
     *Requesting spring to get singleton TariffLoadMappingDAO object.
     */
    @Autowired
    private TariffLoadMappingDAO tariffLoadMappingDAO;


    public TariffLoadMappingInterface getLatestTariffLoadMappingByTariffDetailId(long tariffDetailId){
        String methodName = "getLatestTariffLoadMappingByTariffId() : ";
        TariffLoadMappingInterface tariffDetailInterface = null;
        logger.info(methodName + "called ");
        if(tariffDetailId > 0) {
            tariffDetailInterface = tariffLoadMappingDAO.getTopByTariffDetailIdOrderByIdDesc(tariffDetailId);
        }
        return tariffDetailInterface;
    }

    public TariffLoadMappingInterface getLatestTariffLoadMappingByLoadDetailId(long tariffDetailId){
        String methodName = "getLatestTariffLoadMappingByLoadDetailId() : ";
        TariffLoadMappingInterface tariffDetailInterface = null;
        logger.info(methodName + "called ");
        if(tariffDetailId > 0) {
            tariffDetailInterface = tariffLoadMappingDAO.getTopByLoadDetailIdOrderByIdDesc(tariffDetailId);
        }
        return tariffDetailInterface;
    }

    public TariffLoadMappingInterface insert (TariffLoadMappingInterface tariffLoadMapping) {
        String methodName = "insert() : ";
        TariffLoadMappingInterface insertedLoadDetail1 = null;
        logger.info(methodName + "called ");
        if(tariffLoadMapping != null) {
            insertedLoadDetail1 = tariffLoadMappingDAO.add(tariffLoadMapping);
        }
        return insertedLoadDetail1;
    }
}
