package com.mppkvvcl.ngbapi.services;

import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.ReadMasterKWInterface;
import com.mppkvvcl.ngbdao.daos.ReadMasterKWDAO;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by PREETESH on 6/29/2017.
 */
@Service
public class ReadMasterKWService {
    /**
     * Requesting spring to inject singleton ReadMasterKWRepository object.
     */
    @Autowired
    private ReadMasterKWDAO readMasterKWDAO;

    /**
     * Getting logger object from global resources for current class.
     */
    private static final Logger logger = GlobalResources.getLogger(ReadMasterKWService.class);

    /**
     * This getByReadMasterId method takes input param readMasterId and fetches the ReadMasterKW table from backend database <br>
     * Return readMasterKW object if successful else return null.<br><br>
     * param readMasterId<br>
     * return<br>
     */
    public ReadMasterKWInterface getByReadMasterId(long readMasterId) {
        String methodName = "getByReadMasterId() : ";
        logger.info(methodName + "called with id " + readMasterId);
        ReadMasterKWInterface readMasterKW = null;
        readMasterKW = readMasterKWDAO.getByReadMasterId(readMasterId);
        return readMasterKW;
    }

    /**
     * This insert method takes readMasterKW object and insert it into the ReadMasterKW table in the backend database<br>
     * Return insertedReadMaster object if successful else return null.<br><br>
     * param readMasterKW<br>
     * return insertedReadMaster<br>
     */
    @Transactional(rollbackFor = Exception.class)
    public ReadMasterKWInterface insert(ReadMasterKWInterface readMasterKW) throws Exception {
        String methodName = "insert() : ";
        logger.info(methodName + "called");
        ReadMasterKWInterface insertedReadMaster = null;
        if (readMasterKW != null) {
            //logic to write
            insertedReadMaster = readMasterKWDAO.add(readMasterKW);
            if (insertedReadMaster != null) {
                logger.info(methodName + "successfully inserted ReadMasterKW row");
            } else {
                logger.error(methodName + "Insertion of ReadMasterKW failed");
                throw new Exception("Insertion of ReadMasterKW failed");
            }
        } else {
            logger.error(methodName + "ReadMasterKW to insert is null");
            throw new Exception("ReadMasterKW to insert is null");
        }
        return insertedReadMaster;
    }


    public ReadMasterKWInterface update(ReadMasterKWInterface readMasterKW){
        String methodName = "update ";
        ReadMasterKWInterface updatedReadMasterKW = null ;
        logger.info(methodName + "Called");
        if(readMasterKW != null) {
            setUpdatedDetails(readMasterKW);
            updatedReadMasterKW = readMasterKWDAO.update(readMasterKW);
        }
        return updatedReadMasterKW ;
    }

    private void setUpdatedDetails(ReadMasterKWInterface readMasterKW){
        String methodName = "setUpdatedDetails()  :";
        if(readMasterKW != null) {
            readMasterKW.setUpdatedOn(GlobalResources.getCurrentDate());
            readMasterKW.setUpdatedBy(GlobalResources.getLoggedInUser());
        }
    }
}
