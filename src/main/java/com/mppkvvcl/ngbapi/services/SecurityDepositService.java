package com.mppkvvcl.ngbapi.services;

import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbdao.daos.SecurityDepositDAO;
import com.mppkvvcl.ngbinterface.interfaces.SecurityDepositInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by PREETESH on 12/15/2017.
 */
@Service
public class SecurityDepositService {

    /**
     * getting whole application logger object for performing logs. This logger will only be
     * used in this class methods only.
     */
    private Logger logger = GlobalResources.getLogger(SecurityDepositService.class);


    @Autowired
    private SecurityDepositDAO securityDepositDAO;

    public List<SecurityDepositInterface> getByConsumerNo(String consumerNo) {
        String methodName = " getByConsumerNo() : ";
        logger.info(methodName + "called");
        List<SecurityDepositInterface> securityDepositInterfaces = null;
        if (consumerNo != null) {
            securityDepositInterfaces = securityDepositDAO.getByConsumerNoOrderByIdDesc(consumerNo);
        }

        return securityDepositInterfaces;
    }


    public SecurityDepositInterface getTopByConsumerNo(String consumerNo) {
        String methodName = " getTopByConsumerNo(): ";
        logger.info(methodName + "called");
        SecurityDepositInterface securityDepositInterface = null;
        if (consumerNo != null) {
            securityDepositInterface = securityDepositDAO.getTopByConsumerNoOrderByIdDesc(consumerNo);
        }
        return securityDepositInterface;
    }


    public SecurityDepositInterface insert(SecurityDepositInterface securityDepositInterface){
        String methodName = " insert : ";
        logger.info(methodName + "called ");
        SecurityDepositInterface insertedSecurityDepositInterface = null;
        if(securityDepositInterface != null) {
            insertedSecurityDepositInterface = securityDepositDAO.add(securityDepositInterface);
        }
        return insertedSecurityDepositInterface;
    }

    public SecurityDepositInterface update(SecurityDepositInterface securityDepositInterface){
        String methodName = " update : ";
        logger.info(methodName + "called ");
        SecurityDepositInterface updatedSecurityDepositInterface = null;
        if(securityDepositInterface != null) {
            updatedSecurityDepositInterface = securityDepositDAO.update(securityDepositInterface);
        }
        return updatedSecurityDepositInterface;
    }
}
