package com.mppkvvcl.ngbapi.services;

import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbdao.daos.GMCAccountingDAO;
import com.mppkvvcl.ngbengine.factories.GMCAccountingFactory;
import com.mppkvvcl.ngbinterface.interfaces.GMCAccountingInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public class GMCAccountingService {

    private static final Logger logger = GlobalResources.getLogger(GMCAccountingService.class);

    @Autowired
    private GMCAccountingDAO gmcAccountingDAO;

    public GMCAccountingInterface insert(GMCAccountingInterface gmcAccountingInterfaceToAdd) {
        String methodName = " insert() :";
        logger.info(methodName + "called");
        GMCAccountingInterface insertedGMCAccountingInterface = null;
        if (gmcAccountingInterfaceToAdd != null) {
            insertedGMCAccountingInterface = gmcAccountingDAO.add(gmcAccountingInterfaceToAdd);
        }
        return insertedGMCAccountingInterface;
    }

    public GMCAccountingInterface update(GMCAccountingInterface gmcAccountingInterfaceToUpdate) {
        String methodName = " update() :";
        logger.info(methodName + "called");
        GMCAccountingInterface updatedBillCorrectionInterface = null;
        if (gmcAccountingInterfaceToUpdate != null) {
            updatedBillCorrectionInterface = gmcAccountingDAO.update(gmcAccountingInterfaceToUpdate);
        }
        return updatedBillCorrectionInterface;
    }

    public GMCAccountingInterface getByConsumerNo(String consumerNo) {
        String methodName = "getByConsumer() : ";
        logger.info(methodName + "called");
        GMCAccountingInterface gmcAccountingInterface = null;
        if (consumerNo != null) {
            gmcAccountingInterface = gmcAccountingDAO.getByConsumerNo(consumerNo);
        }
        return gmcAccountingInterface;
    }

    public GMCAccountingInterface getGMCAccountingByReplacingCurrentValuesWithPreviousValues(String consumerNo) {
        final String methodName = "getGMCAccountingByReplacingCurrentValuesWithPreviousValues () : ";
        logger.info(methodName + "called ");
        GMCAccountingInterface updatedGMCAccountingInterface = null;
        if (consumerNo != null) {
            GMCAccountingInterface existingGMCAccountingInterface = gmcAccountingDAO.getByConsumerNo(consumerNo);
            if (existingGMCAccountingInterface != null) {
                updatedGMCAccountingInterface = GMCAccountingFactory.build();

                updatedGMCAccountingInterface.setConsumerNo(consumerNo);

                String previousMonth = existingGMCAccountingInterface.getPreviousMonth();
                if (previousMonth != null) {
                    updatedGMCAccountingInterface.setCurrentBillMonth(previousMonth);
                }
                String previousReadType = existingGMCAccountingInterface.getPreviousReadType();
                if (previousReadType != null) {
                    updatedGMCAccountingInterface.setReadType(previousReadType);
                }
                BigDecimal previousConsumption = existingGMCAccountingInterface.getPreviousConsumption();
                if (previousConsumption != null) {
                    updatedGMCAccountingInterface.setCurrentConsumption(previousConsumption);
                }
                BigDecimal previousActualCumulativeConsumption = existingGMCAccountingInterface.getPreviousActualCumulativeConsumption();
                if (previousActualCumulativeConsumption != null) {
                    updatedGMCAccountingInterface.setActualCumulativeConsumption(previousActualCumulativeConsumption);
                }
                BigDecimal previousMinimumCumulative = existingGMCAccountingInterface.getPreviousMinimumCumulative();
                if (previousMinimumCumulative != null) {
                    updatedGMCAccountingInterface.setMinimumCumulative(previousMinimumCumulative);
                }
                BigDecimal previousHigherOfActualMinimumCumulative = existingGMCAccountingInterface.getPreviousHigherOfActualMinimumCumulative();
                if (previousHigherOfActualMinimumCumulative != null) {
                    updatedGMCAccountingInterface.setHigherOfActualMinimumCumulative(previousHigherOfActualMinimumCumulative);
                }
                BigDecimal previousAlreadyBilled = existingGMCAccountingInterface.getPreviousAlreadyBilled();
                if (previousAlreadyBilled != null) {
                    updatedGMCAccountingInterface.setAlreadyBilled(previousAlreadyBilled);
                }
                BigDecimal previousToBeBilled = existingGMCAccountingInterface.getPreviousToBeBilled();
                if (previousToBeBilled != null) {
                    updatedGMCAccountingInterface.setToBeBilled(previousToBeBilled);
                }
            }
        }
        return updatedGMCAccountingInterface;
    }
}
