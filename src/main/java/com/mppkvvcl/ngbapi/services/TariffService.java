package com.mppkvvcl.ngbapi.services;

import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.TariffInterface;
import com.mppkvvcl.ngbdao.daos.TariffDAO;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by SUMIT on 19-05-2017.
 *
 *  This tariff Service defines various services which serve controllers requests for resources under nsc database
 * and nsc schema for nextgenbillingbackend.This tariff service for tariffs has the required and related
 * methods for fulfilling controller calls.
 * */
@Service
public class TariffService {
  /**
   * Getting whole logger object from global resources for current class.
   */
  private static final Logger logger = GlobalResources.getLogger(TariffService.class);
  /**
   * Requesting spring to inject singleton TariffRepository object.
   */
  @Autowired
  private TariffDAO tariffDAO;

  /**
   * This getAllTariffs service method fulfills controller calls<br>
   *for getting all tariffs from tariff resource<br>
   **/
  public List<? extends TariffInterface> getAllTariffs(){
    final String methodName = "getAllTariffs() : ";
    logger.info(methodName + "called");
    List<? extends TariffInterface> allTariffs = tariffDAO.getAll();
    return allTariffs;
  }

  /**
   * This getTariffById service method fulfills controller calls for<br>
   *getting a tariff from tariff by passed id<br>
   **/
  public TariffInterface getTariffById(int id){
    final String methodName = "getTariffById() : ";
    logger.info(methodName + "called");
    TariffInterface tariff= tariffDAO.getById(id);
    return tariff;
  }

  /**
   * This method takes below input params and fetches list of Tariff table from backend database<br>
   * Return list if successful else return null.<br><br>
   * param tariffCategory<br>
   * param meteringStatus<br>
   * param connectionType<br>
   * param effectiveDate<br><br>
   * return null if there are no tariff available<br>
   */
  public List<TariffInterface> getByTariffCategoryAndMeteringStatusAndConnectionTypeAndEffectiveDate(String tariffCategory,String meteringStatus,String connectionType, String effectiveDate){
    logger.info("getByTariffCategoryAndMeteringStatusAndConnectionTypeAndEffectiveDate: Getting tariff by ");
    if(tariffCategory != null && meteringStatus != null && connectionType != null){
      if(effectiveDate == null){
        String currentDate = makeCurrentDate();
        logger.info("getByTariffCategoryAndMeteringStatusAndConnectionTypeAndEffectiveDate: Got current Date from tomcat as: "+currentDate);
        effectiveDate = currentDate;
      }
      return tariffDAO.getByTariffCategoryAndMeteringStatusAndConnectionTypeAndEffectiveDate(tariffCategory,meteringStatus,connectionType,effectiveDate);
    }
    return null;
  }

  /**
   * This method takes below input params and fetches the list of Tariff table from backend database<br>
   * Return list if successful else return null.<br><br>
   * param tariffCategory<br>
   * param meteringStatus<br>
   * param connectionType<br>
   * param tariffType<br>
   * param effectiveDate<br><br>
   * return null if there is no tariff available.<br>
   */
  public List<TariffInterface> getByTariffCategoryAndMeteringStatusAndConnectionTypeAndTariffTypeAndEffectiveDate(String tariffCategory,String meteringStatus,String connectionType,String tariffType ,String effectiveDate){
    logger.info("getByTariffCategoryAndMeteringStatusAndConnectionTypeAndTariffTypeAndEffectiveDate : Getting tariff by ");
    if(tariffCategory != null && meteringStatus != null && tariffType!=null && connectionType != null){
      if(effectiveDate == null){
        String currentDate = makeCurrentDate();
        logger.info("getByTariffCategoryAndMeteringStatusAndConnectionTypeAndTariffTypeAndEffectiveDate: Got current Date from tomcat as: "+currentDate);
        effectiveDate = currentDate;
      }
      return tariffDAO.getByTariffCategoryAndMeteringStatusAndConnectionTypeAndTariffTypeAndEffectiveDate(tariffCategory,meteringStatus,connectionType,tariffType,effectiveDate);
    }
    return null;
  }


  private String makeCurrentDate(){
    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
    String currentDate = sdf.format(new Date());
    return currentDate;
  }

  /**
   * This getByTariffCode method takes tariffCode and effectiveDate as input parameter and fetches Tariff table from backend database<br>
   * Return tariff object if successful else return null<br><br>
   * param tariffCode<br>
   * param effectiveDate<br>
   * return tariff<br>
   */
  public TariffInterface getByTariffCode(String tariffCode,String effectiveDate) {
    String methodName="getByTariffCode()";
    logger.info(methodName+"Getting tariff by: "+tariffCode);
    TariffInterface tariff = null;
    if(tariffCode != null){
      tariffCode = tariffCode.trim();
      if(effectiveDate != null){
        tariff = tariffDAO.getByTariffCodeAndEffectiveDate(tariffCode,effectiveDate);
      }else{
        tariff = tariffDAO.getTopByTariffCodeOrderByIdDesc(tariffCode);
      }
      if (tariff != null){
        logger.info(methodName+"Got tariff as: "+tariff);
      }else{
        logger.error(methodName+"tariff not retrieved for: "+tariffCode);
      }
    }
    return tariff;
  }
}
