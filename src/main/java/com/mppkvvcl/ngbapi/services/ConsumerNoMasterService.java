package com.mppkvvcl.ngbapi.services;

import com.mppkvvcl.ngbapi.custombeans.GroupConsumerData;
import com.mppkvvcl.ngbapi.custombeans.Read;
import com.mppkvvcl.ngbapi.custombeans.ReadingDiaryConsumerData;
import com.mppkvvcl.ngbapi.custombeans.ZoneConsumerData;
import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbengine.EngineIgnition;
import com.mppkvvcl.ngbengine.interfaces.HolderInterface;
import com.mppkvvcl.ngbentity.beans.*;
import com.mppkvvcl.ngbinterface.interfaces.*;
import com.mppkvvcl.ngbdao.daos.ConsumerNoMasterDAO;
import com.mppkvvcl.ngbentity.beans.Adjustment;
import com.mppkvvcl.ngbentity.beans.ErrorMessage;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by SUMIT on 20-06-2017.
 */

@Service
public class ConsumerNoMasterService {
    /**
     * Asking Spring to inject ConsumerNoMasterRepository dependency in the variable
     * so that we get various handles for performing CRUD operation on  table
     * at the backend Database
     */
    @Autowired
    private ConsumerNoMasterDAO consumerNoMasterDAO;

    @Autowired
    private ReadService readService;

    @Autowired
    private TariffDescriptionService tariffDescriptionService;

    @Autowired
    private ConfiguratorService configuratorService;

    @Autowired
    private ReadMasterService readMasterService;

    @Autowired
    private ReadMasterPFService readMasterPFService;

    @Autowired
    private ReadMasterKWService readMasterKWService;

    @Autowired
    private TariffService tariffService;

    @Autowired
    private ConsumerConnectionInformationService consumerConnectionInformationService;

    @Autowired
    private ReadTypeConfiguratorService readTypeConfiguratorService;

    @Autowired
    private MeterMasterService meterMasterService;

    @Autowired
    private ConsumerMeterMappingService consumerMeterMappingService;

    @Autowired
    private  ScheduleService scheduleService;

    @Autowired
    private  AdjustmentService adjustmentService;

    @Autowired
    private ConsumerConnectionAreaInformationService consumerConnectionAreaInformationService;

    @Autowired
    private ZoneService zoneService;

    @Autowired
    private GroupService groupService;

    @Autowired
    private ReadingDiaryNoService readingDiaryNoService;

    @Autowired
    private HolderService holderService;

    @Autowired
    private TariffDetailService tariffDetailService;

    @Autowired
    private BillService billService;

    @Autowired
    private ConsumerInformationService consumerInformationService;

    @Autowired
    private EmployeeConnectionMappingService employeeConnectionMappingService;

    @Autowired
    private BPLConnectionMappingService bplConnectionMappingService;

    @Autowired
    private PaymentService paymentService;

    @Autowired
    private XrayConnectionInformationService xrayConnectionInformationService;

    @Autowired
    private SecurityDepositService securityDepositService;

    /**
     /**
     * getting whole application logger object for performing logs. This logger will only be
     * used in this class methods only.
     */
    private static final Logger logger = GlobalResources.getLogger(ConsumerNoMasterService.class);

    /**
     * This method takes oldServiceNoOne from backend database.Return consumerNoMaster if successful else return null.<br><br>
     * param oldServiceNoOne<br>
     * return consumerNoMaster
     */
    public ConsumerNoMasterInterface getByOldServiceNoOne(String oldServiceNoOne){
        String methodName = " getByOldServiceNoOne : ";
        logger.info(methodName + "Started getByOldServiceNoOne");
        ConsumerNoMasterInterface consumerNoMaster = null;
        if(oldServiceNoOne != null){
            logger.info(methodName + " Calling ConsumerNoMasterRepository for getting ConsumerNoMaster ");
            consumerNoMaster = consumerNoMasterDAO.getByOldServiceNoOne(oldServiceNoOne);
            if (consumerNoMaster != null){
                logger.info(methodName + "Successfully got one row for consumerNoMaster");
            }else {
                logger.error(methodName + "Unable to row from ConsumerNoMasterRepository. ConsumerNoMasterRepository returning null");
            }
        }else{
            logger.error(methodName + " Received oldServiceNoOne  is null ");
        }
        return consumerNoMaster;

    }

    /**
     * This method takes oldServiceNoOne and locationCode from backend database.Return consumerNoMaster if successful else return null.<br><br>
     * @param oldServiceNoOne
     * @param locationCode
     * return consumerNoMaster
     */

    public ConsumerNoMasterInterface getByOldServiceNoOneAndLocationCode(String oldServiceNoOne, String locationCode){
        String methodName = " getByOldServiceNoOneAndLocationCode() : ";
        logger.info(methodName + "called");
        ConsumerNoMasterInterface consumerNoMaster = null;
        if(oldServiceNoOne != null && locationCode != null){
            consumerNoMaster = consumerNoMasterDAO.getByOldServiceNoOneAndLocationCode(oldServiceNoOne,locationCode);
        }
        return consumerNoMaster;

    }

    /**
     * added by : Preetesh 21 Aug 2017
     * this method will return the custom bean Read, so that Final Read can be prepared for PDC consumer.
     * before that we check for domestic , single-phase connection, and than difference in connection date and current date.
     * As per Supple Code 2013 --
     * Consumers other than domestic and single-phase non-domestic category can terminate the agreement
     * after the expiry of the initial period of two years.
     * @param consumerNo
     * @return
     * @throws Exception
     */
    public Read getPDCDataForConsumerNo(String consumerNo) throws Exception {
        String methodName = " getPDCDataForConsumerNo : ";
        logger.info(methodName + "called");
        ConsumerNoMasterInterface consumerNoMaster = null;
        Read readToReturn = null;
        if(consumerNo == null ) {
            logger.error(methodName + " Received consumer no is null ");
            throw new Exception("consumer no passed is null");
        }

        consumerNo = consumerNo.trim();
        consumerNoMaster = consumerNoMasterDAO.getByConsumerNo(consumerNo);
        if (consumerNoMaster == null){
            logger.error(methodName + "Consumer Not found with consumer no: "+consumerNo);
            throw new Exception("Consumer Not found with consumer no: "+consumerNo);
        }

        String status = consumerNoMaster.getStatus();
        if(status == null ){
            logger.error(methodName + "status found null");
            throw new Exception("status found null");
        }

        if(status.equals(ConsumerNoMasterInterface.STATUS_INACTIVE)){
            logger.error(methodName + "Consumer is not in ACTIVE state, can't process PDC");
            throw new Exception("Consumer is not in ACTIVE state, can't process PDC");
        }

        Read readBeforeCheck = readService.getLatestByConsumerNoForPunchingFinalReading(consumerNo);
        if(readBeforeCheck == null) {
            logger.error(methodName + "some error in fetching CustomRead");
            throw new Exception("some error in fetching CustomRead");
        }

        ConsumerConnectionInformationInterface consumerConnectionInformation = consumerConnectionInformationService.getByConsumerNo(consumerNo);
        if(consumerConnectionInformation == null){
            logger.error(methodName + "consumer connection information not found");
            throw new Exception("consumer connection information not found");
        }

        String tariffCategory = consumerConnectionInformation.getTariffCategory();
        if(tariffCategory == null) {
            logger.error(methodName + "tariffCategory not found");
            throw new Exception("tariffCategory not found");
        }
        TariffDescriptionInterface tariffDescription = tariffDescriptionService.getByTariffCategory(tariffCategory);
        if(tariffDescription == null){
            logger.error(methodName + "tariffDescription not found");
            throw new Exception("tariffDescription not found");
        }
        String tariffDescriptionString = tariffDescription.getTariffDescription();
        if(tariffDescriptionString.equals(TariffDescriptionInterface.TARIFF_DESCRIPTION_DOMESTIC)){
            readToReturn = readBeforeCheck;
        }else{
            String phase = consumerConnectionInformation.getPhase();
            if(phase == null){
                logger.error(methodName + "phase is null in consumerConnectionInformation");
                throw new Exception("phase is null in consumerConnectionInformation");
            }
            if(phase.equals(ConsumerConnectionInformationInterface.PHASE_SINGLE)){
                readToReturn = readBeforeCheck;
            }else{
                Date dateOfConnection = consumerConnectionInformation.getConnectionDate();
                long connectionAge = GlobalResources.getDateDiffInDays(dateOfConnection,GlobalResources.getCurrentDate());
                /**
                 * checking if connection is more than 2 years old, i.e.consumer has completed agreement period
                 */

                long agreementPeriodInDays = configuratorService.getValueForCode(ConfiguratorInterface.AGREEMENT_PERIOD);
                if(connectionAge < agreementPeriodInDays){
                    logger.error(methodName + "consumer is under agreement period");
                    throw new Exception("consumer is under agreement period");
                }
                readToReturn = readBeforeCheck;
            }
        }
        return readToReturn;
    }


    /**
     *
     * @param consumerNo
     * @param finalRead
     * @param pdcDateString
     * @param pdcRemark
     * @return
     * @throws Exception
     */
    @Transactional(rollbackFor = Exception.class)
    public BillInterface pdcConsumerAndInsertFinalRead(String consumerNo, ReadMasterInterface finalRead, String pdcDateString, String pdcRemark)throws Exception {
        String methodName = " pdcConsumerAndInsertFinalRead() : ";
        logger.info(methodName+"called");
        ConsumerNoMasterInterface consumerNoMaster = null;
        BillInterface updatedBill;

        if(consumerNo == null || pdcDateString == null || pdcRemark == null) {
            logger.error(methodName + " Received consumer no is null ");
            throw new Exception("consumer no passed is null");
        }
        consumerNo = consumerNo.trim();
        Date pdcDate = GlobalResources.getDateFromString(pdcDateString);

        consumerNoMaster = consumerNoMasterDAO.getByConsumerNo(consumerNo);
        if (consumerNoMaster == null){
            logger.error(methodName + "Unable to fetch row from ConsumerNoMasterRepository");
            throw new Exception("Unable to fetch row from ConsumerNoMasterRepository");
        }

        String status = consumerNoMaster.getStatus();
        if(status == null ){
            logger.error(methodName + "status found null");
            throw new Exception("status found null");
        }
        if(status.equals(ConsumerNoMasterInterface.STATUS_INACTIVE)){
            logger.error(methodName + "Consumer is not in ACTIVE state, can not process PDC");
            throw new Exception("Consumer is not in ACTIVE state, can not process PDC");
        }

        ConsumerConnectionInformationInterface consumerConnectionInformation = consumerConnectionInformationService.getByConsumerNo(consumerNo);
        if(consumerConnectionInformation == null){
            logger.error(methodName + "consumer connection information not found");
            throw new Exception("consumer connection information not found");
        }
        String tariffCategory = consumerConnectionInformation.getTariffCategory();
        if(tariffCategory == null) {
            logger.error(methodName + "tariffCategory not found");
            throw new Exception("tariffCategory not found");
        }
        TariffDescriptionInterface tariffDescription = tariffDescriptionService.getByTariffCategory(tariffCategory);
        if(tariffDescription == null){
            logger.error(methodName + "tariffDescription not found");
            throw new Exception("tariffDescription not found");
        }
        String tariffDescriptionString = tariffDescription.getTariffDescription();
        if(tariffDescriptionString.equals(TariffDescriptionInterface.TARIFF_DESCRIPTION_DOMESTIC)){
            updatedBill = processPDC(consumerNo,consumerNoMaster, finalRead, consumerConnectionInformation, pdcDate, pdcRemark);
        }else{
            String phase = consumerConnectionInformation.getPhase();
            if(phase.equals(ConsumerConnectionInformationInterface.PHASE_SINGLE)){
                updatedBill = processPDC(consumerNo,consumerNoMaster, finalRead,consumerConnectionInformation, pdcDate, pdcRemark);
            }else{
                Date dateOfConnection = consumerConnectionInformation.getConnectionDate();
                long connectionAge = GlobalResources.getDateDiffInDays(dateOfConnection,pdcDate);
                /**
                 * checking if connection is more than 2 years old, i.e.consumer has completed agreement period
                 */
                long agreementPeriodInDays = configuratorService.getValueForCode(ConfiguratorInterface.AGREEMENT_PERIOD);
                if(connectionAge < agreementPeriodInDays){
                    logger.error(methodName + "consumer is under agreement period");
                    throw new Exception("consumer is under agreement period");
                }
                updatedBill = processPDC(consumerNo,consumerNoMaster, finalRead, consumerConnectionInformation, pdcDate, pdcRemark);
            }
        }
        return updatedBill;
    }

    @Transactional(rollbackFor = Exception.class)
    private BillInterface processPDC(String consumerNo, ConsumerNoMasterInterface consumerNoMaster, ReadMasterInterface finalRead, ConsumerConnectionInformationInterface consumerConnectionInformation, Date pdcDate, String pdcRemark)throws Exception {
        String methodName = " processPDC() : ";
        logger.info(methodName+"called");
        logger.info(methodName+"final read "+finalRead);
        ScheduleInterface schedule = null;
        ReadMasterInterface insertedFinalRead = null;

        /**
         * fetching custom read and checking its sub-beans
         */

        Read customRead = readService.getLatestByConsumerNoForPunchingFinalReading(consumerNo);
        if(customRead == null) {
            logger.error(methodName + "some error in fetching CustomRead");
            throw new Exception("some error in fetching CustomRead");
        }

        String meteringStatus = consumerConnectionInformation.getMeteringStatus();
        if(meteringStatus == null){
            logger.error(methodName + "Metering Status is null in method .");
            throw new Exception("Metering Status is null in method");
        }
        if(meteringStatus.equals(ConsumerConnectionInformationInterface.METERING_STATUS_METERED)){
            if (finalRead == null) {
                logger.error(methodName + "final read is null in method .");
                throw new Exception("final read is null in method");
            }
            insertedFinalRead = readMasterService.insertFinalRead(consumerNo,finalRead,customRead);
            AdjustmentInterface rcdcAdjustment = raiseRCDC(consumerNo,insertedFinalRead.getBillMonth(),pdcDate,pdcRemark);

            logger.info(methodName + "data updated successfully for metered consumer: "+rcdcAdjustment+
                    " RCDCAdjustment: "+rcdcAdjustment);
        }else if(meteringStatus.equals(ConsumerConnectionInformationInterface.METERING_STATUS_UNMETERED)) {
            schedule = customRead.getSchedule();
            String billMonth;
            if(schedule != null){
                logger.info(methodName + " Not-Submitted, Pending Schedule present for consumer's group ");
                billMonth = schedule.getBillMonth();
            }else{
                logger.info(methodName + " Not-Submitted, Pending Schedule not present for consumer's group ");
                schedule = scheduleService.getLatestCompletedScheduleByConsumerNo(consumerNo);
                if(schedule == null){
                    logger.error(methodName + "completed schedule not found");
                    throw new Exception("completed schedule not found");
                }
                String lastScheduleBillMonth = schedule.getBillMonth();
                billMonth = GlobalResources.getNextMonth(lastScheduleBillMonth);
            }
            AdjustmentInterface rcdcAdjustment = raiseRCDC(consumerNo,billMonth,pdcDate,pdcRemark);
            logger.info(methodName + "adjustment RCDC raised successfully for unmetered consumer: "+rcdcAdjustment);
        } else{
            logger.error(methodName + "invalid metering status"+meteringStatus);
            throw new Exception("invalid metering status");
        }

        /**
         * Preparing temporary schedule for preparing PDC bill
         */
        ScheduleInterface latestCompletedSchedule = scheduleService.getLatestCompletedScheduleByConsumerNo(consumerNo);
        if(latestCompletedSchedule == null){
            logger.error(methodName + "Last completed Schedule not found");
            throw new Exception("Last completed Schedule not found");
        }

        ScheduleInterface scheduleForPDCOnly = scheduleService.createDummyScheduleForPDC(latestCompletedSchedule,pdcDate);
        if(scheduleForPDCOnly == null){
            logger.error(methodName + "Error in creating schedule for PDC process");
            throw new Exception("Error in creating schedule for PDC process");
        }

        TariffDetailInterface tariffDetail = tariffDetailService.getLatestTariffByConsumerNo(consumerNo);
        if(tariffDetail == null){
            logger.error(methodName+ " tariff detail not found");
            throw new Exception("tariff detail not found");
        }

        HolderInterface holderInterface = holderService.prepareHolderForPDC(consumerNoMaster,tariffDetail,insertedFinalRead,scheduleForPDCOnly);
        if(holderInterface == null){
            logger.error(methodName+ " some error in preparing holder");
            throw new Exception("some error in preparing holder");
        }
        BillInterface finalBill = EngineIgnition.start(holderInterface);
        if(finalBill == null){
            logger.error(methodName+ " some error to preparing bill");
            throw new Exception("some error to prepare bill");
        }

        /**
         * Pending Tasks
         * 1. SD payment against last month bill need to settled
         * 2. need to delete SD installment pending to billing
         *
         * Steps in PDC
         * 1. Settle net-bill with SD amount
         */
        String ngbEngineName = finalBill.getCreatedBy();
        Date ngbEngineRunTime = finalBill.getCreatedOn();
        BigDecimal netBillInBD = finalBill.getNetBill();
        long netBill = netBillInBD.longValue();
        SecurityDepositInterface existingSecurityDeposit = securityDepositService.getTopByConsumerNo(consumerNo);
        if(existingSecurityDeposit != null){
            BigDecimal sdAmountInBD = existingSecurityDeposit.getAmount();
            Date effectiveEndDateInExistingSD = existingSecurityDeposit.getEffectiveEndDate();
            long sdAmount = sdAmountInBD.longValue();
            if (sdAmount > 0){
                if(netBill >= sdAmount){
                    //SD is less than NetBill, make sd amount 0
                    AdjustmentInterface adjustment2732 = raise2732(consumerNo,finalBill.getBillMonth(),pdcDate,sdAmountInBD,pdcRemark);
                    if(adjustment2732 == null){
                        logger.error(methodName+ " some error in preparing 2732 adjustment");
                        throw new Exception("some error in preparing 2732 adjustment");
                    }
                    finalBill.setNetBill(finalBill.getNetBill().subtract(existingSecurityDeposit.getAmount()));
                    finalBill.setOtherAdjustment(existingSecurityDeposit.getAmount());
                    //insert SD-0 row in sd table

                    existingSecurityDeposit.setEffectiveEndDate(GlobalResources.addDaysInDate(-1,pdcDate));
                    existingSecurityDeposit.setUpdatedBy(ngbEngineName);
                    existingSecurityDeposit.setUpdatedOn(ngbEngineRunTime);
                    SecurityDepositInterface updatedExistingSD = securityDepositService.update(existingSecurityDeposit);
                    if(updatedExistingSD == null){
                        logger.error(methodName+ " some error in updating existing SD");
                        throw new Exception("some error in updating existing SD");
                    }
                    SecurityDepositInterface updatedSDHeld = new SecurityDeposit();
                    updatedSDHeld.setConsumerNo(consumerNo);
                    updatedSDHeld.setAmount(BigDecimal.ZERO);
                    updatedSDHeld.setEffectiveStartDate(pdcDate);
                    updatedSDHeld.setEffectiveEndDate(effectiveEndDateInExistingSD);
                    updatedSDHeld.setCreatedBy(ngbEngineName);
                    updatedSDHeld.setCreatedOn(ngbEngineRunTime);
                    updatedSDHeld.setUpdatedOn(ngbEngineRunTime);
                    updatedSDHeld.setUpdatedBy(ngbEngineName);
                    SecurityDepositInterface insertedNewSD = securityDepositService.insert(updatedSDHeld);
                    if(insertedNewSD == null){
                        logger.error(methodName+ " some error in adding SD");
                        throw new Exception("some error in adding SD");
                    }
                }else{
//SD greater than net bill, make new Bill as Zero,
                    AdjustmentInterface adjustment2732 = raise2732(consumerNo,finalBill.getBillMonth(),pdcDate,netBillInBD,pdcRemark);
                    if(adjustment2732 == null){
                        logger.error(methodName+ " some error in preparing 2732 adjustment");
                        throw new Exception("some error in preparing 2732 adjustment");
                    }
                    finalBill.setNetBill(BigDecimal.ZERO);
                    finalBill.setOtherAdjustment(netBillInBD);
                    //insert new SD with updated amount in sd table
                    existingSecurityDeposit.setEffectiveEndDate(GlobalResources.addDaysInDate(-1,pdcDate));
                    existingSecurityDeposit.setUpdatedBy(ngbEngineName);
                    existingSecurityDeposit.setUpdatedOn(ngbEngineRunTime);
                    SecurityDepositInterface updatedExistingSD = securityDepositService.update(existingSecurityDeposit);
                    if(updatedExistingSD == null){
                        logger.error(methodName+ " some error in updating existing SD");
                        throw new Exception("some error in updating existing SD");
                    }
                    SecurityDepositInterface updatedSDHeld = new SecurityDeposit();
                    updatedSDHeld.setConsumerNo(consumerNo);
                    updatedSDHeld.setAmount(sdAmountInBD.subtract(netBillInBD));
                    updatedSDHeld.setEffectiveStartDate(pdcDate);
                    updatedSDHeld.setEffectiveEndDate(effectiveEndDateInExistingSD);
                    updatedSDHeld.setCreatedBy(ngbEngineName);
                    updatedSDHeld.setCreatedOn(ngbEngineRunTime);
                    updatedSDHeld.setUpdatedOn(ngbEngineRunTime);
                    updatedSDHeld.setUpdatedBy(ngbEngineName);
                    SecurityDepositInterface insertedNewSD = securityDepositService.insert(updatedSDHeld);
                    if(insertedNewSD == null){
                        logger.error(methodName+ " some error in adding SD");
                        throw new Exception("some error in adding SD");
                    }

                }
            }
        }
        finalBill.setUpdatedBy(GlobalResources.getLoggedInUser());
        finalBill.setUpdatedOn(GlobalResources.getCurrentDate());
        /**
         * Final Bill has generated, Now saving FinalBill and other components
         */
        BillInterface insertedBill = billService.insert(finalBill);
        if(insertedBill == null){
            logger.error(methodName+ " bill not saved");
            throw new Exception("bill not saved");
        }

        /**
         *Saving updated adjustments
         */
        List<AdjustmentInterface> updatedAdjustments = finalBill.getAdjustmentInterfaces();
        if(updatedAdjustments != null && updatedAdjustments.size() >0){
            List<AdjustmentInterface> insertedAdjustmentInterfaces = new ArrayList<>();
            for(AdjustmentInterface adjustmentInterface : updatedAdjustments){
                if(adjustmentInterface != null) {
                    insertedAdjustmentInterfaces.add(adjustmentService.update(adjustmentInterface));
                }
            }
            if(insertedAdjustmentInterfaces == null || insertedAdjustmentInterfaces.size() != updatedAdjustments.size()){
                logger.error(methodName + "Inserted Adjustments is NULL.Rolling Back Transaction");
                throw new Exception("Inserted Adjustments is NULL.Rolling Back Transaction");
            }
        }
        logger.info(methodName + "Adjustments Posted");
/**
 * saving payments
 */
        List<PaymentInterface> updatedPayments = finalBill.getPaymentInterfaces();
        if(updatedPayments != null && updatedPayments.size() > 0){
            List<PaymentInterface> savedPayments = new ArrayList<>();
            for(PaymentInterface paymentInterface : updatedPayments){
                if(paymentInterface != null) {
                    savedPayments.add(paymentService.update(paymentInterface));
                }
            }
            if(savedPayments == null || savedPayments.size() != updatedPayments.size()){
                logger.error(methodName + "Inserted payments is NULL.Rolling Back Transaction");
                throw new Exception("Inserted payments NULL.Rolling Back Transaction");
            }
        }
        logger.info(methodName + "payments updated");

        /**
         * Breaking ConsumerMeterMapping and saving reading updated by engine
         */
        if(consumerConnectionInformation.getMeteringStatus().equals(ConsumerConnectionInformationInterface.METERING_STATUS_METERED)){
            ConsumerMeterMappingInterface inActiveConsumerMeterMapping = consumerMeterMappingService.InActivateMappingByConsumerNo(consumerNo,finalRead);
            if(inActiveConsumerMeterMapping == null){
                logger.error(methodName + " removing ConsumerMeterMapping failed  "+inActiveConsumerMeterMapping);
                throw new Exception("removing MeterCTRMapping failed");
            }
            /**
             * saving updated Reading
             */
            ReadMasterInterface updatedReading = finalBill.getReadMasterInterface();
            if(updatedReading != null){
                ReadMasterInterface insertedReading = readMasterService.update(updatedReading);
                if(insertedReading == null){
                    logger.error(methodName + "Inserted reading is NULL.Rolling Back Transaction");
                    throw new Exception("Inserted reading is NULL.Rolling Back Transaction");
                }
//            /**
                //not needed
//             * inserting PF and KW if present
//             */
//            ReadMasterKWInterface updatedReadMasterKWInterface = updatedReading.getReadMasterKW();
//            if(updatedReadMasterKWInterface != null){
//                readMasterKWService.update(updatedReadMasterKWInterface);
//            }
//            ReadMasterPFInterface updatedReadMasterPFInterface = updatedReading.getReadMasterPF();
//            if((updatedReadMasterPFInterface != null)){
//                readMasterPFService.update(updatedReadMasterPFInterface);
//            }
            }
        }

        /**
         * Updating Employee Mapping to inactive
         */
        ConsumerInformationInterface consumerInformation = consumerInformationService.getConsumerInformationByConsumerNo(consumerNo);
        if(consumerInformation == null){
            logger.error(methodName + "failed in retrieving Consumer Information ");
            throw new Exception("failed in retrieving Consumer Information");
        }
        if(consumerInformation.getIsEmployee()){
            List<EmployeeConnectionMappingInterface> employeeConnectionMappings = employeeConnectionMappingService.getByConsumerNoAndStatus(consumerNo,EmployeeConnectionMappingInterface.STATUS_ACTIVE);

            if(employeeConnectionMappings == null || employeeConnectionMappings.size() != 1){
                logger.error(methodName + "failed in retrieving Employee Mapping list ");
                throw new Exception("failed in retrieving Employee Mapping list");
            }

            EmployeeConnectionMappingInterface employeeConnectionMappingInterface = employeeConnectionMappings.get(0);
            if(employeeConnectionMappingInterface == null){
                logger.error(methodName + "failed in retrieving Employee Mapping ");
                throw new Exception("failed in retrieving Employee Mapping ");
            }

            employeeConnectionMappingInterface.setStatus(EmployeeConnectionMappingInterface.STATUS_INACTIVE);
            employeeConnectionMappingInterface.setUpdatedBy(GlobalResources.getLoggedInUser());
            employeeConnectionMappingInterface.setUpdatedOn(GlobalResources.getCurrentDate());
            EmployeeConnectionMappingInterface updatedEmployeeConnectionMappingInterface = employeeConnectionMappingService.update(employeeConnectionMappingInterface);
            if(updatedEmployeeConnectionMappingInterface == null){
                logger.error(methodName + "failed in updating Employee Mapping ");
                throw new Exception("failed in updating Employee Mapping ");
            }
        }

        /**
         * Remove Consumer-BPL mapping
         */
        if(consumerInformation.getIsBPL()){
            List<BPLConnectionMappingInterface> bplConnectionMappings = bplConnectionMappingService.getByConsumerNoAndStatus(consumerNo,BPLConnectionMappingInterface.STATUS_ACTIVE);

            if(bplConnectionMappings == null || bplConnectionMappings.size() != 1){
                logger.error(methodName + "failed in retrieving BPL Mapping list ");
                throw new Exception("failed in retrieving BPL Mapping list");
            }

            BPLConnectionMappingInterface bplConnectionMappingInterface = bplConnectionMappings.get(0);
            if(bplConnectionMappingInterface == null){
                logger.error(methodName + "failed in retrieving BPL Mapping ");
                throw new Exception("failed in retrieving BPL Mapping ");
            }

            bplConnectionMappingInterface.setStatus(BPLConnectionMappingInterface.STATUS_INACTIVE);
            bplConnectionMappingInterface.setUpdatedBy(GlobalResources.getLoggedInUser());
            bplConnectionMappingInterface.setUpdatedOn(GlobalResources.getCurrentDate());
            BPLConnectionMappingInterface updatedBplConnectionMappingInterface = bplConnectionMappingService.update(bplConnectionMappingInterface);
            if(updatedBplConnectionMappingInterface == null){
                logger.error(methodName + "failed in updating bpl Mapping ");
                throw new Exception("failed in updating bpl Mapping ");
            }
        }

        /**
         * remove Xray mapping
         */
        if(consumerConnectionInformation.getIsXray()){
            List<XrayConnectionInformationInterface> xrayConnectionInformationInterfaces = xrayConnectionInformationService.getByConsumerNoAndStatus(consumerNo,XrayConnectionInformationInterface.STATUS_ACTIVE);

            if(xrayConnectionInformationInterfaces == null || xrayConnectionInformationInterfaces.size() != 1){
                logger.error(methodName + "failed in retrieving xray Mapping list ");
                throw new Exception("failed in retrieving xray Mapping list");
            }

            XrayConnectionInformationInterface xrayConnectionInformationInterface = xrayConnectionInformationInterfaces.get(0);
            if(xrayConnectionInformationInterface == null){
                logger.error(methodName + "failed in retrieving xray Mapping ");
                throw new Exception("failed in retrieving xray Mapping ");
            }

            xrayConnectionInformationInterface.setStatus(ConsumerNoMasterInterface.STATUS_INACTIVE);
            xrayConnectionInformationInterface.setUpdatedBy(GlobalResources.getLoggedInUser());
            xrayConnectionInformationInterface.setUpdatedOn(GlobalResources.getCurrentDate());
            XrayConnectionInformationInterface updatedXrayConnectionInformationInterface = xrayConnectionInformationService.update(xrayConnectionInformationInterface);
            if(updatedXrayConnectionInformationInterface == null){
                logger.error(methodName + "failed in updating xray Mapping ");
                throw new Exception("failed in updating xray Mapping ");
            }
        }

        consumerNoMaster.setStatus(ConsumerNoMasterInterface.STATUS_INACTIVE);
        updateAuditDetails(consumerNoMaster);
        ConsumerNoMasterInterface updatedConsumerNoMaster = consumerNoMasterDAO.update(consumerNoMaster);
        if(updatedConsumerNoMaster == null){
            logger.error(methodName + "failed in saving consumerNoMaster ");
            throw new Exception("failed in saving consumerNoMaster");
        }
        logger.info(methodName + "PDC process completed successfully, ConsumerNo Master: "+consumerNoMaster);
        return finalBill;
    }


    private AdjustmentInterface raiseRCDC(String consumerNo, String billMonth, Date date, String remark)throws Exception {
        AdjustmentInterface rcdcAdjustment = new Adjustment();
        //hard coded code
        rcdcAdjustment.setCode(3);
        long amount = configuratorService.getValueForCode(AdjustmentInterface.CODE_RC_DC);
        rcdcAdjustment.setAmount(new BigDecimal(amount));
        rcdcAdjustment.setConsumerNo(consumerNo);
        rcdcAdjustment.setPostingBillMonth(billMonth);
        rcdcAdjustment.setPostingDate(date);
        rcdcAdjustment.setDeleted(AdjustmentInterface.DELETED_FALSE);
        rcdcAdjustment.setPosted(AdjustmentInterface.POSTED_FALSE);
        rcdcAdjustment.setRemark(remark);
        AdjustmentInterface insertedAdjustment = adjustmentService.insert(rcdcAdjustment);
        return insertedAdjustment;
    }

    private AdjustmentInterface raise2732(String consumerNo, String billMonth, Date date, BigDecimal amount, String remark)throws Exception {
        AdjustmentInterface adjustment2732 = new Adjustment();
        //hard coded code
        //// TODO: 12/18/2017  inset 2 adjustment for 27-32.
        adjustment2732.setCode(27);
        adjustment2732.setAmount(amount);
        adjustment2732.setConsumerNo(consumerNo);
        adjustment2732.setPostingBillMonth(billMonth);
        adjustment2732.setPostingDate(date);
        adjustment2732.setDeleted(AdjustmentInterface.DELETED_FALSE);
        adjustment2732.setPosted(AdjustmentInterface.POSTED_TRUE);
        adjustment2732.setRemark(remark);
        AdjustmentInterface insertedAdjustment = adjustmentService.insert(adjustment2732);
        return insertedAdjustment;
    }


    public void updateAuditDetails(ConsumerNoMasterInterface consumerNoMaster) {
        consumerNoMaster.setUpdatedBy(GlobalResources.getLoggedInUser());
        consumerNoMaster.setUpdatedOn(GlobalResources.getCurrentDate());
    }

    public ConsumerNoMasterInterface getByConsumerNo(String consumerNo)  {
        String methodName = "getByConsumerNo() :";
        ConsumerNoMasterInterface consumerNoMaster = null;
        logger.info( methodName + "called");
        if(consumerNo != null) {
            consumerNoMaster = consumerNoMasterDAO.getByConsumerNo(consumerNo);
        }
        return consumerNoMaster;
    }

    public long countByLocationCodeAndStatus(String locationCode, String status){
        final String methodName = "countByLocationCodeAndStatus() : ";
        logger.info(methodName + "called");
        long count = 0;
        if(locationCode != null && status != null){
            count = consumerNoMasterDAO.countByLocationCodeAndStatus(locationCode,status);
        }
        return count;
    }

    public long countByLocationCode(String locationCode) {
        final String methodName = "countByLocationCode() : ";
        logger.info(methodName + "called");
        long count = 0;
        if(locationCode != null ){
            count = consumerNoMasterDAO.countByLocationCode(locationCode);
        }
        return count;
    }

    public long countByGroupNoAndStatus(String groupNo, String status){
        final String methodName = "countByGroupNoAndStatus() : ";
        logger.info(methodName + "called");
        long count = 0;
        if(groupNo != null && status != null){
            count = consumerNoMasterDAO.countByGroupNoAndStatus(groupNo,status);
        }
        return count;
    }

    public long countByGroupNo(String groupNo) {
        final String methodName = "countByGroupNo() : ";
        logger.info(methodName + "called");
        long count = 0;
        if(groupNo != null ){
            count = consumerNoMasterDAO.countByGroupNo(groupNo);
        }
        return count;
    }

    public long countByGroupNoAndReadingDiaryNoAndStatus(String groupNo,String readingDiaryNo, String status){
        final String methodName = "countByGroupNoAndReadingDiaryNoAndStatus() : ";
        logger.info(methodName + "called");
        long count = 0;
        if(groupNo != null && status != null){
            count = consumerNoMasterDAO.countByGroupNoAndReadingDiaryNoAndStatus(groupNo,readingDiaryNo,status);
        }
        return count;
    }

    public long countByGroupNoAndReadingDiaryNo(String groupNo, String readingDiaryNo) {
        final String methodName = "countByGroupNoAndReadingDiaryNo() : ";
        logger.info(methodName + "called");
        long count = 0;
        if(groupNo != null && readingDiaryNo != null ){
            count = consumerNoMasterDAO.countByGroupNoAndReadingDiaryNo(groupNo,readingDiaryNo);
        }
        return count;
    }

    public ZoneConsumerData getZoneConsumerDataByLocationCode(String locationCode, ErrorMessage errorMessage) {
        final String methodName = "getZoneConsumerDataByLocationCode() : ";
        logger.info(methodName + "called");

        if(locationCode == null){
            errorMessage.setErrorMessage("location code is null");
            return null;
        }
        ZoneInterface zone = zoneService.getByLocationCode(locationCode);

        if(zone == null) {
            errorMessage.setErrorMessage("zone does not exist with given location code");
            return null;
        }

        ZoneConsumerData zoneConsumerData = new ZoneConsumerData();
        zoneConsumerData.setZone(zone);
        zoneConsumerData.setActiveConsumers(countByLocationCodeAndStatus(locationCode, ConsumerNoMasterInterface.STATUS_ACTIVE));
        zoneConsumerData.setInactiveConsumers(countByLocationCodeAndStatus(locationCode, ConsumerNoMasterInterface.STATUS_INACTIVE));
        zoneConsumerData.setConsumers(countByLocationCode(locationCode));
        List<GroupInterface> groups = groupService.getByLocationCode(locationCode);
        List<GroupConsumerData> groupConsumerDataList = new ArrayList<>() ;
        for (GroupInterface group : groups ) {
            GroupConsumerData groupData = getGroupConsumerDataByGroup(group,errorMessage);
            if(groupData == null){
                return null;
            }
            groupConsumerDataList.add(groupData);
        }
        zoneConsumerData.setGroups(groupConsumerDataList);
        return zoneConsumerData;
    }


    public GroupConsumerData getGroupConsumerDataByGroupNo(String groupNo, ErrorMessage errorMessage) {
        final String methodName = "getGroupConsumerDataByGroupNo() : ";
        logger.info(methodName + "called");
        GroupConsumerData groupConsumerData = null;

        if(groupNo == null){
            errorMessage.setErrorMessage("group no is null");
            return null;
        }
        GroupInterface group = groupService.getByGroupNo(groupNo);

        if(group == null) {
            errorMessage.setErrorMessage("group does not exist with given group no");
            return null;
        }

        groupConsumerData = getGroupConsumerDataByGroup(group, errorMessage);
        return groupConsumerData;
    }

    public GroupConsumerData getGroupConsumerDataByGroup(GroupInterface group, ErrorMessage errorMessage) {
        final String methodName = "getGroupConsumerDataByGroup() : ";
        logger.info(methodName + "called");
        if(group == null){
            errorMessage.setErrorMessage("group object is null");
            return null;
        }
        String groupNo = group.getGroupNo();
        GroupConsumerData groupConsumerData = new GroupConsumerData();
        groupConsumerData.setGroup(group);
        groupConsumerData.setActiveConsumers(countByGroupNoAndStatus(groupNo, ConsumerNoMasterInterface.STATUS_ACTIVE));
        groupConsumerData.setInactiveConsumers(countByGroupNoAndStatus(groupNo, ConsumerNoMasterInterface.STATUS_INACTIVE));
        groupConsumerData.setConsumers(countByGroupNo(groupNo));

        List<ReadingDiaryConsumerData> readingDiaryConsumerDataList = new ArrayList<>();
        List<ReadingDiaryNoInterface> readingDiaries = readingDiaryNoService.getByGroupNo(groupNo);

        for (ReadingDiaryNoInterface readingDiary:readingDiaries) {
            ReadingDiaryConsumerData readingDiaryConsumerData = getReadingDiaryConsumerDataByReadingDiary(readingDiary,errorMessage);
            if(readingDiaryConsumerData == null){
                return null;
            }
            readingDiaryConsumerDataList.add(readingDiaryConsumerData);
        }
        groupConsumerData.setReadingDiaries(readingDiaryConsumerDataList);
        return groupConsumerData;
    }


    public ReadingDiaryConsumerData getReadingDiaryConsumerDataByGroupNoAndReadingDiaryNo(String groupNo, String readingDiaryNo, ErrorMessage errorMessage) {
        final String methodName = "getReadingDiaryConsumerDataByReadingDiaryNo() : ";
        logger.info(methodName + "called");

        if(readingDiaryNo == null){
            errorMessage.setErrorMessage("readingDiaryNo is null");
            return null;
        }

        if(groupNo == null){
            errorMessage.setErrorMessage("groupNo is null");
            return null;
        }

        ReadingDiaryNoInterface readingDiary = readingDiaryNoService.getByGroupNoAndReadingDiaryNo(groupNo,readingDiaryNo);

        if(readingDiary == null) {
            errorMessage.setErrorMessage("reading diary does not exist with given group no and diary no");
            return null;
        }

        ReadingDiaryConsumerData readingDiaryConsumerData = getReadingDiaryConsumerDataByReadingDiary(readingDiary,errorMessage);
        return readingDiaryConsumerData;
    }

    public ReadingDiaryConsumerData getReadingDiaryConsumerDataByReadingDiary(ReadingDiaryNoInterface readingDiary, ErrorMessage errorMessage) {
        final String methodName = "getReadingDiaryConsumerDataByReadingDiary() : ";
        logger.info(methodName + "called");

        if(readingDiary == null){
            errorMessage.setErrorMessage("group object is null");
            return null;
        }
        String readingDiaryNo = readingDiary.getReadingDiaryNo();
        String groupNo = readingDiary.getGroupNo();
        ReadingDiaryConsumerData readingDiaryConsumerData = new ReadingDiaryConsumerData();
        readingDiaryConsumerData.setReadingDiary(readingDiary);
        readingDiaryConsumerData.setActiveConsumers(countByGroupNoAndReadingDiaryNoAndStatus(groupNo, readingDiaryNo, ConsumerNoMasterInterface.STATUS_ACTIVE));
        readingDiaryConsumerData.setInactiveConsumers(countByGroupNoAndReadingDiaryNoAndStatus(groupNo, readingDiaryNo, ConsumerNoMasterInterface.STATUS_INACTIVE));
        readingDiaryConsumerData.setConsumers(countByGroupNoAndReadingDiaryNo(groupNo,readingDiaryNo));
        return readingDiaryConsumerData;
    }

    public ConsumerNoMasterInterface getByOldConsumerNoOneAndLocationCode(String oldServiceNoOne , String locationCode) {
        final String methodName = "getByOldConsumerNoOneAndLocationCode() : ";
        logger.info(methodName + "called");
        ConsumerNoMasterInterface consumerNoMasterInterface = null;
        if (oldServiceNoOne != null && locationCode != null) {
            consumerNoMasterInterface = consumerNoMasterDAO.getByOldServiceNoOneAndLocationCode(oldServiceNoOne, locationCode);
        }
        return consumerNoMasterInterface;
    }
}
