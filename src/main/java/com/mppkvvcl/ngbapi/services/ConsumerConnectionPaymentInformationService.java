package com.mppkvvcl.ngbapi.services;

import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.ConsumerConnectionPaymentInformationInterface;
import com.mppkvvcl.ngbdao.daos.ConsumerConnectionPaymentInformationDAO;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by SUMIT on 14-06-2017.
 */
@Service
public class ConsumerConnectionPaymentInformationService {

    /**
     * Asking Spring to inject consumerConnectionMeterInformationRepository dependency in the variable
     * so that we get various handles for performing CRUD operation on consumer connection meter information table
     * at the backend Database
     */
    @Autowired
    private ConsumerConnectionPaymentInformationDAO consumerConnectionPaymentInformationDAO;

    /**
     * getting whole application logger object for performing logs. This logger will only be
     * used in this class methods only.
     */
    private static final Logger logger = GlobalResources.getLogger(ConsumerConnectionPaymentInformationService.class);

    /**
     * This metod takes consumerConnectionPaymentInformation object and insert it into the backend database.
     * Return object if insertion successful else return null.<br><br>
     * param consumerConnectionPaymentInformation<br>
     * return insertedConsumerConnectionPaymentInformation
     */
    public ConsumerConnectionPaymentInformationInterface insert(ConsumerConnectionPaymentInformationInterface consumerConnectionPaymentInformation){
        String methodName = "insert : " ;
        ConsumerConnectionPaymentInformationInterface insertedConsumerConnectionPaymentInformation = null;
        logger.info(methodName + "Started insertion for ConsumerConnectionPaymentInformation ");
        if(consumerConnectionPaymentInformation != null){
            logger.info(methodName + "Calling ConsumerConnectionPaymentInformationRepository for insertingConsumerConnectionPaymentInformation");
            insertedConsumerConnectionPaymentInformation = consumerConnectionPaymentInformationDAO.add(consumerConnectionPaymentInformation);
            if (insertedConsumerConnectionPaymentInformation != null) {
                logger.info(methodName + "Successfully inserted consumerConnectionPaymentInformation");
            }else{
                logger.error(methodName + "unable to insert consumer connection payment information row. Repository sending null");
            }
        }else{
            logger.error(methodName + "consumerConnectionPaymentInformation received is null .");
        }
        return insertedConsumerConnectionPaymentInformation ;
    }

}
