package com.mppkvvcl.ngbapi.services;

import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbdao.daos.EmployeeConnectionMappingDAO;
import com.mppkvvcl.ngbinterface.interfaces.EmployeeConnectionMappingInterface;
import com.mppkvvcl.ngbinterface.interfaces.NSCStagingStatusInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by SUMIT on 15-06-2017.
 */
@Service
public class EmployeeConnectionMappingService {

    /**
     * getting whole application logger object for performing logs. This logger will only be
     * used in this class methods only.
     */
    private static final Logger logger = GlobalResources.getLogger(EmployeeConnectionMappingService.class);


    @Autowired
    private EmployeeConnectionMappingDAO employeeConnectionMappingDAO;

    @Autowired
    private NSCStagingStatusService nscStagingStatusService;

    @Autowired
    private GlobalResources globalResources;

    /**
     * This method takes employeeNo and status and fetches list of employeeConnectionMappings.
     * Return employeeConnectionMappings list if successful else return null.<br><br>
     * param employeeNo<br>
     * param status<br>
     * return employeeConnectionMappings
     */
    public List<EmployeeConnectionMappingInterface> getByEmployeeNoAndStatus(String employeeNo, String status){
        String methodName = "getByEmployeeNoAndStatus() :";
        List<EmployeeConnectionMappingInterface> employeeConnectionMappings = null;
        logger.info(methodName+"got request to find Employee connection mapping by Employee no :"+employeeNo+"status:"+status);
        if(employeeNo != null && status != null){
            employeeConnectionMappings = employeeConnectionMappingDAO.getByEmployeeNoAndStatus(employeeNo,status);
            if(employeeConnectionMappings != null && employeeConnectionMappings.size() > 0){
                logger.info(methodName+"Successfully Fetched record for Employee no:"+employeeNo+"status"+status);
            }else{
                logger.error(methodName+"Unable to find Employee connection mapping for Employee"+employeeNo+"status"+status);
            }
        }else {
            logger.error(methodName+"got request EmployeeNo:"+employeeNo+"Or status:"+status+" as null ");
        }

        return employeeConnectionMappings;
    }

    /**
     * This method takes consumerNo and status and fetches list of employeeConnectionMappings.
     * Return employeeConnectionMappings list if successful else return null.<br><br>
     * param consumerNo<br>
     * param status<br>
     * return employeeConnectionMappings
     */
    public List<EmployeeConnectionMappingInterface> getByConsumerNoAndStatus(String consumerNo, String status){
        String methodName = "getByConsumerNoAndStatus() :";
        List<EmployeeConnectionMappingInterface> employeeConnectionMappings = null;
        logger.info(methodName+"got request to find Employee connection mapping by Consumer no :"+consumerNo+"status"+status);
        if(consumerNo != null && status != null ){
            employeeConnectionMappings = employeeConnectionMappingDAO.getByConsumerNoAndStatus(consumerNo,status);
            if(employeeConnectionMappings != null && employeeConnectionMappings.size() > 0){
                logger.info(methodName+"Successfully Fetched Employee connection mapping"+employeeConnectionMappings);
            }else{
                logger.error(methodName+"Unable to find Employee connection mapping for ConsumerNo:"+consumerNo+"status :"+status);
            }
        }else {
            logger.error(methodName+"got request ConsumerNo:"+consumerNo+"Or status:"+status+" as null ");

        }
        return  employeeConnectionMappings;
    }

    /**
     * This method takes consumerNo, employeeNo and status and fetches list of employeeConnectionMappings.
     * Return employeeConnectionMappings list if successful else return null.<br><br>
     * param consumerNo<br>
     * param employeeNo<br>
     * param status<br>
     * return employeeConnectionMappings
     */
    public List<EmployeeConnectionMappingInterface> getByConsumerNoAndEmployeeNoAndStatus(String consumerNo,String employeeNo, String status){
        String methodName = "getByConsumerNoAndEmployeeNoAndStatus() :";
        List<EmployeeConnectionMappingInterface> employeeConnectionMappings = null;
        logger.info(methodName+"got request to find Employee Connection Mapping by Consumer no:"+consumerNo+"Employee no:"+employeeNo+"status :"+status);
        if(consumerNo != null && employeeNo != null && status !=null){
            employeeConnectionMappings = employeeConnectionMappingDAO.getByEmployeeNoAndConsumerNoAndStatus(employeeNo,consumerNo,status);
            if(employeeConnectionMappings != null && employeeConnectionMappings.size() >0){
                logger.info(methodName+"Successfully Fetched Employee connection mapping"+employeeConnectionMappings);
            }else {
                logger.error(methodName+"Unable to find Employee connection mapping for Employee no :"+employeeNo+"ConsumerNo:"+consumerNo+"status :"+status);
            }
        }else {
            logger.error(methodName+"got request ConsumerNo:"+consumerNo+"Or Employee no "+employeeNo+"status:"+status+" as null");
        }
        return employeeConnectionMappings;
    }

    public boolean checkEmployeeNoUsed(String employeeNo, String status) {
        String methodName = "checkEmployeeNoUsed() : ";
        logger.info(methodName + "called " + employeeNo + " status " + status);
        boolean isUsed = false;
        if (employeeNo != null && status != null) {
            List<EmployeeConnectionMappingInterface> employeeConnectionMappingInterfaces = getByEmployeeNoAndStatus(employeeNo, status);
            if (employeeConnectionMappingInterfaces != null && employeeConnectionMappingInterfaces.size() > 0) {
                isUsed = true;
            }
            NSCStagingStatusInterface nscStagingStatusInterface = nscStagingStatusService.getByPendingStatusAndEmployeeNo("PENDING", employeeNo);
            if (nscStagingStatusInterface != null) {
                isUsed = true;
            }
        }
        return isUsed;
    }

    public EmployeeConnectionMappingInterface insert(EmployeeConnectionMappingInterface employeeConnectionMapping)  {
        String methodName = " insert() :";
        EmployeeConnectionMappingInterface insertedEmployeeConnectionMapping = null;
        logger.info(methodName + "called");
        if(employeeConnectionMapping != null) {
            setAuditDetails(employeeConnectionMapping);
            insertedEmployeeConnectionMapping = employeeConnectionMappingDAO.add(employeeConnectionMapping);
        }
        return insertedEmployeeConnectionMapping;
    }

    public EmployeeConnectionMappingInterface update(EmployeeConnectionMappingInterface employeeConnectionMapping)  {
        String methodName = " update() :";
        EmployeeConnectionMappingInterface updatedEmployeeConnectionMapping = null;
        logger.info(methodName + "called");
        if(employeeConnectionMapping != null) {
            setUpdateAuditDetails(employeeConnectionMapping);
            updatedEmployeeConnectionMapping = employeeConnectionMappingDAO.update(employeeConnectionMapping);
        }
        return updatedEmployeeConnectionMapping;
    }

    private void setAuditDetails(EmployeeConnectionMappingInterface employeeConnectionMappingInterface) {
        final String methodName = "setAuditDetails() : ";
        logger.info(methodName + "called");
        if (employeeConnectionMappingInterface != null) {
            String user = GlobalResources.getLoggedInUser();
            employeeConnectionMappingInterface.setCreatedBy(user + " " + globalResources.getProjectDetail());
            employeeConnectionMappingInterface.setCreatedOn(GlobalResources.getCurrentDate());
            employeeConnectionMappingInterface.setUpdatedBy(user + " " + globalResources.getProjectDetail());
            employeeConnectionMappingInterface.setUpdatedOn(GlobalResources.getCurrentDate());
        }
    }

    private void setUpdateAuditDetails(EmployeeConnectionMappingInterface employeeConnectionMappingInterface) {
        final String methodName = "setUpdateAuditDetails() : ";
        logger.info(methodName + "called");
        if (employeeConnectionMappingInterface != null) {
            String user = GlobalResources.getLoggedInUser();
            employeeConnectionMappingInterface.setUpdatedBy(user + " " + globalResources.getProjectDetail());
            employeeConnectionMappingInterface.setUpdatedOn(GlobalResources.getCurrentDate());
        }
    }
}
