package com.mppkvvcl.ngbapi.services;

import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.PremiseTypeInterface;
import com.mppkvvcl.ngbdao.daos.PremiseTypeDAO;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by SHIVANSHU on 18-07-2017.
 */

@Service
public class PremiseTypeService {
    /**
     * Asking spring to inject PremiseTypeRepository to that we can use it.
     */
    @Autowired
    PremiseTypeDAO premiseTypeDAO;

    /**
     * Getting logger for logging in current class.
     */
    Logger logger = GlobalResources.getLogger(PremiseTypeService.class);

    /**
     * This getAll() method is used for getting All PremiseType objects.<br><br>
     * return premiseTypes<br>
     */
    public List<? extends PremiseTypeInterface> getAll(){
        String methodName = "getAll() :";
        logger.info(methodName+"Starting to get all PremiseType objects");
        List<? extends PremiseTypeInterface> premiseTypes = premiseTypeDAO.getAll();
        if (premiseTypes != null){
            if (premiseTypes.size() > 0){
                logger.info(methodName+"Successfully found PremiseType list with size"+premiseTypes.size());
            }else{
                logger.error(methodName+"PremiseType List found with size"+premiseTypes.size());
            }
        }else{
            logger.error(methodName+"PremiseType list return null");
        }
        return premiseTypes;
    }
}
