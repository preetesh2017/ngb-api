package com.mppkvvcl.ngbapi.factories;

import com.mppkvvcl.ngbentity.beans.BillCorrection;
import com.mppkvvcl.ngbinterface.interfaces.BillCorrectionInterface;

import java.math.BigDecimal;
import java.util.Date;

public class BillCorrectionFactory {
    public static BillCorrectionInterface build( long billId, long referenceBillId, String locationCode, String groupNo, String readingDiaryNo, String consumerNo, String billMonth, String billTypeCode,Date billDate, Date dueDate, Date chequeDueDate, Date currentReadDate, BigDecimal currentRead, BigDecimal previousRead, BigDecimal difference, BigDecimal mf, BigDecimal meteredUnit, BigDecimal assessment, BigDecimal totalUnit, BigDecimal gmcUnit, BigDecimal billedUnit, BigDecimal billedMD, BigDecimal billedPF, BigDecimal loadFactor, BigDecimal fixedCharge, BigDecimal additionalFixedCharges1, BigDecimal additionalFixedCharges2, BigDecimal energyCharge, BigDecimal fcaCharge, BigDecimal electricityDuty, BigDecimal meterRent, BigDecimal pfCharge, BigDecimal weldingTransformerSurcharge, BigDecimal loadFactorIncentive, BigDecimal sdInterest, BigDecimal ccbAdjustment, BigDecimal lockCredit, BigDecimal otherAdjustment, BigDecimal employeeRebate, BigDecimal onlinePaymentRebate, BigDecimal prepaidMeterRebate, BigDecimal promptPaymentIncentive, BigDecimal advancePaymentIncentive, BigDecimal demandSideIncentive, BigDecimal subsidy, BigDecimal currentBill, BigDecimal arrear, BigDecimal cumulativeSurcharge, BigDecimal surchargeDemanded, BigDecimal netBill, BigDecimal asdBilled, BigDecimal asdArrear, BigDecimal asdInstallment, boolean deleted, String createdBy, Date createdOn, String updatedBy, Date updatedOn, BigDecimal pristineElectricityDuty, BigDecimal pristineNetBill, BigDecimal currentBillSurcharge, BigDecimal xrayFixedCharge, boolean usedOnMis) {
        BillCorrectionInterface billCorrectionInterface = new BillCorrection();
        billCorrectionInterface.setBillId(billId);
        billCorrectionInterface.setReferenceBillId(referenceBillId);
        billCorrectionInterface.setLocationCode(locationCode);
        billCorrectionInterface.setGroupNo(groupNo);
        billCorrectionInterface.setReadingDiaryNo(readingDiaryNo);
        billCorrectionInterface.setConsumerNo(consumerNo);
        billCorrectionInterface.setBillMonth(billMonth);
        billCorrectionInterface.setBillTypeCode(billTypeCode);
        billCorrectionInterface.setBillDate(billDate);
        billCorrectionInterface.setDueDate(dueDate);
        billCorrectionInterface.setChequeDueDate(chequeDueDate);
        billCorrectionInterface.setCurrentReadDate(currentReadDate);
        billCorrectionInterface.setCurrentRead(currentRead);
        billCorrectionInterface.setPreviousRead(previousRead);
        billCorrectionInterface.setDifference(difference);
        billCorrectionInterface.setMf(mf);
        billCorrectionInterface.setMeteredUnit(meteredUnit);
        billCorrectionInterface.setAssessment(assessment);
        billCorrectionInterface.setTotalUnit(totalUnit);
        billCorrectionInterface.setGmcUnit(gmcUnit);
        billCorrectionInterface.setBilledUnit(billedUnit);
        billCorrectionInterface.setBilledMD(billedMD);
        billCorrectionInterface.setBilledPF(billedPF);
        billCorrectionInterface.setLoadFactor(loadFactor);
        billCorrectionInterface.setFixedCharge(fixedCharge);
        billCorrectionInterface.setAdditionalFixedCharges1(additionalFixedCharges1);
        billCorrectionInterface.setAdditionalFixedCharges2(additionalFixedCharges2);
        billCorrectionInterface.setEnergyCharge(energyCharge);
        billCorrectionInterface.setFcaCharge(fcaCharge);
        billCorrectionInterface.setElectricityDuty(electricityDuty);
        billCorrectionInterface.setMeterRent(meterRent);
        billCorrectionInterface.setPfCharge(pfCharge);
        billCorrectionInterface.setWeldingTransformerSurcharge(weldingTransformerSurcharge);
        billCorrectionInterface.setLoadFactorIncentive(loadFactorIncentive);
        billCorrectionInterface.setSdInterest(sdInterest);
        billCorrectionInterface.setCcbAdjustment(ccbAdjustment);
        billCorrectionInterface.setLockCredit(lockCredit);
        billCorrectionInterface.setOtherAdjustment(otherAdjustment);
        billCorrectionInterface.setEmployeeRebate(employeeRebate);
        billCorrectionInterface.setOnlinePaymentRebate(onlinePaymentRebate);
        billCorrectionInterface.setPrepaidMeterRebate(prepaidMeterRebate);
        billCorrectionInterface.setPromptPaymentIncentive(promptPaymentIncentive);
        billCorrectionInterface.setAdvancePaymentIncentive(advancePaymentIncentive);
        billCorrectionInterface.setDemandSideIncentive(demandSideIncentive);
        billCorrectionInterface.setSubsidy(subsidy);
        billCorrectionInterface.setCurrentBill(currentBill);
        billCorrectionInterface.setArrear(arrear);
        billCorrectionInterface.setCumulativeSurcharge(cumulativeSurcharge);
        billCorrectionInterface.setSurchargeDemanded(surchargeDemanded);
        billCorrectionInterface.setNetBill(netBill);
        billCorrectionInterface.setAsdBilled(asdBilled);
        billCorrectionInterface.setAsdArrear(asdArrear);
        billCorrectionInterface.setAsdInstallment(asdInstallment);
        billCorrectionInterface.setDeleted(deleted);
        billCorrectionInterface.setCreatedBy(createdBy);
        billCorrectionInterface.setCreatedOn(createdOn);
        billCorrectionInterface.setUpdatedBy(updatedBy);
        billCorrectionInterface.setUpdatedOn(updatedOn);
        billCorrectionInterface.setPristineElectricityDuty(pristineElectricityDuty);
        billCorrectionInterface.setPristineNetBill(pristineNetBill);
        billCorrectionInterface.setCurrentBillSurcharge(currentBillSurcharge);
        billCorrectionInterface.setXrayFixedCharge(xrayFixedCharge);
        billCorrectionInterface.setUsedOnMis(usedOnMis);
        return billCorrectionInterface;
    }
}
