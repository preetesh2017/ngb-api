package com.mppkvvcl.ngbapi.factories;

import com.mppkvvcl.ngbentity.beans.ReadMasterPF;
import com.mppkvvcl.ngbinterface.interfaces.ReadMasterPFInterface;

public class ReadMasterPFFactory {
    public static ReadMasterPFInterface build(){
        ReadMasterPFInterface readMasterPFInterface = new ReadMasterPF();
        return readMasterPFInterface;
    }
}
