package com.mppkvvcl.ngbapi.utility;

import com.mppkvvcl.ngbapi.custombeans.CustomReadMaster;
import com.mppkvvcl.ngbentity.beans.Bill;
import com.mppkvvcl.ngbentity.beans.ReadMaster;
import com.mppkvvcl.ngbinterface.interfaces.BillInterface;
import com.mppkvvcl.ngbinterface.interfaces.ReadMasterInterface;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by NITISH on 25-05-2017.
 */
@Component
public class GlobalResources {

    private Logger logger = getLogger(GlobalResources.class);

    public static Logger getLogger(Class className) {
        return LoggerFactory.getLogger(className);
    }

    @Value("${version}")
    private String version;

    @Value("${artifactId}")
    private String artifactId;

    public String getProjectDetail(){
        final String methodName = "getProjectDetail() : ";
        logger.info(methodName + "called");
        return artifactId + "-" + version;
    }

    public static String getNextMonth(String date)  {

        String nextMonth = null;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("MMM-yyyy");
            Calendar cal = Calendar.getInstance();
            cal.setTime(sdf.parse(date));
            cal.add(Calendar.MONTH, 1);
            nextMonth = sdf.format(cal.getTime());

        } catch (Exception e) {
            e.getStackTrace();
        }
        return nextMonth.toString().toUpperCase();
    }

    public static String getPreviousMonth(String date) {
        String previousMonth = null;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("MMM-yyyy");
            Calendar cal = Calendar.getInstance();
            cal.setTime(sdf.parse(date));
            cal.add(Calendar.MONTH, -1);
            previousMonth = sdf.format(cal.getTime());

        } catch (Exception e) {
            e.getStackTrace();
        }
        return previousMonth.toString().toUpperCase();
    }


    public static String addMonth(String date, int month) throws ParseException {
        String newMonth = null;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("MMM-yyyy");
            Calendar cal = Calendar.getInstance();
            cal.setTime(sdf.parse(date));
            cal.add(Calendar.MONTH, month); //minus number would decrement the month
            newMonth = sdf.format(cal.getTime());

        } catch (Exception e) {
            e.getStackTrace();
        }
        return newMonth;
    }

    public static Date getCurrentDate() {
        return Calendar.getInstance().getTime();
    }

    /**
     * Get a diff between two dates
     *
     * @param date1 the oldest date
     * @param date2 the newest date
     * @return the diff value, in the days
     */
    public static long getDateDiffInDays(Date date1, Date date2) {
        long diffInDays = ((date2.getTime() - date1.getTime())
                / (1000 * 60 * 60 * 24));
        return diffInDays;
    }

    public static String getLoggedInUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null) {
            return authentication.getName();
        }
        return null;
    }

    /**
     * To check given date (dateToCheck) is between Start Date and End Date
     * @param startDate
     * @param endDate
     * @param dateToCheck
     * @return
     */
    public static boolean checkDateBetween(Date startDate, Date endDate, Date dateToCheck) {
        if(startDate != null && endDate != null && dateToCheck != null){
            return dateToCheck.compareTo(startDate) * endDate.compareTo(dateToCheck) >= 0;
        }
        return false;
    }

    /**
     *  To check given date (readingDate) is between Previous Date and CurrentDate(Server Date)
     * @param previousDate
     * @param readingDate
     * @return
     */
    public static boolean checkDateBetweenPreviousDateAndCurrentDate(Date previousDate, Date readingDate){
        Date currentDate = getCurrentDate();
        if(previousDate != null && readingDate != null){
           return readingDate.compareTo(previousDate) * currentDate.compareTo(readingDate) >= 0;
        }
        return false;
    }

    public static Date getDateFromString(String dateInStringFormat) {

        Date dateInDateFormat = null ;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            dateInDateFormat = sdf.parse(dateInStringFormat);
        } catch (Exception e) {
            e.getStackTrace();
        }
        return dateInDateFormat;
    }

    public static Date getDateFromStringFormat(String dateInStringFormat ,String dateFormat) {
        Date dateInDateFormat = null ;
        try {
            if(dateInStringFormat != null && dateFormat != null) {
                SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
                dateInDateFormat = new Date();
                dateInDateFormat = sdf.parse(dateInStringFormat);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return dateInDateFormat;
    }

    public static Date addDaysInDate(int days,Date date ){
        Date updatedDate = null;
        //Changed the code , where check of positive days was being done earlier code
        if(date != null){
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
             cal.add(Calendar.DATE, days);
            updatedDate = cal.getTime();
        }
        return updatedDate;
    }

    /**
     *  compare given two given bill month , returns integer  (0 if same both bill months are same)/ (-1, if first bill month< 2nd bill month)/(1, if first bill month>2nd bill month),
     * @param month1
     * @param month2
     * @return
     */
    public static int compareMonth(String month1, String month2) throws ParseException {
        int comparisionValue = 0;
        if(month1 != null && month2 != null) {
            SimpleDateFormat sdf = new SimpleDateFormat("MMM-yyyy");
            Calendar cal1 = Calendar.getInstance();
            Calendar cal2 = Calendar.getInstance();
            cal1.setTime(sdf.parse(month1));
            cal2.setTime(sdf.parse(month2));
            comparisionValue = cal1.compareTo(cal2);
        }
        return comparisionValue;
    }

    public static ReadMaster convertCustomReadMasterToReadMaster(CustomReadMaster customReadMaster){
        ReadMaster readMaster = null;
        if(customReadMaster != null){
            readMaster = new ReadMaster();
            readMaster.setId(customReadMaster.getId());
            readMaster.setBillMonth(customReadMaster.getBillMonth());
            readMaster.setGroupNo(customReadMaster.getGroupNo());
            readMaster.setReadingDiaryNo(customReadMaster.getReadingDiaryNo());
            readMaster.setConsumerNo(customReadMaster.getConsumerNo());
            readMaster.setMeterIdentifier(customReadMaster.getMeterIdentifier());
            readMaster.setReadingDate(customReadMaster.getReadingDate());
            readMaster.setReadingType(customReadMaster.getReadingType());
            readMaster.setMeterStatus(customReadMaster.getMeterStatus());
            readMaster.setReplacementFlag(customReadMaster.getReplacementFlag());
            readMaster.setSource(customReadMaster.getSource());
            readMaster.setReading(customReadMaster.getReading());
            readMaster.setDifference(customReadMaster.getDifference());
            readMaster.setMf(customReadMaster.getMf());
            readMaster.setConsumption(customReadMaster.getConsumption());
            readMaster.setAssessment(customReadMaster.getAssessment());
            readMaster.setPropagatedAssessment(customReadMaster.getPropagatedAssessment());
            readMaster.setTotalConsumption(customReadMaster.getTotalConsumption());
            readMaster.setUsedOnBill(customReadMaster.isUsedOnBill());
            readMaster.setRemark(customReadMaster.getRemark());
            readMaster.setReadMasterKW(customReadMaster.getReadMasterKW());
            readMaster.setReadMasterPF(customReadMaster.getReadMasterPF());
            readMaster.setCreatedBy(customReadMaster.getCreatedBy());
            readMaster.setCreatedOn(customReadMaster.getCreatedOn());
            readMaster.setUpdatedBy(customReadMaster.getUpdatedBy());
            readMaster.setUpdatedOn(customReadMaster.getUpdatedOn());
        }
        return readMaster;
    }

    public static CustomReadMaster convertReadMasterToCustomReadMaster(ReadMasterInterface reading){
        CustomReadMaster customReadMaster = null;
        if(reading != null){
            customReadMaster = new CustomReadMaster();
            customReadMaster.setId(reading.getId());
            customReadMaster.setBillMonth(reading.getBillMonth());
            customReadMaster.setGroupNo(reading.getGroupNo());
            customReadMaster.setReadingDiaryNo(reading.getReadingDiaryNo());
            customReadMaster.setConsumerNo(reading.getConsumerNo());
            customReadMaster.setMeterIdentifier(reading.getMeterIdentifier());
            customReadMaster.setReadingDate(reading.getReadingDate());
            customReadMaster.setReadingType(reading.getReadingType());
            customReadMaster.setMeterStatus(reading.getMeterStatus());
            customReadMaster.setReplacementFlag(reading.getReplacementFlag());
            customReadMaster.setSource(reading.getSource());
            customReadMaster.setReading(reading.getReading());
            customReadMaster.setDifference(reading.getDifference());
            customReadMaster.setMf(reading.getMf());
            customReadMaster.setConsumption(reading.getConsumption());
            customReadMaster.setAssessment(reading.getAssessment());
            customReadMaster.setPropagatedAssessment(reading.getPropagatedAssessment());
            customReadMaster.setTotalConsumption(reading.getTotalConsumption());
            customReadMaster.setUsedOnBill(reading.isUsedOnBill());
            customReadMaster.setRemark(reading.getRemark());
            customReadMaster.setReadMasterKW(reading.getReadMasterKW());
            customReadMaster.setReadMasterPF(reading.getReadMasterPF());
            customReadMaster.setCreatedBy(reading.getCreatedBy());
            customReadMaster.setCreatedOn(reading.getCreatedOn());
            customReadMaster.setUpdatedBy(reading.getUpdatedBy());
            customReadMaster.setUpdatedOn(reading.getUpdatedOn());
        }
        return customReadMaster;
    }


    /**
     * this method will convert custom bill into bill removes transient entity present inside bill.
     * @param billInterface
     * @return
     */
    public static BillInterface convertCustomBillInterfaceToBillInterface(BillInterface billInterface) {
        BillInterface updatedBillInterface = null;
        if(billInterface != null) {
            updatedBillInterface = new Bill();
            updatedBillInterface.setId(billInterface.getId());
            updatedBillInterface.setLocationCode(billInterface.getLocationCode());
            updatedBillInterface.setGroupNo(billInterface.getGroupNo());
            updatedBillInterface.setReadingDiaryNo(billInterface.getReadingDiaryNo());
            updatedBillInterface.setConsumerNo(billInterface.getConsumerNo());
            updatedBillInterface.setBillMonth(billInterface.getBillMonth());
            updatedBillInterface.setBillDate(billInterface.getBillDate());
            updatedBillInterface.setDueDate(billInterface.getDueDate());
            updatedBillInterface.setChequeDueDate(billInterface.getChequeDueDate());
            updatedBillInterface.setCurrentReadDate(billInterface.getCurrentReadDate());
            updatedBillInterface.setCurrentRead(billInterface.getCurrentRead());
            updatedBillInterface.setPreviousRead(billInterface.getPreviousRead());
            updatedBillInterface.setDifference(billInterface.getDifference());
            updatedBillInterface.setMf(billInterface.getMf());
            updatedBillInterface.setMeteredUnit(billInterface.getMeteredUnit());
            updatedBillInterface.setAssessment(billInterface.getAssessment());
            updatedBillInterface.setTotalUnit(billInterface.getTotalUnit());
            updatedBillInterface.setGmcUnit(billInterface.getGmcUnit());
            updatedBillInterface.setBilledUnit(billInterface.getBilledUnit());
            updatedBillInterface.setBilledMD(billInterface.getBilledMD());
            updatedBillInterface.setBilledPF(billInterface.getBilledPF());
            updatedBillInterface.setLoadFactor(billInterface.getLoadFactor());
            updatedBillInterface.setFixedCharge(billInterface.getFixedCharge());
            updatedBillInterface.setAdditionalFixedCharges1(billInterface.getAdditionalFixedCharges1());
            updatedBillInterface.setAdditionalFixedCharges2(billInterface.getAdditionalFixedCharges2());
            updatedBillInterface.setEnergyCharge(billInterface.getEnergyCharge());
            updatedBillInterface.setFcaCharge(billInterface.getFcaCharge());
            updatedBillInterface.setElectricityDuty(billInterface.getElectricityDuty());
            updatedBillInterface.setMeterRent(billInterface.getMeterRent());
            updatedBillInterface.setPfCharge(billInterface.getPfCharge());
            updatedBillInterface.setWeldingTransformerSurcharge(billInterface.getWeldingTransformerSurcharge());
            updatedBillInterface.setLoadFactorIncentive(billInterface.getLoadFactorIncentive());
            updatedBillInterface.setSdInterest(billInterface.getSdInterest());
            updatedBillInterface.setCcbAdjustment(billInterface.getCcbAdjustment());
            updatedBillInterface.setLockCredit(billInterface.getLockCredit());
            updatedBillInterface.setOtherAdjustment(billInterface.getOtherAdjustment());
            updatedBillInterface.setEmployeeRebate(billInterface.getEmployeeRebate());
            updatedBillInterface.setOnlinePaymentRebate(billInterface.getOnlinePaymentRebate());
            updatedBillInterface.setPrepaidMeterRebate(billInterface.getPrepaidMeterRebate());
            updatedBillInterface.setPromptPaymentIncentive(billInterface.getPromptPaymentIncentive());
            updatedBillInterface.setAdvancePaymentIncentive(billInterface.getAdvancePaymentIncentive());
            updatedBillInterface.setDemandSideIncentive(billInterface.getDemandSideIncentive());
            updatedBillInterface.setSubsidy(billInterface.getSubsidy());
            updatedBillInterface.setCurrentBill(billInterface.getCurrentBill());
            updatedBillInterface.setArrear(billInterface.getArrear());
            updatedBillInterface.setCumulativeSurcharge(billInterface.getCumulativeSurcharge());
            updatedBillInterface.setSurchargeDemanded(billInterface.getSurchargeDemanded());
            updatedBillInterface.setNetBill(billInterface.getNetBill());
            updatedBillInterface.setAsdBilled(billInterface.getAsdBilled());
            updatedBillInterface.setAsdArrear(billInterface.getAsdArrear());
            updatedBillInterface.setAsdInstallment(billInterface.getAsdInstallment());
            updatedBillInterface.setDeleted(billInterface.isDeleted());
            updatedBillInterface.setCreatedBy(billInterface.getCreatedBy());
            updatedBillInterface.setCreatedOn(billInterface.getCreatedOn());
            updatedBillInterface.setUpdatedBy(billInterface.getUpdatedBy());
            updatedBillInterface.setUpdatedOn(billInterface.getUpdatedOn());
            updatedBillInterface.setPristineElectricityDuty(billInterface.getPristineElectricityDuty());
            updatedBillInterface.setPristineNetBill(billInterface.getPristineNetBill());
            updatedBillInterface.setCurrentBillSurcharge(billInterface.getCurrentBillSurcharge());
            updatedBillInterface.setBillTypeCode(billInterface.getBillTypeCode());
            updatedBillInterface.setXrayFixedCharge(billInterface.getXrayFixedCharge());
        }
        return updatedBillInterface;
    }

    public static List<ReadMasterInterface> convertCustomReadMasterToReadMaster(List<CustomReadMaster> customReadings){
        List<ReadMasterInterface> readings= new ArrayList<>();
        for (CustomReadMaster customReadMaster : customReadings) {
            if(customReadMaster != null){
                readings.add(convertCustomReadMasterToReadMaster(customReadMaster));
            }
        }
        return readings;
    }

    public static Date getEndDate() {
        Date endDate = null;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            endDate = new Date();
            endDate = sdf.parse("01-01-2050");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return endDate;
    }

    public static Date getDateWithoutTimeStamp(Date date) {
        Date updatedDate = null;
        if (date != null){
            try{
                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                String withoutTimeStampDate = sdf.format(date);
                updatedDate = sdf.parse(withoutTimeStampDate);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        return updatedDate;
    }

    public static String getTariffCategoryFromTariffCode(String tariffCode) {
        if (tariffCode != null) {
            String tariffCategory = tariffCode.split("\\.")[0];
            return tariffCategory;
        }
        return null;
    }
}
