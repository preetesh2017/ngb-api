package com.mppkvvcl.ngbapi.controllers;

import com.mppkvvcl.ngbapi.services.DiscomService;
import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.DiscomInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


/**
 * Created by SHIVANSHU on 17-07-2017.
 */
@RestController
@RequestMapping(value = "/" +
        "+" +
        "" +
        "discom")
public class DiscomController {

    /**
     * Asking spring to get singleton object of DiscomService.
     */
    @Autowired
    DiscomService discomService;

    /**
     * getting logger object for logging in current class.
     */
    Logger logger = GlobalResources.getLogger(DiscomController.class);

    /**
     * URI : /discom<br><br>
     * This getAll() method is used for getting all objects with URI : /discom<br><br>
     * Response : 200(OK) for found Successfully<br>
     * Response : 204(NO_CONTENT) for found list is empty<br>
     * Response : 417(EXPECTATION_FAILED) for occurring error to get Discom list<br><br>
     * return response
     */

    @RequestMapping(method = RequestMethod.GET , produces = "application/json")
    public ResponseEntity<?> getAll(){
        String methodName = "getAll() : ";
        logger.info(methodName+"Got request to get all Discom");
        ResponseEntity<?> response =  null;
        List<? extends DiscomInterface> discoms = discomService.getAll();
        if (discoms !=  null){
            logger.info(methodName+"Discom list found");
            if (discoms.size() > 0 ){
                logger.info(methodName+"Discom list found with size"+discoms.size());
                response = new ResponseEntity<Object>(discoms , HttpStatus.OK);
            }else{
                logger.error(methodName+"Discom list found but there is no object inside it");
                response = new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
            }
        }else{
            logger.error(methodName+"Discom list return null");
            response = new ResponseEntity<Object>(HttpStatus.EXPECTATION_FAILED);
        }
        return  response;
    }
}
