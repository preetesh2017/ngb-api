package com.mppkvvcl.ngbapi.controllers;

import com.mppkvvcl.ngbapi.services.PaymentStagingService;
import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbentity.beans.ErrorMessage;
import com.mppkvvcl.ngbentity.beans.PaymentStaging;
import com.mppkvvcl.ngbinterface.interfaces.PaymentInterface;
import com.mppkvvcl.ngbinterface.interfaces.PaymentStagingInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by PREETESH on 1/3/2018.
 */
@RestController
@RequestMapping(value = "/payment/staging")
public class PaymentStagingController {

    /**
     * Getting logger object from global resources for current class.
     */
    private static final Logger logger = GlobalResources.getLogger(PaymentStagingController.class);

    /**
     * Requesting spring to get singleton PaymentService object
     */
    @Autowired
    private PaymentStagingService paymentStagingService;

    /**
     * URI - /payment/staging/ivrs/{ivrs}<br>
     * This method takes ivrs and fetch a list of payments.<br>
     * Response 200 for OK status and list of payments is sent if successful.<br>
     * Response 204 NO_CONTENT status is sent if no content in list.<br>
     * Response 404 for NOT_FOUND status is sent when list is null.<br>
     * Response 400 for BAD_REQUEST status is sent if input param are null<br>
     * param consumerNo<br>
     * return
     */
    @RequestMapping(method = RequestMethod.GET, value = "/ivrs/{ivrs}",  produces = "application/json" )
    public ResponseEntity<?> getByIvrs(@PathVariable("ivrs") String ivrs){
        String methodName = "getByIvrs() : ";
        logger.info(methodName + "called ");
        ResponseEntity<?> response = null;
        List<PaymentStagingInterface> paymentStagingInterfaces = null;
        if(ivrs != null){
            paymentStagingInterfaces = paymentStagingService.getByIvrs(ivrs);
            if(paymentStagingInterfaces != null){
                if(paymentStagingInterfaces.size() >0){
                    response = new ResponseEntity<>(paymentStagingInterfaces, HttpStatus.OK);
                }else{
                    response = new ResponseEntity<>(HttpStatus.NO_CONTENT);
                }
            }else{
                response = new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        }else{
            logger.error(methodName + "Input param ivrs :" + ivrs + "found null");
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return response;
    }


    /**
     * URI - /payment/staging/status/{status}<br>
     * This method takes ivrs and fetch a list of payments.<br>
     * Response 200 for OK status and list of payments is sent if successful.<br>
     * Response 204 NO_CONTENT status is sent if no content in list.<br>
     * Response 404 for NOT_FOUND status is sent when list is null.<br>
     * Response 400 for BAD_REQUEST status is sent if input param are null<br>
     * param consumerNo<br>
     * return
     */
    @RequestMapping(method = RequestMethod.GET, value = "/status/{status}",  produces = "application/json" )
    public ResponseEntity<?> getByStatus(@PathVariable("status") String status){
        String methodName = "getByStatus() : ";
        logger.info(methodName + "called ");
        ResponseEntity<?> response = null;
        List<PaymentStagingInterface> paymentStagingInterfaces = null;
        if(status != null){
            paymentStagingInterfaces = paymentStagingService.getByStatus(status);
            if(paymentStagingInterfaces != null){
                if(paymentStagingInterfaces.size() >0){
                    response = new ResponseEntity<>(paymentStagingInterfaces, HttpStatus.OK);
                }else{
                    response = new ResponseEntity<>(HttpStatus.NO_CONTENT);
                }
            }else{
                response = new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        }else{
            logger.error(methodName + "Input param status found null");
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return response;
    }


    /**
     * URI - /payment/staging<br><br>
     * This create method takes PaymentStaging object and insert it into the backend database when user hit the URI.<br><br>
     * Response 201 for CREATED and PaymentStaging object is sent if successful<br>
     * Response 417 for EXPECTATION_FAILED is sent when insertion fail<br>
     * Response 400 for BAD_REQUEST is sent when input param found null.<br><br>
     * param paymentStaging<br>
     * return response
     */
    @RequestMapping(method = RequestMethod.POST, consumes = "application/json",produces = "application/json")
    public ResponseEntity insert(@RequestBody PaymentStaging paymentStaging){
        String methodName = "insert() : ";
        logger.info(methodName + "called");
        ResponseEntity<?> response = null;
        PaymentStagingInterface insertedPaymentStagingInterface = null;
        ErrorMessage errorMessage = new ErrorMessage();
        if(paymentStaging != null ){
            insertedPaymentStagingInterface = paymentStagingService.insert(paymentStaging,errorMessage);
            if(insertedPaymentStagingInterface != null){
                logger.info(methodName + "PaymentStaging created successfully");
                response = new ResponseEntity<>(paymentStaging,HttpStatus.CREATED);
            }else{
                logger.error(methodName + "error in creating PaymentStaging");
                response = new ResponseEntity<>(errorMessage,HttpStatus.EXPECTATION_FAILED);
            }
        }else{
            logger.error(methodName + "PaymentStaging object is NULL");
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return response;
    }
}
