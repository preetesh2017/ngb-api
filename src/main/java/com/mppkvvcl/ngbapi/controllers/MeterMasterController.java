package com.mppkvvcl.ngbapi.controllers;

import com.mppkvvcl.ngbapi.services.ConsumerMeterMappingService;
import com.mppkvvcl.ngbapi.services.MeterMasterService;
import com.mppkvvcl.ngbapi.services.MeterModemMappingService;
import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.MeterMasterInterface;
import com.mppkvvcl.ngbentity.beans.MeterMaster;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

/**
 * Created by PREETESH on 6/21/2017.
 */

@RestController
@RequestMapping(value = "/meter")
public class MeterMasterController {

    /*
    * Getting logger object from GlobalResources for current class.
     */
    private static final Logger logger = GlobalResources.getLogger(MeterMasterController.class);

    /**
     * Requesting spring to get MeterMasterService object.
     */
    @Autowired
    private MeterMasterService meterMasterService;



    /**
     * Requesting spring to get ConsumerMeterMappingService object.
     */
    @Autowired
    private ConsumerMeterMappingService consumerMeterMappingService;


    @Autowired
    private MeterModemMappingService meterModemMappingService;

    /**
     * URI - /meter/make/{make}/serial-no/{serialNo}<br><br>
     * This getMeterByMakeAndSerialNo method
     * takes serialNo and make to fetch the MeterMaster object.<br><br>
     * Response 200 for ok with MeterMaster is sent if successful,<br>
     * Response 404 for NOT_FOUND is sent when MeterMaster not found<br>
     * Response 400 for BAD_REQUEST is sent when input serialNo or make found null.<br><br>
     * param serialNo<br>
     * param make<br>
     * return response
     */
    @RequestMapping(method = RequestMethod.GET, value = "/make/{make}/serial-no/{serialNo}", produces = "application/json")
    public ResponseEntity getMeterByMakeAndSerialNo(@PathVariable("make") String make, @PathVariable("serialNo") String serialNo) {
        String methodName = " getMeterByMakeAndSerialNo() : ";
        logger.info(methodName + "called for make : " + make + " serial no: " + serialNo);
        MeterMasterInterface meterMaster = null;
        ResponseEntity<?> response = null;
        if (serialNo != null && make != null) {
            meterMaster = meterMasterService.getMeterMasterByMakeAndSerialNo(make, serialNo);
            if (meterMaster != null) {
                logger.info(methodName + "Got MeterMaster " + meterMaster);
                // Sending HttpStatus.OK since Angular at front end considers HttpStatus.FOUND as error code
                response = new ResponseEntity<>(meterMaster, HttpStatus.OK);
            } else {
                logger.error(methodName + "Got zero MeterMaster");
                response = new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
        } else {
            logger.error(methodName + "Input paremeter passed does not meet requirement of being NOT NULL, hence returning BAD REQUEST as response");
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return response;
    }


    /**
     * URI : /meter<br><br>
     * This insertMeterMaster method is used for inserting MeterMaster object with
     * URI : /meter with post type.<br><br>
     * Response : CREATED for successfully inserted<br>
     * Response : EXPECTATION_FAILED for unsuccessfully inserted the object<br>
     * Response : BAD_REQUEST for unsuccessfully found MeterMaster object to insert.<br><br>
     * param meterMaster
     * return response
     */

    @RequestMapping(method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
    public ResponseEntity insertMeterMaster(@RequestBody MeterMaster meterMaster) {
        String methodName = "insertMeterMaster() : ";
        logger.info(methodName + "called");
        MeterMasterInterface insertedMeterMaster = null;
        ResponseEntity<?> response = null;
        if (meterMaster != null) {
            insertedMeterMaster = meterMasterService.insert(meterMaster);
            if (insertedMeterMaster != null) {
                logger.info(methodName + "inserted MeterMaster " + insertedMeterMaster);
                response = new ResponseEntity<>(insertedMeterMaster, HttpStatus.CREATED);
            } else {
                logger.error(methodName + "error occurred in inserting MeterMaster");
                response = new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
            }
        } else {
            logger.error(methodName + "Input paremeter passed does not meet requirement of being NOT NULL, hence returning BAD REQUEST as response");
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return response;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/meter-identifier/{meterIdentifier}", produces = "application/json")
    public ResponseEntity getMeterIdentifier(@PathVariable("meterIdentifier") String meterIdentifier) {
        String methodName = "getMeterIdentifier() : ";
        logger.info(methodName + "called for meterIdentifier :  " + meterIdentifier);
        MeterMasterInterface meterMaster = null;
        ResponseEntity<?> response = null;
        if (meterIdentifier != null) {
            meterMaster = meterMasterService.getByIdentifier(meterIdentifier);
            if (meterMaster != null) {
                logger.info(methodName + "Got MeterMaster " + meterMaster);
                response = new ResponseEntity<>(meterMaster, HttpStatus.OK);
            } else {
                logger.error(methodName + "unable to fetch  MeterMaster");
                response = new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
        } else {
            logger.error(methodName + "Input parameter passed does not meet requirement of being NOT NULL, hence returning BAD REQUEST as response");
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return response;
    }
}

