package com.mppkvvcl.ngbapi.controllers;

import com.mppkvvcl.ngbapi.services.ReadTypeConfiguratorService;
import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.ReadTypeConfiguratorInterface;
import com.mppkvvcl.ngbentity.beans.ReadTypeConfigurator;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by SHIVANSHU on 04-07-2017.
 */

@RestController
@RequestMapping(value = "read-type-configurator")
public class ReadTypeConfiguratorController {
    /*
     Asking  spring to inject singleton object for ReadTypeConfiguratorService
     so that we can use its various methods in this class and perform
     various operation through repo
     */
    @Autowired
    private ReadTypeConfiguratorService readTypeConfiguratorService ;

    /*
        Getting Logger object for logging in current class ReadTypeConfiguratorController
     */
    Logger logger = GlobalResources.getLogger(ReadTypeConfiguratorController.class);

    /**
     * URI - /read-type-configurator/consumer-no/{CONSUMER No}<br><br>
     * This getByConsumer() method used for getting ReadTypeConfigurator for
     * URL:- /read-type-configurator/consumer-no/{CONSUMER No}<br><br>
     * Response :- OK status- 200 :- If ReadTypeConfigurator object found successfully<br>
     * Response :- NOT_FOUND Status - 404 : If No ReadTypeConfigurator object found<br>
     * Response :- BAD_REQUEST status - 400 : If Consumer no not found in method<br><br>
     * param consumerNo<br>
     * return response
     */
    @RequestMapping(method = RequestMethod.GET , value = "/consumer-no/{consumerNo}" , produces = "application/json")
    public ResponseEntity<?> getByConsumerNo(@PathVariable("consumerNo") String consumerNo){
        String methodName = "getByConsumerNo() :";
        ResponseEntity< ? > response = null;
        logger.info(methodName+"    Starting to get ReadTypeConfigurator object by consumer no :- " + consumerNo);
        if (consumerNo != null){
            logger.info(methodName+"    consumer no :-"+consumerNo+" got successfully in method");
            ReadTypeConfiguratorInterface readTypeConfigurator = readTypeConfiguratorService.getByConsumerNo(consumerNo);
            if (readTypeConfigurator != null){
                logger.info(methodName+"    ReadTypeConfigurator object found successfully"+readTypeConfigurator);
                response = new ResponseEntity<>(readTypeConfigurator  , HttpStatus.OK);
            }else{
                logger.error(methodName+"   ReadTypeConfigurator object not found");
                response = new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        }else{
            logger.error(methodName+"   consumer no:-"+consumerNo+" not found successfully in method");
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return response;
    }

    /**
     * URI - /read-type-configurator <br><br>
     * This getAll() method is used for getting List of ReadTypeConfigurator objects<br><br>
     * Response : OK status 200 : If List found successfully and at least one object inside it<br>
     * Response : EXPECTATION_FAILED status 417 : If List return Null<br>
     * Response : NOT_FOUND 400 : If there is no object inside List<br><br>
     * return response
     */
    @RequestMapping(method = RequestMethod.GET , produces =  "application/json")
    public ResponseEntity<?> getAll(){
        String methodName = "getAll() : ";
        ResponseEntity<?> response = null;
        logger.info(methodName+"    Starting to get All ReadTypeConfigurator objects' list");
        List<? extends ReadTypeConfiguratorInterface> readTypeConfigurators = readTypeConfiguratorService.getAll();
        if (readTypeConfigurators != null){
            if (readTypeConfigurators.size() > 0){
                logger.info(methodName+"ReadTypeConfigurator objects' list found successfully");
                response = new ResponseEntity<Object>(readTypeConfigurators , HttpStatus.OK);
            }else{
                logger.error(methodName +"There is no object inside List");
                response = new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        }else{
            logger.error(methodName+"   ReadTypeConfigurator objects' list return null");
            response =  new ResponseEntity<Object>(HttpStatus.EXPECTATION_FAILED);
        }
    return response;
    }

    /**
     * URI- read-type-configurator<br><br>
     * This insert() method is used for inserting ReadTypeConfigurator object in respective table<br><br>
     * Response : 201(CREATED): If successfully inserted<br>
     * Response : 417(EXPECTATION_FAILED) : If object not insert successfully<br>
     * Response : 400(BAD_REQUEST) : ReadTypeConfigurator object not found for inserting<br><br>
     * param readTypeConfigurator<br>
     * return insertedReadTypeConfigurator
     */
    @RequestMapping(method = RequestMethod.POST, consumes = "application/json")
    public ResponseEntity<?> insert(@RequestBody ReadTypeConfigurator readTypeConfigurator){
        String methodName = "insert() : ";
        logger.info(methodName+"called");
        ResponseEntity<?> response = null;
        if (readTypeConfigurator != null){
            if (readTypeConfigurator.getSubcategoryCode() != 0l && readTypeConfigurator.getTariffId() != 0l){
                logger.info(methodName+"    ReadTypeConfigurator object found  and started to insert");
                ReadTypeConfiguratorInterface insetedReadTypeConfigurator = readTypeConfiguratorService.insert(readTypeConfigurator);
                if (insetedReadTypeConfigurator != null){
                    logger.info(methodName+"    This ReadTypeConfigurator object:-"+readTypeConfigurator+" inserted successfully");
                    response = new ResponseEntity<>(insetedReadTypeConfigurator , HttpStatus.CREATED);
                }else{
                    logger.error(methodName+"   This ReadTypeConfigurator object not inserted");
                    response = new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
                }
            }else{
                logger.error(methodName+"   ReadTypeConfigurator object found but tariff_id and subcategory_code cant't NULL");
                response = new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
            }
        }else{
            logger.error(methodName+"   ReadTypeConfigurator object not found in method");
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return response;
    }

}
