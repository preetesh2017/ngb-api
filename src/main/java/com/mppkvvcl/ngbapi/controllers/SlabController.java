package com.mppkvvcl.ngbapi.controllers;

import com.mppkvvcl.ngbapi.services.SlabService;
import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.SlabInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by MITHLESH on 15-07-2017.
 */
@RestController
@RequestMapping(value = "Slab")
public class SlabController {

    /**
     * Asking Spring to get singleton object of class SlabService
     */
    @Autowired
    SlabService slabService;
    /**
     * To get logger object to Logging in current class
     */
    Logger logger = GlobalResources.getLogger(SlabController.class);

    /**
     * URI : Slab/tariff-id/?<br><br>
     * This getByTariffId() method is used to get slabs from SlabService and return as ResponseEntity
     * with URI: Slab/tariff-id/?<br><br>
     * Response : 200(OK) : slabs found successfully<br>
     * Response : 417(EXPECTATION_FAILED) : slabs found with size zero<br>
     * Response : 404(NOT_FOUND) : slabs found null<br>
     * Response : 400(BAD_REQUEST) : Received tariffId is zero<br><br>
     * param tariffId<br>
     * return responseEntity
     */
    @RequestMapping(value = "/tariff-id/{tariffId}", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<?> getByTariffId(@PathVariable("tariffId") long tariffId){
        String methodName = "getByTariffId() : ";
        ResponseEntity<?> responseEntity = null;
        List<SlabInterface> slabs = null;
        logger.info(methodName + "Received GET request with tariffId");
        if(tariffId != 0){
            logger.info(methodName + "Calling SlabService to fetch List<Slab> with tariffId : "+tariffId);
            slabs = slabService.getByTariffId(tariffId);
            if(slabs != null){
                if(slabs.size() > 0){
                    logger.info(methodName + "Successfully got List<Slab> from SlabService with size : "+slabs.size());
                    responseEntity = new ResponseEntity<>(slabs , HttpStatus.OK);
                    logger.info(methodName + "Return response with status : OK");
                }else{
                    logger.error(methodName + "Successfully got List<Slab> from SlabService with size : "+slabs.size());
                    responseEntity = new ResponseEntity<Object>(HttpStatus.EXPECTATION_FAILED);
                }
            }else{
                responseEntity = new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
                logger.error(methodName + "Fetched List<Slab> is null");
            }
        }else{
            responseEntity = new ResponseEntity<Object>(HttpStatus.BAD_REQUEST);
            logger.error(methodName + "Received tariffId is zero");
        }
        return responseEntity;
    }
}
