package com.mppkvvcl.ngbapi.controllers;

import com.mppkvvcl.ngbapi.custombeans.CustomAdjustment;
import com.mppkvvcl.ngbapi.services.AdjustmentProfileService;
import com.mppkvvcl.ngbapi.services.AdjustmentService;
import com.mppkvvcl.ngbapi.services.AdjustmentTypeService;
import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.AdjustmentInterface;
import com.mppkvvcl.ngbentity.beans.AdjustmentProfile;
import com.mppkvvcl.ngbentity.beans.ErrorMessage;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by ANKIT on 11-09-2017.
 */
@RestController
@RequestMapping(value = "/adjustment-profile")
public class AdjustmentProfileController {
    /**
     * Asking spring to get Singleton object of AdjustmentService.
     */
    @Autowired
    AdjustmentService adjustmentService;

    /**
     * Asking spring to get Singleton object of AdjustmentProfileService.
     */
    @Autowired
    AdjustmentProfileService adjustmentProfileService;

    /**
     * Asking spring to get Singleton object of AdjustmentTypeService.
     */
    @Autowired
    private AdjustmentTypeService adjustmentTypeService;

    /**
     * To get Logger object for logging in current class.
     */
    Logger logger = GlobalResources.getLogger(AdjustmentProfileController.class);

    /**
     * URI : adjustment-profile <br>
     * Method : POST<br>
     * Response : 201(Created) when inserted the adjustment-profile successfully in the Database.<br>
     * Response : 400 (Bad_Request) If AdjustmentProfile is null.<br>
     * param adjustment<br>
     * return
     */
    @RequestMapping(method = RequestMethod.PUT, consumes = "application/json", produces = "application/json")
    public ResponseEntity update(@RequestBody AdjustmentProfile adjustmentProfile){
        String methodName = "update() : ";
        boolean status;
        ErrorMessage errorMessage = null;
        ResponseEntity response = null;
        CustomAdjustment customAdjustment = null;
        logger.info(methodName + "called");
        if (adjustmentProfile != null) {
            try {
                AdjustmentInterface updatedAdjustment = adjustmentProfileService.update(adjustmentProfile);
                customAdjustment = adjustmentService.prepareCustomAdjustment(updatedAdjustment);
                if(customAdjustment != null){
                    response = new ResponseEntity(customAdjustment, HttpStatus.CREATED);
                }else{
                    errorMessage = new ErrorMessage("Unable to Update Adjustment.");
                    logger.error("Could not update Adjustment");
                    response = new ResponseEntity(errorMessage,HttpStatus.EXPECTATION_FAILED);
                }
            }catch(RuntimeException e){
                errorMessage = new ErrorMessage("Internal Error Occurred.");
                logger.error(methodName + "Received Runtime Exception in controller with error message as :" + e.getMessage());
                response = new ResponseEntity(errorMessage,HttpStatus.EXPECTATION_FAILED);
            }catch(Exception exp){
                errorMessage = new ErrorMessage("Unable to Update Adjustment.");
                logger.error(methodName+" Received Exception approve the adjustment Profile and the reason is: "+exp.getMessage());
                response = new ResponseEntity(errorMessage,HttpStatus.EXPECTATION_FAILED);
            }
        } else {
            logger.error(methodName + "Input param adjustment Profile found null");
            response = new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        return response;
    }

}
