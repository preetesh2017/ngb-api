package com.mppkvvcl.ngbapi.controllers;

import com.mppkvvcl.ngbapi.services.TariffService;
import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.TariffInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * Created by SUMIT on 19-05-2017.
 * This tariff controller defines various uri for tariff resource under nsc database
 * and nsc schema for nextgenbillingbackend.This REST API for tariffs has the required and related
 * methods for fulfilling front end request
 *
 * @author  Sumit Verma
 * @version 1.0
 * last change @since   19-05-2017
 *
 * UPDATED BY : SUMIT VERMA on 03/06/2017
 * UPDATION : Changes done for calls to tariffService method for getting tariffs with added parameter
 * tariffType to achieve more filtering. This is mainly added to cater problem faced in LV 4 SEASONAL tariff.
 * The tariffType adds new level of filtering to the method getTariffs. the tariffType can also be used
 * logically for other such solution, if required in future.
 **/
@Controller
@RequestMapping(value = "/tariff")
public class TariffController {

    /*
    Getting logger object for logging in TariffController class.
     */
    private static final Logger logger = GlobalResources.getLogger(TariffController.class);

    //This provides Dependency Injection by spring at runtime for tariff service
    @Autowired
    private TariffService tariffService;

    /**
     * URI - /tariff/tariff-category/{tariffCategory}<br><br>
     * This getTariffs method returns fetch request for /tariff/tariff-category/{tariffCategory}
     * When tariffs list is returned HTTP status 200 OK is sent.<br>
     * In case of no data found NO_CONTENT  status is sent.<br><br>
     * param tariffCategory<br>
     * param connectionType<br>
     * param meteringStatus<br>
     * return response
     */
    @RequestMapping(method = RequestMethod.GET,value = "/tariff-category/{tariffCategory}",produces = "application/json")
    public ResponseEntity getTariffs(@PathVariable("tariffCategory")String tariffCategory,
                                     @RequestParam("connectionType")String connectionType,
                                     @RequestParam("meteringStatus") String meteringStatus,
                                     @RequestParam("tariffType") String tariffType) {
        logger.info("getTariffs: Got requests to fetch tariffs with parameters as: "+tariffCategory+"  "+connectionType+" "+meteringStatus+" "+tariffType);
        ResponseEntity<?> tariffsResponse=null;
        if(tariffCategory != null && connectionType != null && meteringStatus!= null && tariffType!=null){
            List<TariffInterface> tariffs = tariffService.getByTariffCategoryAndMeteringStatusAndConnectionTypeAndTariffTypeAndEffectiveDate(tariffCategory,meteringStatus,connectionType,tariffType,null);
            if (tariffs != null){
                if (tariffs.size() > 0) {
                    logger.info("getTariffs : Got " + tariffs.size() + " tariffs in controller");
                    tariffsResponse = new ResponseEntity<List<TariffInterface>>(tariffs, HttpStatus.OK);
                }else{
                    logger.info("getTariffs : Got " + tariffs.size() + " tariffs in controller, hence returning no content");
                    tariffsResponse = new ResponseEntity<List<TariffInterface>>(tariffs, HttpStatus.NO_CONTENT);
                }
            }else{
                logger.error("getTariffs : Fetched tariffs is null.Returning error response");
                tariffsResponse = new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
            }
        }else{
            logger.error("getTariffs : inputs parameters is null.Returning bad request");
            tariffsResponse = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        logger.info("getTariffs: Returning from fetch tariffs");
        return tariffsResponse;
    }
}
