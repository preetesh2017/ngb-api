package com.mppkvvcl.ngbapi.controllers;

import com.mppkvvcl.ngbapi.services.EmployeeMasterService;
import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.EmployeeMasterInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by VIKAS on 7/6/2017.
 */

@RestController
@RequestMapping(value = "/employee" )
public class EmployeeMasterController {

    /**
     * This provides Dependency Injection by spring at runtime for employeeMasterService
     */
    @Autowired
    EmployeeMasterService employeeMasterService ;

    /**
     * Getting logger object for logging in EmployeeMasterController class.
     */
    private static final Logger logger = GlobalResources.getLogger(EmployeeMasterController.class);

    /**
     * URI - /employee/employee-no/{employeeNo}<br><br>
     * This method takes employeeNo and fetch employeeMaster object when user hit the URI.<br><br>
     * Response 200 for OK with employeeMaster if successfully.<br>
     * Response 204 for No_Content if no content in employeeMaster object.<br>
     * Response 400 for BAD_REQUEST when employeeNo not found in parameter.<br><br>
     * param employeeNo<br>
     * return
     */
    @RequestMapping(method = RequestMethod.GET, value = "employee-no/{employeeNo}", produces = "application/json" )
    public ResponseEntity getByEmployeeNo(@PathVariable("employeeNo") String employeeNo){
        String methodName = "getByEmployeeNo():";
        ResponseEntity<?> response = null;
        EmployeeMasterInterface employeeMaster = null;
        logger.info(methodName+"Got Request to find Employee"+employeeNo);
        if(employeeNo != null){
            employeeMaster = employeeMasterService.getByEmployeeNo(employeeNo);
            if(employeeMaster != null){
                logger.info(methodName+"Successfully fetched record ");
                response = new ResponseEntity<>(employeeMaster, HttpStatus.OK);
            }else {
                logger.error(methodName+"record not found");
                response = new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
        }else {
            logger.error(methodName+"got request employee no Null"+employeeNo);
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return  response;
    }

    /**
     * URI - /employee/employee-no/{employeeNo}/status/{status}<br><br>
     * This method takes employeeNo and status and fetch employeeMaster object against it when user hit the URI.<br><br>
     * Response 200 for OK with employeeMaster if successfully.<br>
     * Response 204 for No_Content if no content in employeeMaster object.<br>
     * Response 400 for BAD_REQUEST when employeeNo not found in parameter.<br><br>
     * param employeeNo<br>
     * param status<br>
     * return
     */
    @RequestMapping(method = RequestMethod.GET, value = "employee-no/{employeeNo}/status/{status}",produces = "application/json")
    public ResponseEntity getByEmployeeNoAndStatus(@PathVariable("employeeNo")String employeeNo, @PathVariable("status") String status){
        String methodName = "getByEmployeeNoAndStatus() :";
        ResponseEntity<?> response = null;
        EmployeeMasterInterface employeeMaster = null;
        logger.info(methodName+"got request to find Employee by employee no"+employeeNo+ "and status"+status);
        if(employeeNo != null && status != null){
            employeeMaster = employeeMasterService.getByEmployeeNoAndStatus(employeeNo,status);
            if(employeeMaster != null){
                logger.info(methodName+"Successfully fetched record");
                response = new ResponseEntity<>(employeeMaster, HttpStatus.OK);
            }else {
                logger.error(methodName+"record not found");
                response = new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
        }else {
            logger.error(methodName+"got request employee no OR status as Null");
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return response;
    }

    /**
     * URI - /employee/employee-no/{employeeNo}/type/{type}/status/{status}<br><br>
     * This method takes employeeNo ans status and fetch a employeeMaster object against it when user hit the URI.<br><br>
     * Response 200 for OK with employeeMaster if successfully.<br>
     * Response 204 for No_Content if no content in employeeMaster object.<br>
     * Response 400 for BAD_REQUEST when employeeNo not found in parameter.<br><br>
     * param employeeNo<br>
     * param type<br>
     * param status<br>
     * return response
     */
    @RequestMapping(method = RequestMethod.GET, value = "employee-no/{employeeNo}/type/{type}/status/{status}", produces = "application/json")
    public ResponseEntity getByEmployeeNoAndTypeAndStatus(@PathVariable("employeeNo") String employeeNo , @PathVariable("type") String type, @PathVariable("status") String status){
        String methodName = "getByEmployeeNoAndTypeAndStatus():";
        ResponseEntity<?> response = null;
        EmployeeMasterInterface employeeMaster = null;
        logger.info(methodName+"got request to find Employee by employee no :"+employeeNo+"type :"+type+"status :"+status);
        if(employeeNo != null && type != null &&status != null) {
            employeeMaster = employeeMasterService.getByEmployeeNoAndTypeAndStatus(employeeNo,type,status);
            if(employeeMaster != null ){
                logger.info(methodName+"Successfully fetched record");
                response = new ResponseEntity<>(employeeMaster,HttpStatus.OK);
            }else {
                logger.error(methodName+" record not found");
                response = new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
        }else {
            logger.error(methodName+"got request NULL");
        }

        return  response;
    }
}
