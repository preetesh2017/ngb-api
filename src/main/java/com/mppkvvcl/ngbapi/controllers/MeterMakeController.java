package com.mppkvvcl.ngbapi.controllers;

import com.mppkvvcl.ngbapi.services.MeterMakeService;
import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.MeterMakeInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by SHIVANSHU on 18-07-2017.
 * EDITED By NITISH on 25072017
 * changed root path to '/meter/make' earlier was '/meter-make' which is not correct
 */
@RestController
@RequestMapping(value = "/meter/make")
public class MeterMakeController {

    /**
     * Getting Logger object for logging in current class.
     */
    private Logger logger = GlobalResources.getLogger(MeterMakeController.class);

    /**
     * Asking spring to get Singleton object of MeterMakeService.
     */
    @Autowired
    private MeterMakeService meterMakeService;

    /**
     * URI : /meter-make<br><br>
     * This method is used for getting all MeterMake object with URI : /meter-make<br><br>
     * Response : 200(Ok) for successfully found the list of MeterMake<br>
     * Response : 417(EXPECTATION_FAILED) for facing problem to get MeterMake list<br><br>
     * return response
     */
    @RequestMapping(method = RequestMethod.GET , produces = "application/json")
    public ResponseEntity<?> getAll(){
        String methodName = "getAll() :";
        logger.info(methodName+"Get request to find all objects of MeterMake");
        ResponseEntity<?> response = null;
        List<? extends MeterMakeInterface> meterMakes = meterMakeService.getAll();
        if (meterMakes != null){
            if (meterMakes.size() > 0 ){
                logger.info(methodName+"MeterMake list found successfully");
                response = new ResponseEntity<Object>(meterMakes , HttpStatus.OK);
            }else{
                logger.error(methodName+"MetreMake list found with size"+meterMakes.size());
                response  = new ResponseEntity<Object>(HttpStatus.EXPECTATION_FAILED);
            }
        }else{
            logger.error(methodName+"MeterMake list return null");
            response = new ResponseEntity<Object>(HttpStatus.EXPECTATION_FAILED);
        }
        return response;
    }
}
