package com.mppkvvcl.ngbapi.controllers;

import com.mppkvvcl.ngbapi.services.PurposeOfInstallationService;
import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.PurposeOfInstallationInterface;
import com.mppkvvcl.ngbentity.beans.ErrorMessage;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by NITISH on 25-05-2017.
 * Controller class for various rest api provided for
 * Purpose Of Installation Table data at the backend api.
 *
 *
 * EDITED on 26-05-2017
 *
 * changed method from tariffCategory to tariffCode
 */
@RestController
@RequestMapping(value = "/purpose-of-installation")
public class PurposeOfInstallationController {

    /**
     * getting whole application logger object for performing logs. This logger will only be
     * used in this class methods only.
     */
    private static final Logger logger = GlobalResources.getLogger(PurposeOfInstallationController.class);

    /**
     * Asking spring to inject PurposeOfInstallationService in the below variable
     * so that various methods of it can be used to interact and perform business
     * logics on PurposeOfInstallation table data at the backend database.
     */
    @Autowired
    private PurposeOfInstallationService purposeOfInstallationService;

    /**
     * URI - /purpose-of-installation/tariff-code/{tariffCodePrefix}.{tariffCodeSuffix}<br><br>
     * method which will get executed when user will hit /purpose-of-installation/tariff-code/{tariffCodePrefix}.{tariffCodeSuffix}
     * and will return various purpose of installations under passed tariff code in json format with 200 Status code<br><br>
     * param tariffCodePrefix<br>
     * param tariffCodeSuffix<br>
     * return ResponseEntity<br><br>
     *
     *
     * Edited by SUMIT on 26-05-2017 11:15 AM<br>
     * changed getByTariffCategory to getByTariffCode and input parameter tariffCategory to tariffCode<br><br>
     *
     * Edited by SUMIT on 26-05-2017 13:58 PM<br><br>
     *
     * PROBLEM FACED : Spring truncates anything in URL after last DOT (.).
     * As our use case included tariffCode as DOT(.) for example LV1.2 so spring was truncating and
     * providing only LV1 due to this nature of truncating.<br><br>
     *
     * SOLUTION : For this the tariffCode is now received as tariffCodePrefix and tariffCodeSuffix and thereon the
     * final tariffCode is create by concatenating tariffCodePrefix and "." and tariffCodeSuffix<br><br>
     *
     * SUGGESTION :Kindly use this solution wherever required across APP.<br>
     */
    @RequestMapping(method = RequestMethod.GET,value = "/tariff-code/{tariffCodePrefix}.{tariffCodeSuffix}",produces = "application/json")
    public ResponseEntity getByTariffCode(@PathVariable("tariffCodePrefix")String tariffCodePrefix,@PathVariable("tariffCodeSuffix")String tariffCodeSuffix) {
        String tariffCode=tariffCodePrefix+"."+tariffCodeSuffix;
        logger.info("getByTariffCode : Request recieved for fetch purpose of installation by tariff code: "+tariffCode);
        ResponseEntity<?> response = null;
        if(tariffCode != null){
            logger.info("getByTariffCode : Fetching purpose of installations by tariff code");
            List<PurposeOfInstallationInterface> purposeOfInstallations = purposeOfInstallationService.getPurposeOfInstallationsByTariffCode(tariffCode);
            if(purposeOfInstallations != null){
                logger.info("getByTariffCode : Fetched "+purposeOfInstallations.size()+" purpose of installations successfully");
                if(purposeOfInstallations.size() > 0){
                    response = new ResponseEntity<>(purposeOfInstallations, HttpStatus.OK);
                }else{
                    response = new ResponseEntity<>(purposeOfInstallations, HttpStatus.NO_CONTENT);
                }
            }else{
                logger.error("getByTariffCode : Fetched purpose of installations is null. Returning error response");
                response = new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
            }
        }else{
            logger.error("getByTariffCategory : Tariff Category is NULL in getByTariffCategory");
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return response;
    }

    /**
     * URI - /purpose-of-installation/tariff/tariff-category/{tariff-category}<br><br>
     * param tariffCategory<br>
     * param connectionType<br>
     * param meteringStatus<br>
     * return response<br><br>
     *
     * CREATED by SUMIT VERMA on 26-05-2017
     *
     * method which will get executed when user will request for  /purpose-of-installation/tariff/tariff-category/{tariff-category}
     * and will return various purpose of installations under passed parameter value tariffCategory and query param connectionType and meteringStatus
     * in json format with  OK 200 Status code.<br>
     * NO CONTENT status 204 is sent when the input parameters are valid but for the input parameter combination there existed 0 records for purpose of installation.     * in json format with  OK 200 Status code.<br>

     *
     * BAD REQUEST status 400 is sent when input parameters are not as expected.     * in json format with  OK 200 Status code.<br>

     *
     * EXPECTATION FAILED status 417 is sent when there exists no list of valid purpose of installation for the passed input parameter values
     * that is purpose of installation is NULL.     * in json format with  OK 200 Status code.<br>

     */
    @RequestMapping(method = RequestMethod.GET,value="/tariff/tariff-category/{tariffCategory}",produces = "application/json")
    public ResponseEntity getPurposeOfInstallationByTariffCategoryAndMeteringStatusAndConnectionTypeAndTariffType(
            @PathVariable("tariffCategory")String tariffCategory,
            @RequestParam("connectionType")String connectionType,
            @RequestParam("meteringStatus") String meteringStatus,
            @RequestParam("tariffType") String tariffType) {
        String methodName = "getPurposeOfInstallationByTariffCategoryAndMeteringStatusAndConnectionTypeAndTariffType : ";
        logger.info(methodName + " Got requests to fetch purpose-of-installations by tariffCategories , " +
                "connection type and metering status with parameters as: "+tariffCategory+"  "+connectionType+" "+meteringStatus + " " + tariffType);
        ResponseEntity<?> response = null;
        if(tariffCategory != null && connectionType != null && meteringStatus!= null && tariffType != null){
            List<PurposeOfInstallationInterface> purposeOfInstallations = purposeOfInstallationService.getPurposeOfInstallationByTariffCategoryAndMeteringStatusAndConnectionTypeAndTariffType(tariffCategory,meteringStatus,connectionType,tariffType);
            if (purposeOfInstallations != null){
                if (purposeOfInstallations.size() > 0) {
                    logger.info(methodName + " Got " + purposeOfInstallations.size() + " purposeofinstallations in controller");
                    response = new ResponseEntity<>(purposeOfInstallations, HttpStatus.OK);
                }else{
                    logger.info(methodName + "Got " + purposeOfInstallations.size() + " purposeofinstallations in controller, hence returning no content");
                    response = new ResponseEntity<>(purposeOfInstallations, HttpStatus.NO_CONTENT);
                }
            }else{
                logger.error(methodName + " Fetched purpose of installation is null.Returning error response");
                response = new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
            }
        }else{
            logger.error(methodName + " inputs parameters is null.Returning bad request");
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        logger.info(methodName + " Returning from fetch purpose of installlations");
        return response;
    }

    @RequestMapping(method = RequestMethod.GET,value = "/id/{id}",produces = "application/json")
    public ResponseEntity getPurposeById(@PathVariable("id") long id){
        String methodName = "getPurposeById() : ";
        logger.info(methodName +"called with id : " + id);
        ResponseEntity<?> response = null;
        PurposeOfInstallationInterface purposeOfInstallation = null;
        ErrorMessage errorMessage = null;
        logger.info(methodName+": Saving submitted data for load detail");
        try {
            purposeOfInstallation = purposeOfInstallationService.getPurposeOfInstallationById(id);
        }catch (RuntimeException re){
            errorMessage = new ErrorMessage("Some internal error. Try after some time ");
            response = new ResponseEntity<Object>(errorMessage, HttpStatus.EXPECTATION_FAILED);
        }catch (Exception ee){
            errorMessage = new ErrorMessage(ee.getMessage());
            response = new ResponseEntity<>(errorMessage,HttpStatus.EXPECTATION_FAILED);
        }
        if(purposeOfInstallation != null){
            logger.info(methodName+": Purpose of installation fetched successfully : "+purposeOfInstallation.getId());
            response = new ResponseEntity<>(purposeOfInstallation,HttpStatus.OK);
        }else{
            errorMessage = new ErrorMessage("Some internal server Error .");
            logger.error(methodName+": Couldnt fetch purpose with purpose of installation id as : " + id);
            response = new ResponseEntity<>(errorMessage,HttpStatus.EXPECTATION_FAILED);
        }

        return response;
    }

}
