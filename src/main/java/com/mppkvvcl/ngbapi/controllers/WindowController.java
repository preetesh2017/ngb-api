package com.mppkvvcl.ngbapi.controllers;

import com.mppkvvcl.ngbapi.custombeans.CustomWindowDetail;
import com.mppkvvcl.ngbapi.security.services.UserDetailService;
import com.mppkvvcl.ngbapi.services.CashWindowStatusService;
import com.mppkvvcl.ngbapi.services.WindowDetailService;
import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbentity.beans.ErrorMessage;
import com.mppkvvcl.ngbentity.beans.UserDetail;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping(value = "/window")
@RestController
public class WindowController {

    /**
     * getting whole application logger object for performing logs. This logger will only be
     * used in this class methods only.
     */
    private static final Logger logger = GlobalResources.getLogger(WindowController.class);

    /**
     * Asking spring to inject WindowDetailService in the below variable
     * so that various methods of it can be used to interact and perform business
     * logics on PurposeOfInstallation table data at the backend database.
     */

    @Autowired
    WindowDetailService windowDetailService;

    @Autowired
    CashWindowStatusService cashWindowStatusService;

    @Autowired
    UserDetailService userDetailService;


    @RequestMapping(method = RequestMethod.GET, value = "/available", produces = "application/json")
    public ResponseEntity<?> getUnAssignedWindowsListOfLocation() {

        String methodName = "getUnAssignedWindowsListOfLocation() :";

        ErrorMessage errorMessage =null;

        ResponseEntity<?> response = null;

        logger.info(methodName + "Called");
        CustomWindowDetail windowDetails = windowDetailService.getUnAssignedWindowsListOfLocation();
        if (windowDetails != null) {
            logger.info(methodName + "Called" + windowDetails);
            response = new ResponseEntity<>(windowDetails, HttpStatus.OK);
        } else {
            logger.info(methodName + "Window Not Found For Location");
            errorMessage = new ErrorMessage("Window Not Found For Location");
            response = new ResponseEntity<>( errorMessage ,HttpStatus.EXPECTATION_FAILED);
        }
        return response;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/unAssignedUser", produces = "application/json")
    public ResponseEntity<?> getUnassignedUserOfLocation() {

        ErrorMessage errorMessage =null;
        String methodName = "getUnassignedUserOfLocation() : ";
        ResponseEntity<?> response = null;
        logger.info(methodName + "Got request fetch Available User");
        List<UserDetail> userDetails = cashWindowStatusService.getUnAssignedUserList();
        if (userDetails != null) {
            logger.info(methodName + "User Found Successfully : " + userDetails);
            response = new ResponseEntity<>(userDetails, HttpStatus.OK);
        } else {

            logger.error(methodName + "Window not found for Location");
            errorMessage = new ErrorMessage("User Not Found For Location");
            response = new ResponseEntity<>(errorMessage,HttpStatus.EXPECTATION_FAILED);
        }
        return response;
    }



}