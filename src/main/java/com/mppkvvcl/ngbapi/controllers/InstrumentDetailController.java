package com.mppkvvcl.ngbapi.controllers;

import com.mppkvvcl.ngbapi.custombeans.CustomInstrumentDetail;
import com.mppkvvcl.ngbapi.exceptionhandlers.RuntimeExceptionHandler;
import com.mppkvvcl.ngbapi.services.InstrumentDetailService;
import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.InstrumentDishonourInterface;
import com.mppkvvcl.ngbentity.beans.ErrorMessage;
import com.mppkvvcl.ngbinterface.interfaces.InstrumentPaymentMappingInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Vikas on 8/9/2017.
 */

@RestController
@RequestMapping(value = "instrument-detail")
public class InstrumentDetailController {

    private static final Logger logger = GlobalResources.getLogger(InstrumentDetailController.class);

    @Autowired
    private InstrumentDetailService instrumentDetailService;

    @RequestMapping(method = RequestMethod.GET,value ="pay-mode/{payMode}/bank-name/{bankName}/instrument-no/{instrumentNo}",produces ="application/json")
    public ResponseEntity getByPayModeAndBankNameAndInstrumentNo(@PathVariable("payMode")String payMode, @PathVariable ("bankName") String bankName, @PathVariable ("instrumentNo") String instrumentNo){
        String methodName = "getByPayModeAndBankNameAndInstrumentNo() :";
        CustomInstrumentDetail customInstrumentDetail = null;
        ResponseEntity<?> response = null;
        ErrorMessage errorMessage = null;
        logger.info( methodName + " Got request to get Payment details by Given Pay Mode :"+payMode+" Bank Name :"+bankName+" And Instrument No : "+instrumentNo);
        if(payMode != null && bankName != null && instrumentNo != null) {
            try{
                customInstrumentDetail = instrumentDetailService.getByPayModeAndBankNameAndInstrumentNo(payMode,bankName,instrumentNo);
            }catch(RuntimeException e){
                errorMessage = new ErrorMessage(RuntimeExceptionHandler.handleException());
                logger.error(methodName + "Received Runtime Exception in controller with error message as :" + e.getMessage());
                customInstrumentDetail = null;
            }catch (Exception ee){
                errorMessage = new ErrorMessage(ee.getMessage());
                logger.error(methodName + "Received Exception in controller with error message as :" + ee.getMessage());
                customInstrumentDetail = null;
            }
            if (customInstrumentDetail != null) {
                logger.info(methodName + " Fetched Instrument Details and Payment details");
                response = new ResponseEntity<>(customInstrumentDetail, HttpStatus.OK);
            } else {
                logger.error(methodName + "No details found for Given Pay Mode : "+payMode+" Bank Name :" + bankName + " And Instrument Details :" + instrumentNo);
                response = new ResponseEntity<>(errorMessage,HttpStatus.EXPECTATION_FAILED);
            }
        }else {
            logger.info( methodName + "Given input details is Null");
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        logger.info(methodName + "Returning from Fetch Instrument Details");
        return  response;
    }

    @RequestMapping(method = RequestMethod.PUT, value = "id/{id}")
    public ResponseEntity updateById(@PathVariable("id") long id, @RequestParam("remark") String remark){
        final String methodName = "updateById() : ";
        logger.info(methodName + "called");
        List<InstrumentDishonourInterface> dishonouredInstruments = null;
        ResponseEntity<?> response = null;
        ErrorMessage errorMessage = null;
        if (id > 0l) {
            try {
                dishonouredInstruments = instrumentDetailService.updateById(id, remark);
            } catch (Exception ee) {
                errorMessage = new ErrorMessage(ee.getMessage());
                logger.error(methodName + "Received Exception in controller with error message as: " + ee.getMessage());
            }
            if (dishonouredInstruments != null) {
                logger.info(methodName + " Updated Instrument Details");
                response = new ResponseEntity<>(dishonouredInstruments, HttpStatus.OK);
            } else {
                logger.error(methodName + "Some error in updating Instrument Details for Given Instrument id: " + id);
                response = new ResponseEntity<>(errorMessage, HttpStatus.EXPECTATION_FAILED);
            }
        } else {
            logger.info(methodName + "Invalid input detail");
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return response;
    }

    /**
     * Returns the Consumer Payments and Instrument details as per the Consumer No and Payment Modes
     *
     * @param consumerNo
     * @param payModes
     * @return
     */
    @RequestMapping(method = RequestMethod.GET, value = "consumer-no/{consumerNo}/pay-modes/{payModes}", produces = "application/json")
    public ResponseEntity getByConsumerNoAndPayModes(@PathVariable("consumerNo") String consumerNo, @PathVariable("payModes") String[] payModes) {
        final String methodName = "getByConsumerNoAndPayModes() : ";
        CustomInstrumentDetail customInstrumentDetail = null;
        ResponseEntity<?> response = null;
        ErrorMessage errorMessage = null;
        logger.info(methodName + "called with Consumer No: " + consumerNo + " and Pay Mode: " + Arrays.toString(payModes));
        if (consumerNo == null || consumerNo.isEmpty() || payModes == null || payModes.length == 0) {
            logger.info(methodName + "Given input details is Null");
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            return response;
        }
        try {
            customInstrumentDetail = instrumentDetailService.getByConsumerNoAndPayModes(consumerNo, payModes);
        } catch (RuntimeException re) {
            errorMessage = new ErrorMessage(RuntimeExceptionHandler.handleException());
            logger.error(methodName + "Runtime Exception occurred: " + re.getMessage());
            customInstrumentDetail = null;
        } catch (Exception e) {
            errorMessage = new ErrorMessage(e.getMessage());
            logger.error(methodName + "Exception occurred: " + e.getMessage());
            customInstrumentDetail = null;
        }
        if (customInstrumentDetail != null) {
            logger.info(methodName + "Fetched Instrument Details and Payment details");
            response = new ResponseEntity<>(customInstrumentDetail, HttpStatus.OK);
        } else {
            logger.error(methodName + "No details found for Given Consumer No: " + consumerNo + " and Pay Modes: " + Arrays.toString(payModes));
            response = new ResponseEntity<>(errorMessage, HttpStatus.EXPECTATION_FAILED);
        }
        return response;
    }

    /**
     * Returns the Consumer Payments and Instrument details as per the Instrument No
     *
     * @param instrumentNo
     * @return
     */
    @RequestMapping(method = RequestMethod.GET, value = "instrument-no/{instrumentNo}", produces = "application/json")
    public ResponseEntity getByInstrumentNo(@PathVariable("instrumentNo") String instrumentNo) {
        final String methodName = "getByInstrumentNo() : ";
        CustomInstrumentDetail customInstrumentDetail = null;
        ResponseEntity<?> response = null;
        ErrorMessage errorMessage = null;
        logger.info(methodName + "called with Instrument No: " + instrumentNo);
        if (instrumentNo == null || instrumentNo.isEmpty()) {
            logger.info(methodName + "Given input details is Null");
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            return response;
        }
        try {
            customInstrumentDetail = instrumentDetailService.getByInstrumentNo(instrumentNo);
        } catch (RuntimeException re) {
            errorMessage = new ErrorMessage(RuntimeExceptionHandler.handleException());
            logger.error(methodName + "Runtime Exception occurred: " + re.getMessage());
            customInstrumentDetail = null;
        } catch (Exception e) {
            errorMessage = new ErrorMessage(e.getMessage());
            logger.error(methodName + "Exception occurred: " + e.getMessage());
            customInstrumentDetail = null;
        }
        if (customInstrumentDetail != null) {
            logger.info(methodName + "Fetched Instrument Details and Payment details");
            response = new ResponseEntity<>(customInstrumentDetail, HttpStatus.OK);
        } else {
            logger.info(methodName + "No details found for Given Instrument No: " + instrumentNo);
            response = new ResponseEntity<>(errorMessage, HttpStatus.EXPECTATION_FAILED);
        }
        return response;
    }


    /**
     * Returns Instrument details and its associated payments
     *
     * @param paymentId
     * @return
     */
    @RequestMapping(method = RequestMethod.GET, value = "/payment-id/{paymentId}", produces = "application/json")
    public ResponseEntity getByPaymentId(@PathVariable("paymentId") long paymentId) {
        final String methodName = "getByPaymentId() : ";
        logger.info(methodName + "called");
        List<InstrumentPaymentMappingInterface> instrumentPaymentMappingInterfaces = null;
        ResponseEntity<?> response = null;
        ErrorMessage errorMessage = new ErrorMessage();
        instrumentPaymentMappingInterfaces = instrumentDetailService.getByPaymentId(paymentId,errorMessage);

        if (instrumentPaymentMappingInterfaces != null) {
            logger.info(methodName + "Fetched Instrument Details and Payment details");
            response = new ResponseEntity<>(instrumentPaymentMappingInterfaces, HttpStatus.OK);
        } else {
            logger.error(methodName + "No details found for Given Payment Id: " + paymentId );
            response = new ResponseEntity<>(errorMessage, HttpStatus.EXPECTATION_FAILED);
        }
        return response;
    }

    /**
     * Added By: Preetesh Date: 28 Dec 2017
     * URI for deleting instrument
     * @return
     */
    @RequestMapping(method = RequestMethod.DELETE , value ="id/{instrumentId}",produces ="application/json")
    public ResponseEntity deleteByInstrumentId(@PathVariable("instrumentId")long instrumentId){
        String methodName = "deleteByInstrumentId() :";
        ResponseEntity<?> response = null;
        ErrorMessage errorMessage = null;
        CustomInstrumentDetail customInstrumentDetail = null;
        if(instrumentId > 0) {
            try{
                customInstrumentDetail = instrumentDetailService.deleteInstrumentDetailById(instrumentId);
            }catch(RuntimeException e){
                errorMessage = new ErrorMessage(RuntimeExceptionHandler.handleException());
                logger.error(methodName + "Received Runtime Exception in controller with error message as :" + e.getMessage());
                customInstrumentDetail = null;
            }catch (Exception ee){
                errorMessage = new ErrorMessage(ee.getMessage());
                logger.error(methodName + "Received Exception in controller with error message as :" + ee.getMessage());
                customInstrumentDetail = null;
            }

            if (customInstrumentDetail != null) {
                logger.info(methodName + "instrument and associated payments deleted successfully");
                response = new ResponseEntity<>(customInstrumentDetail, HttpStatus.OK);
            } else {
                logger.error(methodName + "some error in saving payment");
                response = new ResponseEntity<>(errorMessage, HttpStatus.EXPECTATION_FAILED);
            }
        }else{
            logger.error(methodName + "some input is null");
            errorMessage = new ErrorMessage("some input is null");
            response = new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
        }
        return response;
    }
}
