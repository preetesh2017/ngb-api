package com.mppkvvcl.ngbapi.controllers;

import com.mppkvvcl.ngbapi.services.EEDivisionMappingService;
import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.EEDivisionMappingInterface;
import com.mppkvvcl.ngbentity.beans.ErrorMessage;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * code by nitish on 23-09-2017
 */
@RestController
@RequestMapping("/ee/division/mapping")
public class EEDivisionMappingController {

    /**
     * Getting whole logger object from global resources for current class.
     */
    private static final Logger logger = GlobalResources.getLogger(EEDivisionMappingController.class);

    @Autowired
    private EEDivisionMappingService eeDivisionMappingService;

    /**
     * code by nitish
     * @param username
     * @return
     */
    @RequestMapping(method = RequestMethod.GET,value = "/username/{username}",produces = "application/json")
    public ResponseEntity<?> getEEDivisionMappingByUsername(@PathVariable("username")String username){
        final String methodName = "getEEDivisionMappingByUsername() : ";
        logger.info(methodName + "called for username " + username);
        ResponseEntity<?> response = null;
        EEDivisionMappingInterface eeDivisionMapping = null;
        if(username != null){
            eeDivisionMapping = eeDivisionMappingService.getByUsername(username);
        }
        if(eeDivisionMapping != null){
            response = new ResponseEntity<>(eeDivisionMapping, HttpStatus.OK);
        }else{
            ErrorMessage errorMessage = new ErrorMessage("No EEDivisionMapping found for username " + username);
            response = new ResponseEntity<>(errorMessage,HttpStatus.EXPECTATION_FAILED);
        }
        return response;
    }

    /**
     * code by nitish
     * @param divisionId
     * @return
     */
    @RequestMapping(method = RequestMethod.GET,value = "/division/id/{divisionId}",produces = "application/json")
    public ResponseEntity<?> getEEDivisionMappingByDivisionId(@PathVariable("divisionId")long divisionId){
        final String methodName = "getEEDivisionMappingByDivisionId() : ";
        logger.info(methodName + "called for divisionId " + divisionId);
        ResponseEntity<?> response = null;
        EEDivisionMappingInterface eeDivisionMapping = null;
        eeDivisionMapping = eeDivisionMappingService.getByDivisionId(divisionId);
        if(eeDivisionMapping != null){
            response = new ResponseEntity<>(eeDivisionMapping, HttpStatus.OK);
        }else{
            ErrorMessage errorMessage = new ErrorMessage("No EEDivisionMapping found for divisionId " + divisionId);
            response = new ResponseEntity<>(errorMessage,HttpStatus.EXPECTATION_FAILED);
        }
        return response;
    }
}
