package com.mppkvvcl.ngbapi.controllers;

import com.mppkvvcl.ngbapi.services.PartPaymentService;
import com.mppkvvcl.ngbapi.services.PaymentService;
import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbentity.beans.ErrorMessage;
import com.mppkvvcl.ngbentity.beans.PartPayment;
import com.mppkvvcl.ngbinterface.interfaces.PartPaymentInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by PREETESH on 11/29/2017.
 */
@RestController
@RequestMapping(value = "/part-payment")
public class PartPaymentController {

    /**
     * Getting logger object from global resources for current class.
     */
    private static final Logger logger = GlobalResources.getLogger(PartPaymentController.class);

    /**
     * Requesting spring to get singleton PaymentController object
     */
    @Autowired
    PaymentService paymentService;

    @Autowired
    PartPaymentService partPaymentService;

    /**
     * URI - /part-payment<br>
     * @param partPayment
     * @return
     */
    @RequestMapping(method = RequestMethod.POST , produces = "application/json", consumes = "application/json")
    public ResponseEntity<?> insertPartPayment(@RequestBody PartPayment partPayment) {
        String methodName = "insertPartPayment() : ";
        logger.info(methodName + "called "+partPayment);
        ResponseEntity<?> response = null;
        ErrorMessage errorMessage = new ErrorMessage();
        PartPaymentInterface insertedPartPayment = null;
        if(partPayment != null){
            insertedPartPayment = partPaymentService.insertPartPayment(partPayment,errorMessage);
        }else{
           errorMessage.setErrorMessage("part payment to insert is null");
        }

        if (insertedPartPayment != null) {
            logger.info(methodName + "part payment saved successfully");
            response = new ResponseEntity<>(insertedPartPayment, HttpStatus.CREATED);
        } else {
            logger.error(methodName + "some error in saving part payment");
            response = new ResponseEntity<>(errorMessage, HttpStatus.EXPECTATION_FAILED);
        }
        return response;
    }


    /**
     * URI - /part-payment/consumer-no/{consumerNo}<br>
     * This method takes consumerNo and fetch a list of part-payments.<br>
     * Response 200 for OK status and list of payments is sent if successful.<br>
     * Response 204 NO_CONTENT status is sent if no content in list.<br>
     * Response 404 for NOT_FOUND status is sent when list is null.<br>
     * Response 400 for BAD_REQUEST status is sent if input param are null<br>
     * param consumerNo<br>
     * return
     */
    @RequestMapping(method = RequestMethod.GET, value = "/consumer-no/{consumerNo}",  produces = "application/json" )
    public ResponseEntity<?> getByConsumerNo(@PathVariable("consumerNo") String consumerNo){
        String methodName = "getByConsumerNo() : ";
        logger.info(methodName + "called for consumerNo : " + consumerNo);
        ResponseEntity<?> response = null;
        ErrorMessage errorMessage = new ErrorMessage();
        List<PartPaymentInterface> partPaymentInterfaces = null;
        if(consumerNo != null) {
            partPaymentInterfaces = partPaymentService.getByConsumerNo(consumerNo,errorMessage);
        }else{
            errorMessage.setErrorMessage("passed parameter consumerNo is null");
        }
        if(partPaymentInterfaces != null && partPaymentInterfaces.size() >0){
            logger.info(methodName + "Got part payments against consumerNo : " + consumerNo );
            response = new ResponseEntity<>(partPaymentInterfaces, HttpStatus.OK);
        }else{
            logger.error(methodName + "part Payment list not found against consumerNo : " + consumerNo);
            response = new ResponseEntity<>(errorMessage,HttpStatus.NO_CONTENT);
        }
        return response;
    }


    /**
     *
     * @param partPayment
     * URI - /part-payment<br>
     * This method takes payment and update the existing one.<br>
     * Response 200 for OK status if payments updated successful.<br>
     * Response 417 for EXPECTATION_FAILED status is sent when unable to update payment.<br>
     * Response 400 for BAD_REQUEST status is sent if input param are null<br>
     * param consumerNo<br>
     * @return
     */
    @RequestMapping(method = RequestMethod.PUT, produces = "application/json" )
    public ResponseEntity<?> update(@RequestBody PartPayment partPayment){
        String methodName = "update() : ";
        logger.info(methodName + "called");
        ResponseEntity<?> response = null;
        ErrorMessage errorMessage = new ErrorMessage();
        PartPaymentInterface updatedPartPayment = null;
        if(partPayment != null){
            updatedPartPayment = partPaymentService.updateWithCheck(partPayment,errorMessage);
        }

        if (updatedPartPayment != null) {
            logger.info(methodName + "part payment updated successfully");
            response = new ResponseEntity<>(updatedPartPayment, HttpStatus.OK);
        } else {
            logger.error(methodName + "some error in updating part payment");
            response = new ResponseEntity<>(errorMessage, HttpStatus.EXPECTATION_FAILED);
        }
        return response;
    }


    @RequestMapping( method = RequestMethod.DELETE, value = "/id/{id}", produces = "application/json" )
    public ResponseEntity<?> deletePartPaymentById(@PathVariable("id") long id){
        String methodName = "deletePaymentById() : ";
        logger.info(methodName + "called for id : " + id);
        ResponseEntity<?> response = null;
        boolean partPaymentDeleted = false;
        ErrorMessage errorMessage = null;
        if(id > 0){
            partPaymentDeleted = partPaymentService.delete(id,errorMessage);
        }

        if (partPaymentDeleted) {
            logger.info(methodName + "part payment deleted successfully");
            response = new ResponseEntity<>(partPaymentDeleted, HttpStatus.OK);
        } else {
            logger.error(methodName + "some error in deleting part payment");
            response = new ResponseEntity<>(errorMessage, HttpStatus.EXPECTATION_FAILED);
        }
        return response;
    }
}
